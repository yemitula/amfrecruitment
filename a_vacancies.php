<?php require_once("_inc_checkSession.php"); ?>
<?php require_once("_inc_applicantsOnly.php"); ?>
<?php $thisPage = basename( $_SERVER['PHP_SELF'] ); ?>
<?php require_once('_inc_config.php'); ?>
<?php require_once('Connections/fer.php'); ?>
<?php include('_inc_Functions.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$maxRows_vacancies = 10;
$pageNum_vacancies = 0;
if (isset($_GET['pageNum_vacancies'])) {
  $pageNum_vacancies = $_GET['pageNum_vacancies'];
}
$startRow_vacancies = $pageNum_vacancies * $maxRows_vacancies;

mysql_select_db($database_fer, $fer);
$query_vacancies = "SELECT v.`id`, `title`, `employer_id`, `summary`, `longDescription`, `techReqs`, `eduQual`, `behavReqs`, `jobType`, `jobCategory_id`, `staffLevel`, `datePosted`, `dateOpening`, logo, 'longDescription', `dateClosing`, `status`, `alertStatus`, e.id AS eID, e.companyName  FROM vacancies v LEFT JOIN  employers e ON (v.employer_id=e.id) WHERE ( (v.dateOpening IS NULL AND v.dateClosing IS NULL) OR (v.dateClosing >= NOW() AND v.dateOpening IS NULL) OR (v.dateClosing >= NOW() AND v.dateOpening <= NOW()) ) AND v.published IS NOT NULL"; 
//keyword filter
if(isset($_GET['keyword']) && $_GET['keyword'] != '') {
	$keyword = mysql_real_escape_string($_GET['keyword']);
	$query_vacancies .= " AND (title LIKE '%$keyword%' 
	OR summary LIKE '%$keyword%' 
	OR longDescription LIKE '%$keyword%' 
	OR techReqs LIKE '%$keyword%' 
	OR eduQual LIKE '%$keyword%' 
	OR behavReqs LIKE '%$keyword%' 
	OR jobType LIKE '%$keyword%')";	
}
//staff level filter
if(isset($_GET['staffLevel']) && $_GET['staffLevel'] != '') {
	$query_vacancies .= sprintf(" AND staffLevel = %s", GetSQLValueString($_GET['staffLevel'],"text"));	
}

if(isset($_GET['category']) && $_GET['category'] != '') {
	$query_vacancies .= sprintf(" AND jobCategory_id = %s", GetSQLValueString($_GET['category'],"int"));	
}

$query_vacancies .= " ORDER BY v.sortOrder, v.title ASC";
//die($_GET['staffLevel']);
//die($query_vacancies);
$query_limit_vacancies = sprintf("%s LIMIT %d, %d", $query_vacancies, $startRow_vacancies, $maxRows_vacancies);
$vacancies = mysql_query($query_limit_vacancies, $fer) or die(mysql_error());
$row_vacancies = mysql_fetch_assoc($vacancies);

if (isset($_GET['totalRows_vacancies'])) {
  $totalRows_vacancies = $_GET['totalRows_vacancies'];
} else {
  $all_vacancies = mysql_query($query_vacancies);
  $totalRows_vacancies = mysql_num_rows($all_vacancies);
}
$totalPages_vacancies = ceil($totalRows_vacancies/$maxRows_vacancies)-1;

mysql_select_db($database_fer, $fer);
$employerID = $row_vacancy['employer_id'];
$query_employer = "SELECT * FROM employers WHERE id = '$employerID'";
$employer = mysql_query($query_employer, $fer) or die(mysql_error());
$row_employer = mysql_fetch_assoc($employer);
$totalRows_employer = mysql_num_rows($employer);
 ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<link rel="shortcut icon" href="favicon.png" />
    
	<title>Vacancies - <?php echo $FER_User['firstname'] ?> <?php echo $FER_User['surname'] ?> | <?php echo $config['shortname'] ?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">
    <link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
    <link href="../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">

    <!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include('-inc-header-top.php'); ?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include('-inc-header-nav.php'); ?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
		    <?php include('-inc-applicant-top.php'); ?>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
		    <div class="row">
		        <div class="col-sm-4 page-sidebar">
		            <?php include('-inc-applicant-side.php'); ?>
	            </div>
		        <!-- end .page-sidebar -->
	          <div class="col-sm-8 page-content">
		            <h3>
		                <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>
		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>
	                </div>-->
		                 Vacancies
                </h3>
                <form id="findAJobForm" name="findAJobForm" method="get" action="a_vacancies.php">
	                    <div class="white-container sign-up-form">
	                        <div>
	                            <h5>Vacancies</h5>
                              <section>
	                                <?php if (isset($_GET['error'])) { ?>
		                                <div class="alert alert-error">
		                                    <h6>Oops!</h6>
		                                    <p><?php echo $_GET['error'] ?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php } ?>
	                                <?php if (isset($_GET['msg'])) { ?>
		                                <div class="alert alert-success">
		                                    <h6>Wow!</h6>
		                                    <p><?php echo $_GET['msg'] ?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php } ?>
                              <div class="row">
							<div class="col-sm-6">
							  <input name="keyword" type="text" id="keyword" placeholder="Keyword, Job Title, Employer or description" style="font-size:12px;" value="<?php echo $_GET['keyword']; ?>" />
							</div>

							<div class="col-sm-4">
							  <select name="staffLevel" class="" style="font-size:12px;" id="staffLevel">
							    <option value="" selected="selected" <?php if (!(strcmp("", $_GET['staffLevel']))) {echo "selected=\"selected\"";} ?>>All Levels</option>
							    <option value="Managerial" <?php if (!(strcmp("Managerial", $_GET['staffLevel']))) {echo "selected=\"selected\"";} ?>>Managerial</option>
							    <option value="Non-Managerial" <?php if (!(strcmp("Non-Managerial", $_GET['staffLevel']))) {echo "selected=\"selected\"";} ?>>Non-Managerial</option>
						      </select>
							</div>

							<div class="col-sm-2">
								<input type="submit" class="btn btn-default btn-block" value="Search">
							</div>
						</div>
                              </section>
						</div>
	                        <div class="clearfix" id="captchaArea">
                            
                            </div>
	                    </div>
                </form>
              <?php if ($totalRows_vacancies > 0) { // Show if recordset not empty ?>
                  <div class="title-lines">
						<h3 class="mt0">Available Jobs(<?php echo $totalRows_vacancies ?>)</h3>
				</div>

					<div class="clearfix mb30">
					  <ul class="jobs-view-toggle pull-left">
					    <li></li>
					  </ul>
						<ul class="pagination pull-right">
						  <li><a href="#" class="fa fa-angle-left"></a></li>
							<li class="active"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#" class="fa fa-angle-right"></a></li>
					  </ul>
              <?php do { ?>
                  <div class="jobs-item with-thumb">
						<div class="thumb">
                        <?php if (isset($row_vacancies['logo'])) { ?>
        <img src="images/employerLogos/<?php echo $row_vacancies['logo']; ?>" alt="">
                                                <?php }else { ?>
        <img src="images/employerLogos/logo.png" style="max-height:150px" alt="" />
                                                <?php } ?>

                        </div>
						<div class="clearfix visible-xs"></div>
					<div class="date"><?php echo date("d",strtotime($row_vacancies['datePosted'])); ?> <span><?php echo date("m-y",strtotime($row_vacancies['datePosted'])); ?></span></div>
						<h6 class="title"><a href="a_vacancyDetails.php?id=<?php echo $row_vacancies['id']; ?>"><?php echo $row_vacancies['title']; ?></a> at <strong><?php echo $row_vacancies['companyName']; ?></strong></h6>
						<span class="meta"><?php echo $row_vacancies['jobType']; ?>, <?php echo $row_vacancies['status']; ?>,<?php echo "<br />"; ?></span>

						<ul class="top-btns">
							<li><a href="a_vacancyDetails.php?id=<?php echo $row_vacancies['id']; ?>" class="btn btn-gray fa fa-plus "></a></li>
					</ul>

						<p class="description"><?php echo ucfirst($row_vacancies['summary']); ?> <?php //<a href="#" class="read-more">Read More</a> ?><br ><hr />
                        <a href="a_vacancyDetails.php?id=<?php echo $row_vacancies['id']; ?>" class="btn btn-default">More Details</a> 
		  <?php if (CVComplete()) { ?>
		  <?php if (AlreadyApplied($row_vacancies['id'])) { ?><a href="a_applications.php" class="btn btn-red">Applied</a>
		  <?php } elseif (NotEligible($row_vacancies['id'], $row_vacancies['employer_id'])) { ?>
          <a href="#" class="btn btn-red" title="Not Eligible: You have already applied for a vacancy from this employer!">N/Eligible</a>
		  <?php } else {  ?>
          <a href="a_applyForVacancy.php?id=<?php echo $row_vacancies['id']; ?>" class="btn btn-gray">Apply</a>
          <?php } ?>
          		<?php } ?>
                        </p>

						<div class="content">
							<p><?php echo ucfirst($row_vacancy['longDescription']); ?></p>

							<h5><strong>Technical Requirements</strong></h5>

							<div class="progress-bar toggle" data-progress="60">
								<a href="#" class="progress-bar-toggle"></a>
								<h6 class="progress-bar-title">Web Design</h6>
								<div class="progress-bar-inner"><span></span></div>
								<div class="progress-bar-content">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, asperiores.
								</div>
							</div>

							<div class="progress-bar toggle" data-progress="60">
								<a href="#" class="progress-bar-toggle"></a>
								<h6 class="progress-bar-title">Development</h6>
								<div class="progress-bar-inner"><span></span></div>
								<div class="progress-bar-content">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, asperiores.
								</div>
							</div>

							<div class="progress-bar toggle" data-progress="60">
								<a href="#" class="progress-bar-toggle"></a>
								<h6 class="progress-bar-title">UI/UX</h6>
								<div class="progress-bar-inner"><span></span></div>
								<div class="progress-bar-content">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, asperiores.
								</div>
							</div>

							<h5>Additional Requirements</h5>

							<ul class="additional-requirements clearfix">
								<li>Work Permit</li>
								<li>5 Years Experience</li>
								<li>MBA</li>
								<li>Magento Certified</li>
								<li>Perfect Written &amp; Spoken English</li>
							</ul>

							<hr>

							<div class="clearfix">
								<a href="a_vacancyDetails.php?id=<?php echo $row_vacancies['id']; ?>" class="btn btn-default">More Details</a> 
		  <?php if (CVComplete()) { ?>
		  <?php if (AlreadyApplied($row_vacancies['id'])) { ?><a href="a_applications.php" class="btn btn-red">Applied</a>
		  <?php } elseif (NotEligible($row_vacancies['id'], $row_vacancies['employer_id'])) { ?>
          <a href="#" class="btn btn-red" title="Not Eligible: You have already applied for a vacancy from this employer!">N/Eligible</a>
		  <?php } else {  ?>
          <a href="a_applyForVacancy.php?id=<?php echo $row_vacancies['id']; ?>" class="btn btn-gray">Apply</a>
          <?php } ?>
          		<?php } ?>

								<ul class="social-icons pull-right">
									<li><span>Share</span></li>
									<li><a href="#" class="btn btn-gray fa fa-facebook"></a></li>
									<li><a href="#" class="btn btn-gray fa fa-twitter"></a></li>
									<li><a href="#" class="btn btn-gray fa fa-google-plus"></a></li>
								</ul>
			  </div>
						</div>
				</div>
                <?php } while ($row_vacancies = mysql_fetch_assoc($vacancies)); ?>
                <?php } // Show if recordset not empty ?>
                <div class="listNav">
                <?php if ($totalRows_vacancies == 0) { // Show if recordset empty ?>
     	                    <div class="white-container sign-up-form">
  <div class="" style="background:#F27657; padding:10px;">No vacancies</div>
  </div>
  <?php } // Show if recordset empty ?>
</div>
	          </div>
		        <!-- end .page-content -->
	        </div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include('-inc-footer-top.php'); ?>

		<div class="copyright">
			<?php include('-inc-footer-bottom.php'); ?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
<script src="js/ValidateDel.js"></script>
<script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="../js/mainNavbar.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#staffLevel").change(function(e) {
        $("#findAJobForm").submit();
    });
});
</script>
</body>
</html>