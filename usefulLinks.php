<?php require_once("_inc_checkSession.php"); ?>
<?php require_once("_inc_applicantsOnly.php"); ?>
<?php require_once("classes/class.upload.php"); ?>
<?php $thisPage = basename( $_SERVER['PHP_SELF'] ); ?>
<?php require_once('_inc_config.php'); ?>
<?php require_once('Connections/fer.php'); ?>
<?php include('_inc_Functions.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_applicant = "-1";
if (isset($_SESSION['FER_User'])) {
  $colname_applicant = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_applicant = sprintf("SELECT * FROM applicants WHERE id = %s", GetSQLValueString($colname_applicant, "int"));
$applicant = mysql_query($query_applicant, $fer) or die(mysql_error());
$row_applicant = mysql_fetch_assoc($applicant);
$totalRows_applicant = mysql_num_rows($applicant);

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "formPersonalDetails")) {

	//print_r($_FILES['cv']);die;
	//cv
	//upload the file
	$cv = new upload($_FILES['cv']);
	//file uploaded?	
	if($cv->uploaded) {
		//is file extension compliant?
		$ext = strtolower($cv->file_src_name_ext);
		if($ext != 'pdf' && $ext != 'doc' && $ext != 'docx') {
			//extension non-compliant, display error
			header("Location: a_uploadCV.php?error=Your CV is NOT in a valid Format!");
			exit;
		}
		//is file size compliant?
		if($cv->file_src_size > 204800) {
			//file size non-compliant, display error
			header("Location: a_uploadCV.php?error=Your CV's file size is greater than 200KB!");
			exit;
		}
		//echo("<br>No errors! Ready to store file");
		
		//delete if it already exists
		if($row_applicant['cv'] != '') {
			//echo "<br>CV exists, delete it";
			unlink('uploadedCVs/'.$row_applicant['cv']);
		}
		

		$cv->file_new_name_body = $row_applicant['surname'].'_'.$row_applicant['firstname'];
		$cv->Process("uploadedCVs/");
		if($cv->processed) {
			//echo "<br>File successfully uploaded"; die;
			$updateSQL = sprintf("UPDATE applicants SET cv=%s WHERE id=%s",
							   GetSQLValueString($cv->file_dst_name, "text"),
							   GetSQLValueString($_POST['id'], "int"));
			
			mysql_select_db($database_fer, $fer);
			$Result1 = mysql_query($updateSQL, $fer) or die(mysql_error());
			
			$updateGoTo = "a_uploadCV.php?msg=".urlencode("CV Succesfully Updated!");
			header(sprintf("Location: %s", $updateGoTo));
		} else {
			echo "<br>File didn't upload! Error From Class:".$cv->error; die;
		}


	} else {
		die('No CV attached, please click BACK on your Browser, attach and try again');
	}
}
//check if section status exists for this applicant and create it if otherwise
if(SectionStatusExists($FER_User['id']) == false) {
	CreateSectionStatus($FER_User['id']);
}

if($row_applicant['cv'] != '' && file_exists('uploadedCVs/'.$row_applicant['cv']))
{
	  //update section status
	 UpdateSectionStatus($FER_User['id'],'uploadCV','1');
}
else
{
	  //update section status
	 UpdateSectionStatus($FER_User['id'],'uploadCV','0');
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<link rel="shortcut icon" href="favicon.png" />
    
	<title>Professional Certifications - <?php echo $FER_User['firstname'] ?> <?php echo $FER_User['surname'] ?>| <?php echo $config['shortname'] ?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">
    <link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
    <link href="../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">

    <!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include('-inc-header-top.php'); ?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include('-inc-header-nav.php'); ?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
		    <?php include('-inc-applicant-top.php'); ?>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
		    <div class="row">
		        <div class="col-sm-4 page-sidebar">
		            <?php include('-inc-applicant-side.php'); ?>
	            </div>
		        <!-- end .page-sidebar -->
	          <div class="col-sm-8 page-content">
		            <h3>
		                <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>
		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>
	                </div>-->
		                Useful Tips
                </h3>
                        <form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="formPersonalDetails" id="formPersonalDetails">
	                    <div class="white-container sign-up-form">
	                        <div>
	                            <?php //<h5>Upload</h5> ?>
                              <section>
	                                <?php if (isset($_GET['error'])) { ?>
		                                <div class="alert alert-error">
		                                    <h6>Oops!</h6>
		                                    <p><?php echo $_GET['error'] ?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php } ?>
	                                <?php if (isset($_GET['msg'])) { ?>
		                                <div class="alert alert-success">
		                                    <h6>Wow!</h6>
		                                    <p><?php echo $_GET['msg'] ?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php } ?>
                                <div class="row">
                                <p style="padding:5px; font-size:16px;">Are you looking for a job? The following tips  can help you land your dream job faster.</p>
                                <p style="padding:5px; font-size:14px;">Applying for jobs is a process that  nearly everyone will have to undertake to secure and employment and possibly  explore career advancement opportunities. There are a variety of tools that are  useful to the applicant applying for a job In addition to a standard CV or  resume, cover letters can help candidates further clarify their qualifications,  introduce themselves to potential employers and assist them in clearly  articulating their aspirations for the vacant position. Job applications can be  filed based on advertised openings or they can be unsolicited. In both  circumstances, it is useful to include a cover letter and a resume or CV, so  that potential employers can easily ascertain a candidate&rsquo;s suitability for the  role.</p>
                                <p>First impressions can make or mar an  application process. Your appearance is important, even when submitting an  application. Every time you make contact with an employer, dress as though you  are ready to go to work. The employer may have a job that needs to be filled  and want to interview you immediately. So, always be prepared.</p>
                                </div>
                              </section>
							</div>
	                    </div>
                  </form>
	          </div>
		        <!-- end .page-content -->
	        </div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include('-inc-footer-top.php'); ?>

		<div class="copyright">
			<?php include('-inc-footer-bottom.php'); ?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
<script src="js/ValidateDel.js"></script>
<script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="../js/mainNavbar.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	$('#otherNationArea').hide();
	$('.stateArea').hide();
	if($('select#nationality').val() == 'Nigeria') {
		$('.stateArea').show();
	}
	
	//datepicker
	$('#dob').datepick({dateFormat: 'yyyy-mm-dd', yearRange: '-60:-15'});

  	//actions for dropdowns - nationality (to display state) and state( to display LGAs)
	$("select#nationality").change(function()
	{
		//alert("change nationality");
		if($(this).val() != "Nigeria")
		{
			$('.stateArea').hide();//alert("not Nigeria");
		}
		else
		{
			$('.stateArea').show();
		}
	});//end of nationality

	$('select#stateOfOrigin').change(function()
	{							 
	  $("select#lga > option").remove();
	  //alert('lga options cleared');
	  $.ajax({
		type: "GET",
		url: "../xml/states.xml",
		dataType: "xml",
		success: parseXml
	  });			
	});
	
});
//functioned used in state and lga dropdowns
function  parseXml(xml)
{
  //alert('started parsing xml');
  //find every state
  $(xml).find("state").each(function()
  {
	var stateSelected = $('select#stateOfOrigin').val();
	var stateName = $(this).attr("name");
	//alert(stateName +'/'+ stateSelected);
	if(stateName == stateSelected)
	{
		//alert(stateName + ' has been selected');	
		var printd = "The LGS:";
		$(this).find("lg").each(function() 
		{
			//alert('entered each LG');
			//printd += $(this).text();
			var dVal = $(this).text();
			$("select#lga").append("<option value='"+dVal+"'>"+dVal+"</option>");
		});
		//alert(printd);
		
		//stop searching
		return false;
	}
	//$("#output").append($(this).attr("name") + "<br />");
  });
}

</script>
</body>
</html>