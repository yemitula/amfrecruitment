<?php require_once("_inc_checkSession.php"); ?>
<?php require_once("_inc_applicantsOnly.php"); ?>
<?php $thisPage = basename( $_SERVER['PHP_SELF'] ); ?>
<?php require_once('_inc_config.php'); ?>
<?php require_once('Connections/fer.php'); ?>
<?php include('_inc_Functions.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$applicant_id = $FER_User['id'];
mysql_select_db($database_fer, $fer);
$query_applications = "SELECT a.id, v.title, a.vacancy_id, a.dateApplied, a.status FROM applications a LEFT JOIN vacancies v ON (a.vacancy_id = v.id) WHERE a.applicant_id = '$applicant_id'";
$applications = mysql_query($query_applications, $fer) or die(mysql_error());
$row_applications = mysql_fetch_assoc($applications);
$totalRows_applications = mysql_num_rows($applications);
?>
<!doctype html><?php //error_reporting(0) ?>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<link rel="shortcut icon" href="favicon.png" />
    
	<title>Professional Certifications - <?php echo $FER_User['firstname'] ?> <?php echo $FER_User['surname'] ?>| <?php echo $config['shortname'] ?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">
    <link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
    <link href="../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">

    <!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include('-inc-header-top.php'); ?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include('-inc-header-nav.php'); ?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
		    <?php include('-inc-applicant-top.php'); ?>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
		    <div class="row">
		        <div class="col-sm-4 page-sidebar">
		            <?php include('-inc-applicant-side.php'); ?>
	            </div>
		        <!-- end .page-sidebar -->
	          <div class="col-sm-8 page-content">
		            <h3>
		                <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>
		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>
	                </div>-->
		                 My Applications
                </h3>
                        <form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="formPersonalDetails" id="formPersonalDetails">
	                    <div class="white-container sign-up-form">
	                        <div>
	                            <?php //<h5>Applications</h5> ?>
                              <section>
	                                <?php if (isset($_GET['error'])) { ?>
		                                <div class="alert alert-error">
		                                    <h6>Oops!</h6>
		                                    <p><?php echo $_GET['error'] ?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php } ?>
	                                <?php if (isset($_GET['msg'])) { ?>
		                                <div class="alert alert-success">
		                                    <h6>Wow!</h6>
		                                    <p><?php echo $_GET['msg'] ?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php } ?>
                              <div class="row">
              <?php if ($totalRows_applications > 0) { // Show if recordset not empty ?>
		                <h6>Number of applications - <?php echo $totalRows_applications ?></h6>
		                <table class="table-striped">
		                    <thead>
		                        <tr>
		                            <th width="40%">Vacancy</th>
		                            <th width="30%">Date Applied</th>
		                            <th width="10%">Current Status</th>
		                            <th width="10%">Action</th>
	                            </tr>
	                        </thead>
		                    <tbody>
		                        <?php do { ?>
                      <tr>
                        <td><a href="a_vacancyDetails.php?id=<?php echo $row_applications['vacancy_id']; ?>"><?php echo $row_applications['title']; ?></a></td>
                        <td><?php echo date("d-M-Y",strtotime($row_applications['dateApplied'])); ?></td>
                        <td><?php echo $row_applications['status']; ?></td>
                        <td><a href="a_cancelApp.php?id=<?php echo $row_applications['id']; ?>" onclick=" return ValidateDel('<?php echo $row_applications['id']; ?>','a_cancelApp.php?id=','Are you sure you want to CANCEL?');" class="fa fa-trash-o"></a></td>
                      </tr>
                      <?php } while ($row_applications = mysql_fetch_assoc($applications)); ?>
	                        </tbody>
	                    </table>
		                <?php } // Show if recordset not empty ?>
                        <?php if ($totalRows_applications == 0) { // Show if recordset empty ?>
  				<div class="col-sm-12" style="background:#F27657; padding:10px;">You have not applied for ANY vacancy!</div>
  						<?php } // Show if recordset empty ?>
                              </div>
                              </section>
</div>
								<?php //<hr /> ?>
	                        <div class="clearfix" id="captchaArea">
                            </div>
	                    </div>
                  </form>
	          </div>
		        <!-- end .page-content -->
	        </div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include('-inc-footer-top.php'); ?>

		<div class="copyright">
			<?php include('-inc-footer-bottom.php'); ?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
<script src="js/ValidateDel.js"></script>
<script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="../js/mainNavbar.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	$('#otherNationArea').hide();
	$('.stateArea').hide();
	if($('select#nationality').val() == 'Nigeria') {
		$('.stateArea').show();
	}
	
	//datepicker
	$('#dob').datepick({dateFormat: 'yyyy-mm-dd', yearRange: '-60:-15'});

  	//actions for dropdowns - nationality (to display state) and state( to display LGAs)
	$("select#nationality").change(function()
	{
		//alert("change nationality");
		if($(this).val() != "Nigeria")
		{
			$('.stateArea').hide();//alert("not Nigeria");
		}
		else
		{
			$('.stateArea').show();
		}
	});//end of nationality

	$('select#stateOfOrigin').change(function()
	{							 
	  $("select#lga > option").remove();
	  //alert('lga options cleared');
	  $.ajax({
		type: "GET",
		url: "../xml/states.xml",
		dataType: "xml",
		success: parseXml
	  });			
	});
	
});
//functioned used in state and lga dropdowns
function  parseXml(xml)
{
  //alert('started parsing xml');
  //find every state
  $(xml).find("state").each(function()
  {
	var stateSelected = $('select#stateOfOrigin').val();
	var stateName = $(this).attr("name");
	//alert(stateName +'/'+ stateSelected);
	if(stateName == stateSelected)
	{
		//alert(stateName + ' has been selected');	
		var printd = "The LGS:";
		$(this).find("lg").each(function() 
		{
			//alert('entered each LG');
			//printd += $(this).text();
			var dVal = $(this).text();
			$("select#lga").append("<option value='"+dVal+"'>"+dVal+"</option>");
		});
		//alert(printd);
		
		//stop searching
		return false;
	}
	//$("#output").append($(this).attr("name") + "<br />");
  });
}

</script>
</body>
</html>