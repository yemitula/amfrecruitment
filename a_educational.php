<?php require_once "_inc_checkSession.php";?>
<?php require_once "_inc_applicantsOnly.php";?>
<?php $thisPage = basename($_SERVER['PHP_SELF']);?>
<?php require_once '_inc_config.php';?>
<?php require_once 'Connections/fer.php';?>
<?php include '_inc_Functions.php';?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "AddForm")) {

	if (testYear4DOB($_POST['year'])) {
		if ($_POST['level'] == 'Secondary') {
			//submit to secondary table
			$insertSQL = sprintf("INSERT INTO secschool (applicant_id, school, certificate, `date`) VALUES (%s, %s, %s, %s)",
				GetSQLValueString($_POST['applicant_id'], "int"),
				GetSQLValueString($_POST['school'], "text"),
				GetSQLValueString(($_POST['otherCert']) ? $_POST['otherCert'] : $_POST['certificate'], "text"),
				GetSQLValueString($_POST['year'] . '-' . $_POST['month'] . '-01', "date"));

			mysql_select_db($database_fer, $fer);
			$Result1 = mysql_query($insertSQL, $fer) or die(mysql_error());

		} else {
			//submit to tertiary table
			$insertSQL = sprintf("INSERT INTO tertiary (applicant_id, institution, course, qualification, `class`, uni_country, Course_Category, `date`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
				GetSQLValueString($_POST['applicant_id'], "int"),
				GetSQLValueString(($_POST['otherInst']) ? $_POST['otherInst'] : $_POST['institution'], "text"),
				GetSQLValueString($_POST['course'], "text"),
				GetSQLValueString(($_POST['otherQual']) ? $_POST['otherQual'] : $_POST['qualification'], "text"),
				GetSQLValueString(($_POST['otherClass']) ? $_POST['otherClass'] : $_POST['class'], "text"),
				GetSQLValueString($_POST['uni_country'], "text"),
				GetSQLValueString($_POST['Course_Category'], "text"),
				GetSQLValueString($_POST['year'] . '-' . $_POST['month'] . '-01', "date"));

			mysql_select_db($database_fer, $fer);
			$Result1 = mysql_query($insertSQL, $fer) or die(mysql_error());
		}
		$insertGoTo = "a_educational.php?msg=" . urlencode("Educational Qualification has been added!");
		header(sprintf("Location: %s", $insertGoTo));
	} else {
		header("Location: a_educational.php?error=" . urlencode("The year you supplied(" . $_POST['year'] . ") is inconsistent with your Date of Birth!!!"));
	}

}

//section status check and update
$colname_secondary = "-1";
if (isset($_SESSION['FER_User']['id'])) {
	$colname_secondary = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_secondary = "SELECT * FROM secschool WHERE applicant_id = '$colname_secondary' ORDER BY `date` DESC";
$secondary = mysql_query($query_secondary, $fer) or die(mysql_error());
$row_secondary = mysql_fetch_assoc($secondary);
$totalRows_secondary = mysql_num_rows($secondary);

$colname_tertiary = "-1";
if (isset($_SESSION['FER_User']['id'])) {
	$colname_tertiary = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_tertiary = "SELECT * FROM tertiary WHERE applicant_id = '$colname_tertiary' ORDER BY `date` DESC";
$tertiary = mysql_query($query_tertiary, $fer) or die(mysql_error());
$row_tertiary = mysql_fetch_assoc($tertiary);
$totalRows_tertiary = mysql_num_rows($tertiary);

//check if section status exists for this applicant and create it if otherwise
if (SectionStatusExists($FER_User['id']) == false) {
	CreateSectionStatus($FER_User['id']);
}

if ($totalRows_tertiary && $totalRows_secondary) {
	//update section status
	UpdateSectionStatus($FER_User['id'], 'educational', '1');
} else {
	//update section status
	UpdateSectionStatus($FER_User['id'], 'educational', '0');
}

$colname_sections = "-1";
if (isset($_SESSION['FER_User']['id'])) {
	$colname_sections = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_sections = sprintf("SELECT * FROM sectionstatus WHERE applicant_id = %s", GetSQLValueString($colname_sections, "int"));
$sections = mysql_query($query_sections, $fer) or die(mysql_error());
$row_sections = mysql_fetch_assoc($sections);
$totalRows_sections = mysql_num_rows($sections);

mysql_select_db($database_fer, $fer);
$query_countries = "SELECT * FROM countries";
$countries = mysql_query($query_countries, $fer) or die(mysql_error());
$row_countries = mysql_fetch_assoc($countries);
$totalRows_countries = mysql_num_rows($countries);

if ($totalRows_sections) {
	$cvStatus = 1;
	foreach ($row_sections as $value) {
		if ($value == 0) {
			$cvStatus = 0;
		}

	}
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="shortcut icon" href="favicon.png" />

	<title>Educational/Academic Qualifications - <?php echo $FER_User['firstname']?> <?php echo $FER_User['surname']?>| ABC Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">

	<!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include '-inc-header-top.php';?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include '-inc-header-nav.php';?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
		    <?php include '-inc-applicant-top.php';?>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
		    <div class="row">
		        <div class="col-sm-4 page-sidebar">
		            <?php include '-inc-applicant-side.php';?>
	            </div>
		        <!-- end .page-sidebar -->
		        <div class="col-sm-8 page-content">
		            <h3>
		                <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>
		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>
	                </div>-->
		                Educational/Academic Qualifications
	                </h3>
		            <form id="formAddEduc" name="formAddEduc" method="post" action="">
	                    <div class="white-container sign-up-form">
	                        <div>
	                            <h5>Add Education</h5>
	                            <section>
	                                <?php if (isset($_GET['error'])) {?>
		                                <div class="alert alert-error">
		                                    <h6>Oops!</h6>
		                                    <p><?php echo $_GET['error']?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php }
?>
	                                <?php if (isset($_GET['msg'])) {?>
		                                <div class="alert alert-success">
		                                    <p><?php echo $_GET['msg']?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php }
?>
	                                <div class="row">
	                                    <div class="col-sm-6">
	                                        <label for="level">Level</label>
	                                        <select name="level" id="level">
                        <option value="Select..." selected="selected">Select...</option>
                        <option value="Secondary">Secondary</option>
                        <option value="Tertiary">Tertiary</option>
                      </select>
	                                    </div>
                                    </div>

	                                <div class="row secArea">
	                                    <div class="col-sm-9">
	                                        <label for="school">School</label>
	                                        <input name="school" type="text" class="fullBox" id="school" placeholder="e.g. Salvation Army High School, Ibesa, Osun State" />
	                                    </div>
                                    </div>

	                                <div class="row secArea">
	                                    <div class="col-sm-6">
	                                        <label for="certificate">Certificate</label>
	                                        <select name="certificate" id="certificate">
                        <option value="WAEC SSCE">WAEC SSCE</option>
                        <option value="NECO SSCE">NECO SSCE</option>
                        <option value="Other">Other</option>
                      </select>
	                                    </div>

                                        <div class="col-sm-6" id="otherCertArea">
	                                        <label for="otherCert">Specify Certificate</label>
	                                       <input type="text" name="otherCert" id="otherCert" placeholder="please specify" />
                      </select>
	                                    </div>
                                    </div>

                                    <div class="row tetArea">
	                                    <div class="col-sm-9">
	                                        <label for="course">Country Of Study</label>
	                                        <select name="uni_country" id="uni_country" >
	                                          <option value=""> --Select Country Of Study-- </option>
	                                          <?php
do {
	?>
	                                          <option value="<?php echo $row_countries['Country']?>"><?php echo $row_countries['Country']?></option>
	                                          <?php
} while ($row_countries = mysql_fetch_assoc($countries));
$rows = mysql_num_rows($countries);
if ($rows > 0) {
	mysql_data_seek($countries, 0);
	$row_countries = mysql_fetch_assoc($countries);
}
?>
                                            </select>
	                                    </div>
                                    </div>

                                     <div class="row tetArea">
	                                    <div class="col-sm-9">
	                                        <label for="institution">Institution</label>
<!-- 	                                        <select name="institution" class="" id="institution">
                          <option value="0" selected="selected">Select One...</option>
                          <option style=" font-weight:bold">---- Universities -----</option>-->
                          <?php //include('_inc_universities.html'); ?>
                          <!-- <option style="font-weight:bold">---- Polytechnics -----</option> -->
                          <?php //include('_inc_polytechnics.html'); ?>
                          <!-- <option value="Other">Others (Not Listed)</option>
                        </select>
                        					<br>
                                            <span id="otherInstArea"><input name="otherInst" type="text" id="otherInst" placeholder="please specify" /></span> -->
	                                        <input name="institution" type="text" class="" id="institution"  placeholder="Institution Attended" />
	                                    </div>
                                    </div>



                                    <div class="row tetArea">
	                                    <div class="col-sm-9">
	                                        <label for="course">Course Category</label>
	                                        <select name="Course_Category" id="Course_Category">
		                                        <option>--Select Course category--</option>
		                                        <option value="ARtS">ARtS</option>
		                                        <option value="BASIC MEDICAL SCIENCES">BASIC MEDICAL SCIENCES</option>
	                                        	<option value="BUSINESS ADMINISTRATION">BUSINESS ADMINISTRATION</option>
	                                        	<option value="CLINICAL SCIENCES">CLINICAL SCIENCES</option>
	                                        	<option value="DENTAL SCIENCES">DENTAL SCIENCES</option>
	                                        	<option value="EDUCATION">EDUCATION</option>
	                                        	<option value="ENGINEERING">ENGINEERING</option>
	                                        	<option value="ENVIRONMENTAL SCIENCES">ENVIRONMENTAL SCIENCES</option>
	                                        	<option value="LAW">LAW</option>
	                                        	<option value="PHARMACY">PHARMACY</option>
	                                        	<option value="SOCIAL SCIENCES">SOCIAL SCIENCES</option>
	                                        	<option value="SCIENCES">SCIENCES</option>
	                                        </select>
	                                    </div>
                                    </div>

                                    <div class="row tetArea">
	                                    <div class="col-sm-9">
	                                        <label for="course">Course of Study</label>
	                                        <input name="course" type="text" class="" id="course"  placeholder="In Full e.g. Industrial and Labour Relations" />
	                                    </div>
                                    </div>

                                    <div class="row tetArea">
	                                    <div class="col-sm-6">
	                                        <label for="qualification">Qualification Obtained</label>
	                                        <select name="qualification" id="qualification">
                        <option value="0">Select...</option>
                        <option value="OND">OND</option>
                        <option value="HND">HND</option>
                        <option value="BSc">BSc</option>
                        <option value="BA">BA</option>
                        <option value="BArch">BArch</option>
                        <option value="Beng">BEng</option>
                        <option value="BEd">BEd</option>
                        <option value="BSc/Ed">BSc/Ed</option>
                        <option value="BA/Ed">BA/Ed</option>
                        <option value="LLB">LLB</option>
                        <option value="MA">MA</option>
                        <option value="MSc">MSc</option>
                        <option value="MPhil">MPhil</option>
                        <option value="LLM">LLM</option>
                        <option value="MBA">MBA</option>
                        <option value="PHD">PHD</option>
                        <option value="Other">Other</option>
                      </select>
	                                    </div>

                                        <div class="col-sm-6" id="otherQualArea">
	                                        <label for="otherQual">Specify Qualification</label>
	                                       <input type="text" name="otherQual" id="otherQual" />
	                                    </div>
                                    </div>

                                    <div class="row tetArea">
	                                    <div class="col-sm-6">
	                                        <label for="class">Class</label>
	                                        <select name="class" id="class">
                        <option value="0" selected="selected">Select...</option>
                        <option value="First Class">First Class</option>
                        <option value="Second Class Upper">Second Class Upper</option>
                        <option value="Second Class Lower">Second Class Lower</option>
                        <option value="Third Class">Third Class</option>
                        <option value="Pass">Pass</option>
                        <option value="Distinction">Distinction</option>
                        <option value="Upper Credit">Upper Credit</option>
                        <option value="Lower Credit">Lower Credit</option>
                        <option value="In View/Awaiting Result">In View/Awaiting Result</option>
                        <option value="Other">Other</option>
                      </select>
	                                    </div>

                                        <div class="col-sm-6" id="otherClassArea">
	                                        <label for="otherClass">Specify Class</label>
	                                       <input type="text" name="otherClass" id="otherClass" />
	                                    </div>
                                    </div>

                                    <div class="row dateArea">
	                                    <div class="col-sm-6">
                                            <label for="cgpaScore">Date Obtained </label> <br>
	                                        <select name="month" id="month" style="width:150px; display:inline">
                          <option selected="selected">Month</option>
                          <?php include '_inc_monthList.php';?>
                        </select>&nbsp;
	                                       	<select name="year" id="year" style="width:150px; display:inline">
                          <option selected="selected">Year</option>
                          <?php include '_inc_yearListFrom1940.php';?>
                        </select>
                                       </div>
                                    </div>


                                </section>
</div>
                            <hr class="mt60">
                            <div class="clearfix">
                                <input name="submit" type="submit" class="btn btn-default pull-right" id="submit" value="Add Entry" />
                            </div>
                           <input name="MM_insert" type="hidden" id="MM_insert" value="AddForm" />
                            <input name="applicant_id" type="hidden" id="applicant_id" value="<?php echo $_SESSION['FER_User']['id'];?>" />
	                    </div>
	                </form>
		            <?php if ($totalRows_secondary > 0 || $totalRows_tertiary > 0) {?>
                    <div class="candidates-item candidates-single-item">
		                <h5 class="title">
		                </h5>
		                <?php if ($totalRows_tertiary > 0) { // Show if recordset not empty ?>
		                <h6>Tertiary</h6>
		                <table class="table-striped">
		                    <thead>
		                        <tr>
		                            <th width="40%">Institution (Course)</th>
		                            <th width="30%">Qualification (Class)</th>
		                            <th width="10%">Date</th>
		                            <th width="5%"></th>
		                            <th width="5%"></th>
	                            </tr>
	                        </thead>
		                    <tbody>
		                        <?php do {?>
		                        <tr>
		                            <td><?php echo $row_tertiary['institution'];?> (<?php echo $row_tertiary['course'];?>)</td>
		                            <td><?php echo $row_tertiary['qualification'];?> (<?php echo $row_tertiary['class'];?>)</td>
		                            <td><?php echo date("F, Y", strtotime($row_tertiary['date']));?></td>
		                            <td><a href="a_editTertiary.php?id=<?php echo $row_tertiary['id'];?>" title="Edit" class="fa fa-edit"></a></td>
		                            <td><a href="#" title="Delete" onclick=" return ValidateDel('<?php echo $row_tertiary['id'];?>','a_delTertiary.php?tID=','Are you sure you want to DELETE THIS ITEM? (This Action is NOT reversible)!!!');" class="fa fa-trash-o"></a></td>
	                            </tr>
		                        <?php } while ($row_tertiary = mysql_fetch_assoc($tertiary));?>
	                        </tbody>
	                    </table>
		                <?php } // Show if recordset not empty ?>
		                <?php if ($totalRows_secondary > 0) { // Show if recordset not empty ?>
		                <h6>Secondary</h6>
		                <table class="table-striped">
		                    <thead>
		                        <tr>
		                            <th width="50%">School</th>
		                            <th width="25%">Certificate</th>
		                            <th width="15%">Date</th>
		                            <th width="5%">&nbsp;</th>
		                            <th width="5%">&nbsp;</th>
	                            </tr>
	                        </thead>
		                    <tbody>
		                        <?php do {?>
		                        <tr>
		                            <td><?php echo $row_secondary['school'];?></td>
		                            <td><?php echo $row_secondary['certificate'];?></td>
		                            <td><?php echo date("F, Y", strtotime($row_secondary['date']));?></td>
		                            <td><a href="a_editSecSchool.php?id=<?php echo $row_secondary['id'];?>" title="Edit" class="fa fa-edit"></a></td>
		                            <td><a href="#" title="Delete" onclick=" return ValidateDel('<?php echo $row_secondary['id'];?>','a_delSecondary.php?sID=','Are you sure you want to DELETE THIS ITEM? (This Action is NOT reversible)!!!');" class="fa fa-trash-o"></a></td>
	                            </tr>
		                        <?php } while ($row_secondary = mysql_fetch_assoc($secondary));?>
	                        </tbody>
	                    </table>
		                <?php } // Show if recordset not empty ?>

	                </div>
                    <?php } // end tertiary and secondary section ?>

		        </div>
		        <!-- end .page-content -->
	        </div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include '-inc-footer-top.php';?>

		<div class="copyright">
			<?php include '-inc-footer-bottom.php';?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
<script src="js/ValidateDel.js"></script>
<script type="text/javascript">
$(document).ready(function() {
$('.secArea').hide();
$('.tetArea').hide();
$('.dateArea').hide();
$('#submit').hide();
$('#otherCertArea').hide();
$('#otherInstArea').hide();
$('#otherQualArea').hide();
$('#otherClassArea').hide();

	$('#level').change(function() {
		switch($('#level').val())
		{
			case 'Select...':
				$('.secArea').hide();
				$('.tetArea').hide();
				$('#submit').hide();
				$('.dateArea').hide();

			break;

			case 'Secondary':
				$('.tetArea').hide();
				$('.secArea').show();
				$('#submit').show();
				$('.dateArea').show();
			break;

			case 'Tertiary':
				$('.secArea').hide();
				$('.tetArea').show();
				$('#submit').show();
				$('.dateArea').show();
		}
	});

	$('#certificate').change(function() {
		if($('#certificate').val() == 'Other')
			$('#otherCertArea').show();
		else
		{	$('#otherCertArea').hide();
			$('#otherCert').val('');
		}
	});

	$('#institution').change(function() {
		if($('#institution').val() == 'Other')
			$('#otherInstArea').show();
		else
		{	$('#otherInstArea').hide();
			$('#otherInst').val('');
		}
	});

	$('#qualification').change(function() {
		if($('#qualification').val() == 'Other')
			$('#otherQualArea').show();
		else
		{	$('#otherQualArea').hide();
			$('#otherQual').val('');
		}
	});

	$('#class').change(function() {
		if($('#class').val() == 'Other')
			$('#otherClassArea').show();
		else
		{	$('#otherClassArea').hide();
			$('#otherClass').val('');
		}
	});

$('#submit').click(function() {
	if($('#level').val() == 'Secondary')
	{
		if($('#school').val() == '')
		{
			alert('SCHOOL is required!');
			return false;
		}
		if($('#certificate').val() == 'Other' && $('#otherCert').val() == '')
		{
			alert('You MUST specify the OTHER certificate!');
			return false;
		}
	}
	else
	{
		if($('#course').val() == '')
		{
			alert('COURSE is required!');
			return false;
		}
		if($('#institution').val() == 'Other' && $('#otherInst').val() == '')
		{
			alert('You MUST specify the OTHER institution!');
			return false;
		}
		if($('#institution').val() == '0' || $('#qualification').val() == '0' || $('#class').val() == '0')
		{
			alert('Please Select the NECESSARY VALUES (Insitution/Qualification/Class)!!!');
			return false;
		}
		if($('#qualification').val() == 'Other' && $('#otherQual').val() == '')
		{
			alert('You MUST specify the OTHER qualification!');
			return false;
		}
		if($('#class').val() == 'Other' && $('#otherClass').val() == '')
		{
			alert('You MUST specify the OTHER class!');
			return false;
		}
	}
});	//SUBMIT FUNCTION

});
</script>
</body>
</html>
<?php
mysql_free_result($secondary);

mysql_free_result($countries);
?>
