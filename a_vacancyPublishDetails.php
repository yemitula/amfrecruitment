<?php //require_once "_inc_checkSession.php";?>
<?php //require_once "_inc_applicantsOnly.php";?>
<?php $thisPage = basename($_SERVER['PHP_SELF']);?>
<?php require_once '_inc_config.php';?>
<?php require_once 'Connections/fer.php';?>
<?php include '_inc_Functions.php';?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}

$colname_vacancy = "-1";
if (isset($_GET['id'])) {
	$colname_vacancy = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_vacancy = sprintf("SELECT * FROM vacancies WHERE id = %s", GetSQLValueString($colname_vacancy, "int"));
$vacancy = mysql_query($query_vacancy, $fer) or die(mysql_error());
$row_vacancy = mysql_fetch_assoc($vacancy);
//var_dump($row_vacancy); die;
$totalRows_vacancy = mysql_num_rows($vacancy);

mysql_select_db($database_fer, $fer);
$employerID = $row_vacancy['employer_id'];
$query_employer = "SELECT * FROM employers WHERE id = '$employerID'";
$employer = mysql_query($query_employer, $fer) or die(mysql_error());
$row_employer = mysql_fetch_assoc($employer);
$totalRows_employer = mysql_num_rows($employer);
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="shortcut icon" href="favicon.png" />

	<title>Vancancy Preview</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">
    <link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
    <link href="../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">

    <!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php //include '-inc-header-top.php';?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php //include '-inc-header-nav.php';?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
		    <?php //include '-inc-applicant-top.php';?>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
		    <div class="row">
		        <!-- <div class="col-sm-4 page-sidebar"> -->
		            <?php //include '-inc-applicant-side.php';?>
	           <!--  </div> -->
		        <!-- end .page-sidebar -->
	          <div class="col-sm-12 page-content">
		            <h3>
		                <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>
		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>
	                </div>-->
		                 Vacancy Perview
                </h3>
                        <form action="<?php echo $editFormAction;?>" method="POST" enctype="multipart/form-data" name="formPersonalDetails" id="formPersonalDetails">
	                    <div class="white-container sign-up-form">
	                        <div>
	                           <?php //<h5>Upload</h5> ?>
                              <section>
	                                <?php if (isset($_GET['error'])) {?>
		                                <div class="alert alert-error">
		                                    <h6>Oops!</h6>
		                                    <p><?php echo $_GET['error']?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php }
?>
	                                <?php if (isset($_GET['msg'])) {?>
		                                <div class="alert alert-success">
		                                    <h6>Wow!</h6>
		                                    <p><?php echo $_GET['msg']?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php }
?>
                                <div class="row">
                                <table width="100%" border="0" cellspacing="0" cellpadding="10">
                    <tr>
                      <td align="right" valign="top"><strong>Employer</strong></td>
                      <td valign="top"><a href="a_employerProfile.php?id=<?php echo $row_vacancy['employer_id'];?>&amp;v=<?php echo $row_vacancy['id'];?>" target="_blank" class="biggerText2"><strong><?php echo strtoupper($row_employer['companyName'])?></strong></a>&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="34%" align="right" valign="top"><strong>Summary</strong></td>
                      <td width="66%" valign="top"><?php echo ucfirst($row_vacancy['summary']);?></td>
                    </tr>
                    <tr>
                      <td align="right" valign="top"><strong>Full Description</strong></td>
                      <td valign="top"><?php echo ucfirst($row_vacancy['longDescription']);?></td>
                    </tr>
                    <tr>
                      <td align="right" valign="top"><strong>Technical Requirements</strong></td>
                      <td valign="top"><?php echo ucfirst($row_vacancy['techReqs']);?></td>
                    </tr>
                    <tr>
                      <td align="right" valign="top"><strong>Educational Qualifications</strong></td>
                      <td valign="top"><?php echo ucfirst($row_vacancy['eduQual']);?></td>
                    </tr>
                    <tr>
                      <td align="right" valign="top"><strong>Behavioural Requirements</strong></td>
                      <td valign="top"><?php echo ucfirst($row_vacancy['behavReqs']);?></td>
                    </tr>
                    <tr>
                      <td align="right" valign="top"><strong>Job Type</strong></td>
                      <td valign="top"><?php echo $row_vacancy['jobType'];?></td>
                    </tr>
                    <tr>
                      <td align="right" valign="top"><strong>Staff Level</strong></td>
                      <td valign="top"><?php echo ($row_vacancy['staffLevel']) ? $row_vacancy['staffLevel'] : 'Any';?></td>
                    </tr>
                    <tr>
                      <td align="right" valign="top"><strong>Date Posted</strong></td>
                      <td valign="top"><?php echo date("d-M-Y", strtotime($row_vacancy['datePosted']));?></td>
                    </tr>
                    <?php if ($row_vacancy['dateOpening']) {?>
                    <tr>
                      <td align="right" valign="top"><strong>Opening Date</strong></td>
                      <td valign="top"><?php echo date("d-M-Y", strtotime($row_vacancy['dateOpening']));?></td>
                    </tr>
                    <?php }
?>
                    <?php if ($row_vacancy['dateClosing']) {?>
                    <tr>
                      <td align="right" valign="top"><strong>Closing Date</strong></td>
                      <td valign="top"><?php echo date("d-M-Y", strtotime($row_vacancy['dateClosing']));?></td>
                    </tr>
                    <?php }
?>
                    <tr>
                      <td align="right" valign="top"><strong>Status</strong></td>
                      <td valign="top"><?php echo (empty($row_vacancy['dateOpening']) && empty($row_vacancy['dateClosing'])) ? 'ongoing' : $row_vacancy['status'];?></td>
                    </tr>
                  </table>
                      </div>
                              </section>
							</div>
                            </div>
	                    </div>
                  </form>
	          </div>
		        <!-- end .page-content -->
	        </div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include '-inc-footer-top.php';?>

		<div class="copyright">
			<?php include '-inc-footer-bottom.php';?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
<script src="js/ValidateDel.js"></script>
<script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="../js/mainNavbar.js"></script>
<script type="text/javascript">
$(document).ready(function() {

});</body>
</html>