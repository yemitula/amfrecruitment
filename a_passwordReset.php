<?php require_once('_inc_config.php'); ?>
<?php require_once('Connections/fer.php'); ?>
<?php require_once('swiftMailer/lib/swift_required.php'); ?>
<?php include('_inc_Functions.php'); ?>
<?php include("_inc_funcHTMLEmail.php"); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_checkEmail = "-1";
if (isset($_POST['email'])) {
	$colname_checkEmail = $_POST['email'];
	mysql_select_db($database_fer, $fer);
	$query_checkEmail = sprintf("SELECT * FROM applicants WHERE email = %s", GetSQLValueString($colname_checkEmail, "text"));
	$checkEmail = mysql_query($query_checkEmail, $fer) or die(mysql_error());
	$row_checkEmail = mysql_fetch_assoc($checkEmail);
	$totalRows_checkEmail = mysql_num_rows($checkEmail);
	
	extract($_POST);
	
	if($totalRows_checkEmail > 0) {
		//found email
		//generate new password
		$newPass = randomPassword();
		//update password in database
		$updateSQL = "UPDATE applicants SET password='$newPass' WHERE email='$email'";
		$updateRS = mysql_query($updateSQL, $fer) or die(mysql_error());
		//send new generated password to the applicant's email
		//store the form contents in variables for easy use in email
		$firstname = $row_checkEmail['firstname'];
		//set the sender email
		$emailFrom = "erecruitment@fitc-ng.com";
		//send recepient email for alerts
		$to = $email;
		
		//Create the Transport
		$transport = Swift_MailTransport::newInstance();
		
		//Create the Mailer using your created Transport
		$mailer = Swift_Mailer::newInstance($transport);
		
		/////////////////	Alert Message	////////////////////
		
		//set the subject of the alert
		$subject = "Password Reset on AMF Recruitment";
		
		//body of the alert email - html
		$body = "
	<p>Dear $firstname,</p>
	<p><strong>You did a password reset on AMF Recruitment portal. A new password has been generated for you.</p>
	<p>Your new password: $newPass</p>
	<p>Thanks.</p>
	<p><strong>AMF Recruitment Team</strong></p>
		";
		$body = CreateHTMLEmail($body);
		//die($body);
		
		//Create a message
		$message = Swift_Message::newInstance($subject)
		->setFrom(array($emailFrom => "AMF Recruitment"))
		->setTo(array($to => $firstname))
		//->setBcc(array("alerts@tulabyte.net" => "Yemi Adetula"))
		->setBody($body, 'text/html')
		;
		//send the message
		$result = $mailer->send($message);
		header("Location: a_passwordReset.php?email=$email&msg=Your password was successfully reset and sent to your email");
		exit;
			
	} else {
		//password not found
		header("Location: a_passwordReset.php?email=$email&error=Sorry, this account does NOT exist");
		exit;
	}
	}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<link rel="shortcut icon" href="favicon.png" />
    
	<title>Password Recovery | <?php echo $config['shortname'] ?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">

	<!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include('-inc-header-top.php'); ?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include('-inc-header-nav.php'); ?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
			<div class="container">
				<h1>Password Recovery</h1>

				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					<li><a href="#">Password Recovery</a></li>
				</ul>
			</div>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 page-content">
					
					<form id="formLogin" name="formLogin" method="POST" action="">
					<div class="white-container sign-up-form">
						<div>
							<h3>Recovery Details</h3>
							<section>

								<?php if (isset($_GET['error'])) { ?>
                                <div class="alert alert-error">
                                    <h6>Oops!</h6>
                                    <p><?php echo $_GET['error'] ?></p>
                                    <a href="#" class="close fa fa-times"></a> </div>
                                <?php } ?>
                                <?php if (isset($_GET['msg'])) { ?>
                                <div class="alert alert-success">
                                    <h6>Wow!</h6>
                                    <p><?php echo $_GET['msg'] ?></p>
                                    <a href="#" class="close fa fa-times"></a> </div>
                                <?php } ?>
                                <h6 class="label">Please enter your email address. We will reset your password and send it to you.</h6>

								<div class="row">
									<div class="col-sm-9"><span id="sprytextfield2">
                                    <label for="email"></label>
                                    <input name="email" type="text" class="form-control" id="email" value="<?php echo $_GET['email'] ?>" />
                                    <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldInvalidFormatMsg">Invalid format.</span></span></div>
								</div>
								<div class="row">
								    </div>
							</section>
						</div>

						<hr class="mt60">

						<div class="clearfix">
							
                            <input name="register" type="submit" class="btn btn-default btn-large pull-right" id="register" value="Submit" />
						</div>
                        <input type="hidden" name="MM_insert" value="formAppReg" />
					</div>
                    </form>
				</div> <!-- end .page-content -->
			</div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include('-inc-footer-top.php'); ?>

		<div class="copyright">
			<?php include('-inc-footer-bottom.php'); ?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript">
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "email");
</script>
</body>
</html>