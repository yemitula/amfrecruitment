<?php 
function CreateHTMLEmail ($bodyContent) {
	//global config variable
	global $config;
//first part of email (before content)
$msgBody = "
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>Skillup Confirmation mail</title>
</head>

<body>
<table border='0' cellpadding='0' cellspacing='0' width='100%'>	
		<tbody id=''><tr id=''>
			<td style='padding:10px 0;'>
				<table align='center' border='0' cellpadding='0' cellspacing='0' width='600' style='border-collapse:collapse;border:1px solid #cccccc;'>
					<tbody id=''><tr id=''>
						<td align='center' bgcolor='#F7F9FA' style='color:#153643;font-family:Arial, sans-serif;font-size:28px;font-weight:bold;padding:0px 0;'>
							<img src='".$config['url']."mail-assets/mail-header.jpg' alt='".$config['shortname']." Mail Header' width='600' height='250' style='display:block;'>
						</td>
					</tr>
					<tr>
						<td bgcolor='#ffffff' style='padding:20px 30px 40px;'>
							<table border='0' cellpadding='0' cellspacing='0' width='100%'>
								<tbody><tr>
									<td style='color:#153643;font-family:Arial, sans-serif;font-size:17px;line-height:25px;padding:0px 0 20px;'>";

//second part (the message body)
$msgBody .= $bodyContent;

//final part (after message body)
$msgBody .= "</td>
								</tr>
							</tbody></table>
						</td>
					</tr>
					<tr>
						<td bgcolor='#006600' style='padding:30px;'>
							<table border='0' cellpadding='0' cellspacing='0' width='100%'>
								<tbody><tr>
									<td style='color:#ffffff;font-family:Arial, sans-serif;font-size:14px;' width='75%'>
										".$config['shortname']." Recruitment<br>
                                        ".$config['notify']."
									</td>
									<td align='right' width='25%'>
										<table border='0' cellpadding='0' cellspacing='0'>
											<tbody><tr>
												<td style='font-family:Arial, sans-serif;font-size:12px;font-weight:bold;padding:0px 3px 0px 0px;'>&nbsp;</td>
												<td style='font-size:0;line-height:0;' width='20'>&nbsp;</td>
												<td style='font-family:Arial, sans-serif;font-size:12px;font-weight:bold;padding:0px 3px 0px 0px;'>
													<a rel='nofollow' target='_blank' href='http://facebook.com/amfacilities' style='color:#ffffff;'>
														<img src='".$config['url']."mail-assets/facebookMail.png' alt='Facebook' width='48' height='48' style='display:block;' border='0'>
													</a>
												</td>
												<td style='font-size:0;line-height:0;' width='20'>&nbsp;</td>
												
												<td style='font-size:0;line-height:0;' width='20'>&nbsp;</td>
												<td style='font-family:Arial, sans-serif;font-size:12px;font-weight:bold;'>
													<a rel='nofollow' target='_blank' href='http://twitter.com/alphamead' style='color:#ffffff;'>
														<img src='".$config['url']."mail-assets/twitterMail.png' alt='Twitter' width='48' height='48' style='display:block;' border='0'>
													</a>
												</td>
											</tr>
										</tbody></table>
									</td>
								</tr>
							</tbody></table>
						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
	</tbody>
  	</table>
</body>
</html>

";

return $msgBody;
									
}
?>
