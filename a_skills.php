<?php require_once "_inc_checkSession.php";?>
<?php require_once "_inc_applicantsOnly.php";?>
<?php $thisPage = basename($_SERVER['PHP_SELF']);?>
<?php require_once '_inc_config.php';?>
<?php require_once 'Connections/fer.php';?>
<?php require_once '_inc_config.php';?>
<?php require_once 'Connections/fer.php';?>
<?php include '_inc_Functions.php';?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}

$colname_skill = "-1";
if (isset($_SESSION['FER_User']['id'])) {
	$colname_skill = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_skill = sprintf("SELECT * FROM skills WHERE applicant_id = %s ORDER BY skill ASC", GetSQLValueString($colname_skill, "int"));
$skill = mysql_query($query_skill, $fer) or die(mysql_error());
$row_skill = mysql_fetch_assoc($skill);
$totalRows_skill = mysql_num_rows($skill);

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "AddForm")) {
	if ($totalRows_skill == 10) {
		header("Location: a_skills.php?error=Sorry! You have already added 10 skills.<br>Please delete some if you want to add more.");
		exit;
	}

	//check if this person has already added this skill
	mysql_select_db($database_fer, $fer);
	$applicant_id = $_POST['applicant_id'];
	$skill = ($_POST['otherSkill']) ? $_POST['otherSkill'] : $_POST['skill'];
	$checkSQL = "SELECT * FROM skills WHERE applicant_id = '$applicant_id' AND skill = '$skill'";
	$checkRS = mysql_query($checkSQL, $fer) or die(mysql_error());
	$skillAlreadyAdded = mysql_num_rows($checkRS);
	if ($skillAlreadyAdded) {
		header("location: a_skills.php?error=You have already added $skill, you cannot add it twice");
		exit;
	}

	$insertSQL = sprintf("INSERT INTO skills (applicant_id, skill, competency) VALUES (%s, %s, %s)",
		GetSQLValueString($_POST['applicant_id'], "int"),
		GetSQLValueString(($_POST['otherSkill']) ? $_POST['otherSkill'] : $_POST['skill'], "text"),
		GetSQLValueString($_POST['competency'], "text"));

	$Result1 = mysql_query($insertSQL, $fer) or die(mysql_error());

	$insertGoTo = "a_skills.php?msg=" . urlencode("The skill has been saved");
	header(sprintf("Location: %s", $insertGoTo));
} // end of post

//check if section status exists for this applicant and create it if otherwise
if (SectionStatusExists($FER_User['id']) == false) {
	CreateSectionStatus($FER_User['id']);
}

if ($totalRows_skill) {
	//update section status
	UpdateSectionStatus($FER_User['id'], 'skills', '1');
} else {
	//update section status
	UpdateSectionStatus($FER_User['id'], 'skills', '0');
}

$colname_sections = "-1";
if (isset($_SESSION['FER_User']['id'])) {
	$colname_sections = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_sections = sprintf("SELECT * FROM sectionstatus WHERE applicant_id = %s", GetSQLValueString($colname_sections, "int"));
$sections = mysql_query($query_sections, $fer) or die(mysql_error());
$row_sections = mysql_fetch_assoc($sections);
$totalRows_sections = mysql_num_rows($sections);

if ($totalRows_sections) {
	$cvStatus = 1;
	foreach ($row_sections as $value) {
		if ($value == 0) {
			$cvStatus = 0;
		}

	}
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="shortcut icon" href="favicon.png" />

	<title>Professional Certifications - <?php echo $FER_User['firstname']?> <?php echo $FER_User['surname']?>| <?php echo $config['shortname']?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">

    <!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include '-inc-header-top.php';?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include '-inc-header-nav.php';?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
		    <?php include '-inc-applicant-top.php';?>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
		    <div class="row">
		        <div class="col-sm-4 page-sidebar">
		            <?php include '-inc-applicant-side.php';?>
	            </div>
		        <!-- end .page-sidebar -->
	          <div class="col-sm-8 page-content">
		            <h3>
		                <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>
		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>
	                </div>-->
		                Work Skills
                </h3>
                <form action="<?php echo $editFormAction;?>" method="post" name="AddForm" id="AddForm">
	                    <div class="white-container sign-up-form">
	                        <div>
	                            <h5>Add Skills</h5>
                              <section>
	                                <?php if (isset($_GET['error'])) {?>
		                                <div class="alert alert-error">
		                                    <h6>Oops!</h6>
		                                    <p><?php echo $_GET['error']?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php }
?>
	                                <?php if (isset($_GET['msg'])) {?>
		                                <div class="alert alert-success">
		                                   <!--  <h6>Wow!</h6> -->
		                                    <p><?php echo $_GET['msg']?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php }
?>
                                <div class="row">
								  <div class="col-sm-8" id="captchaArea2" style="margin:0px 0px 0px 0px;">Skill:<br />
								    <span id="spryselect1">
								    <select name="skill" id="skill">
								      <option value="" selected="selected"></option>
								      <option value="Project Management">Project Management</option>
								      <option value="Analysis/Research">Analysis/Research</option>
								      <option value="Strategy development">Strategy development</option>
								      <option value="Accounting">Accounting</option>
								      <option value="Customer service">Customer service</option>
								      <option value="Marketing">Marketing</option>
								      <option value="Programming">Programming</option>
								      <option value="Public Relation">Public Relation</option>
								      <option value="Report Writing">Report Writing</option>
								      <option value="Networking">Networking</option>
								      <option value="Presentation/Public Speaking">Presentation/Public Speaking</option>
								      <option value="Business Process Improvement">Business Process Improvement</option>
								      <option value="Coaching">Coaching</option>
								      <option value="Web development">Web development</option>
								      <option value="Database Management">Database Management</option>
								      <option value="Other">Other</option>
							        </select>
							      <span class="selectRequiredMsg">Please select an item.</span></span></div>
								  <div class="col-sm-8" id="captchaArea2" style="margin:3px 0px;"><br />
								    <span id="sprytextfield1">
								    <input type="text" name="otherSkill" id="otherSkill" placeholder="Please specify"/>
</span></div>

								  <div class="col-sm-6" id="captchaArea3" style="margin:8px 0px;">Competency:
										<span id="spryselect2">
										<select name="competency" id="competency">
										  <option value="Basic" selected="selected">Basic</option>
										  <option value="Intermediary">Intermediary</option>
										  <option value="Advanced">Advanced</option>
										  <option value="Expert">Expert</option>
										  <!-- <option value="Guru">Guru</option> -->
									    </select>
								  <span class="selectRequiredMsg">Please select an item.</span></span> </div>
								  <div class="col-sm-10" id="explanation" style="margin:8px 0px;">
								  A person assessed as having basic skill in the competency and always requires supervision in order to perform to desired standards.
							      </div>
								  <input name="applicant_id" type="hidden" id="applicant_id" value="<?php echo $_SESSION['FER_User']['id'];?>" />
<input name="MM_insert" type="hidden" id="MM_insert" value="AddForm" />
                                </div>
                              </section>
</div>
								<hr />
	                        <div class="clearfix" id="captchaArea">
                            <input name="submit" type="submit" class="btn btn-default pull-right" id="submit" value="Add" />
                            </div>
	                    </div>
                  </form>
                <div class="candidates-item candidates-single-item">
              <?php if ($totalRows_skill > 0) { // Show if recordset not empty ?>
                  <h6>SKILL(s)</h6>
                  <table class="table-striped">
                    <thead>
                      <tr>
                        <th width="50%">Skill</th>
                        <th width="25%">Competency</th>
                        <th width="15%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php do {?>
                  <tr>
                    <td><?php echo $row_skill['skill'];?></td>
                    <td><?php echo $row_skill['competency'];?></td>
                    <td align="center"><a href="#" onclick=" return ValidateDel('<?php echo $row_skill['id'];?>','a_delSkill.php?id=','Are you sure you want to DELETE THIS SKILL? (This Action is NOT reversible)!!!');" class="fa fa-trash-o"></a></td>
                  </tr>
                  <?php } while ($row_skill = mysql_fetch_assoc($skill));?>
                    </tbody>
                  </table>
                  <?php } // Show if recordset not empty ?>
                </div>
	          </div>
		        <!-- end .page-content -->
	        </div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include '-inc-footer-top.php';?>

		<div class="copyright">
			<?php include '-inc-footer-bottom.php';?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
<script src="js/ValidateDel.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#otherSkill').hide();

	$("#competency").change(function() {
		switch($("#competency").val())
		{
			case 'Basic': $('#explanation').html('Basic Skill: A person assessed as having basic skill in the competency and always requires supervision in order to perform to desired standards.').fast(); break;
			case 'Intermediary': $('#explanation').html('Intermediary: A person assessed as skilled in the competency, but requires occasional supervision.').fast(); break;
			case 'Advanced': $('#explanation').html('Advanced: A person assessed as exceeding the skilled level and does not require any supervision to carry out the task.').fast(); break;
			case 'Expert': $('#explanation').html('Expert: A person assessed as competent with the ability to supervise and train others in the competency.').fast(); break;
			case 'Guru': $('#explanation').html('Guru: A person who is  acknowledged as a guru in the competency by specific industry or  professional association. For e.g. an experienced individual who sits in the  advisory board of the Institute   of Engineers advising on  matters related to mechanical engineering may be considered as a guru in the  competency related to mechanical engineering.').fast();
		}
	});

	$('#skill').change(function() {
		if($('#skill').val() == 'Other')
			$('#otherSkill').show();
		else
		{
			$('#otherSkill').val('');
			$('#otherSkill').hide();
		}
	});

	$('#AddForm').submit(function() {
		if($('#skill').val() == 'Other' && $('#otherSkill').val() == '')
		{
			alert('You MUST specify the skill!');
			return false;
		}
	});
});
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1", {validateOn:["change", "blur"]});
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {validateOn:["blur", "change"], isRequired:false});
var spryselect2 = new Spry.Widget.ValidationSelect("spryselect2", {validateOn:["change", "blur"]});
</script>
</body>
</html>