<?php require_once('_inc_config.php'); ?>
<?php require_once('Connections/fer.php'); ?>
<?php require_once('_inc_Functions.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_fer, $fer);

//get all applicants
$applicants_query = "SELECT id FROM applicants";
$applicants = mysql_query($applicants_query, $fer) or die(mysql_error());
$row_applicants = mysql_fetch_assoc($applicants);
//loop tru and update status
do {
  CVComplete($row_applicants['id']);
} while($row_applicants = mysql_fetch_assoc($applicants));

die("Done");
