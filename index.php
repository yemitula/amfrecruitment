<?php error_reporting(0)?>
<?php require_once "_inc_setSession.php";?>
<?php $thisPage = basename($_SERVER['PHP_SELF']);?>
<?php require_once '_inc_config.php';?>
<?php require_once 'Connections/fer.php';?>
<?php include '_inc_Functions.php';?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}

$maxRows_vacancy = 6;
$pageNum_vacancy = 0;
if (isset($_GET['pageNum_vacancy'])) {
	$pageNum_vacancy = $_GET['pageNum_vacancy'];
}
$startRow_vacancy = $pageNum_vacancy * $maxRows_vacancy;

mysql_select_db($database_fer, $fer);
$query_vacancy = "SELECT v.`id`, `title`, `employer_id`, `summary`, `longDescription`, `techReqs`, `eduQual`, `behavReqs`, `jobType`, `jobCategory_id`, `staffLevel`, `datePosted`, `dateOpening`, state, country, e.logo, 'longDescription', `dateClosing`, `status`, `alertStatus`, e.id AS eID, e.companyName  FROM vacancies v LEFT JOIN  employers e ON (v.employer_id=e.id) WHERE ( (v.dateOpening IS NULL AND v.dateClosing IS NULL) OR (v.dateClosing >= NOW() AND v.dateOpening IS NULL) OR (v.dateClosing >= NOW() AND v.dateOpening <= NOW()) ) AND v.published IS NOT NULL";
$query_limit_vacancy = sprintf("%s LIMIT %d, %d", $query_vacancy, $startRow_vacancy, $maxRows_vacancy);
$vacancy = mysql_query($query_limit_vacancy, $fer) or die(mysql_error());
$row_vacancy = mysql_fetch_assoc($vacancy);
//var_dump($row_vacancy);
//die;

if (isset($_GET['totalRows_vacancy'])) {
	$totalRows_vacancy = $_GET['totalRows_vacancy'];
} else {
	$all_vacancy = mysql_query($query_vacancy);
	$totalRows_vacancy = mysql_num_rows($all_vacancy);
}
$totalPages_vacancy = ceil($totalRows_vacancy / $maxRows_vacancy) - 1;

//category
mysql_select_db($database_fer, $fer);
$query_categories = "SELECT *, (SELECT COUNT(*) FROM vacancies v WHERE v.jobCategory_id = j.id_cat) AS jobCount FROM job_categories j LIMIT 0,10";
$categories = mysql_query($query_categories, $fer) or die(mysql_error());
$row_categories = mysql_fetch_assoc($categories);
$totalRows_categories = mysql_num_rows($categories);

// category record-set 2
mysql_select_db($database_fer, $fer);
$query_catergory2 = "SELECT *, (SELECT COUNT(*) FROM vacancies v WHERE v.jobCategory_id = j.id_cat) AS jobCount FROM job_categories j LIMIT 10,20";
$catergory2 = mysql_query($query_catergory2, $fer) or die(mysql_error());
$row_catergory2 = mysql_fetch_assoc($catergory2);
$totalRows_catergory2 = mysql_num_rows($catergory2);
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="shortcut icon" href="favicon.png" />

	<title>Welcome | <?php echo $config['shortname']?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">

	<!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include '-inc-header-top.php';?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include '-inc-header-nav.php';?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-search-bar">
			<?php include '-inc-header-search.php';?>
		</div> <!-- end .header-search-bar -->

		<div class="header-banner">
			<div class="flexslider header-slider">
				<ul class="slides">
					<li>
						<img src="img/transparent.png" alt="">
						<div data-image="img/content/sliders/slide1.jpg"></div>
					</li>

					<li>
						<img src="img/transparent.png" alt="">
						<div data-image="img/content/slide-2.png"></div>
					</li>

					<li>
						<img src="img/transparent.png" alt="">
						<div data-image="img/content/sliders/slide2.jpg"></div>
					</li>

					<li>
						<img src="img/transparent.png" alt="">
						<div data-image="img/content/sliders/slide4.jpg"></div>
					</li>
				</ul>
			</div>
		</div> <!-- end .header-banner -->
	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 page-content">

					<!-- latest jobs -->
					<div class="title-lines">
						<h3>Latest Jobs</h3>
					</div>

					<div class="latest-jobs-section white-container">
						<div class="flexslider clearfix">
							<ul class="slides">
							<?php do {
	?>
								<li>
									<div class="image">
										<?php if (isset($row_vacancy['logo'])) {?>
        <img src="images/employerLogos/<?php echo $row_vacancy['logo'];?>" style="max-height:150px" alt="" />
										<?php } else {?>
        <img src="images/employerLogos/logo.png" style="max-height:150px" alt="" />
                                        <?php }
	?>
										<a href="a_vacancyDetails.php?id=<?php echo $row_vacancy['id'];?>" class="btn btn-default fa fa-search"></a>
										<?php //<a href="#" class="btn btn-default fa fa-link"></a> ?>
									</div>

									<div class="content">
										<h6><?php echo $row_vacancy['title'];?> at <?php echo $row_vacancy['companyName'];?></h6>
										<span class="location"><?php echo $row_vacancy['state'];?>, <?php echo $row_vacancy['country'];?></span>
										<p><?php echo $row_vacancy['summary'];?>. <a href="a_vacancyDetails.php?id=<?php echo $row_vacancy['id'];?>" class="read-more">More Details</a>
                                        </p>
									</div>
								</li>
  <?php } while ($row_vacancy = mysql_fetch_assoc($vacancy));?>
							</ul>
						</div>
					</div> <!-- end .latest-jobs-section --><!-- end .our-partners-section --><!-- end .pricing-tables -->

					<div class="title-lines">
						<h3 class="mt0">Find a Job By Category</h3>
					</div>

					<div class="find-job-tabs responsive-tabs">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#find-job-tabs-industry">Category</a></li>
							<!-- <li><a href="#find-job-tabs-role">Level</a></li> -->
							<?php //<li><a href="#find-job-tabs-country">Location</a></li> ?>
						</ul>

						<div class="tab-content">
							<div class="tab-pane active" id="find-job-tabs-industry">
								<div class="p30">
									<p>Click on one of the categories below to filter jobs by that category...</p>

									<div class="row">
										<div class="col-sm-6">
											<ul class="filter-list">
                                              <?php do {?>
                                              <li><a href="a_vacancies.php?keyword=&category=<?php echo $row_categories['id_cat'];?>"><?php echo $row_categories['cat_name'];?> <span>(<?php echo $row_categories['jobCount'];?>)</span></a></li>
                                                <?php } while ($row_categories = mysql_fetch_assoc($categories));?>
                                            </ul>
										</div>

										<div class="col-sm-6">
											<ul class="filter-list">
												<?php do {?>
											    <li><a href="a_vacancies.php?keyword=&category=<?php echo $row_catergory2['id_cat'];?>"><?php echo $row_catergory2['cat_name'];?> <span>(<?php echo $row_catergory2['jobCount'];?>)</span></a></li>
												  <?php } while ($row_catergory2 = mysql_fetch_assoc($catergory2));?>
											</ul>
										</div>
									</div>
								</div>
							</div>

							<!-- <div class="tab-pane" id="find-job-tabs-role">
								<div class="p30">
									<p>Click on one of the Levels below to filter jobs by that level...</p>
									<div class="row">
										<div class="col-sm-6">
											<ul class="filter-list">
												<li><a href="a_vacancies.php?keyword=&staffLevel=">All levels <span>()</span></a></li>
												<li><a href="a_vacancies.php?keyword=&staffLevel=HODs">HODs <span>()</span></a></li>
												<li><a href="a_vacancies.php?keyword=&staffLevel=HOUs">HOUs <span>()</span></a></li>
												<li><a href="a_vacancies.php?keyword=&staffLevel=Senior+Officers">Senior Officers <span>(334)</span></a></li>
												<li><a href="a_vacancies.php?keyword=&staffLevel=Officers">Officers <span>()</span></a></li>
                                                <li><a href="a_vacancies.php?keyword=&staffLevel=Assistants">Assistants <span>()</span></a></li>
											</ul>
										</div>

                                        <div class="col-sm-6">
											<ul class="filter-list">
												<li><a href="a_vacancies.php?keyword=&staffLevel=Deputy+Manager">Deputy Manager <span>()</span></a></li>
												<li><a href="a_vacancies.php?keyword=&staffLevel=Executive+Trainee">Executive Trainee <span>()</span></a></li>
												<li><a href="a_vacancies.php?keyword=&staffLevel=Banking+Officer">Banking Officer <span>()</span></a></li>
												<li><a href="a_vacancies.php?keyword=&staffLevel=Assistant+Banking+Officer">Assistant Banking Officer <span>()</span></a></li>
												<li><a href="a_vacancies.php?keyword=&staffLevel=Senior+Banking+Officer">Senior Banking Officer <span>()</span></a></li>
                                                <li><a href="a_vacancies.php?keyword=&staffLevel=Assistant+Manager">Assistant Manager <span>()</span></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div> -->

							<?php /*?><div class="tab-pane" id="find-job-tabs-country">
<div class="row p30">
<div class="col-sm-6">
<ul class="country-list">
<li><a href="#"><img src="img/flag-icons/Ghana.png" alt=""> Ghana <span>(7)</span></a></li>
<li><a href="#"><img src="img/flag-icons/Nigeria.png" alt=""> Nigeria <span>(6)</span></a>
<ul class="filter-list">
<li><a href="#">Abuja</a></li>
<li><a href="#">Lagos</a></li>
<li><a href="#">Port-harcourt</a></li>
<li><a href="#">Warri</a></li>

</ul>
</li>
</ul>
</div>


</div>
</div><?php */?>
						</div>
					</div> <!-- end .find-job-tabs -->

				</div> <!-- end .page-content -->

				<div class="col-sm-4 page-sidebar">
					<aside>
						<div class="widget sidebar-widget white-container social-widget">
							<!-- <h5 class="widget-title">Share Us</h5> -->

						<div class="white-container">
							<div class="widget sidebar-widget">
								<h5 class="widget-title">Welcome to AMF Recruitment</h5>

								<div class="widget-content">
									This is the welcome message
								</div>
							</div>
							
						</div>

					  </div>

					  <div class="widget sidebar-widget white-container social-widget">
							<!-- <h5 class="widget-title">Share Us</h5> -->

						<div class="white-container">

							<div class="widget sidebar-widget">
								<h5 class="widget-title">Employment Opportunity</h5>

								<div class="widget-content">
									We provide equal employment opportunity to all without regard to race, color, religion, sex, national origin, age or disabilty.
								</div>
							</div>

							<div class="widget sidebar-widget">
								<h5 class="widget-title">The Poll</h5>

								<div class="widget-content">
									<script type="text/javascript" charset="utf-8" src="http://static.polldaddy.com/p/9013839.js"></script>
<noscript><a href="http://polldaddy.com/poll/9013839/">Are you satisfied with your current employer?</a></noscript>
								</div>
							</div>
						</div>

					  </div>



					</aside>
				</div> <!-- end .page-sidebar -->
			</div>
		</div> <!-- end .container -->

<!-- 		<div class="success-stories-section">
			<div class="container">
				<h5 class="mt10">Success Stories</h5>

				<div>
					<div class="flexslider">
						<ul class="slides">
							<li>
								<a href="#">
									<img class="thumb" src="img/content/face-0.png" alt="">
								</a>
							</li>

							<li>
								<a href="#">
									<img class="thumb" src="img/content/face-1.png" alt="">
								</a>
							</li>

							<li>
								<a href="#">
									<img class="thumb" src="img/content/face-2.png" alt="">
								</a>
							</li>

							<li>
								<a href="#">
									<img class="thumb" src="img/content/face-3.png" alt="">
								</a>
							</li>

							<li>
								<a href="#">
									<img class="thumb" src="img/content/face-4.png" alt="">
								</a>
							</li>

							<li>
								<a href="#">
									<img class="thumb" src="img/content/face-5.png" alt="">
								</a>
							</li>

							<li>
								<a href="#">
									<img class="thumb" src="img/content/face-6.png" alt="">
								</a>
							</li>

							<li>
								<a href="#">
									<img class="thumb" src="img/content/face-7.png" alt="">
								</a>
							</li>

							<li>
								<a href="#">
									<img class="thumb" src="img/content/face-8.png" alt="">
								</a>
							</li>

							<li>
								<a href="#">
									<img class="thumb" src="img/content/face-9.png" alt="">
								</a>
							</li>

							<li>
								<a href="#">
									<img class="thumb" src="img/content/face-3.png" alt="">
								</a>
							</li>

							<li>
								<a href="#">
									<img class="thumb" src="img/content/face-4.png" alt="">
								</a>
							</li>

							<li>
								<a href="#">
									<img class="thumb" src="img/content/face-5.png" alt="">
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div> -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include '-inc-footer-top.php';?>

		<div class="copyright">
			<?php include '-inc-footer-bottom.php';?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55673e31623bbb6d" async="async"></script>

<script type="text/javascript">
$(document).ready(function() {
	$("#staffLevel").change(function(e) {
        $("#findAJobForm").submit();
    });
});
</script>
</body>
</html>
<?php
mysql_free_result($categories);

mysql_free_result($catergory2);
?>
