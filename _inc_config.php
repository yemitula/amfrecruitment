<?php 
/* This file specifies the configuration constants for the recruitment application */
/* please modify values accordingly */

$config = array();

//Organization's Full name e.g. Alphamead Facilities & Management Services Ltd
$config['fullname'] = 'Alpha Mead Group';

//Organization's Short name e.g. AMF
$config['shortname'] = 'Alpha Mead';

//Sender Email - email that will appear as sender email in outgoing notifications
//This email must exist on the server
$config['sender'] = 'recruitment@amfacilities.com';

//Notification Email - email that will receive admin notifications
$config['notify'] = 'recruitment@amfacilities.com';

//Site Root URL - e.g. http://amfacilities.com/recruitment.
//This is the URL for the website.
//***MUST END WITH A '/'
$config['url'] = 'http://amfacilities.com/recruitment/';

//About Page URL 
//URL of the organization's about page
$config['about'] = 'http://alphamead.com';

//Blog Address
$config['blog'] = 'http://fmroundtable.com/';

//Facebook Address
$config['facebook'] = 'http://www.facebook.com/amfacilities';

//Twitter Address
$config['twitter'] = 'http://twitter.com/alphamead';

//LinkedIn Address
$config['linkedin'] = 'http://www.linkedin.com/company/alpha-mead-facilities-and-management-services-ltd';

$config['summary'] =  "Alpha Mead group is a Total Real Estate Solutions Company established to provide robust business support services to local and international Real Estate investors or ownners.";
?>