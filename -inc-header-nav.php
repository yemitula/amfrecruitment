<div class="container">

				<!-- Logo -->
				<div class="css-table logo">
					<div class="css-table-cell">
						<a href="index.php">
							<img src="img/header-logo.png" alt="">
						</a> <!-- end .logo -->
					</div>
				</div>

				<!-- Mobile Menu Toggle -->
				<a href="#" id="mobile-menu-toggle"><span></span></a>

				<!-- Primary Nav -->
				<nav>
					<ul class="primary-nav">
						<li class="<?php if ($thispage == 'index.php' || $thispage == 'register.php' || $thispage == 'login.php') echo "active" ?>">
							<a href="index.php">Home</a>
						</li>
						<li class="">
							<a href="a_vacancies.php">Vacancies</a>
						</li>
						<li class="">
							<a href="http://alphamead.com" target="_blank">About <?php echo $config['shortname'] ?></a>
						</li>
						<li class="">
							<a href="#">Useful Tips</a>
						</li>
						<?php if(!isset($_SESSION['FER_User'])) { ?>
                        <li><a href="register.php">Register</a></li>
                        <?php } else { ?>
                        <li><a href="a_profile.php">My Profile</a></li>
                        <?php } ?>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</nav>
			</div>