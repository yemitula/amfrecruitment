<?php require_once("_inc_checkSession.php"); ?>
<?php require_once("_inc_applicantsOnly.php"); ?>
<?php require_once('_inc_config.php'); ?>
<?php require_once('Connections/fer.php'); ?>
<?php include('_inc_Functions.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

mysql_select_db($database_fer, $fer);
$query_vacancies = "SELECT v.id, v.title, v.datePosted, e.companyName FROM vacancies v LEFT JOIN  employers e ON (v.employer_id=e.id)  WHERE (v.dateClosing >= NOW() AND v.dateOpening IS NULL) OR (v.dateClosing >= NOW() AND v.dateOpening <= NOW()) ORDER BY v.datePosted DESC LIMIT 3"; // AND v.dateOpening <= NOW() 
$vacancies = mysql_query($query_vacancies, $fer) or die(mysql_error());
$row_vacancies = mysql_fetch_assoc($vacancies);
$totalRows_vacancies = mysql_num_rows($vacancies);

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "formChangePass")) {

	mysql_select_db($database_fer, $fer);
	
	//check if current password supplied is correct
	$currentPass = $_POST['currentPass'];
	$appID = $_POST['appID'];
	$checkSQL = "SELECT password FROM applicants WHERE id='$appID' AND password = '$currentPass'";
	$checkResult = mysql_query($checkSQL, $fer) or die(mysql_error());
	$passwordCorrect = mysql_num_rows($checkResult);
	
	if($passwordCorrect) { 
		$updateSQL = sprintf("UPDATE applicants SET password=%s WHERE id=%s",
						   GetSQLValueString($_POST['newPass'], "text"),
						   GetSQLValueString($_POST['appID'], "int"));
		
		$Result1 = mysql_query($updateSQL, $fer) or die(mysql_error());
		
		$updateGoTo = "a_profile.php?msg=".urlencode("Password Successfully Changed!");
		if (isset($_SERVER['QUERY_STRING'])) {
		$updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
		$updateGoTo .= $_SERVER['QUERY_STRING'];
		}
		header(sprintf("Location: %s", $updateGoTo));
	} else {
		header("Location: a_changePass.php?error=The Current Password you supplied is INCORRECT!");
		exit;
	}
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<link rel="shortcut icon" href="favicon.png" />
    
	<title>Change Password -<?php echo $FER_User['firstname'] ?><?php echo $FER_User['surname'] ?>| <?php echo $config['shortname'] ?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">

	<!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include('-inc-header-top.php'); ?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include('-inc-header-nav.php'); ?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
		    <?php include('-inc-applicant-top.php'); ?>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
		    <div class="row">
		        <div class="col-sm-4 page-sidebar">
		            <?php include('-inc-applicant-side.php'); ?>
	            </div>
		        <!-- end .page-sidebar -->
		        <div class="col-sm-8 page-content">
		            <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>
		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>
	                </div>-->
		            <form id="formChangePass" name="formChangePass" method="POST" action="<?php echo $editFormAction; ?>">
	                    <div class="white-container sign-up-form">
	                        <div>
	                            <h3>Change Password</h3>
	                            <section>
	                                <?php if (isset($_GET['error'])) { ?>
		                                <div class="alert alert-error">
		                                    <h6>Oops!</h6>
		                                    <p><?php echo $_GET['error'] ?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php } ?>
	                                <?php if (isset($_GET['msg'])) { ?>
		                                <div class="alert alert-success">
		                                    <h6>Wow!</h6>
		                                    <p><?php echo $_GET['msg'] ?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php } ?>
	                                <div class="row">
	                                    <div class="col-sm-6"><span id="sprytextfield2">
	                                        <label for="currentPass">Current Password</label>
	                                        <input name="currentPass" type="password" class="form-control" id="currentPass" value="" />
	                                        <span class="textfieldRequiredMsg">A value is required.</span></span></div>
</div>
                                    <div class="row">
                                        <div class="col-sm-6"><span id="sprytextfield">
                                            <label for="newPass">New Password</label>
                                            <input name="newPass" type="password" class="form-control" id="newPass" value="" />
                                            <span class="textfieldRequiredMsg">A value is required.</span></span></div>
                                        <div class="col-sm-6">
                                            <label for="newPass2">Confirm New Password</label>
                                            <span id="spryconfirm1">
                                            <input name="newPass2" type="password" class="form-control" id="newPass2" value="" />
                                            <span class="confirmRequiredMsg">A value is required.</span><span class="confirmInvalidMsg">The values don't match.</span></span></div>
                                    </div>
                                </section>
</div>
                            <hr class="mt60">
                            <div class="clearfix">
                                <input name="register" type="submit" class="btn btn-default btn-large pull-right" id="register" value="Update" />
                            </div>
                            <input type="hidden" name="MM_update" value="formChangePass" />
                            <input name="appID" type="hidden" id="appID" value="<?php echo $FER_User['id']; ?>" />
	                    </div>
	                </form>
                    
		        </div>
		        <!-- end .page-content -->
	        </div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include('-inc-footer-top.php'); ?>

		<div class="copyright">
			<?php include('-inc-footer-bottom.php'); ?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript">
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "none");
var sprytextfield = new Spry.Widget.ValidationTextField("sprytextfield", "none");
var spryconfirm1 = new Spry.Widget.ValidationConfirm("spryconfirm1", "newPass");
</script>
</body>
</html>