<?php require_once("_inc_checkSession.php"); ?>
<?php require_once("_inc_applicantsOnly.php"); ?>
<?php $thisPage = basename( $_SERVER['PHP_SELF'] ); ?>
<?php require_once('_inc_config.php'); ?>
<?php require_once('Connections/fer.php'); ?>
<?php include('_inc_Functions.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "AddForm")) {
  
  //var_dump($_POST);
  $updateSQL = sprintf("UPDATE tertiary SET institution=%s, course=%s, qualification=%s, `class`=%s, Course_Category=%s, uni_country=%s, `date`=%s WHERE id=%s",
                       GetSQLValueString(($_POST['otherInst'])? $_POST['otherInst'] : $_POST['institution'], "text"),
                       GetSQLValueString($_POST['course'], "text"),
                       GetSQLValueString(($_POST['otherQual'])? $_POST['otherQual'] : $_POST['qualification'], "text"),
                       GetSQLValueString(($_POST['otherClass'])? $_POST['otherClass'] : $_POST['class'], "text"),
                       GetSQLValueString($_POST['Course_Category'], "text"),
                       GetSQLValueString($_POST['uni_country'], "text"),
                       GetSQLValueString($_POST['year'].'-'.$_POST['month'].'-01', "date"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($updateSQL, $fer) or die(mysql_error());

  $updateGoTo = "a_educational.php?msg=".urlencode("Tertiary Institution Successfully Edited!");
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}


$colname_sections = "-1";
if (isset($_SESSION['FER_User']['id'])) {
  $colname_sections = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_sections = sprintf("SELECT * FROM sectionstatus WHERE applicant_id = %s", GetSQLValueString($colname_sections, "int"));
$sections = mysql_query($query_sections, $fer) or die(mysql_error());
$row_sections = mysql_fetch_assoc($sections);
$totalRows_sections = mysql_num_rows($sections);

$colname_tertiary = "-1";
if (isset($_GET['id'])) {
  $colname_tertiary = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_tertiary = sprintf("SELECT * FROM tertiary WHERE id = %s", GetSQLValueString($colname_tertiary, "int"));
$tertiary = mysql_query($query_tertiary, $fer) or die(mysql_error());
$row_tertiary = mysql_fetch_assoc($tertiary);
$totalRows_tertiary = mysql_num_rows($tertiary);

mysql_select_db($database_fer, $fer);
$query_countries = "SELECT * FROM countries";
$countries = mysql_query($query_countries, $fer) or die(mysql_error());
$row_countries = mysql_fetch_assoc($countries);
$totalRows_countries = mysql_num_rows($countries);

if($totalRows_sections)
{
$cvStatus = 1;
	foreach($row_sections as $value)
	{
		if($value == 0)
			$cvStatus = 0;
	}
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<link rel="shortcut icon" href="favicon.png" />
    
	<title>Educational/Academic Qualifications - <?php echo $FER_User['firstname'] ?> <?php echo $FER_User['surname'] ?>| <?php echo $config['shortname'] ?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">

	<!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include('-inc-header-top.php'); ?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include('-inc-header-nav.php'); ?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
		    <?php include('-inc-applicant-top.php'); ?>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
		    <div class="row">
		        <div class="col-sm-4 page-sidebar">
		            <?php include('-inc-applicant-side.php'); ?>
	            </div>
		        <!-- end .page-sidebar -->
		        <div class="col-sm-8 page-content">
		            <h3>
		                <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>
		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>
	                </div>-->
		                Educational/Academic Qualifications
	                </h3>
		            <form id="formAddEduc" name="formAddEduc" method="post" action="">
	                    <div class="white-container sign-up-form">
	                        <div>
	                            <h5>Edit TERTIARY EDUCATION</h5>
	                            <section>
	                                <?php if (isset($_GET['error'])) { ?>
		                                <div class="alert alert-error">
		                                    <h6>Oops!</h6>
		                                    <p><?php echo $_GET['error'] ?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php } ?>
	                                <?php if (isset($_GET['msg'])) { ?>
		                                <div class="alert alert-success">
		                                    <h6>Wow!</h6>
		                                    <p><?php echo $_GET['msg'] ?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php } ?>
                                    
                                    <div class="row tetArea">
                                      <div class="col-sm-9">
                                          <label for="course">Country Of Study</label>
                                          <select name="uni_country" id="uni_country" >
                                            <option value="" <?php if (!(strcmp("", $row_tertiary['uni_country']))) {echo "selected=\"selected\"";} ?>> --Select Country Of Study-- </option>
                                            <?php
do {  
?>
                                            <option value="<?php echo $row_countries['Country']?>"<?php if (!(strcmp($row_countries['Country'], $row_tertiary['uni_country']))) {echo "selected=\"selected\"";} ?>><?php echo $row_countries['Country']?></option>
                                            <?php
} while ($row_countries = mysql_fetch_assoc($countries));
  $rows = mysql_num_rows($countries);
  if($rows > 0) {
      mysql_data_seek($countries, 0);
	  $row_countries = mysql_fetch_assoc($countries);
  }
?>
                                        </select>
                                      </div>
                                    </div>

                                     <div class="row tetArea">
                                      <div class="col-sm-9">
                                          <label for="institution">Institution</label>
<!--                                          <select name="institution" class="" id="institution">
                          <option value="0" selected="selected">Select One...</option>
                          <option style=" font-weight:bold">---- Universities -----</option>-->
                          <?php //include('_inc_universities.html'); ?>
                          <!-- <option style="font-weight:bold">---- Polytechnics -----</option> -->
                          <?php //include('_inc_polytechnics.html'); ?>
                          <!-- <option value="Other">Others (Not Listed)</option>
                        </select>
                                  <br>
                                            <span id="otherInstArea"><input name="otherInst" type="text" id="otherInst" placeholder="please specify" /></span> -->
                                          <input name="institution" type="text" class="" id="" value="<?php echo $row_tertiary['institution']; ?>"  placeholder="Institution Attended" />
                                      </div>
                                    </div>
                                    
                                    <div class="row tetArea">
                                      <div class="col-sm-9">
                                          <label for="course">Course Category</label>
                                          <select name="Course_Category" id="Course_Category">
                                            <option value="" <?php if (!(strcmp("", $row_tertiary['Course_Category']))) {echo "selected=\"selected\"";} ?>>--Select Course category--</option>
                                            <option value="ARtS" <?php if (!(strcmp("ARtS", $row_tertiary['Course_Category']))) {echo "selected=\"selected\"";} ?>>ARtS</option>
                                            <option value="BASIC MEDICAL SCIENCES" <?php if (!(strcmp("BASIC MEDICAL SCIENCES", $row_tertiary['Course_Category']))) {echo "selected=\"selected\"";} ?>>BASIC MEDICAL SCIENCES</option>
                                            <option value="BUSINESS ADMINISTRATION" <?php if (!(strcmp("BUSINESS ADMINISTRATION", $row_tertiary['Course_Category']))) {echo "selected=\"selected\"";} ?>>BUSINESS ADMINISTRATION</option>
                                            <option value="CLINICAL SCIENCES" <?php if (!(strcmp("CLINICAL SCIENCES", $row_tertiary['Course_Category']))) {echo "selected=\"selected\"";} ?>>CLINICAL SCIENCES</option>
                                            <option value="DENTAL SCIENCES" <?php if (!(strcmp("DENTAL SCIENCES", $row_tertiary['Course_Category']))) {echo "selected=\"selected\"";} ?>>DENTAL SCIENCES</option>
                                            <option value="EDUCATION" <?php if (!(strcmp("EDUCATION", $row_tertiary['Course_Category']))) {echo "selected=\"selected\"";} ?>>EDUCATION</option>
                                            <option value="ENGINEERING" <?php if (!(strcmp("ENGINEERING", $row_tertiary['Course_Category']))) {echo "selected=\"selected\"";} ?>>ENGINEERING</option>
<option value="ENVIRONMENTAL SCIENCES" <?php if (!(strcmp("ENVIRONMENTAL SCIENCES", $row_tertiary['Course_Category']))) {echo "selected=\"selected\"";} ?>>ENVIRONMENTAL SCIENCES</option>
                                            <option value="LAW" <?php if (!(strcmp("LAW", $row_tertiary['Course_Category']))) {echo "selected=\"selected\"";} ?>>LAW</option>
                                            <option value="PHARMACY" <?php if (!(strcmp("PHARMACY", $row_tertiary['Course_Category']))) {echo "selected=\"selected\"";} ?>>PHARMACY</option>
                                            <option value="SOCIAL SCIENCES" <?php if (!(strcmp("SOCIAL SCIENCES", $row_tertiary['Course_Category']))) {echo "selected=\"selected\"";} ?>>SOCIAL SCIENCES</option>
                                            <option value="SCIENCES" <?php if (!(strcmp("SCIENCES", $row_tertiary['Course_Category']))) {echo "selected=\"selected\"";} ?>>SCIENCES</option>
                                          </select>
                                      </div>
                                    </div>
                                    
                                    <div class="row tetArea">
	                                    <div class="col-sm-9">
	                                        <label for="course">Course of Study</label>
	                                        <input name="course" type="text" class="" id="course"  placeholder="In Full e.g. Industrial and Labour Relations" value="<?php echo $row_tertiary['course']; ?>" />
	                                    </div>
                                    </div>
                                    
                                    <div class="row tetArea">
	                                    <div class="col-sm-6">
	                                        <label for="qualification">Qualification Obtained</label>
	                                        <select name="qualification" id="qualification">
                        <option value="0">Select...</option>
                        <option value="OND">OND</option>
                        <option value="HND">HND</option>
                        <option value="BSc">BSc</option>
                        <option value="BA">BA</option>
                        <option value="BArch">BArch</option>
                        <option value="Beng">BEng</option>
                        <option value="BEd">BEd</option>
                        <option value="BSc/Ed">BSc/Ed</option>
                        <option value="BA/Ed">BA/Ed</option>
                        <option value="LLB">LLB</option>
                        <option value="MA">MA</option>
                        <option value="MSc">MSc</option>
                        <option value="MPhil">MPhil</option>
                        <option value="LLM">LLM</option>
                        <option value="MBA">MBA</option>
                        <option value="PHD">PHD</option>
                        <option value="Other">Other</option>
                      </select>
	                                    </div>
                                        
                                        <div class="col-sm-6" id="otherQualArea">
	                                        <label for="otherQual">Specify Qualification</label>
	                                       <input type="text" name="otherQual" id="otherQual" />
	                                    </div>
                                    </div>
                                    
                                    <div class="row tetArea">
	                                    <div class="col-sm-6">
	                                        <label for="class">Class</label>
	                                        <select name="class" id="class">
                        <option value="0" selected="selected">Select...</option>
                        <option value="First Class">First Class</option>
                        <option value="Second Class Upper">Second Class Upper</option>
                        <option value="Second Class Lower">Second Class Lower</option>
                        <option value="Third Class">Third Class</option>
                        <option value="Pass">Pass</option>
                        <option value="Distinction">Distinction</option>
                        <option value="Upper Credit">Upper Credit</option>
                        <option value="Lower Credit">Lower Credit</option>
                        <option value="In View/Awaiting Result">In View/Awaiting Result</option>
                        <option value="Other">Other</option>
                      </select>
	                                    </div>
                                        
                                        <div class="col-sm-6" id="otherClassArea">
	                                        <label for="otherClass">Specify Class</label>
	                                       <input type="text" name="otherClass" id="otherClass" />
	                                    </div>
                                    </div>
                                    
                                    <div class="row dateArea">
	                                    <div class="col-sm-6">
                                            <label for="cgpaScore">Date Obtained </label> <br>
	                                        <select name="month" id="month" style="width:150px; display:inline">
                          <option value="">Month</option>
                          <?php 
							for($m=1;$m<=12;$m++) {
							?>
                          <option value="<?php echo $m ?>" <?php if (date("m",strtotime($row_tertiary['date'])) == $m) echo "selected='selected'" ?>><?php echo date("M", mktime(0, 0, 0, $m, 10)); ?></option>
                          <?php } ?>
                        </select>
                                            &nbsp; 
	                                       	<select name="year" id="year" style="width:150px; display:inline">
                            <option value="" selected="selected">Year</option>
                            <?php 
							for($y=date("Y");$y>=1940;$y--) {
							?>
                            <option value="<?php echo $y ?>" <?php if (date("Y",strtotime($row_tertiary['date'])) == $y) echo "selected='selected'" ?>><?php echo $y ?></option>
                            <?php } ?>
                          </select>
                                       </div>
                                    </div>
                                    
                                    
                                </section>
</div>
                            <hr class="mt60">
                            <div class="clearfix">
                                <a href="a_educational.php" class="btn btn-red">Cancel</a>
                                &nbsp;
                                <input name="submit" type="submit" class="btn btn-default" id="submit" value="Update" />

                            </div>
                           <input name="MM_insert" type="hidden" id="MM_insert" value="AddForm" />
                           <input type="hidden" name="MM_update" value="AddForm" />
                            <input name="id" type="hidden" id="id" value="<?php echo $row_tertiary['id']; ?>" />
	                    </div>
	                </form>
                    
		        </div>
		        <!-- end .page-content -->
	        </div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include('-inc-footer-top.php'); ?>

		<div class="copyright">
			<?php include('-inc-footer-bottom.php'); ?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
<script src="js/ValidateDel.js"></script>
<script type="text/javascript">
function OptionExists(selectID, key) {
	//alert('invoke');
	var seen = false;
	var selector = '#' + selectID + ' option';
	//alert(selector);
	$(selector).each(function(){
		//alert(this.value);
		if (this.value == key) {
			seen = true;
			return false;
		}
	});
	//alert(seen);
	return seen;
}

$(document).ready(function() {	
$('#otherInstArea').hide();
$('#otherQualArea').hide();
$('#otherClassArea').hide();
//alert('jquery');
if(OptionExists('class','<?php echo $row_tertiary['class'] ?>')) {
	$("#class").val('<?php echo $row_tertiary['class'] ?>');
} else {
	$("#class").val('Other');
	$("#otherClass").val('<?php echo $row_tertiary['class'] ?>');
	$('#otherClassArea').show();
}
if(OptionExists('institution','<?php echo $row_tertiary['institution'] ?>')) {
	$("#institution").val('<?php echo $row_tertiary['institution'] ?>');
} else {
	$("#institution").val('Other');
	$("#otherInst").val('<?php echo $row_tertiary['institution'] ?>');
	$('#otherInstArea').show();
}
if(OptionExists('qualification','<?php echo $row_tertiary['qualification'] ?>')) {
	$("#qualification").val('<?php echo $row_tertiary['qualification'] ?>');
} else {
	$("#qualification").val('Other');
	$("#otherQual").val('<?php echo $row_tertiary['qualification'] ?>');
	$('#otherQualArea').show();
}
	
	$('#institution').change(function() {
		if($('#institution').val() == 'Other')
			$('#otherInstArea').show();
		else
		{	$('#otherInstArea').hide();
			$('#otherInst').val('');
		}
	});
	
	$('#qualification').change(function() {
		if($('#qualification').val() == 'Other')
			$('#otherQualArea').show();
		else
		{	$('#otherQualArea').hide();
			$('#otherQual').val('');
		}
	});

	$('#class').change(function() {
		if($('#class').val() == 'Other')
			$('#otherClassArea').show();
		else
		{	$('#otherClassArea').hide();
			$('#otherClass').val('');
		}
	});
	
	$('#formAddEduc').submit(function() 
	{
		//alert('submit');
		if($('#month').val() == ''  || $('#year').val() == '')
		{
			alert('All fields are required!');
			return false;
		}
		if($('#institution').val() == 'Other' && $('#otherInst').val() == '')
		{
			alert('You MUST specify the OTHER institution!');
			return false;
		}
		if($('#institution').val() == '0' || $('#qualification').val() == '0' || $('#class').val() == '0')
		{
			alert('Please Select the NECESSARY VALUES (Insitution/Qualification/Class)!!!');
			return false;
		}		
		if($('#qualification').val() == 'Other' && $('#otherQual').val() == '')
		{
			alert('You MUST specify the OTHER qualification!');
			return false;
		}
		if($('#class').val() == 'Other' && $('#otherClass').val() == '')
		{
			alert('You MUST specify the OTHER class!');
			return false;
		}
	});
});
</script>
</body>
</html>