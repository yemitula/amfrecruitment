<?php
	if (!isset($_SESSION)) {
	  session_start();
	}
	//check page
	if(isset($_SESSION['FER_Usertype']) && $_SESSION['FER_Usertype'] == 'applicant')
	{
		//continue
	}
	else
	{
		//redirect to applicant login page
		$thisPage = $_SERVER['PHP_SELF'];
		header("Location: a_login.php?error=Please Login as an applicant to continue&srcpage=$thisPage");
		exit;
	}
?>