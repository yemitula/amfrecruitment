<aside>
		                <div class="widget sidebar-widget white-container candidates-single-widget">
		                    <div class="widget-content">
		                        
                                
                                <!--CV Progress-->
                                <h4>Your CV is <u><?php echo $percentage = getPercentComplete() ?>%</u> Complete</h4>
                                <div class="progress-circle" data-progress="<?php echo $percentage ?>">
                                    
                                </div>
                                
		                        <h5 class="bottom-line">CV Sections</h5>
<a class="btn btn-primary btn-<?php if (SectionComplete('personal')) echo "default"; else echo "red"; ?> col-sm-12" style="text-align:left" href="a_personalDetails.php">Personal Details</a>                               
<a class="btn btn-primary btn-<?php if (SectionComplete('educational')) echo "default"; else echo "red"; ?> col-sm-12" style="text-align:left" href="a_educational.php">Educational Qualifications</a>                               
<a class="btn btn-primary btn-<?php if (SectionComplete('profcerts')) echo "default"; else echo "red"; ?> col-sm-12" style="text-align:left" href = "a_profCerts.php">Professional Certifications</a>                               
<a class="btn btn-primary btn-<?php if (SectionComplete('nysc')) echo "default"; else echo "red"; ?> col-sm-12" style="text-align:left" href = "a_nysc.php">NYSC</a>                               
<a class="btn btn-primary btn-<?php if (SectionComplete('workExp')) echo "default"; else echo "red"; ?> col-sm-12" style="text-align:left" href = "a_workExp.php">Work Experience</a>                               
<a class="btn btn-primary btn-<?php if (SectionComplete('areas')) echo "default"; else echo "red"; ?> col-sm-12" style="text-align:left" href = "a_areas.php">Areas of Interest</a>                               
<a class="btn btn-primary btn-<?php if (SectionComplete('skills')) echo "default"; else echo "red"; ?> col-sm-12" style="text-align:left" href = "a_skills.php">Skills</a>                               
<a class="btn btn-primary btn-<?php if (SectionComplete('uploadCV')) echo "default"; else echo "red"; ?> col-sm-12" style="text-align:left" href = "a_uploadCV.php">Uploaded CV</a>
							<div class="clearfix"></div>                               
	                        </div>
	                    </div>
	                </aside>