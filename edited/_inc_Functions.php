<?php 

//function SectionStatusExists
//check if section status entry exists in database for the currently logged in applicant
//returns true or false
function SectionStatusExists($appID) {
	global $fer, $database_fer;
	mysql_select_db($database_fer, $fer);
	$checkSQL = "SELECT * FROM sectionstatus WHERE applicant_id = '$appID'";
	$Result = mysql_query($checkSQL, $fer) or die(mysql_error());
	$statusExists = mysql_num_rows($Result);
	
	if($statusExists > 0) {
		return true;
	} else {
		return false;
	}
}

//function CreateSectionStatus
//creates a new section
function CreateSectionStatus($appID) {
	global $fer, $database_fer;
	mysql_select_db($database_fer, $fer);
	$createSectionSQL = "INSERT INTO sectionstatus (applicant_id) VALUES ('$appID')";
	$Result3 = mysql_query($createSectionSQL, $fer) or die(mysql_error());
	
	return true;
}

//function UpdateSectionStatus
//updates sections status for an applicant
function UpdateSectionStatus ($appID, $section, $value) {
	global $fer, $database_fer;
	mysql_select_db($database_fer, $fer);
	$querySections = "UPDATE sectionstatus SET $section = '$value' WHERE applicant_id = '$appID'";
	mysql_query($querySections, $fer) or die(mysql_error());
	
	return true;
}
//function testYear4DOB
//tests if year of birth is consistent with specified year parameter
function testYear4DOB($year)
{
	global $fer, $database_fer;
	mysql_select_db($database_fer, $fer);
	$applicant_id = $_SESSION['FER_User']['id'];
	$query_yob = "SELECT DATE_FORMAT(dob,'%Y') AS 'yob' FROM applicants WHERE id = '$applicant_id'";
	$yob = mysql_query($query_yob, $fer) or die(mysql_error());
	$row_yob = mysql_fetch_assoc($yob);
	$yob = $row_yob['yob'];
	$yob += 12;
	
	if($year > $yob)
		return true;
	else
		return false;
}

//function generates a 12-character random password
function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 12; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    
	return implode($pass); //turn the array into a string
}

//function updates the current user session
function UpdateUserSession() {
	global $fer, $database_fer;
	mysql_select_db($database_fer, $fer);
	$id = $_SESSION['FER_User']['id'];
	$type = $_SESSION['FER_Usertype'];
	$tableName = $type.'s';
	$userSQL = "SELECT * FROM $tableName WHERE id = '$id'";
	$user = mysql_query($userSQL, $fer) or die(mysql_error());
	$row_user = mysql_fetch_assoc($user);
	$_SESSION['FER_User'] = $row_user;
	
	return true;
}

//function counts number of applications submited for specified vacancy
function CountApplications($vacancyID) {
	global $fer, $database_fer;
	mysql_select_db($database_fer, $fer);
	$countSQL = "SELECT * FROM applications WHERE vacancy_id = '$vacancyID'";
	$countRS = 	mysql_query($countSQL, $fer) or die(mysql_error());
	$totalRows_count = mysql_num_rows($countRS);

	return $totalRows_count;
}

//function checks if CV of currently logged-in applicant is complete
function CVComplete($applicant_id = '') {
	global $fer, $database_fer;
	mysql_select_db($database_fer, $fer);
	$applicant_id = $applicant_id == '' ? $_SESSION['FER_User']['id'] : $applicant_id;
	$checkSQL = "SELECT * FROM sectionstatus WHERE '0' IN (personal,educational,profcerts,nysc,workExp,areas,skills,behavioural,uploadCV,photo)  AND applicant_id = '$applicant_id'";
	$checkRS = mysql_query($checkSQL, $fer) or die(mysql_error());
	$foundIncomplete = mysql_num_rows($checkRS);

	if($foundIncomplete)
		return false;
	else 
		return true;
}

//function checks if a specified section of the currently logged-in applicant is complete
function SectionComplete($section, $applicant_id = '') {
	global $fer, $database_fer;
	mysql_select_db($database_fer, $fer);
	$applicant_id = $applicant_id == '' ? $_SESSION['FER_User']['id'] : $applicant_id;
	$checkSQL = "SELECT * FROM sectionstatus WHERE $section = '0'  AND applicant_id = '$applicant_id'";
	$checkRS = mysql_query($checkSQL, $fer) or die(mysql_error());
	$sectionIncomplete = mysql_num_rows($checkRS);

	return ($sectionIncomplete)? false : true;
}

//function gets the percentage completion of the CV of the currently logged in applicant
function getPercentComplete() {
	global $fer, $database_fer;
	mysql_select_db($database_fer, $fer);
	$applicant_id = $_SESSION['FER_User']['id'];
	$checkSQL = "SELECT * FROM sectionstatus WHERE applicant_id = '$applicant_id'";
	$checkRS = mysql_query($checkSQL, $fer) or die(mysql_error());
	$sections = mysql_fetch_assoc($checkRS);
	
	$totalSections = sizeof($sections) - 2;
	$completeSections = 0;
	
	if($sections['personal'] == '1') $completeSections++;		
	if($sections['educational'] == '1') $completeSections++;		
	if($sections['profcerts'] == '1') $completeSections++;		
	if($sections['nysc'] == '1') $completeSections++;		
	if($sections['workExp'] == '1') $completeSections++;		
	if($sections['areas'] == '1') $completeSections++;		
	if($sections['skills'] == '1') $completeSections++;		
	if($sections['behavioural'] == '1') $completeSections++;		
	if($sections['uploadCV'] == '1') $completeSections++;		
	if($sections['photo'] == '1') $completeSections++;
	
	$percentage = $completeSections/$totalSections*100;
	
	return $percentage;		
}

//function checks if currently logged-in applicant has already applied for the specified vacancy
function AlreadyApplied($vacancy_id) {
	global $fer, $database_fer;
	mysql_select_db($database_fer, $fer);
	$applicant_id = $_SESSION['FER_User']['id'];
	$checkSQL = "SELECT * FROM applications WHERE applicant_id = '$applicant_id' AND vacancy_id = '$vacancy_id'";
	$checkRS = mysql_query($checkSQL, $fer) or die(mysql_error());
	$foundApp = mysql_num_rows($checkRS);
	
	return ($foundApp)? true : false;
}

//function checks if currently logged-in applicant is eligible to apply for the vacancy
function NotEligible($vacancy_id, $employer_id) {
	global $fer, $database_fer;
	mysql_select_db($database_fer, $fer);
	$applicant_id = $_SESSION['FER_User']['id'];
	$checkSQL = "SELECT v.employer_id FROM applications a LEFT JOIN vacancies v ON a.vacancy_id = v.id WHERE a.applicant_id = '$applicant_id'";
	$checkRS = mysql_query($checkSQL, $fer) or die(mysql_error());
	$listOfEmployerIDs = mysql_fetch_assoc($checkRS);
	return in_array($employer_id, $listOfEmployerIDs);
	
	//return ($foundApp)? true : false;
}

//function returns the birth year given a particular age
function GetYear($age)
{
	$thisYear = date('Y');
	$dYear = $thisYear - $age;
	return $dYear;
}
//function returns progress bar based on supplied percentage
function progressBar($percentage) {
    $data = "<div id=\"progress-bar\" class=\"all-rounded\">\n";
    $data .= "<div id=\"progress-bar-percentage\" class=\"all-rounded\" style=\"width: $percentage%\">";
        //if ($percentage > 5) { $data .= "$percentage%";} else {$data .= "<div class=\"spacer\"> </div>";}
    $data .= "</div></div>";
    return $data;
}

//function returns the % equivalent for a skill competency
function GetSkillPercent($competency) {
	switch($competency) {
		case 'Basic': return 20; break;
		case 'Intermediary': return 40; break; 	
		case 'Advanced': return 60; break; 	
		case 'Expert': return 80; break; 	
		case 'Guru': return 100; break; 
		default: return 0;	
	}
}

//function returns the % equivalent for a skill competency
function GetBehaviourPercent($tendency) {
	switch($tendency) {
		case 'Low': return 34; break;
		case 'Medium': return 67; break; 	
		case 'High': return 100; break; 	
		default: return 0;	
	}
}

//function for displaying categories of vacancies
function vacancycategories(){
		
}

function CalcYearsExp($id){

	global $fer, $database_fer;

	mysql_select_db($database_fer, $fer);
	$query_exp = "SELECT * FROM `workexp` WHERE applicant_id = $id ORDER BY endDate DESC";
	$exp = mysql_query($query_exp, $fer) or die(mysql_error());
	$row_exp = mysql_fetch_assoc($exp);
	$totalRows_exp = mysql_num_rows($exp);

	if($totalRows_exp == 0 ){
		return "N/A";
	} else {
	//Get start and end date from database
	$result = 0;
		do{

			 $startDate = strtotime($row_exp["startDate"]); // or your date as well
		     $endDate = (isset($row_exp["endDate"])) ? strtotime($row_exp["endDate"]) : time();
		     $datediff = $endDate - $startDate;
		     //echo $startDate . " - " .$endDate . "<br>";
		     $resultday += floor($datediff/(60*60*24));
		     $divde = $resultday / 365 ;
		     return round( $divde, 1);//round(8.9899, 1, PHP_ROUND_HALF_UP);

			//$result += ($startYear == $endYear)? 1 : date_diff($row_exp["startDate"],$row_exp["endDate"]);

		}while($row_exp = mysql_fetch_assoc($exp));

		//return $result;

		return $result;
	}

}

function GetFirstDegree($id){

	global $fer, $database_fer;

	mysql_select_db($database_fer, $fer);
	$query_tertiary = "SELECT * FROM `tertiary` WHERE applicant_id = $id AND (qualification = 'BSc' or qualification = 'HND' or qualification = 'BA' or qualification = 'BArch' or qualification = 'BEng' or qualification = 'BEd' or qualification = 'BSc/Ed' or qualification = 'BA/Ed' or qualification = 'LLM')";
	$tertiary = mysql_query($query_tertiary, $fer) or die(mysql_error());
	$row_tertiary = mysql_fetch_assoc($tertiary);
	$totalRows_tertiary = mysql_num_rows($tertiary);

	if($totalRows_tertiary == 0){
		return "N/A";
	} else{
		//echo "here";
	return  $row_tertiary['class'];//$row_tertiary["qualification"]; "(".$row_tertiary['class'].")";//($row_tertiary['class']);
	}

} 

?>

