<?php require_once("_inc_checkSession.php"); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php require_once('../_inc_Functions.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_fer, $fer);
$query_discipline = "SELECT * FROM job_categories";
$discipline = mysql_query($query_discipline, $fer) or die(mysql_error());
$row_discipline = mysql_fetch_assoc($discipline);
$totalRows_discipline = mysql_num_rows($discipline);

$thispage = $_SERVER["PHP_SELF"]."?name=".$_GET['name']."&sex=".$_GET['sex']."&discipline=".$_GET['discipline']."&status=".$_GET['status']."&fromDate=".$_GET['fromDate']."&toDate=".$_GET['toDate'];

$sex = $_GET["sex"];
$name = $_GET["name"];
$discipline_form = $_GET["discipline"];

$maxRows_applicants = 30;
$pageNum_applicants = 0;
if (isset($_GET['pageNum_applicants'])) {
  $pageNum_applicants = $_GET['pageNum_applicants'];
}
$startRow_applicants = $pageNum_applicants * $maxRows_applicants;

mysql_select_db($database_fer, $fer);
$query_applicants = "SELECT * FROM applicants  WHERE 1=1";
//gender
if($_GET["sex"] != '') {
	$query_applicants .= " AND gender = '$sex'";
}
//Name
if($_GET["name"] != '') {
	$query_applicants .= " AND (surname LIKE '%$name%' OR firstname LIKE '%$name%' OR middlename LIKE '%$name$') ";
}
//discipline
if($_GET["discipline"] != "") {
 	$query_applicants .= " AND discipline = '$discipline_form'";
}
//Dates
if(!empty($_GET['fromDate']) && !empty($_GET['toDate']) && strtotime($_GET['toDate']) >= strtotime($_GET['fromDate'])) {
	$query_applicants .= sprintf(" AND dateReg BETWEEN %s AND %s", GetSQLValueString(date('Y-m-d h:i:s', strtotime($_GET['fromDate'])),"date"), GetSQLValueString(date('Y-m-d h:i:s', strtotime($_GET['toDate'])),"date"));
}
$query_limit_applicants = sprintf("%s LIMIT %d, %d", $query_applicants, $startRow_applicants, $maxRows_applicants);
$applicants = mysql_query($query_limit_applicants, $fer) or die(mysql_error());
$row_applicants = mysql_fetch_assoc($applicants);

if (isset($_GET['totalRows_applicants'])) {
  $totalRows_applicants = $_GET['totalRows_applicants'];
} else {
  $all_applicants = mysql_query($query_applicants);
  $totalRows_applicants = mysql_num_rows($all_applicants);
}
$totalPages_applicants = ceil($totalRows_applicants/$maxRows_applicants)-1;

$queryString_applicants = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_applicants") == false && 
        stristr($param, "totalRows_applicants") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_applicants = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_applicants = sprintf("&totalRows_applicants=%d%s", $totalRows_applicants, $queryString_applicants);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Applicants | <?php echo $config['shortname'] ?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link href="assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('-inc-top.php'); ?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<?php include('-inc-navbar-side.php'); ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					Widget settings form goes here
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title">Applicants <small>browse applicants </small></h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="applicants.php">Applicants</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#"> Browse Applicants</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) { ?>
			    <div class="alert alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg'] ?></strong> </div>
			    <?php } ?>
			    <?php if ($totalRows_applicants == 0) { // Show if recordset empty ?>
			    <div class="row-fluid">
			        <div class="alert">
			            
			            <strong>Empty List!</strong> No applicants found. </div>
		        </div>
			    <?php } // Show if recordset empty ?>
                <a href="applicant-edit.php">
                <button name="viewShortlist" type="button" class="btn green" id="viewShortlist" value="">
                <i class="icon-plus"></i> Create Applicant</button> </a>
                <a href="applicants-export.php">
                <button type="button" class="btn green"><i class=" icon-arrow-down"></i> Export Applicants</button>
                </a>
                <p>&nbsp;</p>
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN EXAMPLE TABLE PORTLET-->
			            <div class="portlet box light-grey">
			                <div class="portlet-title">
			                    <div class="caption">Applicants</div>
		                    </div>



			                <div class="portlet-body">
			                    
			                    <div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid"> <br>
			                        
			                        <form action="" method="GET" name="formFilter" id="formFilter" >
			                            <div class="row-fluid">
			                                <div class="span12">
			                                    <div id="sample_1_length" class="dataTables_length">
			                                        
                                                    <span class="controls">
			                                        <input id="" name="name" class="m-wrap m-ctrl-medium" type="text" value="<?php echo $_GET["name"] ?>" placeholder="Applicant Name" />
                                                    &nbsp;
                                                    <select name="sex" class="" id="sex" style="width: 100px;" >
			                                            <option value="">All Sexes</option>
			                                            <option value="Female" <?php if(isset($_GET["sex"]) && $_GET["sex"] == "Female") { echo "selected"; } ?> >Female</option>
			                                            <option value="Male" <?php if(isset($_GET["sex"]) && $_GET["sex"] == "Male") { echo "selected"; } ?>>Male</option>
                                          			</select>
                                                    &nbsp;
                                                    <input id="fromDate" name="fromDate" class="m-wrap m-ctrl-medium date-picker" style="width: 120px;" readonly size="16" type="text" value="" placeholder="From" />
                                                    &nbsp;
                                                    <input id="toDate" name="toDate" class="m-wrap m-ctrl-medium date-picker" style="width: 120px;" readonly size="16" type="text" value="" placeholder="To" />
			                                        <select name="discipline" class="discipline" id="discipline" style="margin-top:5px;">
			                                          <option value="" <?php if (!(strcmp("", $_GET['discipline']))) {echo "selected=\"selected\"";} ?>> All Disciplines </option>
			                                          <?php
do {  
?>
			                                          <option value="<?php echo $row_discipline['id_cat']?>"<?php if (!(strcmp($row_discipline['id_cat'], $_GET['discipline']))) {echo "selected=\"selected\"";} ?>><?php echo $row_discipline['cat_name']?></option>
			                                          <?php
} while ($row_discipline = mysql_fetch_assoc($discipline));
  $rows = mysql_num_rows($discipline);
  if($rows > 0) {
      mysql_data_seek($discipline, 0);
	  $row_discipline = mysql_fetch_assoc($discipline);
  }
?>
                                                    </select>
			                                        &nbsp;
                                                    </span>
													<button type="submit" class="btn black">Filter <i class="m-icon-swapright m-icon-white"></i></button>
			                                        &nbsp;
		                                        </div>
		                                    </div>
		                                </div>
		                            </form>
                                    
			                        <div class="row-fluid">
			                            <div class="span6"></div>
			                            <div class="span6">
			                                <!--<div class="dataTables_filter" id="sample_1_filter">
                  <label>Search: <input type="text" aria-controls="sample_1" class="m-wrap medium"></label>
                  </div>-->
		                                </div>
		                            </div>
                                    <?php if ($totalRows_applicants > 0) { // Show if recordset not empty ?>   
			                        <table class="table table-striped table-bordered table-hover" id="sample_1">
			                            <thead>
			                                <tr>
			                                    <th width="5%"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
                                                <?php //<th width="5%">ID</th> ?>
                                                <th width="25%">Name</th>
                                                <?php //<th width="10%">Gender</th> ?>
                                                <th width="15%">Email</th>
                                                <th width="10%">GSM</th>
                                                <th width="5%">Years of Experience </th>
                                                <th width="18%">First Degree </th>
			                                    <th width="10%" >Date Registered</th>
			                                    <th width="18%" >&nbsp;</th>
		                                    </tr>
		                                </thead>
			                            <tbody>
			                                <?php do { ?>
			                                <tr class="odd gradeX">
			                                    <td><input type="checkbox" name="" id="<?php echo $row_applicants['id']; ?>" value="<?php echo $row_applicants['id']; ?>" class="checkboxes" /></td>
                                                <?php /*<td><?php echo $row_applicants['id']; ?></td> */?>
			                                    <td ><a href="applicant-details.php?id=<?php echo $row_applicants['id']; ?>" target="_blank" class="tooltips" title="View <?php echo $row_applicants['firstname']; ?>'s Details"><?php echo $row_applicants['surname']; ?> <?php echo $row_applicants['firstname']; ?></a></td>
			                                    <?php /*<td ><?php echo ($row_applicants['gender'])? $row_applicants['gender'] : 'N/A'; ?></td> */ ?>
			                                    <td ><?php echo $row_applicants['email']; ?></td>
			                                    <td ><?php echo $row_applicants['gsm']; ?></td>
			                                    <td ><?php echo CalcYearsExp($row_applicants['id']) ?></td>
			                                    <td ><?php echo GetFirstDegree($row_applicants['id'])?></td>
			                                    <td ><?php echo date("d-M-Y", strtotime($row_applicants['dateReg'])); ?></td>
			                                    <td >
			                                        <a href="applicant-applications.php?id=<?php echo $row_applicants['id']; ?>" target="_blank" title="View Vacancies <?php echo $row_applicants['firstname']; ?> Applied for" class="tooltips"><i class=" icon-briefcase"></i></a>&nbsp;&nbsp;
			                                        <a href="applicant-details.php?id=<?php echo $row_applicants['id']; ?>" target="_blank" title="View <?php echo $row_applicants['firstname']; ?>'s Details" class="tooltips"><i class="icon-eye-open"></i></a>&nbsp;&nbsp;
			                                        <a href="applicant-edit.php?id=<?php echo $row_applicants['id']; ?>" title="Edit" class="tooltips"><i class="icon-edit"></i></a>&nbsp;&nbsp;
		                                        <a title="Delete" class="ValidateAction tooltips" data-placement="top" data-confirm-msg="Are you sure you want to Delete!" href="applicant-delete.php?id=<?php echo $row_applicants['id']; ?>"><i class="icon-remove"></i></a></td>
		                                    </tr>
			                                <?php } while ($row_applicants = mysql_fetch_assoc($applicants)); ?>
		                                </tbody>
		                            </table>
			                        <div class="row-fluid">
			                            <div class="span6">
			                                <div class="dataTables_info" id="sample_1_info">Showing <?php echo ($startRow_applicants + 1) ?> to <?php echo min($startRow_applicants + $maxRows_applicants, $totalRows_applicants) ?> of <?php echo $totalRows_applicants ?> entries</div>
		                                </div>
			                            <div class="span6">
			                                <div class="dataTables_paginate paging_bootstrap pagination">
			                                    <ul>
			                                        <?php if ($pageNum_applicants > 0) { // Show if not first page ?>
			                                        <li class="prev"><a href="<?php printf("%s?pageNum_applicants=%d%s", $thispage, 0, $queryString_applicants); ?>" title="First Page">&laquo; </a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_applicants > 0) { // Show if not first page ?>
			                                        <li class="prev"><a href="<?php printf("%s?pageNum_applicants=%d%s", $thispage, max(0, $pageNum_applicants - 1), $queryString_applicants); ?>" title="Previous Page">&lsaquo;</a></li>
			                                        <?php } ?>
			                                        <?php $thisPageNumber = $pageNum_applicants+1;
                                                //determine startPage and endPage
                                                $totalPages = $totalPages_applicants +1;
                                                if($totalPages < 4) {
                                                    $startPage = 1;
                                                    $endPage = $totalPages;
                                                } elseif($thisPageNumber <= 4) {
                                                    $startPage = 1;
                                                    $endPage = 4;
                                                } else {
                                                    $startPage = $thisPageNumber - 3;
                                                    $endPage = $thisPageNumber;
                                                }
                                              ?>
			                                        <?php for ($i = $startPage; $i<=$endPage; $i++) { ?>
			                                        <li class="<?php if ($i == $thisPageNumber) echo "active"; ?>"><a href="<?php echo $thispage ?>?pageNum_applicants=<?php echo $i-1 ?>&amp;totalRows_applicants=<?php echo $totalRows_applicants ?>" title="Page <?php echo $i ?>"><?php echo $i ?></a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_applicants < $totalPages_applicants) { // Show if not last page ?>
			                                        <li class="next"><a href="<?php printf("%s?pageNum_applicants=%d%s", $thispage, min($totalPages_applicants, $pageNum_applicants + 1), $queryString_applicants); ?>" title="Next Page">&rsaquo; </a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_applicants < $totalPages_applicants) { // Show if not last page ?>
			                                        <li class="next"><a href="<?php printf("%s?pageNum_applicants=%d%s", $thispage, $totalPages_applicants, $queryString_applicants); ?>" title="Last Page">&raquo; </a></li>
			                                        <?php } ?>
		                                        </ul>
		                                    </div>
		                                </div>
		                            </div>
                    			    <?php } // Show if applicant list not empty ?>

		                        </div>
                                
		                    </div>
		                </div>
			            <!-- END EXAMPLE TABLE PORTLET-->
		            </div>
		        </div>
			    <div class="clearfix"></div>
		    </div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include('-inc-footer.php'); ?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>   
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  
	<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
	<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>     
	<script src="assets/scripts/table-managed.js"></script>     
	<!-- END PAGE LEVEL SCRIPTS -->  
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
    <script>
		$(document).ready(function(e) {
		   $("#company").val('<?php echo $_GET['company'] ?>');
		   $("#status").val('<?php echo $_GET['status'] ?>');
		   $("#fromDate").val('<?php echo $_GET['fromDate'] ?>');
		   $("#toDate").val('<?php echo $_GET['toDate'] ?>');
        });
	</script>
	<script>
		jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   FormComponents.init();
		   TableManaged.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<?php
mysql_free_result($discipline);

mysql_free_result($companies);

mysql_free_result($sites);
?>
