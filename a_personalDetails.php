<?php require_once "_inc_checkSession.php";?>
<?php require_once "_inc_applicantsOnly.php";?>
<?php $thisPage = basename($_SERVER['PHP_SELF']);?>
<?php require_once '_inc_config.php';?>
<?php require_once 'Connections/fer.php';?>
<?php include '_inc_Functions.php';?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "formPersonalDetails")) {

	mysql_select_db($database_fer, $fer);

	$post_dob = date("Y-m-d", strtotime($_POST['dobYear'] . '-' . $_POST['dobMonth'] . '-' . $_POST['dobDay']));

	//check for duplicates
	$checkSQL = sprintf("SELECT * FROM applicants
	WHERE id <> %s
	AND
	(email = %s
	OR gsm =%s
	OR (firstname = %s AND surname = %s AND dob = %s AND nationality = %s AND stateOfOrigin = %s AND lga = %s))",
		GetSQLValueString($FER_User['id'], "text"),
		GetSQLValueString($_POST['email'], "text"),
		GetSQLValueString($_POST['gsm'], "text"),
		GetSQLValueString($_POST['firstname'], "text"),
		GetSQLValueString($_POST['surname'], "text"),
		GetSQLValueString($post_dob, "date"),
		GetSQLValueString($_POST['nationality'], "text"),
		GetSQLValueString($_POST['stateOfOrigin'], "text"),
		GetSQLValueString($_POST['lga'], "text")
	);
	//echo  $checkSQL. "<br>";
	$checkRS = mysql_query($checkSQL, $fer) or die(mysql_error());
	$duplicateFound = mysql_num_rows($checkRS);

	$posted = $_POST['firstname'] . $_POST['surname'] . $post_dob . $_POST['nationality'] . $_POST['stateOfOrigin'] . $_POST['lga'];
	$saved = $FER_User['firstname'] . $FER_User['surname'] . $FER_User['dob'] . $FER_User['nationality'] . $FER_User['stateOfOrigin'] . $FER_User['lga'];
//	echo "Duplicate Found: $duplicateFound <br>";
	//	echo  $posted;
	//	echo "<br>";
	//	die ($saved);
	if ($duplicateFound && $posted != $saved) {
		$goto = "a_personalDetails.php?error=" . urlencode("Sorry, the information you supplied indicates that you might have already registered. Please check your Email, Names, Date of Birth, Nationality, State of Origin and LGA and be sure they are correct. Thanks");
		header("Location: $goto");
		exit;
	}

	$updateSQL = sprintf("UPDATE applicants SET email=%s, title=%s, surname=%s, firstname=%s, middlename=%s, gender=%s, marital=%s, dob=%s, languages=%s, houseNumber=%s, street=%s, busStop=%s, city=%s, stateOfResidence=%s, countryOfResidence=%s, gsm=%s, gsm2=%s, prefTestLoc=%s, email2=%s WHERE id=%s",
		GetSQLValueString($_POST['email'], "text"),
		GetSQLValueString($_POST['title'], "text"),
		GetSQLValueString($_POST['surname'], "text"),
		GetSQLValueString($_POST['firstname'], "text"),
		GetSQLValueString($_POST['middlename'], "text"),
		GetSQLValueString($_POST['gender'], "text"),
		GetSQLValueString($_POST['marital'], "text"),
		GetSQLValueString($_POST['dobYear'] . '-' . $_POST['dobMonth'] . '-' . $_POST['dobDay'], "date"),
		GetSQLValueString($_POST['languages'], "text"),
		GetSQLValueString($_POST['houseNumber'], "text"),
		GetSQLValueString($_POST['street'], "text"),
		GetSQLValueString($_POST['busStop'], "text"),
		GetSQLValueString($_POST['city'], "text"),
		GetSQLValueString($_POST['stateOfResidence'], "text"),
		GetSQLValueString($_POST['countryOfResidence'], "text"),
		GetSQLValueString($_POST['gsm'], "text"),
		GetSQLValueString($_POST['gsm2'], "text"),
		GetSQLValueString($_POST['prefTestLoc'], "text"),
		GetSQLValueString($_POST['email2'], "text"),
		GetSQLValueString($_POST['id'], "int"));

	$Result1 = mysql_query($updateSQL, $fer) or die(mysql_error());

	//update session - get current details
	$colname_user = "-1";
	if (isset($FER_User)) {
		$colname_user = $FER_User['id'];
	}
	mysql_select_db($database_fer, $fer);
	$query_user = sprintf("SELECT * FROM applicants WHERE id = %s", GetSQLValueString($colname_user, "int"));
	$user = mysql_query($query_user, $fer) or die(mysql_error());
	$row_user = mysql_fetch_assoc($user);
	$FER_User = $row_user;

	//check if section status exists for this applicant and create it if otherwise
	if (SectionStatusExists($FER_User['id']) == false) {
		CreateSectionStatus($FER_User['id']);
	}

	//update section status
	UpdateSectionStatus($FER_User['id'], 'personal', '1');

	$updateGoTo = "a_personalDetails.php?msg=" . urlencode("Details successfully updated!");
//	if (isset($_SERVER['QUERY_STRING'])) {
	//	$updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
	//	$updateGoTo .= $_SERVER['QUERY_STRING'];
	//	}
	header(sprintf("Location: %s", $updateGoTo));
}

$colname_sections = "-1";
if (isset($FER_User['id'])) {
	$colname_sections = $FER_User['id'];
}
mysql_select_db($database_fer, $fer);
$query_sections = sprintf("SELECT * FROM sectionstatus WHERE applicant_id = %s", GetSQLValueString($colname_sections, "int"));
$sections = mysql_query($query_sections, $fer) or die(mysql_error());
$row_sections = mysql_fetch_assoc($sections);
$totalRows_sections = mysql_num_rows($sections);

if ($totalRows_sections) {
	$cvStatus = 1;
	foreach ($row_sections as $value) {
		if ($value == 0) {
			$cvStatus = 0;
		}

	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="shortcut icon" href="favicon.png" />

	<title>Personal Details - <?php echo $FER_User['firstname']?> <?php echo $FER_User['surname']?> | <?php echo $config['shortname']?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="dist/bootstrap-tagsinput.css">
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://raw.githubusercontent.com/aehlke/tag-it/master/css/jquery.tagit.css">
	<!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include '-inc-header-top.php';?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include '-inc-header-nav.php';?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
		    <?php include '-inc-applicant-top.php';?>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
		    <div class="row">
		        <div class="col-sm-4 page-sidebar">
		            <?php include '-inc-applicant-side.php';?>
	            </div>
		        <!-- end .page-sidebar -->
		        <div class="col-sm-8 page-content">
		            <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>
		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>
	                </div>-->
		            <form id="formPersonalDetails" name="formPersonalDetails" method="POST" action="<?php echo $editFormAction;?>">
	                    <div class="white-container sign-up-form">
	                        <div>
	                            <h3>Personal Details</h3>
                                <p><strong><em>NOTE: All fields marked * are required</em></strong></p>
	                            <section>
	                                <?php if (isset($_GET['error'])) {?>
		                                <div class="alert alert-error">
		                                    <h6>Oops!</h6>
		                                    <p><?php echo $_GET['error']?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php }
?>
	                                <?php if (isset($_GET['msg'])) {?>
		                                <div class="alert alert-success">
		                                    <!-- <h6>Wow!</h6> -->
		                                    <p><?php echo $_GET['msg']?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php }
?>
	                                <div class="row">
	                                    <div class="col-sm-4">
	                                        <label for="title">Title</label>
	                                        <select type="text" id="title" name="title" class="form-control">
                                              <option selected="selected" value="" <?php if (!(strcmp("", $FER_User['title']))) {echo "selected=\"selected\"";}
?>>None</option>
                                                <br>
                                                <option value="Mr" <?php if (!(strcmp("Mr", $FER_User['title']))) {echo "selected=\"selected\"";}
?>>Mr</option>
                                                <option value="Ms" <?php if (!(strcmp("Ms", $FER_User['title']))) {echo "selected=\"selected\"";}
?>>Ms</option>
                                                <option value="Mrs" <?php if (!(strcmp("Mrs", $FER_User['title']))) {echo "selected=\"selected\"";}
?>>Mrs</option>
                                                <option value="Miss" <?php if (!(strcmp("Miss", $FER_User['title']))) {echo "selected=\"selected\"";}
?>>Miss</option>
                                                <option value="Dr" <?php if (!(strcmp("Dr", $FER_User['title']))) {echo "selected=\"selected\"";}
?>>Dr</option>
                                                <option value="Chief" <?php if (!(strcmp("Chief", $FER_User['title']))) {echo "selected=\"selected\"";}
?>>Chief</option>
                                                <option value="Prof" <?php if (!(strcmp("Prof", $FER_User['title']))) {echo "selected=\"selected\"";}
?>>Prof</option>
                                                <option value="Rev" <?php if (!(strcmp("Rev", $FER_User['title']))) {echo "selected=\"selected\"";}
?>>Rev</option>
                                                <option value="Alhaji" <?php if (!(strcmp("Alhaji", $FER_User['title']))) {echo "selected=\"selected\"";}
?>>Alhaji</option>
                                                <option value="Mallam" <?php if (!(strcmp("Mallam", $FER_User['title']))) {echo "selected=\"selected\"";}
?>>Mallam</option>
                                                <option value="Hajia" <?php if (!(strcmp("Hajia", $FER_User['title']))) {echo "selected=\"selected\"";}
?>>Hajia</option>
                                                <option value="Engr" <?php if (!(strcmp("Engr", $FER_User['title']))) {echo "selected=\"selected\"";}
?>>Engr</option>
                                                <option value="Arc" <?php if (!(strcmp("Arc", $FER_User['title']))) {echo "selected=\"selected\"";}
?>>Arc</option>
                                            </select></div>
</div>

                                    <div class="row">
                                        <div class="col-sm-6"><span id="sprytextfield">
                                            <label for="firstname">First Name*</label>
                                            <span id="sprytextfield2">
                                            <label for="firstname"></label>
                                            <input name="firstname" type="text" class="form-control" id="firstname" value="<?php echo $FER_User['firstname'];?>"/>
                                            <span class="textfieldRequiredMsg">A value is required.</span></span><span class="textfieldRequiredMsg">A value is required.</span></span>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="firstname3">Middle Name</label>

                                                <label for="firstname4"></label>
                                                <input name="middlename" type="text" class="width244" id="middlename" value="<?php echo $FER_User['middlename'];?>"/>
                                                </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-sm-6">
                                            <span id="sprytextfield1">
                                            <label for="surname">Surname*</label>
                                            <input name="surname" type="text" class="width244" id="surname" value="<?php echo $FER_User['surname'];?>" />
                                         <span class="textfieldRequiredMsg">A value is required.</span></span></div>
                                    </div>

                                     <div class="row">
                                         <div class="col-sm-6">
                                             <label for="gender">Gender*</label><br>
                                             <label>
                                                    <input  checked="checked"  type="radio" name="gender" value="Male" id="gender_0" />
                                                    Male</label>
                                                <label>
                                                    &nbsp;
                                                    &nbsp;
                                                    <input <?php if (!(strcmp($FER_User['gender'], "Female"))) {echo "checked=\"checked\"";}
?>  name="gender" type="radio" id="gender_1" value="Female"/>
                                                    Female</label>
                                             </div>
</div>
                                     <div class="row">
                                         <div class="col-sm-6">
                                             <label for="dobDay">Date of Birth*</label><br>

                                              <select name="dobDay" id="dobDay"  class="col-sm-4" >
                                                <option value="">Day</option>
                                                <?php
for ($d = 1; $d <= 31; $d++) {
	?>
                                                <option value="<?php echo $d;?>" <?php if (date("d", strtotime($FER_User['dob'])) == $d && $FER_User['dob'] != '') {
		echo "selected='selected'";
	}
	?>><?php echo sprintf("%02d", $d);?></option>
                                                <?php }
?>
                                              </select>

                                              <select name="dobMonth" id="dobMonth" class="col-sm-4">
                                                <option value="">Month</option>
                                                <?php
for ($m = 1; $m <= 12; $m++) {
	?>
                                                <option value="<?php echo $m?>" <?php if (date("m", strtotime($FER_User['dob'])) == $m && $FER_User['dob'] != '') {
		echo "selected='selected'";
	}
	?>><?php echo date("M", mktime(0, 0, 0, $m, 10));?></option>
                                                <?php }
?>
                                              </select>

                                              <select name="dobYear" id="dobYear" class="col-sm-4">
                                                <option value="">Year</option>
                                                <?php
for ($y = date("Y"); $y >= 1950; $y--) {
	?>
                                                <option value="<?php echo $y?>" <?php if (date("Y", strtotime($FER_User['dob'])) == $y && $FER_User['dob'] != '') {
		echo "selected='selected'";
	}
	?>><?php echo $y?></option>
                                                <?php }
?>
                                              </select>

                                               </div>
                                         <div class="col-sm-6">
                                             <label for="marital">Marital Status*</label>
                                             <span id="spryselect1">
                                             <select name="marital" id="marital">
                                                 <option value="" <?php if (!(strcmp("", $FER_User['marital']))) {echo "selected=\"selected\"";}
?>>Select One...</option>
                                                 <option value="Single" <?php if (!(strcmp("Single", $FER_User['marital']))) {echo "selected=\"selected\"";}
?>>Single</option>
                                                 <option value="Married" <?php if (!(strcmp("Married", $FER_User['marital']))) {echo "selected=\"selected\"";}
?>>Married</option>
                                                 <option value="Divorced" <?php if (!(strcmp("Divorced", $FER_User['marital']))) {echo "selected=\"selected\"";}
?>>Divorced</option>
                                                 <option value="Separated" <?php if (!(strcmp("Separated", $FER_User['marital']))) {echo "selected=\"selected\"";}
?>>Separated</option>
                                                 <option value="Widowed" <?php if (!(strcmp("Widowed", $FER_User['marital']))) {echo "selected=\"selected\"";}
?>>Widowed</option>
                                             </select>
                                         <span class="selectRequiredMsg">Please select an item.</span></span></div>
                                     </div>

                                     <div class="row">
                                         <div class="col-sm-9"> <span id="sprytextfield8">
                                             <label for="languages">Languages Spoken*</label>
                                             <input name="languages" type="text" class="width244" id="languages" value="<?php echo $FER_User['languages'];?>" />
                                             <span class="textfieldRequiredMsg">A value is required.</span></span></div>
</div>
                                     <span class="stateArea">                                     </span>
                                     <br>
                                     <div class="row">

                                        <div class="col-sm-3">
                                            <label for="houseNumber">House Number</label>
                                            <input type="text" name="houseNumber" id="houseNumber" value="<?php echo $FER_User['houseNumber']?>"/>
                                         </div>

                                        <div class="col-sm-9">
                                            <label for="street"> Address*</label>
                                            <span id="sprytextfield3">
                                            <input name="street" type="text" class="width244" id="street" value="<?php echo $FER_User['street']?>" />
                                         <span class="textfieldRequiredMsg">A value is required.</span></span></div>

                                    </div>

                                    <div class="row">

                                        <div class="col-sm-6">
                                            <label for="busStop">Nearest Bus Stop*</label>
                                            <span id="sprytextfield4">
                                            <input name="busStop" type="text" class="width244" id="busStop" value="<?php echo $FER_User['busStop'];?>"/>
                                        <span class="textfieldRequiredMsg">A value is required.</span></span></div>

                                        <div class="col-sm-6">
                                            <label for="city">City/Town*</label>
                                            <span id="sprytextfield5">
                                            <input name="city" type="text" class="width244" id="city" value="<?php echo $FER_User['city'];?>"/>
                                        <span class="textfieldRequiredMsg">A value is required.</span></span></div>

                                    </div>

                                     <div class="row">
                                         <div class="col-sm-6">
                                             <label for="stateOfResidence">State of Residence*</label>
                                             <span id="spryselect3">
                                             <select name="stateOfResidence" id="stateOfResidence">
                                                 <?php include '_inc_stateList.php';?>
                                             </select>
                                         <span class="selectRequiredMsg">Please select an item.</span></span></div>
                                     </div>
                                     <div class="row">
                                         <div class="col-sm-6">
                                             <label for="countryOfResidence">Country of Residence*</label>
                                             <span id="spryselect5">
                                                 <select name="countryOfResidence" class="width244" id="countryOfResidence">
                                                     <option value="">Select Country</option>
                                                     <?php include '_inc_countryList.php';?>
                                                 </select>
                                                 <span class="selectRequiredMsg">Please select an item.</span></span></div>
</div>

                                     <div class="row">

                                        <div class="col-sm-6">
                                            <label for="gsm">GSM Number*</label>
                                            <span id="sprytextfield6">
                                            <input name="gsm" type="text" class="width244" id="gsm" value="<?php echo $FER_User['gsm'];?>"/>
                                            <span class="textfieldRequiredMsg">A value is required.</span></span></div>

                                        <div class="col-sm-6">
                                            <label for="gsm2">Alternative GSM Number</label>
                                            <input name="gsm2" type="text" class="width244" id="gsm2" value="<?php echo $FER_User['gsm2'];?>"/>
                                         </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-sm-6">
                                            <label for="email">Email Address*</label>
                                            <span id="sprytextfield7">
                                            <input name="email" type="text" class="width244" id="email" value="<?php echo $FER_User['email'];?>" readonly/>
                                            <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldInvalidFormatMsg">Invalid format.</span></span></div>

                                        <div class="col-sm-6">
                                            <label for="email2">Alternative Email Address</label>
                                            <input name="email2" type="text" class="width244" id="email2" value="<?php echo $FER_User['email2'];?>"/>
                                        </div>

                                    </div>

                                    <div class="row">
                                         <div class="col-sm-6">
                                             <label for="prefTestLoc">Preferred Job Location *</label>
                                            <input name="prefTestLoc" type="text" id="prefTestLoc myTags" value="<?php echo $FER_User["prefTestLoc"];?>" />
                                         </div>
                                    </div>
                                    <input name="id" type="hidden" id="id" value="<?php echo $FER_User['id'];?>" />

                                </section>
</div>
                            <hr class="mt60">
                            <div class="clearfix">
                                <input name="submit" type="submit" class="btn btn-default btn-large pull-right" id="submit" value="Update" />
                            </div>
                            <input type="hidden" name="MM_update" value="formPersonalDetails" />
	                    </div>
	                </form>

		        </div>
		        <!-- end .page-content -->
	        </div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include '-inc-footer-top.php';?>

		<div class="copyright">
			<?php include '-inc-footer-bottom.php';?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	//$('#otherNationArea').hide();
	//alert('jquery');
	$('.stateArea').hide(); //alert('check');
	$("#nationality").val('Nigerian');
	if ('<?php echo $FER_User['nationality']?>' != '') {
		$("#nationality").val('<?php echo $FER_User['nationality']?>');
		if('<?php echo $FER_User['nationality']?>' == 'Nigerian') {
			$("#stateOfOrigin").val('<?php echo $FER_User['stateOfOrigin']?>');
			if('<?php echo $FER_User['lga']?>' != '') {
					 //populate lga dropdown
					  $("select#lga > option").remove();
					  //alert('lga options cleared');
					  $.ajax({
						type: "GET",
						url: "xml/states.xml",
						dataType: "xml",
						success: parseXml
					  });

					//select current lga
					$("#lga").val('<?php echo $FER_User['lga']?>');
			}
		}
	}
	if($("#nationality").val() == 'Nigerian') {
		$('.stateArea').show();
	}


	$("#stateOfResidence").val('<?php echo $FER_User['stateOfResidence']?>');
	if('<?php echo $FER_User['countryOfResidence']?>' == '')
		$("#countryOfResidence").val('Nigeria');
	else
		$("#countryOfResidence").val('<?php echo $FER_User['countryOfResidence']?>');

	//datepicker
	//$('#dob').datepick({dateFormat: 'yyyy-mm-dd', yearRange: '-60:-15'});

  	//actions for dropdowns - nationality (to display state) and state( to display LGAs)
	$("select#nationality").change(function()
	{
		//alert("change nationality");
		if($(this).val() != "Nigerian")
		{
			$('.stateArea').hide();//alert("not Nigerian");
		}
		else
		{
			$('.stateArea').show();
		}
	});//end of nationality

	$('select#stateOfOrigin').change(function()
	{
	  $("select#lga > option").remove();
	  //alert('lga options cleared');
	  $.ajax({
		type: "GET",
		url: "xml/states.xml",
		dataType: "xml",
		success: parseXml
	  });
	});

	//submit form
	$("#formPersonalDetails").submit(function(e) {
        //alert('submitted');
		if($("#dobDay").val() == '' || $("#dobMonth").val() == '' || $("#dobYear").val() == '' ) {
			alert('Please fill in your Date of Birth');
			$("#dobDay").focus();
		}

    });

});
//functioned used in state and lga dropdowns
function  parseXml(xml)
{
  //alert('started parsing xml');
  //find every state
  $(xml).find("state").each(function()
  {
	var stateSelected = $('select#stateOfOrigin').val();
	var stateName = $(this).attr("name");
	//alert(stateName +'/'+ stateSelected);
	if(stateName == stateSelected)
	{
		//alert(stateName + ' has been selected');
		$("select#lga").append("<option value=''>Select LGA</option>");
		var printd = "The LGS:";
		$(this).find("lg").each(function()
		{
			//alert('entered each LG');
			//printd += $(this).text();
			var dVal = $(this).text();
			$("select#lga").append("<option value='"+dVal+"'>"+dVal+"</option>");
		});
		//alert(printd);
		$("#lga").val('<?php echo $FER_User['lga']?>');
		//stop searching
		return false;
	}
	//$("#output").append($(this).attr("name") + "<br />");
  });
}
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
var spryselect2 = new Spry.Widget.ValidationSelect("spryselect2");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4");
var sprytextfield5 = new Spry.Widget.ValidationTextField("sprytextfield5");
var spryselect3 = new Spry.Widget.ValidationSelect("spryselect3");
var sprytextfield6 = new Spry.Widget.ValidationTextField("sprytextfield6");
var sprytextfield7 = new Spry.Widget.ValidationTextField("sprytextfield7", "email");
var spryselect4 = new Spry.Widget.ValidationSelect("spryselect4");
</script>
<script src="js/script.js"></script>
<script type="text/javascript" src="dist/bootstrap-tagsinput.js"></script>
<script type="text/javascript" src="dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {validateOn:["blur"]});
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "none", {validateOn:["blur"]});
var sprytextfield8 = new Spry.Widget.ValidationTextField("sprytextfield8", "none", {validateOn:["blur"]});
var spryselect5 = new Spry.Widget.ValidationSelect("spryselect5");
</script>
<!--<script type="text/javascript">
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
var spryselect2 = new Spry.Widget.ValidationSelect("spryselect2");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4");
var sprytextfield5 = new Spry.Widget.ValidationTextField("sprytextfield5");
var spryselect3 = new Spry.Widget.ValidationSelect("spryselect3");
var sprytextfield6 = new Spry.Widget.ValidationTextField("sprytextfield6");
var sprytextfield7 = new Spry.Widget.ValidationTextField("sprytextfield7", "email");
var spryselect4 = new Spry.Widget.ValidationSelect("spryselect4");
</script>
-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://raw.githubusercontent.com/aehlke/tag-it/master/js/tag-it.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#myTags").tagit();
    });
</script>
</body>
</html>