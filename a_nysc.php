<?php require_once("_inc_checkSession.php"); ?>
<?php require_once("_inc_applicantsOnly.php"); ?>
<?php $thisPage = basename( $_SERVER['PHP_SELF'] ); ?>
<?php require_once('_inc_config.php'); ?>
<?php require_once('Connections/fer.php'); ?>
<?php include('_inc_Functions.php'); ?>
<?php

if (!function_exists("GetSQLValueString")) {

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 

{

  if (PHP_VERSION < 6) {

    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  }



  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);



  switch ($theType) {

    case "text":

      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";

      break;    

    case "long":

    case "int":

      $theValue = ($theValue != "") ? intval($theValue) : "NULL";

      break;

    case "double":

      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";

      break;

    case "date":

      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";

      break;

    case "defined":

      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;

      break;

  }

  return $theValue;

}

}



$colname_nysc = "-1";

if (isset($_SESSION['FER_User']['id'])) {

  $colname_nysc = $_SESSION['FER_User']['id'];

}

mysql_select_db($database_fer, $fer);

$query_nysc = sprintf("SELECT * FROM nysc WHERE applicant_id = %s", GetSQLValueString($colname_nysc, "int"));

$nysc = mysql_query($query_nysc, $fer) or die(mysql_error());

$row_nysc = mysql_fetch_assoc($nysc);

$totalRows_nysc = mysql_num_rows($nysc);



$editFormAction = $_SERVER['PHP_SELF'];

if (isset($_SERVER['QUERY_STRING'])) {

  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);

}



if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "AddForm")) {

	if($totalRows_nysc < 1) { //no nysc record yet, insert new

		$insertSQL = sprintf("INSERT INTO nysc (applicant_id, nyscStatus, nyscNumber, nyscYear) VALUES (%s, %s, %s, %s)",

						   GetSQLValueString($_POST['applicant_id'], "int"),

						   GetSQLValueString($_POST['nyscStatus'], "text"),

						   GetSQLValueString($_POST['nyscNumber'], "text"),

						   GetSQLValueString($_POST['nyscYear'], "int"));

		

		mysql_select_db($database_fer, $fer);

		$Result1 = mysql_query($insertSQL, $fer) or die(mysql_error());

	} else { // record exists, update it

		$updateSQL = sprintf("UPDATE nysc SET nyscStatus=%s, nyscNumber=%s, nyscYear=%s WHERE applicant_id=%s",

						   GetSQLValueString($_POST['nyscStatus'], "text"),

						   GetSQLValueString($_POST['nyscNumber'], "text"),

						   GetSQLValueString($_POST['nyscYear'], "int"),

						   GetSQLValueString($_POST['applicant_id'], "int"));

		

		mysql_select_db($database_fer, $fer);

		$Result1 = mysql_query($updateSQL, $fer) or die(mysql_error());

	

	}

	$updateGoto = "a_nysc.php?msg=".urlencode("Details Updated Successfully!");

	if (isset($_SERVER['QUERY_STRING'])) {

	$updateGoto .= (strpos($updateGoto, '?')) ? "&" : "?";

	$updateGoto .= $_SERVER['QUERY_STRING'];

	}

	header(sprintf("Location: %s", $updateGoto));	

	

}



//check if section status exists for this applicant and create it if otherwise

if(SectionStatusExists($FER_User['id']) == false) {

	CreateSectionStatus($FER_User['id']);

}



if($totalRows_nysc)

{

	  //update section status

	 UpdateSectionStatus($FER_User['id'],'nysc','1');

}

else

{

	  //update section status

	 UpdateSectionStatus($FER_User['id'],'nysc','0');

}



$colname_sections = "-1";

if (isset($_SESSION['FER_User']['id'])) {

  $colname_sections = $_SESSION['FER_User']['id'];

}

mysql_select_db($database_fer, $fer);

$query_sections = sprintf("SELECT * FROM sectionstatus WHERE applicant_id = %s", GetSQLValueString($colname_sections, "int"));

$sections = mysql_query($query_sections, $fer) or die(mysql_error());

$row_sections = mysql_fetch_assoc($sections);

$totalRows_sections = mysql_num_rows($sections);

 

if($totalRows_sections)

{

$cvStatus = 1;

	foreach($row_sections as $value)

	{

		if($value == 0)

			$cvStatus = 0;

	}

}

?>
<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

    

	<link rel="shortcut icon" href="favicon.png" />

    

	<title>Professional Certifications - <?php echo $FER_User['firstname'] ?> <?php echo $FER_User['surname'] ?>| <?php echo $config['shortname'] ?> Recruitment Portal</title>



	<!-- Stylesheets -->

	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/bootstrap.css">

	<link rel="stylesheet" href="css/font-awesome.min.css">

	<link rel="stylesheet" href="css/flexslider.css">

	<link rel="stylesheet" href="css/style.css">

	<link rel="stylesheet" href="css/responsive.css">

    <link rel="stylesheet" href="css/color/green.css">

    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">

    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">

    <link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">



	<!--[if IE 9]>

		<script src="js/media.match.min.js"></script>

	<![endif]-->

<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>

<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>

<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>

</head>



<body>

<div id="main-wrapper">



	<header id="header" class="header-style-1">

		<div class="header-top-bar">

			<?php include('-inc-header-top.php'); ?>

             <!-- end .container -->

		</div> <!-- end .header-top-bar -->



		<div class="header-nav-bar">

			<?php include('-inc-header-nav.php'); ?>

             <!-- end .container -->



			<div id="mobile-menu-container" class="container">

				<div class="login-register"></div>

				<div class="menu"></div>

			</div>

		</div> <!-- end .header-nav-bar -->



		<div class="header-page-title">

		    <?php include('-inc-applicant-top.php'); ?>

		</div>



	</header> <!-- end #header -->



	<div id="page-content">

		<div class="container">

		    <div class="row">

		        <div class="col-sm-4 page-sidebar">

		            <?php include('-inc-applicant-side.php'); ?>

	            </div>

		        <!-- end .page-sidebar -->

	          <div class="col-sm-8 page-content">

		            <h3>

		                <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>

		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>

	                </div>-->

		                Nysc dEtails

	                </h3>

	            <form action="<?php echo $editFormAction; ?>" method="post" name="AddForm" id="AddForm">

	                    <div class="white-container sign-up-form">

	                        <div>

	                            <h5>Nysc Details</h5>

                              <section>

	                                <?php if (isset($_GET['error'])) { ?>

		                                <div class="alert alert-error">

		                                    <h6>Oops!</h6>

		                                    <p><?php echo $_GET['error'] ?></p>

	                                    <a href="#" class="close fa fa-times"></a></div>

		                                <?php } ?>

	                                <?php if (isset($_GET['msg'])) { ?>

		                                <div class="alert alert-success">

		                                    <p><?php echo $_GET['msg'] ?></p>

	                                    <a href="#" class="close fa fa-times"></a></div>

		                                <?php } ?>

                                <div class="row">

	                                    <div class="col-sm-8">Nysc status

	                                      <span id="spryselect1">

	                                      <select name="nyscStatus" id="nyscStatus">

	                                        <option value="" selected="selected">Select</option>

	                                        <option value="Yet to start" <?php if (!(strcmp("Yet to start", $row_nysc['nyscStatus']))) {echo "selected=\"selected\"";} ?>>Yet to start</option>

	                                        <option value="Completed" <?php if (!(strcmp("Completed", $row_nysc['nyscStatus']))) {echo "selected=\"selected\"";} ?>>Completed</option>

	                                        <option value="Currently Serving" <?php if (!(strcmp("Currently Serving", $row_nysc['nyscStatus']))) {echo "selected=\"selected\"";} ?>>Currently Serving</option>

	                                        <option value="Exempted" <?php if (!(strcmp("Exempted", $row_nysc['nyscStatus']))) {echo "selected=\"selected\"";} ?>>Exempted</option>

                                          </select>

	                                      <span class="selectRequiredMsg">Please select an item.</span></span>

<div class="nyscArea" style="margin:3px 0px 3px 0px;">

                        NYSC Number:

                        <br />

                        <span id="sprytextfield1">

                        <input name="nyscNumber" type="text" id="nyscNumber" value="<?php echo $row_nysc['nyscNumber']; ?>" class="form-control col-sm-6" placeholder="please specify" >

</span></div>

                            </div>

                                         

                                        <div class="row" style="margin:3px 0px;">

	                                    <div class="col-sm-6 nyscArea" style="margin:4px 1px 0px;">

                                          <label for="year">Year (of Service or Exemption)</label>

                                          <span id="spryselect2">

                                          <select name="nyscYear" id="nyscYear">

                                            <option value="">Select Year</option>

                                            <?php 

							$thisYear = date('Y');

							$stopYear = 1960;

							for($y=$thisYear;$y>=$stopYear;$y--) {

							?>

                                            <option value="<?php echo $y ?>" <?php if ($row_nysc['nyscYear'] == $y) echo "selected='selected'" ?>><?php echo $y ?></option>

                                            <?php } ?>

                                          </select>

                                          <span class="selectRequiredMsg">Please select an item.</span></span></div>

                                        <div class="" id="captchaArea"></div>

                                        <input name="applicant_id" type="hidden" id="applicant_id" value="<?php echo $_SESSION['FER_User']['id']; ?>" />

                                        <input type="hidden" name="MM_insert2" value="AddForm" />

                                        <input type="hidden" name="MM_update" value="AddForm" />

                                    </div>

                                        

                                </div>

                              </section>

</div>

                            <hr class="mt60">

                            <div class="clearfix">

                                <input name="submit" type="submit" class="btn btn-default pull-right" id="submit" value="<?php if ($row_nysc['nyscStatus']) echo "Update"; else echo "Save"; ?>" />

                            </div>

	                    </div>

                  </form>

	          </div>

		        <!-- end .page-content -->

	        </div>

		</div> <!-- end .container -->

	</div> <!-- end #page-content -->



	<footer id="footer">

		<?php include('-inc-footer-top.php'); ?>



		<div class="copyright">

			<?php include('-inc-footer-bottom.php'); ?>

		</div>

	</footer> <!-- end #footer -->



</div> <!-- end #main-wrapper -->



<!-- Scripts -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>

<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>

<script src="js/maplace.min.js"></script>

<script src="js/jquery.ba-outside-events.min.js"></script>

<script src="js/jquery.responsive-tabs.js"></script>

<script src="js/jquery.flexslider-min.js"></script>

<script src="js/jquery.fitvids.js"></script>

<script src="js/jquery-ui-1.10.4.custom.min.js"></script>

<script src="js/jquery.inview.min.js"></script>

<script src="js/script.js"></script>

<script src="js/ValidateDel.js"></script>

<script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>

<script type="text/javascript" src="../js/mainNavbar.js"></script>

<script type="text/javascript">

$(document).ready(function() {



		if($('#nyscStatus').val() == '' || $('#nyscStatus').val() == 'Yet to start')

		{

			$(".nyscArea").hide();	

		}

		if($('#nyscStatus').val() != '' && $('#nyscStatus').val() != 'Yet to start')

		{				

			$(".nyscArea").show();

		} 

		$('#nyscStatus').change(function() { 

			if($('#nyscStatus').val() != 'Yet to start' && $('#nyscStatus').val() != '')

			{				

				$(".nyscArea").show();

			}

			else

			{	

				$(".nyscArea").hide();

				$('#nyscNumber').val('');

				$("#nyscYear").val('');

			}

		});



		$('#submit').click(function() {

			if($('#nyscStatus').val() != 'Yet to start' && $('#nyscStatus').val() != '' && ($('#nyscNumber').val() == '' || $("#nyscYear").val() == ''))

			{

				alert('Please specify the NYSC Number and Year!');

				return false;

			}

		});

});

var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1", {validateOn:["change", "blur"]});

var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {isRequired:false, validateOn:["blur", "change"]});

var spryselect2 = new Spry.Widget.ValidationSelect("spryselect2", {validateOn:["change", "blur"]});

</script>

</body>

</html>