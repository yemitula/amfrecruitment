<?php
// *** Logout the current user.
if (!isset($_SESSION)) {
  session_start();
}
switch($_SESSION['FER_Usertype']) {
	case 'applicant': $logoutGoTo = "index.php"; break;
	case 'employer': $logoutGoTo = "e_index.php"; break;
}
$_SESSION['FER_User'] = NULL;
$_SESSION['FER_Usertype'] = NULL;
unset($_SESSION['FER_User']);
unset($_SESSION['FER_Usertype']);
if ($logoutGoTo != "") {header("Location: $logoutGoTo");
exit;
}
?>
