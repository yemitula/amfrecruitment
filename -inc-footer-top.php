<div class="container">
			<div class="row">
				<div class="col-sm-3 col-md-4">
					<div class="widget">
						<div class="widget-content">
						<img class="logo" src="images/employerLogos/logo.png" alt="">
					<p><?php echo $config['summary'] ?> <a href="http://alphamead.com">Read More</a></p>

						</div>
					</div>
				</div>

				<div class="col-sm-3 col-md-4">
					<div class="widget">
						<h6 class="widget-title">Navigation</h6>

						<div class="widget-content">
							<div class="row">
								<div class="col-xs-6 col-sm-12 col-md-6">
									<ul class="footer-links">
										<li><a href="index.php">Home</a></li>
										<li><a href="#">Vacancies</a></li>
										<?php if(!isset($_SESSION['FER_User'])) { ?>
                                        <li><a href="a_login.php">Login/Register</a></li>
                                        <?php } else { ?>
                                        <li><a href="a_profile.php">My Profile</a></li>
                                        <li><a href="logout.php">Logout</a></li>
                                        <?php } ?>
									</ul>
								</div>

								<div class="col-xs-6 col-sm-12 col-md-6">
									<ul class="footer-links">
										<li><a href="http://alphamead.com">About <?php echo $config['shortname'] ?></a></li>
										<li><a href="#">Contact Us</a></li>
										<li><a href="#">Terms &amp; Conditions</a></li>
										<li><a href="#">Privacy Policy</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-3 col-md-2">
					<div class="widget">
						<h6 class="widget-title">Follow Us</h6>

						<div class="widget-content">
							<ul class="footer-links">
								<li><a href="<?php echo $config['blog'] ?>" target="_blank">The <?php echo $config['shortname'] ?> Blog</a></li>
								<li><a href="<?php echo $config['twitter'] ?>" target="_blank">Twitter</a></li>
								<li><a href="<?php echo $config['facebook'] ?>" target="_blank">Facebook</a></li>
								<li><a href="<?php echo $config['linkedin'] ?>" target="_blank">LinkedIn</a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-sm-3 col-md-2">
					<div class="widget">
						<h6 class="widget-title">Recent Jobs</h6>
<?php 
//get recent jobs
mysql_select_db($database_fer, $fer);
$recent_query = "SELECT v.`id`, `title` FROM vacancies v LEFT JOIN  employers e ON (v.employer_id=e.id) WHERE ( (v.dateOpening IS NULL AND v.dateClosing IS NULL) OR (v.dateClosing >= NOW() AND v.dateOpening IS NULL) OR (v.dateClosing >= NOW() AND v.dateOpening <= NOW()) ) AND v.published IS NOT NULL LIMIT 0,4";
$recent = mysql_query($recent_query, $fer) or die(mysql_error());
$row_recent = mysql_fetch_assoc($recent);
 ?>
						
						<div class="widget-content">
							<?php if(mysql_num_rows($recent)) { ?>
							<ul class="footer-links">
								<?php do { ?>
								<li><a href="a_vacancyDetails.php?id=<?php echo $row_recent['id'] ?>"><?php echo $row_recent['title'] ?></a></li>
								<?php } while($row_recent = mysql_fetch_assoc($recent)); ?>
							</ul>
							<?php } else { ?>
							<p>No vacancies!</p>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>