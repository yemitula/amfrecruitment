<?php require_once("_inc_checkSession.php"); ?>
<?php require_once("_inc_applicantsOnly.php"); ?>
<?php $thisPage = basename( $_SERVER['PHP_SELF'] ); ?>
<?php require_once('_inc_config.php'); ?>
<?php require_once('Connections/fer.php'); ?>
<?php include('_inc_Functions.php'); ?>
<?php

if (!function_exists("GetSQLValueString")) {

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 

{

  if (PHP_VERSION < 6) {

    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  }



  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);



  switch ($theType) {

    case "text":

      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";

      break;    

    case "long":

    case "int":

      $theValue = ($theValue != "") ? intval($theValue) : "NULL";

      break;

    case "double":

      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";

      break;

    case "date":

      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";

      break;

    case "defined":

      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;

      break;

  }

  return $theValue;

}

}



$editFormAction = $_SERVER['PHP_SELF'];

if (isset($_SERVER['QUERY_STRING'])) {

  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);

}

$colname_certs = "-1";

if (isset($_SESSION['FER_User']['id'])) {

  $colname_certs = $_SESSION['FER_User']['id'];

}

mysql_select_db($database_fer, $fer);

$query_certs = sprintf("SELECT id, certification, `year` FROM profcerts WHERE applicant_id = %s ORDER BY `year` DESC", GetSQLValueString($colname_certs, "int"));

$certs = mysql_query($query_certs, $fer) or die(mysql_error());

$row_certs = mysql_fetch_assoc($certs);

$totalRows_certs = mysql_num_rows($certs); 



if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "AddForm")) {

  

  if(testYear4DOB($_POST['year']) || $_POST['certification'] == 'No Certification')

  {

	if($totalRows_certs > 0 && $_POST['certification'] == 'No Certification')	

	{  

	  //die('Stop!!!');

	  header("Location: a_profCerts.php?error=".urlencode("Sorry! You have already added certification(s).<br>You need to delete ALL of them before your can add 'No Certification'"));exit;

	 //die("Sorry! You have already added certification(s).<br>You need to delete ALL of them before your can add 'No Certification<br><br>Please Click your browser's BACK button to RETURN'");

	}

  

  if($row_certs['certification'] == 'No Certification')

  {

	  header("Location: a_profCerts.php?error=".urlencode("Sorry! You must delete your NO CERTIFICATION entry before proceeding!"));

  }

  else

  {

	$applicant_id = $_POST['applicant_id'];

	if($_POST['certification'] == 'No Certification')

	{

	  $insertSQL = "INSERT INTO profcerts (applicant_id, certification, `year`) VALUES ('$applicant_id','No Certification', '0')";

	}

	else

	  $insertSQL = sprintf("INSERT INTO profcerts (applicant_id, certification, `year`) VALUES (%s, %s, %s)",

						 GetSQLValueString($_POST['applicant_id'], "int"),

						 GetSQLValueString(($_POST['certification'] == 'Other')? $_POST['otherCert'] : $_POST['certification'], "text"),

						 GetSQLValueString($_POST['year'], "int"));

  

	mysql_select_db($database_fer, $fer);

	$Result1 = mysql_query($insertSQL, $fer) or die(mysql_error());

  

	$insertGoTo = "a_profCerts.php?msg=".urlencode("The certification has been added!");

	header(sprintf("Location: %s", $insertGoTo));

	

  }

 }

 else

 {

	  header("Location: a_profCerts.php?error=".urlencode("The year you supplied(".$_POST['year'].") is inconsistent with your Date of Birth!!!"));

 }

  }



//check if section status exists for this applicant and create it if otherwise

if(SectionStatusExists($FER_User['id']) == false) {

	CreateSectionStatus($FER_User['id']);

}



if($totalRows_certs)

{

	  //update section status

	 UpdateSectionStatus($FER_User['id'],'profcerts','1');

}

else

{

	  //update section status

	 UpdateSectionStatus($FER_User['id'],'profcerts','0');

}



$colname_sections = "-1";

if (isset($_SESSION['FER_User']['id'])) {

  $colname_sections = $_SESSION['FER_User']['id'];

}

mysql_select_db($database_fer, $fer);

$query_sections = sprintf("SELECT * FROM sectionstatus WHERE applicant_id = %s", GetSQLValueString($colname_sections, "int"));

$sections = mysql_query($query_sections, $fer) or die(mysql_error());

$row_sections = mysql_fetch_assoc($sections);

$totalRows_sections = mysql_num_rows($sections);

 

if($totalRows_sections)

{

$cvStatus = 1;

	foreach($row_sections as $value)

	{

		if($value == 0)

			$cvStatus = 0;

	}

}

?>

<!doctype html>

<html lang="en">

<head>

	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

    

	<link rel="shortcut icon" href="favicon.png" />

    

	<title>Professional Certifications - <?php echo $FER_User['firstname'] ?> <?php echo $FER_User['surname'] ?>| <?php echo $config['shortname'] ?> Recruitment Portal</title>



	<!-- Stylesheets -->

	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/bootstrap.css">

	<link rel="stylesheet" href="css/font-awesome.min.css">

	<link rel="stylesheet" href="css/flexslider.css">

	<link rel="stylesheet" href="css/style.css">

	<link rel="stylesheet" href="css/responsive.css">

    <link rel="stylesheet" href="css/color/green.css">

    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">

    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">

    <link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">



	<!--[if IE 9]>

		<script src="js/media.match.min.js"></script>

	<![endif]-->

<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>

<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>

<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>

</head>



<body>

<div id="main-wrapper">



	<header id="header" class="header-style-1">

		<div class="header-top-bar">

			<?php include('-inc-header-top.php'); ?>

             <!-- end .container -->

		</div> <!-- end .header-top-bar -->



		<div class="header-nav-bar">

			<?php include('-inc-header-nav.php'); ?>

             <!-- end .container -->



			<div id="mobile-menu-container" class="container">

				<div class="login-register"></div>

				<div class="menu"></div>

			</div>

		</div> <!-- end .header-nav-bar -->



		<div class="header-page-title">

		    <?php include('-inc-applicant-top.php'); ?>

		</div>



	</header> <!-- end #header -->



	<div id="page-content">

		<div class="container">

		    <div class="row">

		        <div class="col-sm-4 page-sidebar">

		            <?php include('-inc-applicant-side.php'); ?>

	            </div>

		        <!-- end .page-sidebar -->

		        <div class="col-sm-8 page-content">

		            <h3>

		                <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>

		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>

	                </div>-->

		                Professional Qualifications

	                </h3>

		            <form action="<?php echo $editFormAction; ?>" method="post" name="AddForm" id="AddForm">

	                    <div class="white-container sign-up-form">

	                        <div>

	                            <h5>Add Certification</h5>

                              <section>

	                                <?php if (isset($_GET['error'])) { ?>

		                                <div class="alert alert-error">

		                                    <h6>Oops!</h6>

		                                    <p><?php echo $_GET['error'] ?></p>

	                                    <a href="#" class="close fa fa-times"></a></div>

		                                <?php } ?>

	                                <?php if (isset($_GET['msg'])) { ?>

		                                <div class="alert alert-success">

		                                    <p><?php echo $_GET['msg'] ?></p>

	                                    <a href="#" class="close fa fa-times"></a></div>

		                                <?php } ?>

                                <div class="row">

	                                    <div class="col-sm-8">

	                                        <label for="level">Certificate</label>

	                                        <span id="spryselect1">

	                                        <select name="certification" id="certification">

	                                            <option value="" selected="selected">Select...</option>

	                                            <option value="No Certification">No Certification</option>

	                                            <option value="ACCOUNTING TECH. SCHEME">ACCOUNTING TECH. SCHEME</option>

	                                            <option value="ASSOCIATE CHARTERED  INSTITUTE. OF INSURANCE">ASSOCIATE CHARTERED  INSTITUTE. OF INSURANCE</option>

	                                            <option value="ASSOCIATE INSTITUTE OF STOCK BROKER">ASSOCIATE INSTITUTE OF STOCK BROKER</option>

	                                            <option value="ASSOCIATE CHARTERED INSTITUTE OF BANKING">ASSOCIATE CHARTERED INSTITUTE OF BANKING</option>

	                                            <option value="ASSOCIATE INSTITUTE OF PERSONNEL MANGT.">ASSOCIATE INSTITUTE OF PERSONNEL MANGT.</option>

	                                            <option value="ASSOCIATE. OF INSTITUTE OF COST MGT">ASSOCIATE. OF INSTITUTE OF COST MGT</option>

	                                            <option value="ASSOCIATE INSTITUTE OF NIG INSTITUTE EST SUR">ASSOCIATE INSTITUTE OF NIG INSTITUTE EST SUR</option>

	                                            <option value="ASSOCIATE MEMBER OF NIG INSTITUTE OF MGT">ASSOCIATE MEMBER OF NIG INSTITUTE OF MGT</option>

	                                            <option value="ASSOCIATE OF CHARTED CERTIFIED ACC">ASSOCIATE OF CHARTED CERTIFIED ACC</option>

	                                            <option value="ASSOCIATE. CHARTERED INSTITUTE.OF TAXATION">ASSOCIATE. CHARTERED INSTITUTE.OF TAXATION</option>

	                                            <option value="ASSOCIATE. CHARTERED INSTITUTE.OF TAXATN.">ASSOCIATE. CHARTERED INSTITUTE.OF TAXATN.</option>

	                                            <option value="ASSOCIATE.CERTIFIEDPENSION INSTITUTE.">ASSOCIATE.CERTIFIEDPENSION INSTITUTE.</option>

	                                            <option value="ASSOCIATE.CHARTERED INSTITUTE OF MARKETING">ASSOCIATE.CHARTERED INSTITUTE OF MARKETING</option>

	                                            <option value="ASSOCIATE.CHARTD.INSTITUTE.OF ADMIN">ASSOCIATE.CHARTD.INSTITUTE.OF ADMIN</option>

	                                            <option value="ASSOCIATION OF CHARTERED ACCONTANTS">ASSOCIATION OF CHARTERED ACCONTANTS</option>

	                                            <option value="ASSOCIATE NIG.INSTITUTE OF ARCHITECTURE">ASSOCIATE NIG.INSTITUTE OF ARCHITECTURE</option>

	                                            <option value="ASSOCIATED CHARTERED ACCOUNTANT">ASSOCIATED CHARTERED ACCOUNTANT</option>

	                                            <option value="CERTIFIED INTERNET WEBMASTER PROF.">CERTIFIED INTERNET WEBMASTER PROF.</option>

	                                            <option value="CERTIFICATE">CERTIFICATE</option>

	                                            <option value="CERTIFIED FACILITY MGR">CERTIFIED FACILITY MGR</option>

	                                            <option value="CERTIFIED INFO. SYS. AUDITOR">CERTIFIED INFO. SYS. AUDITOR</option>

	                                            <option value="CERTIFIED INTERNET WEBMASTER">CERTIFIED INTERNET WEBMASTER</option>

	                                            <option value="CERTIFIED PUCLIC ACCOUNTANT">CERTIFIED PUCLIC ACCOUNTANT</option>

	                                            <option value="CETIFICATE OF MEMBERSHIP">CETIFICATE OF MEMBERSHIP</option>

	                                            <option value="CISCO CERTIFIED NETWORK PROFESSION">CISCO CERTIFIED NETWORK PROFESSION</option>

	                                            <option value="CISCO CERTIFIED DESIGN ASSOCIATE.">CISCO CERTIFIED DESIGN ASSOCIATE.</option>

	                                            <option value="CISCO CERTIFIED NETWORK ASSOCIATE">CISCO CERTIFIED NETWORK ASSOCIATE</option>

	                                            <option value="ENVIRONMENTAL PARTNER MEMBER">ENVIRONMENTAL PARTNER MEMBER</option>

	                                            <!--                          <option value="EXECUTIVE MASTERS IN BUS.ADMIN">EXECUTIVE MASTERS IN BUS.ADMIN</option>

-->

	                                            <option value="FULL MEMBER INSTITUTE OF STRAG MGT">FULL MEMBER INSTITUTE OF STRAG MGT</option>

	                                            <option value="IBM CERTIFIED SPECIALIST">IBM CERTIFIED SPECIALIST</option>

	                                            <option value="ICAN CERTIFICATE">ICAN CERTIFICATE</option>

	                                            <option value="INFO. SYS. AUDIT &amp; CONT. ASSO.">INFO. SYS. AUDIT &amp; CONT. ASSO.</option>

	                                            <option value="INTITUTE OF DATA PROCESS &amp; MGT">INTITUTE OF DATA PROCESS &amp; MGT</option>

	                                            <option value="MASTER CIW DESIGNER">MASTER CIW DESIGNER</option>

	                                            <option value="MEMBER, NIG.INSTITUTE. OF ARCHITECT">MEMBER, NIG.INSTITUTE. OF ARCHITECT</option>

	                                            <option value="MEMBER,ASSOCIATE.OF NAT.ACCTS NIG">MEMBER,ASSOCIATE.OF NAT.ACCTS NIG</option>

	                                            <option value="MEMBER,NIG INSTITUTE. OF STAT.">MEMBER,NIG INSTITUTE. OF STAT.</option>

	                                            <option value="MICROSOFT CERTIFIED PROFESS">MICROSOFT CERTIFIED PROFESS</option>

	                                            <option value="MICROSOFT CERTIFIED PROFESSIONAL">MICROSOFT CERTIFIED PROFESSIONAL</option>

	                                            <option value="MICROSOFT CERTIFIED SYS. ENGINEER">MICROSOFT CERTIFIED SYS. ENGINEER</option>

	                                            <option value="MICROSOFT ENGR &amp; D.BASE ADMIN">MICROSOFT ENGR &amp; D.BASE ADMIN</option>

	                                            <option value="MMBR -INSTITUTE. OF CONSTR IND ARB">MMBR -INSTITUTE. OF CONSTR IND ARB</option>

	                                            <option value="ORACLE CERTIFIED PROF D.BASE ADMIN">ORACLE CERTIFIED PROF D.BASE ADMIN</option>

	                                            <option value="ORACLE CERTIFIED PROF. PROGRAM.">ORACLE CERTIFIED PROF. PROGRAM.</option>

	                                            <option value="ORACLE CERTIFIED APPLICATN DEV">ORACLE CERTIFIED APPLICATN DEV</option>

	                                            <option value="ORACLE9I DATABASE ADMINISTRATR">ORACLE9I DATABASE ADMINISTRATR</option>

	                                            <option value="REGISTERED EST SURVEYOR">REGISTERED EST SURVEYOR</option>

	                                            <option value="SUN CERTIFIED JAVA PROGRAMMER (SCJP)">SUN CERTIFIED JAVA PROGRAMMER (SCJP)</option>

	                                            <option value="TECHNICIAN MEMBER">TECHNICIAN MEMBER</option>

	                                            <option value="Other">Other (Not Listed)</option>

                                            </select>

	                                        <span class="selectInvalidMsg">Please select a valid item.</span><span class="selectRequiredMsg">Please select an item.</span></span><br />

                                          

	                                        <input name="otherCert" type="text" id="otherCert" class="form-control col-sm-6" placeholder="please specify" >

</div>

                                         

                                        <div class="row" style="margin:3px 0px;">

	                                    <div class="col-sm-6" style="margin:0px">

                                          <label for="year">Year Obtained</label>

                                          <span id="spryselect2">

                                          <select name="year" id="year">

                                              <option value="" selected="selected">Year</option>

                                              <option value="0" selected="selected">None</option>

                                              <?php include('_inc_yearListFrom1940.php'); ?>

                                          </select>

                                          <span class="selectRequiredMsg">Please select an item.</span></span></div>

                                    </div>

                                        

                                  </div>

	                            </section>

</div>

                            <hr class="mt60">

                            <div class="clearfix">

                                <input name="submit" type="submit" class="btn btn-default pull-right" id="submit" value="Add Entry" />

                            </div>

                           <input name="MM_insert" type="hidden" id="MM_insert" value="AddForm" />

                            <input name="applicant_id" type="hidden" id="applicant_id" value="<?php echo $_SESSION['FER_User']['id']; ?>" />

	                    </div>

	                </form>

		            <?php // starting of the listing?>

                     <div class="candidates-item candidates-single-item">

		<?php if ($totalRows_certs > 0) { // Show if recordset not empty ?>

			<table class="table-striped">

		                    <thead>

		                        <tr>

		                            <th width="40%"><strong>Certification</strong></th>

		                            <th width="30%">Year obtained</th>

		                            <th width="10%">Action</th>

	                            </tr>

	                        </thead>

		                    <tbody>

		                        <?php do { ?>

    <tr>

      <td><?php echo $row_certs['certification']; ?></td>

      <td><?php if($row_certs['year'] != 0) echo $row_certs['year']; ?></td>

      <td align="center"><a href="#" onclick=" return ValidateDel('<?php echo $row_certs['id']; ?>','a_delCert.php?cID=','Are you sure you want to DELETE THIS ITEM? (This Action is NOT reversible)!!!');" class="fa fa-trash-o"></a></td>

    </tr>

    <?php } while ($row_certs = mysql_fetch_assoc($certs)); ?>

	                        </tbody>

	                    </table>

		<?php } // Show if recordset not empty ?>

        </div>

                    <?php //end of listing ?>

		        </div>

		        <!-- end .page-content -->

	        </div>

		</div> <!-- end .container -->

	</div> <!-- end #page-content -->



	<footer id="footer">

		<?php include('-inc-footer-top.php'); ?>



		<div class="copyright">

			<?php include('-inc-footer-bottom.php'); ?>

		</div>

	</footer> <!-- end #footer -->



</div> <!-- end #main-wrapper -->



<!-- Scripts -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>

<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>

<script src="js/maplace.min.js"></script>

<script src="js/jquery.ba-outside-events.min.js"></script>

<script src="js/jquery.responsive-tabs.js"></script>

<script src="js/jquery.flexslider-min.js"></script>

<script src="js/jquery.fitvids.js"></script>

<script src="js/jquery-ui-1.10.4.custom.min.js"></script>

<script src="js/jquery.inview.min.js"></script>

<script src="js/script.js"></script>

<script src="js/ValidateDel.js"></script>

<script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>

<script type="text/javascript" src="../js/mainNavbar.js"></script>

<script type="text/javascript">

$(document).ready(function() {



		$("#otherCert").hide();	

		$("#year option:contains(None)").attr("selected", true);

		$('#certification').change(function() { 

			if($('#certification').val() == 'Other')

			{	//alert('to date');				

				$("#otherCert").show();

			}

			else

			{	

				$("#otherCert").hide();

			}

			if($('#certification').val() == 'No Certification')

			{	

				$("#year option:contains(None)").attr("selected", true);

			}

			else

				$("#year option:contains(Year)").attr("selected", true);

		});



		$('#submit').click(function() {

			if($('#certification').val() == 'Other' && $('#otherCert').val() == 'please specify')

			{

				alert('Please specify the certification');

				return false;

			}

			if($(('#certification').val() != 'No Certification') && $('#year').val() == '')

				return false;

		});

});

var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1", {invalidValue:"-1"});

var spryselect2 = new Spry.Widget.ValidationSelect("spryselect2");

</script>

</body>

</html>