<?php require_once "_inc_checkSession.php";?>
<?php $thisPage = basename($_SERVER['PHP_SELF']);?>
<?php require_once '_inc_config.php';?>
<?php require_once 'Connections/fer.php';?>
<?php include '_inc_Functions.php';?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}

if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}

if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "AddForm")) {
	$year2 = ($_POST['year2'] == '') ? date('Y') + 1 : $_POST['year2'];
	if (testYear4DOB($_POST['year']) && testYear4DOB($year2)) {
		$startDate = $_POST['year'] . '-' . $_POST['month'] . '-01';
		$endDate = ($_POST['year2'] == '') ? '' : $_POST['year2'] . '-' . $_POST['month2'] . '-01';
		$insertSQL = sprintf("INSERT INTO workexp (applicant_id, company, `position`, industry, job_salary, state, special_skills, country, `description`, startDate, endDate) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
			GetSQLValueString($_POST['applicant_id'], "int"),
			GetSQLValueString($_POST['company'], "text"),
			GetSQLValueString($_POST['position'], "text"),
			GetSQLValueString($_POST['industry'], "int"),
			GetSQLValueString($_POST['job_salary'], "text"),
			GetSQLValueString($_POST['State'], "text"),
			GetSQLValueString($_POST['special_skills'], "text"),
			GetSQLValueString($_POST['country'], "text"),
			GetSQLValueString($_POST['description'], "text"),
			GetSQLValueString($startDate, "date"),
			GetSQLValueString($endDate, "date"));

		mysql_select_db($database_fer, $fer);
		$Result1 = mysql_query($insertSQL, $fer) or die(mysql_error());

		$insertGoTo = "a_workExp.php?msg=" . urlencode("The Job has been added!");
		header(sprintf("Location: %s", $insertGoTo));
	} else {
		header("Location: a_workExp.php?error=The year(s) you supplied is(are) inconsistent with your Date of Birth!!!");
	}
}

$colname_workExp = "-1";
if (isset($_SESSION['FER_User']['id'])) {
	$colname_workExp = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_workExp = "SELECT * FROM workexp WHERE applicant_id = '$colname_workExp' AND endDate IS NOT NULL ORDER BY endDate DESC, startDate DESC";
$workExp = mysql_query($query_workExp, $fer) or die(mysql_error());
$row_workExp = mysql_fetch_assoc($workExp);
$totalRows_workExp = mysql_num_rows($workExp);

mysql_select_db($database_fer, $fer);
$query_currentJob = sprintf("SELECT * FROM workexp WHERE applicant_id = %s AND endDate IS NULL", GetSQLValueString($_SESSION['FER_User']['id'], "int"));
$currentJob = mysql_query($query_currentJob, $fer) or die(mysql_error());
$row_currentJob = mysql_fetch_assoc($currentJob);
$totalRows_currentJob = mysql_num_rows($currentJob);

//check if section status exists for this applicant and create it if otherwise
if (SectionStatusExists($FER_User['id']) == false) {
	CreateSectionStatus($FER_User['id']);
}

if ($totalRows_workExp || $totalRows_currentJob) {
	//update section status
	UpdateSectionStatus($FER_User['id'], 'workexp', '1');
} else {
	//update section status
	UpdateSectionStatus($FER_User['id'], 'workexp', '0');
}

$colname_sections = "-1";
if (isset($_SESSION['FER_User']['id'])) {
	$colname_sections = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_sections = sprintf("SELECT * FROM sectionstatus WHERE applicant_id = %s", GetSQLValueString($colname_sections, "int"));
$sections = mysql_query($query_sections, $fer) or die(mysql_error());
$row_sections = mysql_fetch_assoc($sections);
$totalRows_sections = mysql_num_rows($sections);

mysql_select_db($database_fer, $fer);
$query_industries = "SELECT * FROM job_categories";
$industries = mysql_query($query_industries, $fer) or die(mysql_error());
$row_industries = mysql_fetch_assoc($industries);
$totalRows_industries = mysql_num_rows($industries);

mysql_select_db($database_fer, $fer);
$query_countries = "SELECT * FROM countries";
$countries = mysql_query($query_countries, $fer) or die(mysql_error());
$row_countries = mysql_fetch_assoc($countries);
$totalRows_countries = mysql_num_rows($countries);

if ($totalRows_sections) {
	$cvStatus = 1;
	foreach ($row_sections as $value) {
		if ($value == 0) {
			$cvStatus = 0;
		}

	}
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="shortcut icon" href="favicon.png" />

	<title>Work Experience - <?php echo $FER_User['firstname']?> <?php echo $FER_User['surname']?>| <?php echo $config['shortname']?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">

	<!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include '-inc-header-top.php';?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include '-inc-header-nav.php';?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
		    <?php include '-inc-applicant-top.php';?>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
		    <div class="row">
		        <div class="col-sm-4 page-sidebar">
		            <?php include '-inc-applicant-side.php';?>
	            </div>
		        <!-- end .page-sidebar -->
	          <div class="col-sm-8 page-content">
		            <h3>
		                <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>
		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>
	                </div>-->
		                Work Experience
	                </h3>
                <form action="<?php echo $editFormAction;?>" method="post" name="AddForm" id="AddForm">
	                    <div class="white-container sign-up-form">
	                        <div>
	                            <h5>Add Job</h5>
                              <section>
	                                <?php if (isset($_GET['error'])) {?>
		                                <div class="alert alert-error">
		                                    <h6>Oops!</h6>
		                                    <p><?php echo $_GET['error']?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php }
?>
	                                <?php if (isset($_GET['msg'])) {?>
		                                <div class="alert alert-success">
		                                   <!--  <h6>Wow!</h6> -->
		                                    <p><?php echo $_GET['msg']?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php }
?>
                                <div class="row">
                                <div class="row">
								  <div class="col-sm-8" style="margin:4px 0px;">Company
										<span id="sprytextfield1">
										<input name="company" type="text" class="form-control" id="company" placeholder="Company Name...">
								  <span class="textfieldRequiredMsg">A value is required.</span></span> </div>

  								  <div class="col-sm-8" style="margin:4px 0px;">Industry
									<span id="spryselect6">
									<select name="industry" id="industry">
									  <option value="">--Select Company Industry--</option>
									  <?php
do {
	?>
									  <option value="<?php echo $row_industries['id_cat']?>"><?php echo $row_industries['cat_name']?></option>
									  <?php
} while ($row_industries = mysql_fetch_assoc($industries));
$rows = mysql_num_rows($industries);
if ($rows > 0) {
	mysql_data_seek($industries, 0);
	$row_industries = mysql_fetch_assoc($industries);
}
?>
								    </select>
								  <span class="selectRequiredMsg">Please select an item.</span></span> </div>

  								  <div class="col-sm-8" style="margin:4px 0px;">Country
									<select name="country" id="country">
									  <option value="">--Select Company Country--</option>
									  <?php
do {
	?>
									  <option value="<?php echo $row_countries['Country']?>"><?php echo $row_countries['Country']?></option>
									  <?php
} while ($row_countries = mysql_fetch_assoc($countries));
$rows = mysql_num_rows($countries);
if ($rows > 0) {
	mysql_data_seek($countries, 0);
	$row_countries = mysql_fetch_assoc($countries);
}
?>
                                    </select>
								</div>

  								  <div class="col-sm-8" style="margin:4px 0px;">State
									<span id="sprytextfield3">
									<input name="State" type="text" class="form-control" id="State" placeholder="Job state...">
								  <span class="textfieldRequiredMsg">A value is required.</span></span> </div>

								  <div class="col-sm-8" style="margin:4px 0px;">Position/Job Title
										<span id="sprytextfield2">
										<input name="position" type="text" class="form-control" id="position" placeholder="Position Held...">
								  <span class="textfieldRequiredMsg">A value is required.</span></span> </div>

									<div class="col-sm-8" style="margin:4px 0px;">Job Description(2500 Characters maximum)
									  <span id="sprytextarea1">
                                      <textarea name="description" rows="3" class="form-control" id="description" placeholder="" type="text"></textarea>
                                  <span id="countsprytextarea1">&nbsp;</span><span class="textareaRequiredMsg">A value is required.</span><span class="textareaMinCharsMsg">Minimum number of characters not met.</span><span class="textareaMaxCharsMsg">Exceeded maximum number of characters.</span></span> </div>

  								  <div class="col-sm-8" style="margin:0px 0px;">Job Current/Last Salary
									<span id="sprytextfield4">
                                    <input name="job_salary" type="text" class="form-control" id="job_salary" placeholder="Job Current/Last Salary...">
                                  <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldInvalidFormatMsg">Invalid format.</span></span> </div>

  									<div class="col-sm-8" style="margin:4px 0px;">List any special skills or experience you acquired while on this job
									  <span id="sprytextarea2">
                                      <textarea name="special_skills" rows="3" class="form-control" id="special_skills" placeholder="" type="text"></textarea>
                                  <span id="countsprytextarea2">&nbsp;</span><span class="textareaRequiredMsg">A value is required.</span><span class="textareaMinCharsMsg">Minimum number of characters not met.</span><span class="textareaMaxCharsMsg">Exceeded maximum number of characters.</span></span> </div>

								</div>
                                <div class="row">
									<div class="col-sm-3">Start date:
										<span id="sprydate1">
										<select name="month" id="month">
										  <option value="">Month</option>
										  <?php
for ($m = 1; $m <= 12; $m++) {
	?>
										  <option value="<?php echo $m?>" ><?php echo date("M", mktime(0, 0, 0, $m, 10));?></option>
										  <?php }
?>
									    </select>
								  <span class="selectRequiredMsg">Select an item.</span></span> </div>

								  <div class="col-sm-3"><br />
								    <span id="spryselect2">
								    <select name="year" id="year">
								      <option value="" selected="selected">Year</option>
								      <?php
for ($y = date("Y"); $y >= 1960; $y--) {
	?>
								      <option value="<?php echo $y?>" ><?php echo $y?></option>
								      <?php }
?>
							        </select>
							      <span class="selectRequiredMsg">Select an item.</span></span></div>

                                  <div class="col-sm-3"><br />
                                      <input type="checkbox" name="currentJob" id="currentJob" />
                                    <label for="currentJob" style="font-size:10px;">I currently work here</label>
                                    </div>
                                </div>
                                <div class="row" id="endDateArea">
                                  <div class="col-sm-3">End date:
                                    <span id="spryselect3">
                                    <select name="month2" id="month2">
                                      <option value="">Month</option>
                                      <?php
for ($m = 1; $m <= 12; $m++) {
	?>
                                      <option value="<?php echo $m?>"><?php echo date("M", mktime(0, 0, 0, $m, 10));?></option>
                                      <?php }
?>
                                    </select>
                                  <span class="selectRequiredMsg">Select an item.</span></span> </div>
                                  <div class="col-sm-3"><br />
                                    <span id="spryselect4">
                                    <select name="year2" id="year2">
                                      <option value="">Year</option>
                                      <?php
for ($y = date("Y"); $y >= 1960; $y--) {
	?>
                                      <option value="<?php echo $y?>"><?php echo $y?></option>
                                      <?php }
?>
                                    </select>
                                  <span class="selectRequiredMsg">Select an item.</span></span></div>
                                </div>
                                <?php // form select field end ?>
                                <span class="row endDateArea">
                                <input name="applicant_id" type="hidden" id="applicant_id" value="<?php echo $_SESSION['FER_User']['id'];?>" />
                                <input name="MM_insert" type="hidden" id="MM_insert" value="AddForm" />
                                </span></div>
                              </section>
</div>
                            <hr class="mt60">
                            <div class="clearfix" id="captchaArea">
                                <input name="submit" type="submit" class="btn btn-default pull-right" id="submit" value="Add" />
                            </div>
	                    </div>
                  </form>
                  <?php //start of listing ?>
                <div class="candidates-item candidates-single-item">
<?php if ($totalRows_workExp > 0 || $totalRows_currentJob > 0) { // Show if recordset not empty ?>
				 <table class="table-striped">
                      <thead>
                        <tr>
                          <th width="30%"><strong>Company</strong></th>
                          <th width="30%"><strong>Description</strong></th>
                          <th width="20%">Period</th>
                          <th width="20%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if ($totalRows_currentJob > 0) { // Show if recordset not empty ?>
                    <tr>
                      <td valign="top" style="word-wrap: break-word;"><strong><?php echo $row_currentJob['company'];?></strong><br />
                      <?php echo $row_currentJob['position'];?><br>
                      <em><?php echo $row_currentJob['job_level'];?></em>
                      </td>
                      <td valign="top" align="justify" style="word-wrap: break-word;"><?php echo $row_currentJob['description'];?></td>
                      <td valign="top"><?php echo date("M Y", strtotime($row_currentJob['startDate']));?> - Date</td>
                      <td align="center" valign="top"><a href="a_editWorkExp.php?id=<?php echo $row_currentJob['id'];?>" class="fa fa-edit"></a>
                       &nbsp;&nbsp;<a href="#" onclick=" return ValidateDel('<?php echo $row_currentJob['id'];?>','a_delWork.php?id=','Are you sure you want to DELETE THIS ITEM? (This Action is NOT reversible)!!!');" class="fa fa-trash-o"></a></td>
                    </tr>
                    <?php } // Show if recordset not empty ?>
                   <?php } // Show if recordset not empty ?>
<?php if ($totalRows_workExp > 0) { //previous jobs?>
<?php do {?>
                  <tr>
                    <td valign="top" style="word-wrap: break-word;"><strong><?php echo $row_workExp['company'];?></strong><br />
                      <?php echo $row_workExp['position'];?><br>
                      <em><?php echo $row_workExp['job_level'];?></em>
                    </td>
                    <td valign="top" align="justify" style="word-wrap: break-word;"><?php echo $row_workExp['description'];?></td>
                    <td valign="top"><?php echo date("M Y", strtotime($row_workExp['startDate']));?> - <?php echo date("M Y", strtotime($row_workExp['endDate']));?></td>
                    <td align="center" valign="top"><p><a href="a_editWorkExp.php?id=<?php echo $row_workExp['id'];?>" class="fa fa-edit"></a></p>
                      <p><a href="#" onclick=" return ValidateDel('<?php echo $row_workExp['id'];?>','a_delWork.php?id=','Are you sure you want to DELETE THIS ITEM? (This Action is NOT reversible)!!!');" class="fa fa-trash-o"></a></p></td>
                  </tr>
                  <?php } while ($row_workExp = mysql_fetch_assoc($workExp));?>
                  <?php }
?>
                      </tbody>
                  </table>
                  </div>
<?php //end of listing ?>
	          </div>
		        <!-- end .page-content -->
	        </div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include '-inc-footer-top.php';?>

		<div class="copyright">
			<?php include '-inc-footer-bottom.php';?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
<script src="js/ValidateDel.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	//reset all date dropdowns
	$("#month").val('');
	$("#year").val('');
	$("#month2").val('');
	$("#year2").val('');

	//on edit page, set values based on check!
	if ($('#currentJob').is(':checked')) {
		$("#endDateArea").hide();
		$("#month2").val('');
		$("#year2").val('');
	} else {
		$("#endDateArea").show();
	}

	$('#currentJob').change(function() {
		if ($('#currentJob').is(':checked')) {
			$("#endDateArea").hide();
			$("#month2").val('');
			$("#year2").val('');
		} else {
			$("#endDateArea").show();
		}
	});

	$('#AddForm').submit(function()
	{
		if($('#currentJob').is(':checked'))
		{//current job case
			today = new Date();
			startDate = new Date($('#year').val(),$('#month').val(),01);
			if(startDate > today)
			{
				alert('Your Start Date is in the FUTURE! Please correct.');
				return false;
			}
		}
		else
		{
			//check if end date is filled
			if($('#month2').val() == '' || $('#year2').val() == '') {
				alert("Please supply your End Date for this Job or check 'I currently work here' if this is your current job");
				return false;
			}

			startDate = new Date($('#year').val(),$('#month').val(),01);
			endDate = new Date($('#year2').val(),$('#month2').val(),01);
			if (startDate >= endDate)
			{
				alert('Your Start Date is greater than your End Date! Please correct.');
				return false;
			}


		}
	});
});
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {validateOn:["blur", "change"]});
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "none", {validateOn:["blur", "change"]});
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1", {validateOn:["blur", "change"], counterId:"countsprytextarea1", minChars:10, maxChars:2500});
var spryselect1 = new Spry.Widget.ValidationSelect("sprydate1", {validateOn:["blur", "change"]});
var spryselect2 = new Spry.Widget.ValidationSelect("spryselect2", {validateOn:["blur", "change"]});
var spryselect3 = new Spry.Widget.ValidationSelect("spryselect3", {validateOn:["change", "blur"], isRequired:false});
var spryselect4 = new Spry.Widget.ValidationSelect("spryselect4", {validateOn:["change", "blur"], isRequired:false});
var spryselect5 = new Spry.Widget.ValidationSelect("spryselect5");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea2", {validateOn:["blur", "change"], counterId:"countsprytextarea2", minChars:10, maxChars:2500});
var spryselect6 = new Spry.Widget.ValidationSelect("spryselect6", {validateOn:["change", "blur"]});
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3", "none", {validateOn:["blur", "change"]});
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4", "currency", {validateOn:["blur", "change"]});
</script>
</body>
</html>