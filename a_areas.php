<?php require_once "_inc_checkSession.php";?>
<?php require_once "_inc_applicantsOnly.php";?>
<?php $thisPage = basename($_SERVER['PHP_SELF']);?>
<?php require_once '_inc_config.php';?>
<?php require_once 'Connections/fer.php';?>
<?php include '_inc_Functions.php';?>
<?php

if (!function_exists("GetSQLValueString")) {

	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {

		if (PHP_VERSION < 6) {

			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {

			case "text":

				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";

				break;

			case "long":

			case "int":

				$theValue = ($theValue != "") ? intval($theValue) : "NULL";

				break;

			case "double":

				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";

				break;

			case "date":

				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";

				break;

			case "defined":

				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;

				break;

		}

		return $theValue;

	}

}

mysql_select_db($database_fer, $fer);

$query_categories = "SELECT * FROM job_categories ORDER BY cat_name ASC";

$categories = mysql_query($query_categories, $fer) or die(mysql_error());

$row_categories = mysql_fetch_assoc($categories);

$totalRows_categories = mysql_num_rows($categories);

$editFormAction = $_SERVER['PHP_SELF'];

if (isset($_SERVER['QUERY_STRING'])) {

	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);

}

$app_id = $FER_User['id'];

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "AreasForm")) {

	//echo "submitted! <br>";

	//print_r($_POST['areas']); die;

	mysql_select_db($database_fer, $fer);

	//delete all current areas of interest values for this applicant

	$deleteSQL = "DELETE FROM applicants_categories WHERE app_id = '$app_id'";

	$deleteRS = mysql_query($deleteSQL, $fer) or die(mysql_error());

	//insert new values

	foreach ($_POST['areas'] as $area_id) {

		$insertSQL = "INSERT INTO applicants_categories (app_id, cat_id) VALUES ('$app_id', '$area_id')";

		$insertRS = mysql_query($insertSQL, $fer) or die(mysql_error());

	}

	//Update Job level and type

	$jobLevel = $_POST['jobLevel'];
	$jobType = $_POST['jobType'];

	$updateSQL = sprintf("UPDATE applicants SET prefJobLevel = %s, prefJobType = %s WHERE id = '$app_id'", GetSQLValueString($jobLevel, "text"), GetSQLValueString($jobType, "text"));

	$updateRS = mysql_query($updateSQL, $fer) or die(mysql_error());

	//update section status

	UpdateSectionStatus($FER_User['id'], 'areas', '1');

	$updateGoTo = "a_areas.php?msg=" . urlencode("Your options have been saved!");

	if (isset($_SERVER['QUERY_STRING'])) {

		$updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";

		$updateGoTo .= $_SERVER['QUERY_STRING'];

	}

	header(sprintf("Location: %s", $updateGoTo));

}

//check if section status exists for this applicant and create it if otherwise

if (SectionStatusExists($FER_User['id']) == false) {

	CreateSectionStatus($FER_User['id']);

}

$colname_sections = "-1";

if (isset($_SESSION['FER_User']['id'])) {

	$colname_sections = $_SESSION['FER_User']['id'];

}

mysql_select_db($database_fer, $fer);

$query_sections = sprintf("SELECT * FROM sectionstatus WHERE applicant_id = %s", GetSQLValueString($colname_sections, "int"));

$sections = mysql_query($query_sections, $fer) or die(mysql_error());

$row_sections = mysql_fetch_assoc($sections);

$totalRows_sections = mysql_num_rows($sections);

mysql_select_db($database_fer, $fer);

$query_current_areas = "SELECT * FROM applicants_categories WHERE app_id = '$app_id'";

$current_areas = mysql_query($query_current_areas, $fer) or die(mysql_error());

$row_current_areas = mysql_fetch_assoc($current_areas);

$totalRows_current_areas = mysql_num_rows($current_areas);

do {

	$myAreas[] = $row_current_areas['cat_id'];

} while ($row_current_areas = mysql_fetch_assoc($current_areas));

if ($totalRows_sections) {

	$cvStatus = 1;

	foreach ($row_sections as $value) {

		if ($value == 0) {
			$cvStatus = 0;
		}

	}

}

?>

<!doctype html>

<html lang="en">

<head>

	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">



	<link rel="shortcut icon" href="favicon.png" />



	<title>Professional Certifications - <?php echo $FER_User['firstname']?> <?php echo $FER_User['surname']?>| <?php echo $config['shortname']?> Recruitment Portal</title>



	<!-- Stylesheets -->

	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/bootstrap.css">

	<link rel="stylesheet" href="css/font-awesome.min.css">

	<link rel="stylesheet" href="css/flexslider.css">

	<link rel="stylesheet" href="css/style.css">

	<link rel="stylesheet" href="css/responsive.css">

    <link rel="stylesheet" href="css/color/green.css">

    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">

    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">

    <link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">

    <link href="../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">



	<!--[if IE 9]>

		<script src="js/media.match.min.js"></script>

	<![endif]-->

<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>

<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>

<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>

<script src="../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>

</head>



<body>

<div id="main-wrapper">



	<header id="header" class="header-style-1">

		<div class="header-top-bar">

			<?php include '-inc-header-top.php';?>

             <!-- end .container -->

		</div> <!-- end .header-top-bar -->



		<div class="header-nav-bar">

			<?php include '-inc-header-nav.php';?>

             <!-- end .container -->



			<div id="mobile-menu-container" class="container">

				<div class="login-register"></div>

				<div class="menu"></div>

			</div>

		</div> <!-- end .header-nav-bar -->



		<div class="header-page-title">

		    <?php include '-inc-applicant-top.php';?>

		</div>



	</header> <!-- end #header -->



	<div id="page-content">

		<div class="container">

		    <div class="row">

		        <div class="col-sm-4 page-sidebar">

		            <?php include '-inc-applicant-side.php';?>

	            </div>

		        <!-- end .page-sidebar -->

	          <div class="col-sm-8 page-content">

		            <h3>

		                <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>

		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>

	                </div>-->

		               Areas of Interest

                </h3>

                <form action="<?php echo $editFormAction;?>" method="post" name="AreasForm" id="AreasForm">

	                    <div class="white-container sign-up-form">

	                        <div>

	                            <h5>Areas of Interest</h5>

                              <section>

	                                <?php if (isset($_GET['error'])) {?>

		                                <div class="alert alert-error">

		                                    <h6>Oops!</h6>

		                                    <p><?php echo $_GET['error']?></p>

	                                    <a href="#" class="close fa fa-times"></a></div>

		                                <?php }
?>

	                                <?php if (isset($_GET['msg'])) {?>

		                                <div class="alert alert-success">

		                                   <!--  <h6>Wow!</h6> -->

		                                    <p><?php echo $_GET['msg']?></p>

	                                    <a href="#" class="close fa fa-times"></a></div>

		                                <?php }
?>

                                <div class="row">

                                	<div class="" style="margin:5px 0px;">

                                    Select the areas/categories you are interested in, this will determine the vacancy alerts you receive

                                    </div>

                                    <hr />

                     <table width="100%" border="0" cellspacing="0" cellpadding="5">

                       <tr>

                         <td valign="top"><a href="#" id="selectAll" class="btn btn-red">Select All</a></td>

                         <td>&nbsp;</td>

                       </tr>

                       <tr>

                         <?php $count = 1;?>

                         <?php do {
	?>

                         <td width="50%" valign="top"><label>

                           <input name="areas[]" type="checkbox" id="area_<?php echo $row_categories['id_cat'];?>" value="<?php echo $row_categories['id_cat'];?>" <?php if (in_array($row_categories['id_cat'], $myAreas)) {
		echo "checked='checked'";
	}
	?>  />

                           <?php echo $row_categories['cat_name'];?></label></td>

                         <?php $count++;

	if ($count % 2 == 0) {

		echo "</tr> <tr>";

	}
	?>

                         <?php } while ($row_categories = mysql_fetch_assoc($categories));?>

                         <?php if ($count % 2 == 0) {
	echo "</tr>";
}
?>

                       </tr>

                     </table>

                     <hr />

                     <p><input type="hidden" name="MM_insert" value="AddForm" />

                    <input type="hidden" name="MM_update" value="AreasForm" /></p>

                    <div class="row">

                      <div class="col-sm-6">Preferred Job Level

						  <select name="jobLevel" class="width244" id="jobLevel">

						    <option value="" selected="selected" <?php if (!(strcmp("", $FER_User['prefJobLevel']))) {echo "selected=\"selected\"";}
?>>Any Level</option>

						    <option value="Managerial" <?php if (!(strcmp("Managerial", $FER_User['prefJobLevel']))) {echo "selected=\"selected\"";}
?>>Managerial</option>

						    <option value="Non-Managerial" <?php if (!(strcmp("Non-Managerial", $FER_User['prefJobLevel']))) {echo "selected=\"selected\"";}
?>>Non-Managerial</option>

					    </select>

                      </div>

					<div class="col-sm-6">Preferred Job Type

						<select name="jobType" class="width244" id="jobType">

						  <option value="" <?php if (!(strcmp("", $FER_User['prefJobType']))) {echo "selected=\"selected\"";}
?>>Any Type</option>

						  <option value="Full Time" <?php if (!(strcmp("Full Time", $FER_User['prefJobType']))) {echo "selected=\"selected\"";}
?>>Full Time</option>

						  <option value="Part Time" <?php if (!(strcmp("Part Time", $FER_User['prefJobType']))) {echo "selected=\"selected\"";}
?>>Part Time</option>

					    </select>

                    </div>

					<input name="applicant_id" type="hidden" id="applicant_id" value="<?php echo $_SESSION['FER_User']['id'];?>" />

								</div>

                                </div>

                              </section>

</div>

                            <hr class="mt60">

                            <div class="clearfix" id="captchaArea">

                              <input name="submit" type="submit" class="btn btn-default pull-right" id="submit" value="Save" />

                            </div>

	                    </div>

                  </form></div>

		        <!-- end .page-content -->

	        </div>

		</div> <!-- end .container -->

	</div> <!-- end #page-content -->



	<footer id="footer">

		<?php include '-inc-footer-top.php';?>



		<div class="copyright">

			<?php include '-inc-footer-bottom.php';?>

		</div>

	</footer> <!-- end #footer -->



</div> <!-- end #main-wrapper -->



<!-- Scripts -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>

<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>

<script src="js/maplace.min.js"></script>

<script src="js/jquery.ba-outside-events.min.js"></script>

<script src="js/jquery.responsive-tabs.js"></script>

<script src="js/jquery.flexslider-min.js"></script>

<script src="js/jquery.fitvids.js"></script>

<script src="js/jquery-ui-1.10.4.custom.min.js"></script>

<script src="js/jquery.inview.min.js"></script>

<script src="js/script.js"></script>

<script src="js/ValidateDel.js"></script>

<script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>

<script type="text/javascript" src="../js/mainNavbar.js"></script>

<script type="text/javascript">

$(document).ready(function() {

	$('#selectAll').click(function(e){

		e.preventDefault();

		if($('#selectAll').text() == 'Select All')

		{

			$('#AreasForm input:checkbox').each(function(){

				this.click()

			});

			$('#selectAll').text('Deselect All');

		}

		else

		{

			$('#AreasForm input:checkbox').each(function(){

				this.click()

			});

			$('#selectAll').text('Select All');

		}

	});



});

</script>

</body>

</html>