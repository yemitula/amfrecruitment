<?php $thispage = basename( $_SERVER['PHP_SELF'] ); ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<link rel="shortcut icon" href="favicon.png" />
    
	<title>Registration Successful! | ABC Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">

	<!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include('-inc-header-top.php'); ?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include('-inc-header-nav.php'); ?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
			<div class="container">
				<h1>Register</h1>

				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					<li><a href="#">Register</a></li>
				</ul>
			</div>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 page-content">
					

					<div class="white-container sign-up-form">
						<div>
							<h2>Registration Successful!</h2>
                            <p>Thank you for registering.  </p>
                            <p>Your Account has been created and your automatically generated password has been sent to your email address (<?php echo $_GET['e'] ?>).
                            </p>
                            <div class="alert alert-warning">
						<h6>Important Notice</h6>
						<p>Please ensure that you also check your SPAM box if you can't find the message in your inbox.</p>
					</div>
						</div>
						<div class="clearfix">
							<a href="a_login.php" class="btn btn-default btn-large pull-right">Continue to Login</a>
						</div>
					</div>
				</div> <!-- end .page-content -->
			</div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include('-inc-footer-top.php'); ?>

		<div class="copyright">
			<?php include('-inc-footer-bottom.php'); ?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>

</body>
</html>