<?php require_once("_inc_checkSession.php"); ?>
<?php require_once("_inc_applicantsOnly.php"); ?>
<?php $thisPage = basename( $_SERVER['PHP_SELF'] ); ?>
<?php require_once('_inc_config.php'); ?>
<?php require_once('Connections/fer.php'); ?>
<?php include('_inc_Functions.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "AddForm")) {
  $updateSQL = sprintf("UPDATE secschool SET school=%s, certificate=%s, `date`=%s WHERE id=%s",
                       GetSQLValueString($_POST['school'], "text"),
                       GetSQLValueString(($_POST['otherCert'])? $_POST['otherCert'] : $_POST['certificate'], "text"),
                       GetSQLValueString($_POST['year'].'-'.$_POST['month'].'-01' , "date"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($updateSQL, $fer) or die(mysql_error());

  $updateGoTo = "a_educational.php?msg=".urlencode("Secondary School successfully Edited");
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}


$colname_sections = "-1";
if (isset($_SESSION['FER_User']['id'])) {
  $colname_sections = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_sections = sprintf("SELECT * FROM sectionstatus WHERE applicant_id = %s", GetSQLValueString($colname_sections, "int"));
$sections = mysql_query($query_sections, $fer) or die(mysql_error());
$row_sections = mysql_fetch_assoc($sections);
$totalRows_sections = mysql_num_rows($sections);

$colname_school = "-1";
if (isset($_GET['id'])) {
  $colname_school = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_school = sprintf("SELECT * FROM secschool WHERE id = %s", GetSQLValueString($colname_school, "int"));
$school = mysql_query($query_school, $fer) or die(mysql_error());
$row_school = mysql_fetch_assoc($school);
$totalRows_school = mysql_num_rows($school);

if($totalRows_sections)
{
$cvStatus = 1;
	foreach($row_sections as $value)
	{
		if($value == 0)
			$cvStatus = 0;
	}
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<link rel="shortcut icon" href="favicon.png" />
    
	<title>Educational/Academic Qualifications - <?php echo $FER_User['firstname'] ?> <?php echo $FER_User['surname'] ?>| <?php echo $config['shortname'] ?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">

	<!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include('-inc-header-top.php'); ?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include('-inc-header-nav.php'); ?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
		    <?php include('-inc-applicant-top.php'); ?>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
		    <div class="row">
		        <div class="col-sm-4 page-sidebar">
		            <?php include('-inc-applicant-side.php'); ?>
	            </div>
		        <!-- end .page-sidebar -->
		        <div class="col-sm-8 page-content">
		            <h3>
		                <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>
		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>
	                </div>-->
		                Educational/Academic Qualifications
	                </h3>
		            <form id="formAddEduc" name="formAddEduc" method="post" action="">
		                <div class="white-container sign-up-form">
		                    <div>
		                        <h5>Edit SECONDARY EDUCATION</h5>
		                        <section>
		                            <?php if (isset($_GET['error'])) { ?>
		                            <div class="alert alert-error">
		                                <h6>Oops!</h6>
		                                <p><?php echo $_GET['error'] ?></p>
		                                <a href="#" class="close fa fa-times"></a></div>
		                            <?php } ?>
		                            <?php if (isset($_GET['msg'])) { ?>
		                            <div class="alert alert-success">
		                                <h6>Wow!</h6>
		                                <p><?php echo $_GET['msg'] ?></p>
		                                <a href="#" class="close fa fa-times"></a></div>
		                            <?php } ?>
		                            <div class="row"></div>
		                            <div class="row secArea">
		                                <div class="col-sm-9">
		                                    <label for="school">School</label>
		                                    <input name="school" type="text" class="fullBox" id="school" placeholder="e.g. Salvation Army High School, Ibesa, Osun State" value="<?php echo $row_school['school']; ?>" />
	                                    </div>
	                                </div>
		                            <div class="row secArea">
		                                <div class="col-sm-6">
		                                    <label for="certificate">Certificate</label>
		                                    <select name="certificate" id="certificate">
		                                        <option value="WAEC SSCE">WAEC SSCE</option>
		                                        <option value="NECO SSCE">NECO SSCE</option>
		                                        <option value="Other">Other</option>
	                                        </select>
	                                    </div>
		                                <div class="col-sm-6" id="otherCertArea">
		                                    <label for="otherCert">Specify Certificate</label>
		                                    <input type="text" name="otherCert" id="otherCert" placeholder="please specify" />
		                                    </select>
	                                    </div>
	                                </div>
		                            <div class="row dateArea">
		                                <div class="col-sm-6">
		                                    <label for="cgpaScore">Date Obtained </label>
		                                    <br>
		                                    <select name="month" id="month" style="width:150px; display:inline">
		                                        <option value="">Month</option>
		                                        <?php 
							for($m=1;$m<=12;$m++) {
							?>
		                                        <option value="<?php echo $m ?>" <?php if (date("m",strtotime($row_school['date'])) == $m) echo "selected='selected'" ?>><?php echo date("M", mktime(0, 0, 0, $m, 10)); ?></option>
		                                        <?php } ?>
	                                        </select>
		                                    &nbsp;
		                                    <select name="year" id="year" style="width:150px; display:inline">
		                                        <option value="" selected="selected">Year</option>
		                                        <?php 
							for($y=date("Y");$y>=1940;$y--) {
							?>
		                                        <option value="<?php echo $y ?>" <?php if (date("Y",strtotime($row_school['date'])) == $y) echo "selected='selected'" ?>><?php echo $y ?></option>
		                                        <?php } ?>
	                                        </select>
	                                    </div>
	                                </div>
		                        </section>
	                        </div>
		                    <hr class="mt60">
		                    <div class="clearfix"> <a href="a_educational.php" class="btn btn-red">Cancel</a> &nbsp; <input name="submit" type="submit" class="btn btn-default" id="submit" value="Update" />

                            </div>
		                    <input type="hidden" name="MM_insert" value="AddForm" />
                  <input type="hidden" name="MM_update" value="AddForm" />
		                <input name="id" type="hidden" id="id" value="<?php echo $row_school['id']; ?>" />
		                </div>
		            </form>
                    
		        </div>
		        <!-- end .page-content -->
	        </div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include('-inc-footer-top.php'); ?>

		<div class="copyright">
			<?php include('-inc-footer-bottom.php'); ?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
<script src="js/ValidateDel.js"></script>
<script type="text/javascript">
function OptionExists(selectID, key) {
	//alert('invoke');
	var seen = false;
	var selector = '#' + selectID + ' option';
	//alert(selector);
	$(selector).each(function(){
		//alert(this.value);
		if (this.value == key) {
			seen = true;
			return false;
		}
	});
	//alert(seen);
	return seen;
}

$(document).ready(function() {	
$('#otherCertArea').hide();
if(OptionExists('certificate','<?php echo $row_school['certificate'] ?>')) {
	$("#certificate").val('<?php echo $row_school['certificate'] ?>');
} else {
	$("#certificate").val('Other');
	$("#otherCert").val('<?php echo $row_school['certificate'] ?>');
	$('#otherCertArea').show();
}
	
	$('#certificate').change(function() {
		if($('#certificate').val() == 'Other')
			$('#otherCertArea').show();
		else
		{	$('#otherCertArea').hide();
			$('#otherCert').val('');
		}
	});
	
	$('#AddForm').submit(function() 
	{
		if($('#school').val() == '')
		{
			alert('SCHOOL is required!');
			return false;
		}
		if($('#certificate').val() == 'Other' && $('#otherCert').val() == '')
		{
			alert('You MUST specify the OTHER certificate!');
			return false;
		}
	});
});
</script>
</body>
</html>