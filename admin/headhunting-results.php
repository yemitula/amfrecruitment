<?php ini_set('max_execution_time', 3600); //execution time in seconds ?>
<?php error_reporting(0); ?>
<?php require_once("_inc_checkSession.php"); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php include('../_inc_Functions.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if(isset($_GET['headhunt'])) {
	//print_r($_GET); die;
	$_SESSION['formHeadhunt'] = $_GET;
	extract($_GET);


/*	$maxRows_applicants = 30;
	$pageNum_applicants = 0;
	if (isset($_GET['pageNum_applicants'])) {
	  $pageNum_applicants = $_GET['pageNum_applicants'];
	}
	$startRow_applicants = $pageNum_applicants * $maxRows_applicants;*/
	
	mysql_select_db($database_fer, $fer);

	//$mysql->query("SET SQL_BIG_SELECTS=1");
	$query_applicants = "SET amfacili_recruit MAX_JOIN_SIZE=1";
	$query_applicants = "SELECT DISTINCT(a.id) AS id, a.surname, a.firstname, a.middlename, a.gender, a.email, a.gsm  FROM applicants a LEFT JOIN sectionstatus s ON (a.id = s.applicant_id) LEFT JOIN nysc n ON (a.id = n.applicant_id) WHERE 1=1";
		
	//gender
	if($gender != '') {
		$query_applicants .= " AND gender = '$gender'";
	}
	//age
	if($ageOp != '') {
		$year = GetYear($age);
		if($ageOp == 'BETWEEN') {
			$year2 = GetYear($age2);
			$query_applicants .= " AND DATE_FORMAT( dob, '%Y' ) <= '$year' AND DATE_FORMAT( dob, '%Y' ) >= '$year2' ";
		} else {
			$query_applicants .= " AND DATE_FORMAT( dob, '%Y' ) $ageOp '$year' ";
		}
	}

	//discipline
	if($discipline != '') {
		$query_applicants .= " AND discipline = '$discipline'";
	}

	//nysc
	if($nyscStatus != '') {
		$query_applicants .= " AND n.nyscStatus = '$nyscStatus'";
	}
	//profcerts
	if($profcert != '')
	{	
		if($profcert == 'No')
			$query_applicants .= " AND (SELECT COUNT(p.applicant_id) FROM profcerts p WHERE a.id = p.applicant_id AND p.certification = 'No Certification') > 0";
		else
			$query_applicants .= " AND (SELECT COUNT(p.applicant_id) FROM profcerts p WHERE a.id = p.applicant_id AND p.certification <> 'No Certification') > 0";
	}
	//course
	if($course != '') {
		$query_applicants .= " AND (SELECT COUNT(t.applicant_id) FROM tertiary t WHERE a.id = t.applicant_id AND t.course LIKE '%$course%') > 0";
	}
	//institution
	if($institution != '') {
		$query_applicants .= " AND (SELECT COUNT(t.applicant_id) FROM tertiary t WHERE a.id = t.applicant_id AND t.institution LIKE '%$institution%') > 0";
	}
	//class
	if($class != '') {
		$query_applicants .= " AND (SELECT COUNT(t.applicant_id) FROM tertiary t WHERE a.id = t.applicant_id AND t.class = '$class') > 0";
	}
	//country of eduction
	if($country != '') {
		$query_applicants .= " AND (SELECT COUNT(t.applicant_id) FROM tertiary t WHERE a.id = t.applicant_id AND uni_country = '$country') > 0";
	}

	//work experience position
	if($position != '') {
		$query_applicants .= " AND (SELECT COUNT(w.applicant_id) FROM workexp w WHERE a.id = w.applicant_id AND w.position LIKE '%$position%') > 0";
	}
	//work experience level
	/*if($job_level != '') {
		$query_applicants .= " AND (SELECT COUNT(w.applicant_id) FROM workexp w WHERE a.id = w.applicant_id AND w.job_level = '$job_level') > 0";
	}*/
	//work experience description
	if($description != '') {
		$query_applicants .= " AND (SELECT COUNT(w.description) FROM workexp w WHERE a.id = w.applicant_id AND w.description LIKE '%$description%') > 0";
	}
	//work experience description
	if($employer != '') {
		$query_applicants .= " AND (SELECT COUNT(w.company) FROM workexp w WHERE a.id = w.applicant_id AND w.company LIKE '%$employer%') > 0";
	}
	//location
	if($location != '') {
		if(strpos($location,',')) {
			//use advanced regex matching
			//change the , to |
			$location = str_replace(',','|',$location);
			$query_applicants .= " AND prefTestLoc REGEXP '$location'";
		} else {
			//normal LIKE matching
			$query_applicants .= " AND prefTestLoc LIKE '%$location%'";
		}	
	}
	//industry
	if($industry != '') {
		$query_applicants .= " AND (SELECT COUNT(w.industry) FROM workexp w WHERE a.id = w.applicant_id AND w.industry = '$industry') > 0";
	}

	//completed only?
	if($completed != "") {
	 	$query_applicants .=" AND s.status = '1'";
	}

	//preview query
	//die($query_applicants);
	//put query in a session variable for later reference
	$_SESSION['HeadHuntingQuery'] = $query_applicants;

	$query_applicants .= " ORDER BY surname";
	// $query_limit_applicants = sprintf("%s LIMIT %d, %d", $query_applicants, $startRow_applicants, $maxRows_applicants);
	$applicants = mysql_query($query_applicants, $fer) or die(mysql_error());
	$row_applicants = mysql_fetch_assoc($applicants);
	$totalRows_applicants = mysql_num_rows($applicants);
	
	$_SESSION['row_applicants'] = $row_applicants;

	/*$applicants2 = mysql_query($query_limit_applicants, $fer) or die(mysql_error());
	$row_applicants2 = mysql_fetch_assoc($applicants2);*/
/*	function export()
	{
			$line = array();
			$i= 1;

					do {
			// loop for each line
				$line[$i] = $row_applicants2;
				//$profileLink = "http://fitc-ng.com/fer/admin/applicant-details.php?id=".$row_applicants2['applicant_id'];
				//$line[$i]['profile'] = $profileLink;
				$i++; //increment counter
			} while ($row_applicants2 = mysql_fetch_assoc($applicants2));
			
			// do output to CSV
			$fp = fopen('php://output', 'w');
			if ($fp) {
				header('Content-Type: text/csv');
				header('Content-Disposition: attachment; filename="HeadHunting Shortlist - ('.date("d-m-Y").').csv"');
				header('Pragma: no-cache');
				header('Expires: 0');
				fputcsv($fp, array_keys($line[1]));
				for($i=1;$i<= count($line);$i++)
					fputcsv($fp, $line[$i]);
				die;
			}
					
	}	
*/
	/*if (isset($_GET['totalRows_applicants'])) {
	  $totalRows_applicants = $_GET['totalRows_applicants'];
	} else {
	  $all_applicants = mysql_query($query_applicants);
	  $totalRows_applicants = mysql_num_rows($all_applicants);
	}
	$totalPages_applicants = ceil($totalRows_applicants/$maxRows_applicants)-1;*/
}
/*$queryString_applicants = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_applicants") == false && 
        stristr($param, "totalRows_applicants") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_applicants = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_applicants = sprintf("&totalRows_applicants=%d%s", $totalRows_applicants, $queryString_applicants);*/
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Headhunting | <?php echo $config['shortname'] ?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link href="assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<link rel="stylesheet" href="assets/plugins/data-tables/DT_bootstrap.css" />
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('-inc-top.php'); ?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<?php include('-inc-navbar-side.php'); ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Headhunting <small>hunt for applicants matching your criteria</small></h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="vacancies.php">Applicants</a> <i class="icon-angle-right"></i></li>
			                <li><a href="headhunting.php">Headhunting</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#">Headhunting Results</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) { ?>
			    <div class="alert alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg'] ?></strong> </div>
			    <?php } ?>
			    <?php if ($totalRows_applicants == 0) { // Show if recordset empty ?>
			    <div class="row-fluid">
			        <div class="alert">
			            
			            <strong>Empty List!</strong> No applicants found matching your criteria. </div>
		        </div>
			    <?php } // Show if recordset empty ?>
                <?php if ($totalRows_applicants > 0) { // Show if recordset not empty ?>    
                
                <p>&nbsp;</p>
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN EXAMPLE TABLE PORTLET-->
			            <div class="portlet box light-grey">
			                <div class="portlet-title">
			                    <div class="caption">Headhunting Results</strong> </div>
		                    </div>
			                <div class="portlet-body">
			                    <form id="formShortlist" name="formShortlist" method="post" action="vacancy-applicants.php?id=<?php echo $row_vacancy['id'] ?>">
			                    <div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid"> <br>
			                        
                                    
			                        
                                    
			                        <table class="table table-striped table-bordered table-hover" id="sample_1">
			                            <thead>
			                                <tr>
			                                    <th width="5%"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
                                                <th width="5%">ID</th>
                                                <th width="30%">Name</th>
			                                    <th width="25%" >Email</th>
			                                    <th width="10%" >GSM</th>
			                                    <th width="10%" >Profile Status</th>
			                                    <th width="10%" >&nbsp;</th>
		                                    </tr>
		                                </thead>
			                            <tbody>
			                                <?php do { ?>
			                                <tr class="odd gradeX">
			                                    <td><input type="checkbox" name="shortlistedApps[]" id="<?php echo $row_applicants['id']; ?>" value="<?php echo $row_applicants['id']; ?>" <?php if ($row_applicants['shortlisted']) echo "checked='checked'" ?> class="checkboxes" /></td>
                                                <td><?php echo $row_applicants['id']; ?></td>
			                                    <td ><a href="applicant-details.php?id=<?php echo $row_applicants['id']; ?>" target="_blank" title="View Applicant Details" class="tooltips"><?php echo $row_applicants['surname']; ?> <?php echo $row_applicants['firstname']; ?></a></td>
			                                    <td ><?php echo $row_applicants['email'] ?></td>
			                                    <td ><?php echo $row_applicants['gsm'] ?></td>
			                                    <td ><?php echo CVComplete($row_applicants['id'])? '<span class="label label-success">complete</span>' : '<span class="label label-important">incomplete</span>'; ?></td>
			                                    <td >
			                                        <a href="applicant-details.php?id=<?php echo $row_applicants['id']; ?>" target="_blank" title="View Applicant Details" class="tooltips"><i class="icon-eye-open"></i></a>&nbsp;&nbsp;
		                                    </tr>
			                                <?php } while ($row_applicants = mysql_fetch_assoc($applicants)); ?>
		                                </tbody>
		                            </table>
			                        
                                    
		                        </div>
                                
                                <p>
                                <br><br>
                        <a href="headhunting.php">
                        <button class="btn black" type="button"><i class=" m-icon-big-swapleft"></i> Back to Criteria</button>
                      </a>
                        <a href="headhunting-results-export.php">
                        <button class="btn green" type="button"><i class=" icon-arrow-down"></i> Export Result</button>
                      </a>
                                </p>
                                </form>
		                    </div>
		                </div>
			            <!-- END EXAMPLE TABLE PORTLET-->
		            </div>
		        </div>
			    <?php } // Show if applicant list not empty ?>
			    <div class="clearfix"></div>
		    </div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include('-inc-footer.php'); ?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>   
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  
	<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
	<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>
	<script src="assets/scripts/table-advanced.js"></script>  
	<script src="assets/scripts/form-components.js"></script>     
	<!-- END PAGE LEVEL SCRIPTS -->  
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
	<script>
		jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   TableAdvanced.init();
		   FormComponents.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>