<?php require_once("_inc_checkSession.php"); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php require_once("../classes/class.upload.php"); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "formEmployer")) {

	$_SESSION['aForm'] =  $_POST;
	
	mysql_select_db($database_fer, $fer);
	//check email
	$email = $_POST['email'];
	$query_applicants = "SELECT * FROM applicants WHERE email = '$email'";
	$applicants = mysql_query($query_applicants, $fer) or die(mysql_error());
	$row_applicants = mysql_fetch_assoc($applicants);
	$totalRows_applicants = mysql_num_rows($applicants);
	
	if($totalRows_applicants) {
		//applicant already exists
		header("Location: applicant-add.php?error=".urlencode("This email has already been used for registration by another applicant!"));
		exit;
	}
	
	if ($_POST['dobYear'] && $_POST['dobMonth'] && $_POST['dobDay']) {
		$dob = $_POST['dobYear'].'-'.$_POST['dobMonth'].'-'.$_POST['dobDay'];
	}
	
  $insertSQL = sprintf("INSERT INTO applicants (email, password, title, surname, firstname, middlename, gender, marital, dob, nationality, stateOfOrigin, lga, languages, houseNumber, street, busStop, city, stateOfResidence, countryOfResidence, gsm, gsm2, prefTestLoc, email2, dateReg) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['password'], "text"),
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['surname'], "text"),
                       GetSQLValueString($_POST['firstname'], "text"),
                       GetSQLValueString($_POST['middlename'], "text"),
                       GetSQLValueString($_POST['gender'], "text"),
                       GetSQLValueString($_POST['marital'], "text"),
                       GetSQLValueString($dob, "date"),
                       GetSQLValueString($_POST['nationality'], "text"),
                       GetSQLValueString($_POST['stateOfOrigin'], "text"),
                       GetSQLValueString($_POST['lga'], "text"),
                       GetSQLValueString($_POST['languages'], "text"),
                       GetSQLValueString($_POST['houseNumber'], "text"),
                       GetSQLValueString($_POST['street'], "text"),
                       GetSQLValueString($_POST['busStop'], "text"),
                       GetSQLValueString($_POST['city'], "text"),
                       GetSQLValueString($_POST['stateOfResidence'], "text"),
                       GetSQLValueString($_POST['countryOfResidence'], "text"),
                       GetSQLValueString($_POST['gsm'], "text"),
                       GetSQLValueString($_POST['gsm2'], "text"),
                       GetSQLValueString($_POST['prefTestLoc'], "text"),
                       GetSQLValueString($_POST['email2'], "text"),
                       GetSQLValueString(date('Y-m-d h:i:s'), "date"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($insertSQL, $fer) or die(mysql_error());
  
  unset($_SESSION['aForm']);

  $insertGoTo = "applicants.php?msg=".urlencode("Applicant Added!");
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Applicants | <?php echo $config['shortname'] ?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
	<link href="../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
	<link href="../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
	<link href="../SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">
<script src="../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('-inc-top.php'); ?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<?php include('-inc-navbar-side.php'); ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Applicants <small>create applicant </small> </h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="applicants.php">Applicants</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#"> Create Applicant</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) { ?>
			    <div class="alert  alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg'] ?></strong> </div>
			    <?php } ?>
			    <?php if (isset($_GET['error'])) { ?>
			    <div class="alert alert-error">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['error'] ?></strong> </div>
			    <?php } ?>
			    <div id="dashboard">
			        <!-- BEGIN DASHBOARD STATS -->
			        <div class="row-fluid">
			            <div class="span12">
			                <!-- BEGIN SAMPLE FORM PORTLET-->
			                <div class="portlet box green tabbable">
			                    <div class="portlet-title">
			                        <div class="caption"> <i class="icon-reorder"></i> <span class="hidden-480">Applicant's Personal Details</span> </div>
		                        </div>
			                    <div class="portlet-body form">
			                        <div class="tabbable portlet-tabs">
			                            <!-- BEGIN FORM-->
			                            <form id="formPersonalDetails" name="formPersonalDetails" method="POST" action="<?php echo $editFormAction; ?>">
<div>
    <p></p>
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td align="right" valign="top">Email Address*</td>
            <td valign="top"><label for="email"></label>
                <span id="sprytextfield9">
                    <input name="email" type="text" class="width244" id="email" value="<?php echo $_SESSION['aForm']['email']; ?>"/>
                    <span class="textfieldRequiredMsg">A value is required.</span></span></td>
</tr>
        <tr>
            <td align="right" valign="top">Password*</td>
            <td><span id="sprytextfield3">
                <label for="password"></label>
                <input name="password" type="password" class="width244" id="password" />
                <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldMinCharsMsg">Minimum number of characters not met.</span></span></td>
</tr>
        <tr>
            <td align="right" valign="top">Retype Password*</td>
            <td><span id="spryconfirm1">
                <input name="password2" type="password" class="width244" id="password2" />
                <span class="confirmRequiredMsg">A value is required.</span><span class="confirmInvalidMsg">The values don't match.</span></span></td>
</tr>
        <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
        </tr>
        <tr>
            <td width="24%" align="right" valign="top">Title</td>
            <td width="76%" valign="top"><select type="text" id="title" name="title">
                <option selected="selected" value="" <?php if (!(strcmp("", $_SESSION['aForm']['title']))) {echo "selected=\"selected\"";} ?>>None</option>
                <br>
                <option value="Mr" <?php if (!(strcmp("Mr", $_SESSION['aForm']['title']))) {echo "selected=\"selected\"";} ?>>Mr</option>
                <option value="Ms" <?php if (!(strcmp("Ms", $_SESSION['aForm']['title']))) {echo "selected=\"selected\"";} ?>>Ms</option>
                <option value="Mrs" <?php if (!(strcmp("Mrs", $_SESSION['aForm']['title']))) {echo "selected=\"selected\"";} ?>>Mrs</option>
                <option value="Miss" <?php if (!(strcmp("Miss", $_SESSION['aForm']['title']))) {echo "selected=\"selected\"";} ?>>Miss</option>
                <option value="Dr" <?php if (!(strcmp("Dr", $_SESSION['aForm']['title']))) {echo "selected=\"selected\"";} ?>>Dr</option>
                <option value="Chief" <?php if (!(strcmp("Chief", $_SESSION['aForm']['title']))) {echo "selected=\"selected\"";} ?>>Chief</option>
                <option value="Prof" <?php if (!(strcmp("Prof", $_SESSION['aForm']['title']))) {echo "selected=\"selected\"";} ?>>Prof</option>
                <option value="Rev" <?php if (!(strcmp("Rev", $_SESSION['aForm']['title']))) {echo "selected=\"selected\"";} ?>>Rev</option>
                <option value="Alhaji" <?php if (!(strcmp("Alhaji", $_SESSION['aForm']['title']))) {echo "selected=\"selected\"";} ?>>Alhaji</option>
                <option value="Mallam" <?php if (!(strcmp("Mallam", $_SESSION['aForm']['title']))) {echo "selected=\"selected\"";} ?>>Mallam</option>
                <option value="Hajia" <?php if (!(strcmp("Hajia", $_SESSION['aForm']['title']))) {echo "selected=\"selected\"";} ?>>Hajia</option>
                <option value="Engr" <?php if (!(strcmp("Engr", $_SESSION['aForm']['title']))) {echo "selected=\"selected\"";} ?>>Engr</option>
                <option value="Arc" <?php if (!(strcmp("Arc", $_SESSION['aForm']['title']))) {echo "selected=\"selected\"";} ?>>Arc</option>
            </select></td>
        </tr>
        <tr>
            <td align="right" valign="top">First Name*</td>
            <td valign="top"><span id="sprytextfield2">
                <label for="firstname"></label>
                <input name="firstname" type="text" class="width244" id="firstname" value="<?php echo $_SESSION['aForm']['firstname']; ?>"/>
                <span class="textfieldRequiredMsg">A value is required.</span></span></td>
</tr>
        <tr>
            <td align="right" valign="top">Surname*</td>
            <td valign="top"><span id="sprytextfield1">
                <label for="surname"></label>
                <input name="surname" type="text" class="width244" id="surname" value="<?php echo $_SESSION['aForm']['surname']; ?>" />
                <span class="textfieldRequiredMsg">A value is required.</span></span></td>
</tr>
        <tr>
            <td align="right" valign="top">Middle Name</td>
            <td valign="top"><input name="middlename" type="text" class="width244" id="middlename" value="<?php echo $_SESSION['aForm']['middlename']; ?>"/></td>
        </tr>
        <tr>
            <td align="right" valign="top">Gender*</td>
            <td valign="top">
                <label>
                    <input  checked="checked"  type="radio" name="gender" value="Male" id="gender_0" />
                    Male</label>
                <label> &nbsp; <input <?php if (!(strcmp($_SESSION['aForm']['gender'],"Female"))) {echo "checked=\"checked\"";} ?>  name="gender" type="radio" id="gender_1" value="Female"/>
                    Female</label>
                <br />
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">Marital Status</td>
            <td valign="top"><span id="spryselect1">
                <select name="marital" id="marital">
                    <option value="" <?php if (!(strcmp("", $_SESSION['aForm']['marital']))) {echo "selected=\"selected\"";} ?>>Select One...</option>
                    <option value="Single" <?php if (!(strcmp("Single", $_SESSION['aForm']['marital']))) {echo "selected=\"selected\"";} ?>>Single</option>
                    <option value="Married" <?php if (!(strcmp("Married", $_SESSION['aForm']['marital']))) {echo "selected=\"selected\"";} ?>>Married</option>
                    <option value="Divorced" <?php if (!(strcmp("Divorced", $_SESSION['aForm']['marital']))) {echo "selected=\"selected\"";} ?>>Divorced</option>
                    <option value="Separated" <?php if (!(strcmp("Separated", $_SESSION['aForm']['marital']))) {echo "selected=\"selected\"";} ?>>Separated</option>
                    <option value="Widowed" <?php if (!(strcmp("Widowed", $_SESSION['aForm']['marital']))) {echo "selected=\"selected\"";} ?>>Widowed</option>
                    </select>
            </span></td>
</tr>
        <tr>
            <td align="right" valign="top">Date of Birth </td>
            <td valign="top"><span id="spryselect5">
                <select name="dobDay" id="dobDay" class="span2">
                    <option value="">Day</option>
                    <?php 
							for($d=1;$d<=31;$d++) {
							?>
                    <option value="<?php echo $d; ?>" <?php if (date("d",strtotime($_SESSION['aForm']['dob'])) == $d && $_SESSION['aForm']['dob'] != '') echo "selected='selected'" ?>><?php echo sprintf("%02d",$d); ?></option>
                    <?php } ?>
                    </select>
                </span><span id="spryselect6">
                    <select name="dobMonth" id="dobMonth" class="span2">
                        <option value="">Month</option>
                        <?php 
							for($m=1;$m<=12;$m++) {
							?>
                        <option value="<?php echo $m ?>" <?php if (date("m",strtotime($_SESSION['aForm']['dob'])) == $m && $_SESSION['aForm']['dob'] != '') echo "selected='selected'" ?>><?php echo date("M", mktime(0, 0, 0, $m, 10)); ?></option>
                        <?php } ?>
                        </select>
                    </span><span id="spryselect7">
                        <select name="dobYear" id="dobYear" class="span2">
                            <option value="" selected="selected">Year</option>
                            <?php 
							for($y=date("Y");$y>=1950;$y--) {
							?>
                            <option value="<?php echo $y ?>" <?php if (date("Y",strtotime($_SESSION['aForm']['dob'])) == $y && $_SESSION['aForm']['dob'] != '') echo "selected='selected'" ?>><?php echo $y ?></option>
                            <?php } ?>
                            </select>
                    </span></td>
</tr>
        <tr>
            <td align="right" valign="top">Nationality*</td>
            <td valign="top"><span id="spryselect4">
                <select name="nationality" class="width244" id="nationality">
                    <?php include('../_inc_nationalityList.php'); ?>
                    </select>
                <span class="selectRequiredMsg">Please select an item.</span></span>&nbsp; <span id="otherNationArea">
                    <label for="otherNation"></label>
                    <input type="text" name="otherNation" id="otherNation" />
                    <input type="hidden" name="selectNationality" id="selectNationality" value="<?php echo $_SESSION['aForm']['nationality'] ?>" />
                </span></td>
</tr>
        <tr class="stateArea">
            <td align="right" valign="top">State of Origin</td>
            <td valign="top"><select name="stateOfOrigin" id="stateOfOrigin">
                <?php include('../_inc_stateList.php'); ?>
            </select>
                <input type="hidden" name="selectStateO" id="selectStateO" value="<?php echo $_SESSION['aForm']['stateOfOrigin'] ?>" /></td>
        </tr>
        <tr class="stateArea">
            <td align="right" valign="top">Local Government Area</td>
            <td valign="top"><select name="lga" id="lga">
            </select>
                <input type="hidden" name="selectLGA" id="selectLGA" value="<?php echo $_SESSION['aForm']['lga'] ?>" /></td>
        </tr>
        <tr class="">
            <td align="right" valign="top">Languages Spoken</td>
            <td valign="top"><input name="languages" type="text" class="width244" id="languages" value="" /></td>
        </tr>
        <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
        </tr>
        <tr>
            <td align="right" valign="top"><strong>Residential Address</strong></td>
            <td valign="top">&nbsp;</td>
        </tr>
        <tr>
            <td align="right" valign="top">House Number (if available)</td>
            <td valign="top"><label for="houseNumber"></label>
                <input type="text" name="houseNumber" id="houseNumber" value="<?php echo $_SESSION['aForm']['houseNumber'] ?>"/></td>
        </tr>
        <tr>
            <td align="right" valign="top">Street Name</td>
            <td valign="top"><label for="street"></label>
                <span id="sprytextfield5">
                    <input name="street" type="text" class="width244" id="street" value="<?php echo $_SESSION['aForm']['street'] ?>" />
                </span></td>
</tr>
        <tr>
            <td align="right" valign="top">Major Bus Stop</td>
            <td valign="top"><label for="busStop"></label>
                <span id="sprytextfield6">
                    <input name="busStop" type="text" class="width244" id="busStop" value="<?php echo $_SESSION['aForm']['busStop']; ?>"/>
                </span></td>
</tr>
        <tr>
            <td align="right" valign="top">City/Town</td>
            <td valign="top"><label for="city"></label>
                <span id="sprytextfield7">
                    <input name="city" type="text" class="width244" id="city" value="<?php echo $_SESSION['aForm']['city']; ?>"/>
                </span></td>
</tr>
        <tr class="">
            <td align="right" valign="top">Country of Residence</td>
            <td valign="top"><select name="countryOfResidence" class="width244" id="countryOfResidence">
                                                     <option value="">Select Country</option>
                                                     <?php include('../_inc_countryList.php'); ?>
                                                 </select></td>
        </tr>
        
        <tr class="stateRArea">
            <td align="right" valign="top">State of Residence</td>
            <td valign="top"><span id="spryselect2">
                <select name="stateOfResidence" id="stateOfResidence">
                    <?php include('../_inc_stateList.php'); ?>
                    </select>
                </span>
                <input type="hidden" name="selectStateR" id="selectStateR" value="<?php echo $_SESSION['aForm']['stateOfResidence'] ?>" /></td>
</tr>
        
        <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
        </tr>
        <tr>
            <td align="right" valign="top">GSM Number*</td>
            <td valign="top"><label for="gsm"></label>
                <span id="sprytextfield8">
                    <input name="gsm" type="text" class="width244" id="gsm" value="<?php echo $_SESSION['aForm']['gsm']; ?>"/>
                    <span class="textfieldRequiredMsg">A value is required.</span></span></td>
</tr>
        <tr>
            <td align="right" valign="top">Alternative GSM Number</td>
            <td valign="top"><label for="gsm2"></label>
                <input name="gsm2" type="text" class="width244" id="gsm2" value="<?php echo $_SESSION['aForm']['gsm2']; ?>"/>
                <span class="fieldDescription">optional</span></td>
        </tr>
        <tr>
            <td align="right" valign="top">Alternative Email Address</td>
            <td valign="top"><label for="email2"></label>
                <span id="sprytextfield11">
                    <input name="email2" type="text" class="width244" id="email2" value="<?php echo $_SESSION['aForm']['email2']; ?>"/>
                    <span class="textfieldInvalidFormatMsg">Invalid format.</span></span><span class="fieldDescription">optional</span></td>
</tr>
        <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
        </tr>
        <tr>
            <td align="right" valign="top"><p>Preferred Test Location</p>
                <p>(if shortlisted)</p></td>
            <td valign="top"><label for="prefTestLoc"></label>
                <span id="spryselect3">
                    <select name="prefTestLoc" id="prefTestLoc">
                        <option selected="selected" value="" <?php if (!(strcmp("", $_SESSION['aForm']['prefTestLoc']))) {echo "selected=\"selected\"";} ?>>- Select -</option>
                        <option value="Abuja" <?php if (!(strcmp("Abuja", $_SESSION['aForm']['prefTestLoc']))) {echo "selected=\"selected\"";} ?>>Abuja</option>
                        <option value="Enugu" <?php if (!(strcmp("Enugu", $_SESSION['aForm']['prefTestLoc']))) {echo "selected=\"selected\"";} ?>>Enugu</option>
                        <option value="Ibadan" <?php if (!(strcmp("Ibadan", $_SESSION['aForm']['prefTestLoc']))) {echo "selected=\"selected\"";} ?>>Ibadan</option>
                        <option value="Ilorin" <?php if (!(strcmp("Ilorin", $_SESSION['aForm']['prefTestLoc']))) {echo "selected=\"selected\"";} ?>>Ilorin</option>
                        <option value="Lagos" <?php if (!(strcmp("Lagos", $_SESSION['aForm']['prefTestLoc']))) {echo "selected=\"selected\"";} ?>>Lagos</option>
                        <option value="Port Harcourt" <?php if (!(strcmp("Port Harcourt", $_SESSION['aForm']['prefTestLoc']))) {echo "selected=\"selected\"";} ?>>Port Harcourt</option>
                        </select>
                </span></td>
</tr>
        <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td valign="top"><a href="applicants.php">
                <input name="cancel" type="button" class="btn red" id="cancel" value="Cancel" />
                </a>
                <input name="submit" type="submit" class="btn green" id="submit" value="Submit" /></td>
        </tr>
</table>
<p></p>
</div>
<input type="hidden" name="MM_insert" value="formEmployer" />
			                            </form>
			                            <!-- END FORM-->
		                            </div>
</div>
</div>
                            <!-- END SAMPLE FORM PORTLET-->
                        </div>
</div>
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>
                
		    </div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include('-inc-footer.php'); ?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script> 
	<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>   
	<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>   
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script> 
	<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>     
	<!-- END PAGE LEVEL SCRIPTS -->  
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	$('#otherNationArea').hide();
	$('.stateArea').hide();
	$('.stateRArea').hide();
	if($('select#nationality').val() == 'Nigerian') {
		$('.stateArea').show();
	}
	
	if($("#selectNationality").val() != '') {
		$("#nationality").val($("#selectNationality").val());
	}
	if($("#selectStateO").val() != '') {
		$("#stateOfOrigin").val($("#selectStateO").val());
	}
	if($("#selectStateR").val() != '') {
		$("#stateOfResidence").val($("#selectStateR").val());
	}
	if($('select#countryOfResidence').val() == 'Nigeria') {
		$('.stateRArea').show();
	}
	if($("#selectLGA").val() != '') {
			 //populate lga dropdown
			  $("select#lga > option").remove();
			  //alert('lga options cleared');
			  $.ajax({
				type: "GET",
				url: "../xml/states.xml",
				dataType: "xml",
				success: parseXml
			  });			
			
			//select current lga
			$("#lga").val($("#selectLGA").val());
	}
	//datepicker
	

  	//actions for dropdowns - nationality (to display state) and state( to display LGAs)
	$("select#nationality").change(function()
	{
		if($(this).val() != "Nigerian")
		{
			$('.stateArea').hide();//alert("not Nigerian");
		}
		else
		{
			$('.stateArea').show();
		}
	});//end of nationality

	$("select#countryOfResidence").change(function()
	{
		//alert("change nationality");
		if($(this).val() != "Nigerian")
		{
			$('.stateRArea').hide();//alert("not Nigerian");
			$("#stateOfResidence").val('');
		}
		else
		{
			$('.stateRArea').show();
		}
	});//end of nationality

	$('select#stateOfOrigin').change(function()
	{							 
	  $("select#lga > option").remove();
	  //alert('lga options cleared');
	  $.ajax({
		type: "GET",
		url: "../xml/states.xml",
		dataType: "xml",
		success: parseXml
	  });			
	});
	
});
//functioned used in state and lga dropdowns
function  parseXml(xml)
{
  //alert('started parsing xml');
  //find every state
  $(xml).find("state").each(function()
  {
	var stateSelected = $('select#stateOfOrigin').val();
	var stateName = $(this).attr("name");
	//alert(stateName +'/'+ stateSelected);
	if(stateName == stateSelected)
	{
		//alert(stateName + ' has been selected');	
		var printd = "The LGS:";
		$(this).find("lg").each(function() 
		{
			//alert('entered each LG');
			//printd += $(this).text();
			var dVal = $(this).text();
			$("select#lga").append("<option value='"+dVal+"'>"+dVal+"</option>");
		});
		//alert(printd);
		$("#lga").val($("#selectLGA").val());
		//stop searching
		return false;
	}
	//$("#output").append($(this).attr("name") + "<br />");
  });
}

</script>
<script>
jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   FormComponents.init();
		});
var spryselect2 = new Spry.Widget.ValidationSelect("spryselect2", {validateOn:["blur"], isRequired:false});
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3", "none", {minChars:8});
var spryselect3 = new Spry.Widget.ValidationSelect("spryselect3", {validateOn:["blur"], isRequired:false});
var sprytextfield11 = new Spry.Widget.ValidationTextField("sprytextfield11", "email", {validateOn:["blur"], isRequired:false});
var sprytextfield8 = new Spry.Widget.ValidationTextField("sprytextfield8", "none", {validateOn:["blur"]});
var sprytextfield7 = new Spry.Widget.ValidationTextField("sprytextfield7", "none", {validateOn:["blur"], isRequired:false});
var sprytextfield6 = new Spry.Widget.ValidationTextField("sprytextfield6", "none", {validateOn:["blur"], isRequired:false});
var sprytextfield5 = new Spry.Widget.ValidationTextField("sprytextfield5", "none", {validateOn:["blur"], isRequired:false});
var spryselect4 = new Spry.Widget.ValidationSelect("spryselect4", {validateOn:["blur"]});
var spryselect7 = new Spry.Widget.ValidationSelect("spryselect7", {isRequired:false});
var spryselect6 = new Spry.Widget.ValidationSelect("spryselect6", {isRequired:false});
var spryselect5 = new Spry.Widget.ValidationSelect("spryselect5", {isRequired:false});
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1", {validateOn:["blur"], isRequired:false});
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {validateOn:["blur"]});
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "none", {validateOn:["blur"]});
var sprytextfield9 = new Spry.Widget.ValidationTextField("sprytextfield9", "none", {validateOn:["blur"]});
var spryconfirm1 = new Spry.Widget.ValidationConfirm("spryconfirm1", "password");
    </script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>