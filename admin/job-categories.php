<?php require_once("_inc_checkSession.php"); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_categories = 20;
$pageNum_categories = 0;
if (isset($_GET['pageNum_categories'])) {
  $pageNum_categories = $_GET['pageNum_categories'];
}
$startRow_categories = $pageNum_categories * $maxRows_categories;

mysql_select_db($database_fer, $fer);
$query_categories = "SELECT * FROM job_categories ORDER BY cat_name ASC";
$query_limit_categories = sprintf("%s LIMIT %d, %d", $query_categories, $startRow_categories, $maxRows_categories);
$categories = mysql_query($query_limit_categories, $fer) or die(mysql_error());
$row_categories = mysql_fetch_assoc($categories);

if (isset($_GET['totalRows_categories'])) {
  $totalRows_categories = $_GET['totalRows_categories'];
} else {
  $all_categories = mysql_query($query_categories);
  $totalRows_categories = mysql_num_rows($all_categories);
}
$totalPages_categories = ceil($totalRows_categories/$maxRows_categories)-1;


$queryString_categories = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_categories") == false && 
        stristr($param, "totalRows_categories") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_categories = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_categories = sprintf("&totalRows_categories=%d%s", $totalRows_categories, $queryString_categories);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Job Categories | <?php echo $config['shortname'] ?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link href="assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('-inc-top.php'); ?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<?php include('-inc-navbar-side.php'); ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					Widget settings form goes here
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Job Categories <small>browse  job categories</small></h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#">Job Categories</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#"> List Job Categories</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) { ?>
			    <div class="alert alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg'] ?></strong> </div>
			    <?php } ?>
			    <?php if ($totalRows_categories == 0) { // Show if recordset empty ?>
			    <div class="row-fluid">
			        <div class="alert">
			            <button class="close" data-dismiss="alert"></button>
			            <strong>Empty List!</strong> No categories here. </div>
		        </div>
			    <?php } // Show if recordset empty ?>
			    <div class="btn-group"> <a href="job-category-add.php">
			        <button id="sample_editable_1_new" class="btn green"> Create Category <i class="icon-plus"></i></button>
			        </a>
			        
                    </div>
			    <p>
			        
		        </p>
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN EXAMPLE TABLE PORTLET-->
			            <div class="portlet box light-grey">
			                <div class="portlet-title">
			                    <div class="caption"><i class="icon-barcode"></i>Job Categories</div>
		                    </div>
			                <div class="portlet-body">
			                    <div class="table-toolbar"></div>
			                    <div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid"> <br>
			                        <?php if ($totalRows_categories > 0) { // Show if recordset not empty ?>
			                        <div class="row-fluid">
			                            <div class="span6"></div>
			                            <div class="span6">
			                                <!--<div class="dataTables_filter" id="sample_1_filter">
                  <label>Search: <input type="text" aria-controls="sample_1" class="m-wrap medium"></label>
                  </div>-->
		                                </div>
		                            </div>
			                        <table class="table table-striped table-bordered table-hover" id="sample_1">
			                            <thead>
			                                <tr>
			                                    <th width="10%">ID</th>
			                                    <th width="40%" >Category Name</th>
			                                    <th width="20%" >&nbsp;</th>
		                                    </tr>
		                                </thead>
			                            <tbody>
			                                <?php do { ?>
			                                <tr class="odd gradeX">
			                                    <td><?php echo $row_categories['id_cat']; ?></td>
			                                    <td ><?php echo $row_categories['cat_name']; ?></td>
			                                    <td >
			                                        <a href="job-category-edit.php?id=<?php echo $row_categories['id_cat']; ?>" title="Edit" class="tooltips"><i class="icon-edit"></i></a>&nbsp;&nbsp; 
		                                        <a title="Delete" class="ValidateAction tooltips" data-placement="top" data-confirm-msg="Are you sure you want to Delete? All information associated with this category will also be cleared!!!" href="job-category-delete.php?id=<?php echo $row_categories['id_cat']; ?>"><i class="icon-remove"></i></a></td>
		                                    </tr>
			                                <?php } while ($row_categories = mysql_fetch_assoc($categories)); ?>
		                                </tbody>
		                            </table>
			                        <div class="row-fluid">
			                            <div class="span6">
			                                <div class="dataTables_info" id="sample_1_info">Showing <?php echo ($startRow_categories + 1) ?> to <?php echo min($startRow_categories + $maxRows_categories, $totalRows_categories) ?> of <?php echo $totalRows_categories ?> entries</div>
		                                </div>
			                            <div class="span6">
			                                <div class="dataTables_paginate paging_bootstrap pagination">
			                                    <ul>
			                                        <?php if ($pageNum_categories > 0) { // Show if not first page ?>
			                                        <li class="prev"><a href="<?php printf("%s?pageNum_categories=%d%s", $thispage, 0, $queryString_categories); ?>" title="First Page">&laquo; </a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_categories > 0) { // Show if not first page ?>
			                                        <li class="prev"><a href="<?php printf("%s?pageNum_categories=%d%s", $thispage, max(0, $pageNum_categories - 1), $queryString_categories); ?>" title="Previous Page">&lsaquo;</a></li>
			                                        <?php } ?>
			                                        <?php $thisPageNumber = $pageNum_categories+1;
                                                //determine startPage and endPage
                                                $totalPages = $totalPages_categories +1;
                                                if($totalPages < 4) {
                                                    $startPage = 1;
                                                    $endPage = $totalPages;
                                                } elseif($thisPageNumber <= 4) {
                                                    $startPage = 1;
                                                    $endPage = 4;
                                                } else {
                                                    $startPage = $thisPageNumber - 3;
                                                    $endPage = $thisPageNumber;
                                                }
                                              ?>
			                                        <?php for ($i = $startPage; $i<=$endPage; $i++) { ?>
			                                        <li class="<?php if ($i == $thisPageNumber) echo "active"; ?>"><a href="<?php echo $thispage ?>?pageNum_categories=<?php echo $i-1 ?>&amp;totalRows_categories=<?php echo $totalRows_categories ?>" title="Page <?php echo $i ?>"><?php echo $i ?></a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_categories < $totalPages_categories) { // Show if not last page ?>
			                                        <li class="next"><a href="<?php printf("%s?pageNum_categories=%d%s", $thispage, min($totalPages_categories, $pageNum_categories + 1), $queryString_categories); ?>" title="Next Page">&rsaquo; </a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_categories < $totalPages_categories) { // Show if not last page ?>
			                                        <li class="next"><a href="<?php printf("%s?pageNum_categories=%d%s", $thispage, $totalPages_categories, $queryString_categories); ?>" title="Last Page">&raquo; </a></li>
			                                        <?php } ?>
		                                        </ul>
		                                    </div>
		                                </div>
		                            </div>
                                    <?php } // Show if recordset not empty ?>
		                        </div>
		                    </div>
		                </div>
			            <!-- END EXAMPLE TABLE PORTLET-->
		            </div>
		        </div>
			    
			    <div class="clearfix"></div>
		    </div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include('-inc-footer.php'); ?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>   
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  
	<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
	<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>     
	<!-- END PAGE LEVEL SCRIPTS -->  
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
    <script>
		$(document).ready(function(e) {
		   $("#company").val('<?php echo $_GET['company'] ?>');
		   $("#status").val('<?php echo $_GET['status'] ?>');
		   $("#fromDate").val('<?php echo $_GET['fromDate'] ?>');
		   $("#toDate").val('<?php echo $_GET['toDate'] ?>');
        });
	</script>
	<script>
		jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   FormComponents.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<?php
mysql_free_result($categories);

mysql_free_result($sites);
?>
