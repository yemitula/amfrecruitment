<?php ini_set('max_execution_time', 3600); //execution time in seconds ?>
<?php require_once("_inc_checkSession.php"); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "import")) {
	$csvfile = $_FILES['vacfile']['tmp_name'];
	$file = fopen($csvfile, 'r');
	$headers = fgetcsv($file);
	if (!$headers) {
		header("location: vacancies-import.php?error=".urlencode("Problem with file!!!"));
		exit;
	}
	$i = 1;
	while (($line = fgetcsv($file)) !== FALSE) {
	  //$line is an array of the csv elements
	  //print_r($line); echo "<br><br>";
		foreach($line as $key=>$value) {
			$headerName = $headers[$key];
			$vacs[$i][$headerName] = $value;
		}
	  $i++;
	}
	fclose($file);	
	//var_dump($vacs); die;
	
	//insert each vacancy in the database
	foreach ($vacs as $vacancy) {
		$insertSQL = sprintf("INSERT INTO vacancies (title, employer_id, summary, longDescription, techReqs, eduQual, behavReqs, jobCategory_id, jobType, staffLevel, datePosted, dateOpening, dateClosing, sortOrder) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
						   GetSQLValueString($vacancy['title'], "text"),
						   GetSQLValueString($_POST['employer_id'], "int"),
						   GetSQLValueString($vacancy['summary'], "text"),
						   GetSQLValueString(str_replace("'","\'",$vacancy['longDescription']), "text"),
						   GetSQLValueString($vacancy['techReqs'], "text"),
						   GetSQLValueString($vacancy['eduQual'], "text"),
						   GetSQLValueString($vacancy['behavReqs'], "text"),
						   GetSQLValueString($vacancy['jobCategory_id'], "int"),
						   GetSQLValueString($vacancy['jobType'], "text"),
						   GetSQLValueString($vacancy['staffLevel'], "text"),
						   GetSQLValueString($_POST['datePosted'], "date"),
						   GetSQLValueString($vacancy['dateOpening'], "date"),
						   GetSQLValueString($vacancy['dateClosing'], "date"),
						   GetSQLValueString($vacancy['sortOrder'], "int"));
		mysql_select_db($database_fer, $fer);
		$Result1 = mysql_query($insertSQL, $fer);
		$error = mysql_errno();
		
		if ($error == 1062) { //duplicate entry
			//vacancy with same title already exists, do an update instead
			$updateSQL = sprintf("UPDATE vacancies SET summary=%s, longDescription=%s, techReqs=%s, eduQual=%s, behavReqs=%s, jobCategory_id=%s, jobType=%s, staffLevel=%s, datePosted=%s, dateOpening=%s, dateClosing=%s, sortOrder=%s WHERE title=%s",
							   GetSQLValueString($vacancy['summary'], "text"),
							   GetSQLValueString(str_replace("'","\'",$vacancy['longDescription']), "text"),
							   GetSQLValueString($vacancy['techReqs'], "text"),
							   GetSQLValueString($vacancy['eduQual'], "text"),
							   GetSQLValueString($vacancy['behavReqs'], "text"),
							   GetSQLValueString($vacancy['jobCategory_id'], "int"),
							   GetSQLValueString($vacancy['jobType'], "text"),
							   GetSQLValueString($vacancy['staffLevel'], "text"),
							   GetSQLValueString($_POST['datePosted'], "date"),
							   GetSQLValueString($vacancy['dateOpening'], "date"),
							   GetSQLValueString($vacancy['dateClosing'], "date"),
							   GetSQLValueString($vacancy['sortOrder'], "int"),
							   GetSQLValueString($vacancy['title'], "text"));
			mysql_select_db($database_fer, $fer);
			$updateRS = mysql_query($updateSQL, $fer);
				
		}
	}
	
	header("Location: vacancies.php?msg=".urlencode("Vacancies Successfully Imported"));
	exit;
}

mysql_select_db($database_fer, $fer);
$query_employers = "SELECT * FROM employers ORDER BY companyName ASC";
$employers = mysql_query($query_employers, $fer) or die(mysql_error());
$row_employers = mysql_fetch_assoc($employers);
$totalRows_employers = mysql_num_rows($employers);

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Vacancies | <?php echo $config['shortname'] ?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
	<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
	<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
	<link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('-inc-top.php'); ?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<?php include('-inc-navbar-side.php'); ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					Widget settings form goes here
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Vacancies <small>import vacancies </small> </h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="vacancies.php">Vacancies</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#"> Import Vacancies</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) { ?>
			    <div class="alert  alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg'] ?></strong> </div>
			    <?php } ?>
			    <?php if (isset($_GET['error'])) { ?>
			    <div class="alert alert-error">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['error'] ?></strong> </div>
			    <?php } ?>
			    <div id="dashboard">
			        <!-- BEGIN DASHBOARD STATS -->
			        <div class="row-fluid">
			            <div class="span12">
			                <!-- BEGIN SAMPLE FORM PORTLET-->
			                <div class="portlet box green tabbable">
			                    <div class="portlet-title">
			                        <div class="caption"> <i class="icon-reorder"></i> <span class="hidden-480">Vacancy Details</span> </div>
		                        </div>
			                    <div class="portlet-body form">
			                        <div class="tabbable portlet-tabs">
			                            <!-- BEGIN FORM-->
			                            <form method="POST" action="<?php echo $editFormAction; ?>" class="form-horizontal" enctype="multipart/form-data" name="formImport" id="formImport">
			                                <p>&nbsp;</p>
			                                <div class="control-group">
			                                    <label class="control-label">Company</label>
			                                    <div class="controls">
			                                        <label for="site_id"></label>
			                                        <span id="spryselect1">
			                                        <select name="employer_id" class="m-wrap span6" id = "employer_id">
                                              <option value=""> -- Select a Company -- </option>
                                              <?php
do {  
?>
                                              <option value="<?php echo $row_employers['id']?>"><?php echo $row_employers['companyName']?></option>
                                              <?php
} while ($row_employers = mysql_fetch_assoc($employers));
  $rows = mysql_num_rows($employers);
  if($rows > 0) {
      mysql_data_seek($employers, 0);
	  $row_employers = mysql_fetch_assoc($employers);
  }
?>
                                            </select>
			                                        <span class="selectRequiredMsg">Please select an item.</span></span><span class="help-inline"></span></div>
		                                    </div>
			                                
                                            <div class="control-group">
			                                    <label class="control-label">CSV File</label>
			                                    <div class="controls">
			                                        <input type="file" name="vacfile" id="vacfile" />
		                                        </div>
    										</div>
                                            
                                            <div class="control-group">
			                                    <label class="control-label"></label>
			                                    <div class="controls">
			                                        <p>&nbsp;</p>
                                      <p>* File must be saved in the .csv format</p>
                                      <p>* CSV File must be arranged in <a href="fer-vacancies-import-format.xlsx"><strong>THIS FORMAT</strong></a>                                      </p>
                                      <p>&nbsp;</p>
                                      <p><strong>NOTES ON CSV FIELDS</strong></p>
                                      <p>*<strong> title, summary, longDescription, techReqs, eduQual </strong>and<strong> behavReqs</strong> are text fields</p>
                                      <p>* <strong>jobType</strong> must be one of (<em>Full Time/Part Time/Freelance/Internship/Contract</em>)</p>
                                      <p>* <strong>jobCategory_id</strong> must be a&nbsp;number from <strong><a href="job-categories.php" target="_blank">THIS LIST</a></strong><a href="job-categories.php"></a></p>
                                      <p>* <strong>staffLevel</strong> is optional or can be one of (<em>Entry Level/Middle Management/Senior Management/Executive</em>),  leave blank for <em>Any Level</em></p>
                                      <p>* <strong>dateOpening</strong> and <strong>dateClosing</strong> must be dates in the <em>yyyy-mm-dd</em> format, <strong>dateOpening</strong> is optional</p>
		                                        </div>
    										</div>
			                                

			                                <div class="form-actions">
			                                    <button type="submit" class="btn green"><i class="icon-ok"></i> Save</button>
			                                    <a href="vacancies.php">
			                                        <button type="button" class="btn">Cancel</button>
		                                        </a>
			                                </div>
                                            <input name="datePosted" type="hidden" id="datePosted" value="<?php echo date("Y-m-d h:i:s"); ?>" />
		                                    <input type="hidden" name="MM_insert" value="import" />
			                            </form>
			                            <!-- END FORM-->
		                            </div>
</div>
</div>
                            <!-- END SAMPLE FORM PORTLET-->
                        </div>
</div>
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>
                
		    </div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include('-inc-footer.php'); ?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script> 
	<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>   
	<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>   
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script> 
	<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>     
	<!-- END PAGE LEVEL SCRIPTS -->  
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
<script>
		$(document).ready(function(e) {
		   $("#formImport").submit(function(e) {
            if($("#vacfile").val() == '') {
				alert('Please attach a CSV file');
				return false;
			}
        });
        });
	</script>
	<script>
jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   FormComponents.init();
		});
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
    </script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<?php
mysql_free_result($sites);

mysql_free_result($report);
?>
