<?php require_once("_inc_checkSession.php"); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php include('../_inc_Functions.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_fer, $fer);
$query_companies = "SELECT * FROM employers ORDER BY companyName ASC";
$companies = mysql_query($query_companies, $fer) or die(mysql_error());
$row_companies = mysql_fetch_assoc($companies);
$totalRows_companies = mysql_num_rows($companies);

$maxRows_vacancies = 20;
$pageNum_vacancies = 0;
if (isset($_GET['pageNum_vacancies'])) {
  $pageNum_vacancies = $_GET['pageNum_vacancies'];
}
$startRow_vacancies = $pageNum_vacancies * $maxRows_vacancies;

mysql_select_db($database_fer, $fer);
$query_vacancies = "SELECT *, (SELECT COUNT(*) FROM applications a WHERE a.vacancy_id=v.id) AS appCount FROM vacancies v WHERE 1=1";
if(!empty($_GET['company'])) {
	$query_vacancies .= sprintf(" AND v.employer_id = %s", GetSQLValueString($_GET['company'],"int"));
}
if(!empty($_GET['status'])) {
	$query_vacancies .= sprintf(" AND v.status = %s", GetSQLValueString($_GET['status'],"text"));
}
if(!empty($_GET['fromDate']) && !empty($_GET['toDate']) && strtotime($_GET['toDate']) >= strtotime($_GET['fromDate'])) {
	$query_vacancies .= sprintf(" AND v.datePosted BETWEEN %s AND %s", GetSQLValueString(date('Y-m-d h:i:s', strtotime($_GET['fromDate'])),"date"), GetSQLValueString(date('Y-m-d h:i:s', strtotime($_GET['toDate'])),"date"));
}
$query_vacancies .= " ORDER BY v.datePosted DESC";
$query_limit_vacancies = sprintf("%s LIMIT %d, %d", $query_vacancies, $startRow_vacancies, $maxRows_vacancies);
$vacancies = mysql_query($query_limit_vacancies, $fer) or die(mysql_error());
$row_vacancies = mysql_fetch_assoc($vacancies);

if (isset($_GET['totalRows_vacancies'])) {
  $totalRows_vacancies = $_GET['totalRows_vacancies'];
} else {
  $all_vacancies = mysql_query($query_vacancies);
  $totalRows_vacancies = mysql_num_rows($all_vacancies);
}
$totalPages_vacancies = ceil($totalRows_vacancies/$maxRows_vacancies)-1;


$queryString_vacancies = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_vacancies") == false && 
        stristr($param, "totalRows_vacancies") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_vacancies = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_vacancies = sprintf("&totalRows_vacancies=%d%s", $totalRows_vacancies, $queryString_vacancies);

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Vacancies | <?php echo $config['shortname'] ?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link href="assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('-inc-top.php'); ?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<?php include('-inc-navbar-side.php'); ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					Widget settings form goes here
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Vacancies <small>browse  vacancies</small></h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#">Vacancies</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#"> List Vacancies</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) { ?>
			    <div class="alert alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg'] ?></strong> </div>
			    <?php } ?>
			    <?php if ($totalRows_vacancies == 0) { // Show if recordset empty ?>
			    <div class="row-fluid">
			        <div class="alert">
			            <button class="close" data-dismiss="alert"></button>
			            <strong>Empty List!</strong> No vacancies here. </div>
		        </div>
			    <?php } // Show if recordset empty ?>
			    <div class="btn-group"> <a href="vacancy-edit.php">
			        <button id="sample_editable_1_new" class="btn green"> Create Vacancy <i class="icon-plus"></i></button>
			        </a>
			        &nbsp;
                    <a href="vacancy-import.php">
			        <button id="sample_editable_1_new" class="btn yellow"> Import Vacancies <i class="icon-plus"></i></button>
			        </a>
                    </div>
			    <p>
			        
		        </p>
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN EXAMPLE TABLE PORTLET-->
			            <div class="portlet box light-grey">
			                <div class="portlet-title">
			                    <div class="caption"><i class="icon-barcode"></i>Vacancies</div>
		                    </div>
			                <div class="portlet-body">
			                    <div class="table-toolbar"></div>
			                    <div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid"> <br>
			                        <form action="" method="get" name="formFilter" id="formFilter" >
			                            <div class="row-fluid">
			                                <div class="span12">
			                                    <div id="sample_1_length" class="dataTables_length">
			                                        
                                                    <span class="controls">
			                                        <select name="company" id="company" class="mwrap medium">
			                                            <option value="">All Companies</option>
			                                            <?php
do {  
?>
			                                            <option value="<?php echo $row_companies['id']?>"><?php echo $row_companies['companyName']?></option>
			                                            <?php
} while ($row_companies = mysql_fetch_assoc($companies));
  $rows = mysql_num_rows($companies);
  if($rows > 0) {
      mysql_data_seek($companies, 0);
	  $row_companies = mysql_fetch_assoc($companies);
  }
?>
                                                    </select>
                                                    &nbsp;
                                                    <select name="status" class="" id="status">
                                            <option value="">Any Status</option>
                                            <option value="closed">Closed</option>
                                            <option value="open">Open</option>
                                          </select>
                                                    &nbsp;
                                                    <input id="fromDate" name="fromDate" class="m-wrap m-ctrl-medium date-picker" readonly size="16" type="text" value="" placeholder="From" />
                                                    &nbsp;
                                                    <input id="toDate" name="toDate" class="m-wrap m-ctrl-medium date-picker" readonly size="16" type="text" value="" placeholder="To" />
                                                    </span>
			                                        &nbsp;
<button type="submit" class="btn black">Filter <i class="m-icon-swapright m-icon-white"></i></button>
		                                        </div>
		                                    </div>
		                                </div>
		                            </form>
                                    <?php if ($totalRows_vacancies > 0) { // Show if recordset not empty ?>
			                        <div class="row-fluid">
			                            <div class="span6"></div>
			                            <div class="span6">
			                                <!--<div class="dataTables_filter" id="sample_1_filter">
                  <label>Search: <input type="text" aria-controls="sample_1" class="m-wrap medium"></label>
                  </div>-->
		                                </div>
		                            </div>
			                        <table class="table table-striped table-bordered table-hover" id="sample_1">
			                            <thead>
			                                <tr>
			                                    <th width="20%">Title</th>
			                                    <th width="30%" ><strong>Summary</strong></th>
			                                    <th width="10%" ><strong>Date Posted</strong></th>
			                                    <th width="10%" >Applied</th>
			                                    <th width="10%" >Shortlisted</th>
			                                    <th width="20%" >&nbsp;</th>
		                                    </tr>
		                                </thead>
			                            <tbody>
			                                <?php do { ?>
			                                <tr class="odd gradeX">
			                                    <td><a href="vacancy-details.php?id=<?php echo $row_vacancies['id']; ?>"><?php echo $row_vacancies['title']; ?></a></td>
			                                    <td ><?php echo $row_vacancies['summary']; ?></td>
			                                    <td ><?php echo date("d-M-Y",strtotime($row_vacancies['datePosted'])); ?></td>
			                                    <td ><a href="vacancy-applicants.php?id=<?php echo $row_vacancies['id']; ?>">Applicants (<?php echo $row_vacancies['appCount'] ?>)</a></td>
			                                    <td class="center"><a href="vacancy-shortlist.php?id=<?php echo $row_vacancies['id'] ?>">Shortlisted</a></td>
			                                    <td >
			                                        <a href="vacancy-details.php?id=<?php echo $row_vacancies['id']; ?>" target="_blank" title="View Vacancy Details" class="tooltips"><i class="icon-eye-open"></i></a>&nbsp;&nbsp;
			                                        <a href="vacancy-edit.php?id=<?php echo $row_vacancies['id']; ?>" title="Edit Vacancy" class="tooltips"><i class="icon-edit"></i></a>&nbsp;&nbsp; 
		                                        <a title="Delete Vacancy" class="ValidateAction tooltips" data-placement="top" data-confirm-msg="Are you sure you want to Delete? All information associated with this report will also be cleared!!!" href="vacancy-delete.php?id=<?php echo $row_vacancies['id']; ?>"><i class="icon-remove"></i></a></td>
		                                    </tr>
			                                <?php } while ($row_vacancies = mysql_fetch_assoc($vacancies)); ?>
		                                </tbody>
		                            </table>
			                        <div class="row-fluid">
			                            <div class="span6">
			                                <div class="dataTables_info" id="sample_1_info">Showing <?php echo ($startRow_vacancies + 1) ?> to <?php echo min($startRow_vacancies + $maxRows_vacancies, $totalRows_vacancies) ?> of <?php echo $totalRows_vacancies ?> entries</div>
		                                </div>
			                            <div class="span6">
			                                <div class="dataTables_paginate paging_bootstrap pagination">
			                                    <ul>
			                                        <?php if ($pageNum_vacancies > 0) { // Show if not first page ?>
			                                        <li class="prev"><a href="<?php printf("%s?pageNum_vacancies=%d%s", $thispage, 0, $queryString_vacancies); ?>" title="First Page">&laquo; </a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_vacancies > 0) { // Show if not first page ?>
			                                        <li class="prev"><a href="<?php printf("%s?pageNum_vacancies=%d%s", $thispage, max(0, $pageNum_vacancies - 1), $queryString_vacancies); ?>" title="Previous Page">&lsaquo;</a></li>
			                                        <?php } ?>
			                                        <?php $thisPageNumber = $pageNum_vacancies+1;
                                                //determine startPage and endPage
                                                $totalPages = $totalPages_vacancies +1;
                                                if($totalPages < 4) {
                                                    $startPage = 1;
                                                    $endPage = $totalPages;
                                                } elseif($thisPageNumber <= 4) {
                                                    $startPage = 1;
                                                    $endPage = 4;
                                                } else {
                                                    $startPage = $thisPageNumber - 3;
                                                    $endPage = $thisPageNumber;
                                                }
                                              ?>
			                                        <?php for ($i = $startPage; $i<=$endPage; $i++) { ?>
			                                        <li class="<?php if ($i == $thisPageNumber) echo "active"; ?>"><a href="<?php echo $thispage ?>?pageNum_vacancies=<?php echo $i-1 ?>&amp;totalRows_vacancies=<?php echo $totalRows_vacancies ?>" title="Page <?php echo $i ?>"><?php echo $i ?></a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_vacancies < $totalPages_vacancies) { // Show if not last page ?>
			                                        <li class="next"><a href="<?php printf("%s?pageNum_vacancies=%d%s", $thispage, min($totalPages_vacancies, $pageNum_vacancies + 1), $queryString_vacancies); ?>" title="Next Page">&rsaquo; </a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_vacancies < $totalPages_vacancies) { // Show if not last page ?>
			                                        <li class="next"><a href="<?php printf("%s?pageNum_vacancies=%d%s", $thispage, $totalPages_vacancies, $queryString_vacancies); ?>" title="Last Page">&raquo; </a></li>
			                                        <?php } ?>
		                                        </ul>
		                                    </div>
		                                </div>
		                            </div>
                                    <?php } // Show if recordset not empty ?>
		                        </div>
		                    </div>
		                </div>
			            <!-- END EXAMPLE TABLE PORTLET-->
		            </div>
		        </div>
			    
			    <div class="clearfix"></div>
		    </div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include('-inc-footer.php'); ?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>   
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  
	<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
	<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>     
	<!-- END PAGE LEVEL SCRIPTS -->  
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
    <script>
		$(document).ready(function(e) {
		   $("#company").val('<?php echo $_GET['company'] ?>');
		   $("#status").val('<?php echo $_GET['status'] ?>');
		   $("#fromDate").val('<?php echo $_GET['fromDate'] ?>');
		   $("#toDate").val('<?php echo $_GET['toDate'] ?>');
        });
	</script>
	<script>
		jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   FormComponents.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<?php
mysql_free_result($companies);

mysql_free_result($sites);
?>
