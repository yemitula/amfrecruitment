<?php //require_once('Connections/fer.php'); ?>
<?php
/*if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
*/?>
<?php 
//function does rankings
function DoRankings ($month, $year) {
	global $database_fer;
	global $fer;
	
	//1. Overall Sites Ranking
	//retrieve site reports for the month
	mysql_select_db($database_fer, $fer);
	$reportsSQL = sprintf("SELECT * FROM site_reports WHERE month = %s AND year = %s",
						GetSQLValueString($month, "int"),
						GetSQLValueString($year, "int"));
	$reports = mysql_query($reportsSQL, $fer) or die(mysql_error());
	$row_reports = mysql_fetch_assoc($reports);
	$totalRows_reports = mysql_num_rows($reports);
	//put it in an associative array
	$reportData = array();
	do {
		$reportData[] = array('site' => $row_reports['site_id'], 'percent' => $row_reports['netProfitPer']);
	} while($row_reports = mysql_fetch_assoc($reports));
	//redistribute into columns
	foreach ($reportData as $key => $row) {
		$site[$key]  = $row['site'];
		$percent[$key] = $row['percent'];
	}	
	// Sort the data with site ascending, percent descending
	// Add $data as the last parameter, to sort by the common key
	array_multisort($percent, SORT_DESC, $site, SORT_ASC, $reportData);
	
	//print_r($reportData); die;
	
	//clear rankings for this month from db
	$clearSQL = sprintf("DELETE FROM site_overall_rank WHERE sor_month = %s AND sor_year = %s",
						GetSQLValueString($month, "int"),
						GetSQLValueString($year, "int"));
	$clearRS = mysql_query($clearSQL, $fer) or die(mysql_error());
	
	//insert new rankings for this month into db
	if ($totalRows_reports > 0) foreach ($reportData as $key => $row) {
		$rank = $key+1; //rank 
		$insertSQL = sprintf("INSERT INTO site_overall_rank (sor_site_id, sor_rank, sor_month, sor_year) VALUES (%s, %s, %s, %s)",
						GetSQLValueString($row['site'], "int"),
						GetSQLValueString($rank, "int"),
						GetSQLValueString($month, "int"),
						GetSQLValueString($year, "int"));
		$insertRS = mysql_query($insertSQL, $fer) or die(mysql_error());
	}
	
	//overall site rankings done
	
	//2. Sites in a Cluster Ranking
	// retrieve site reports for the month in each cluster
	//get all clusters
	$clusterSQL = "SELECT * FROM clusters";
	$clusters = mysql_query($clusterSQL, $fer) or die(mysql_error());
	$row_clusters  = mysql_fetch_assoc($clusters);
	if (mysql_num_rows($clusters) > 0) do {
		//get this month's reports for sites in current cluster
		$reportsSQL = sprintf("SELECT * FROM site_reports r LEFT JOIN sites s ON r.site_id = s.site_id WHERE month = %s AND year = %s AND cluster_id = %s",
							GetSQLValueString($month, "int"),
							GetSQLValueString($year, "int"),
							GetSQLValueString($row_clusters['cluster_id'], "int")); 
		$reports = mysql_query($reportsSQL, $fer) or die(mysql_error());
		$row_reports = mysql_fetch_assoc($reports);
		if (mysql_num_rows($reports) > 0) {
			//report(s) found
			//put it in an associative array
			$reportData = array();
			do {
				$reportData[] = array('site' => $row_reports['site_id'], 'percent' => $row_reports['netProfitPer']);
			} while($row_reports = mysql_fetch_assoc($reports));
			//redistribute into columns
			foreach ($reportData as $key => $row) {
				$site[$key]  = $row['site'];
				$percent[$key] = $row['percent'];
			}	
			// Sort the data with site ascending, percent descending
			// Add $data as the last parameter, to sort by the common key
			array_multisort($percent, SORT_DESC, $site, SORT_ASC, $reportData);
			
			//print_r($reportData); die;
			
			//clear rankings for this month from db
			$clearSQL = sprintf("DELETE FROM site_cluster_rank WHERE scr_month = %s AND scr_year = %s AND scr_cluster_id = %s",
								GetSQLValueString($month, "int"),
								GetSQLValueString($year, "int"),
								GetSQLValueString($row_clusters['cluster_id'], "int"));
			$clearRS = mysql_query($clearSQL, $fer) or die(mysql_error());
			
			//insert new rankings for this month into db
			foreach ($reportData as $key => $row) {
				$rank = $key+1; //rank 
				$insertSQL = sprintf("INSERT INTO site_cluster_rank (scr_site_id, scr_rank, scr_month, scr_year, scr_cluster_id) VALUES (%s, %s, %s, %s, %s)",
								GetSQLValueString($row['site'], "int"),
								GetSQLValueString($rank, "int"),
								GetSQLValueString($month, "int"),
								GetSQLValueString($year, "int"),
								GetSQLValueString($row_clusters['cluster_id'], "int"));
				$insertRS = mysql_query($insertSQL, $fer) or die(mysql_error());
			}
		}
		//next cluster!
	} while ($row_clusters  = mysql_fetch_assoc($clusters));
	// sites in a cluster rankings done


	//3. Overall Cluster Ranking (based on site-cluster totals)
	//retrieve cluster totals for the month
	mysql_select_db($database_fer, $fer);
	$reportsSQL = sprintf("SELECT * FROM  site_cluster_totals WHERE sct_month = %s AND sct_year = %s",
						GetSQLValueString($month, "int"),
						GetSQLValueString($year, "int"));
	$reports = mysql_query($reportsSQL, $fer) or die(mysql_error());
	$row_reports = mysql_fetch_assoc($reports);
	//put it in an associative array
	$reportData = array();
	do {
		$reportData[] = array('cluster' => $row_reports['sct_cluster_id'], 'percent' => $row_reports['sct_netProfitPer']);
	} while($row_reports = mysql_fetch_assoc($reports));
	//redistribute into columns
	foreach ($reportData as $key => $row) {
		$cluster[$key]  = $row['cluster'];
		$percent[$key] = $row['percent'];
	}	
	// Sort the data with site ascending, percent descending
	// Add $data as the last parameter, to sort by the common key
	array_multisort($percent, SORT_DESC, $cluster, SORT_ASC, $reportData);
	
	//print_r($reportData); die;
	
	//clear rankings for this month from db
	$clearSQL = sprintf("DELETE FROM cluster_overall_rank WHERE cor_month = %s AND cor_year = %s",
						GetSQLValueString($month, "int"),
						GetSQLValueString($year, "int"));
	$clearRS = mysql_query($clearSQL, $fer) or die(mysql_error());
	
	//insert new rankings for this month into db
	foreach ($reportData as $key => $row) {
		$rank = $key+1; //rank 
		$insertSQL = sprintf("INSERT INTO cluster_overall_rank (cor_cluster_id, cor_rank, cor_month, cor_year) VALUES (%s, %s, %s, %s)",
						GetSQLValueString($row['cluster'], "int"),
						GetSQLValueString($rank, "int"),
						GetSQLValueString($month, "int"),
						GetSQLValueString($year, "int"));
		$insertRS = mysql_query($insertSQL, $fer) or die(mysql_error());
	}
	
	//overall site rankings done
	
	//remove mnu tag from db
	$deleteSQL = sprintf("DELETE FROM month_needs_update WHERE mnu_month = %s AND mnu_year = %s",
						GetSQLValueString($month, "int"),
						GetSQLValueString($year, "int"));
	$deleteRS = mysql_query($deleteSQL, $fer) or die(mysql_error());
	
	return true;
}

//DoRankings("08","2014");

//function to tag a month for ranking update
function PromptMonthUpdate() {
	global $database_fer;
	global $fer;
	if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "formCreate")) {
		//insert ignore a "month needs update" entry
		mysql_select_db($database_fer, $fer);
		$insertSQL = sprintf("INSERT IGNORE INTO month_needs_update (mnu_month, mnu_year) VALUES (%s,%s)",
							GetSQLValueString($_POST['month'], "int"),
							GetSQLValueString($_POST['year'], "int"));
		$insertRS = mysql_query($insertSQL, $fer) or die(mysql_error());	
	}
}

//function to determine if re-ranking is needed
function RankingRequired ($month, $year) {
	global $database_fer;
	global $fer;
	mysql_select_db($database_fer, $fer);
	//check mnu table in db
	$checkSQL = sprintf("SELECT * FROM month_needs_update WHERE mnu_month = %s AND mnu_year = %s",
						GetSQLValueString($month, "int"),
						GetSQLValueString($year, "int"));
	$checkRS = mysql_query($checkSQL, $fer) or die(mysql_error());
	
	if(mysql_num_rows($checkRS) > 0) {
		//ranking required
		return true;
	} else {
		//ranking not required
		return false;
	}	
}

//function to recalculate site overall total for a particular month
function SiteOverallTotal ($month, $year) {
	global $database_fer;
	global $fer;
	mysql_select_db($database_fer, $fer);
	
	//create totals entry for the month if it's not available
	$insertSQL = sprintf("INSERT IGNORE INTO site_overall_totals (sot_month, sot_year) VALUES (%s, %s)",
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
	$insertRS = mysql_query($insertSQL, $fer) or die(mysql_error());
	
	//retrieve all site reports for this month
	$reportsSQL = sprintf("SELECT * FROM site_reports WHERE month=%s AND year=%s",
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
	$reports = mysql_query($reportsSQL, $fer) or die(mysql_error());
	$row_reports = mysql_fetch_assoc($reports);
	//initialize totals
	$total_revenue = 0;
	$total_directCost = 0;	 
	//loop through the reports and add up their $revenue and $directCost
	do {
		$total_revenue += $row_reports['revenue'];
		$total_directCost += $row_reports['directCost'];
	} while ($row_reports = mysql_fetch_assoc($reports));
	//update totals entry for the month with the total $revenue and $directCost
	$updateSQL = sprintf("UPDATE site_overall_totals SET sot_revenue=%s, sot_directCost=%s WHERE sot_month=%s AND sot_year=%s",
					GetSQLValueString($total_revenue, "double"),
					GetSQLValueString($total_directCost, "double"),
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
	$updateRS = mysql_query($updateSQL, $fer) or die(mysql_error());
	
	//return
	return;
}

//function to recalculate sites in a cluster total for a particular month
function SiteClusterTotal ($cluster_id, $month, $year) {
	global $database_fer;
	global $fer;
	mysql_select_db($database_fer, $fer);
	
	//create totals entry for the cluster-month if it's not available
	$insertSQL = sprintf("INSERT IGNORE INTO site_cluster_totals (sct_cluster_id, sct_month, sct_year) VALUES (%s, %s, %s)",
					GetSQLValueString($cluster_id, "int"),
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
	$insertRS = mysql_query($insertSQL, $fer) or die(mysql_error());
	
	//retrieve all site reports for this month in this cluster
	$reportsSQL = sprintf("SELECT * FROM site_reports r LEFT JOIN sites s ON r.site_id = s.site_id WHERE s.cluster_id=%s AND month=%s AND year=%s",
					GetSQLValueString($cluster_id, "int"),
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
	$reports = mysql_query($reportsSQL, $fer) or die(mysql_error());
	$row_reports = mysql_fetch_assoc($reports);
	//initialize totals
	$total_revenue = 0;
	$total_directCost = 0;
	$total_apportionedCost = 0;
	$total_totalCost = 0;
	$total_projectProfit = 0;
	$total_netProfit = 0;
	//loop through the reports and add up their $revenue and $directCost
	do {
		$total_revenue += $row_reports['revenue'];
		$total_directCost += $row_reports['directCost'];
		$total_apportionedCost += $row_reports['apportionedCost'];
		$total_totalCost += $row_reports['totalCost'];
		$total_projectProfit += $row_reports['projectProfit'];
		$total_netProfit += $row_reports['netProfit'];
	} while ($row_reports = mysql_fetch_assoc($reports));
	//update totals entry for the cluster-month with the total $revenue and $directCost
	$updateSQL = sprintf("UPDATE site_cluster_totals SET sct_revenue=%s, sct_directCost=%s, sct_apportionedCost=%s, sct_totalCost=%s, sct_projectProfit=%s, sct_netProfit=%s WHERE sct_cluster_id=%s AND sct_month=%s AND sct_year=%s",
					GetSQLValueString($total_revenue, "double"),
					GetSQLValueString($total_directCost, "double"),
					GetSQLValueString($total_apportionedCost, "double"),
					GetSQLValueString($total_totalCost, "double"),
					GetSQLValueString($total_projectProfit, "double"),
					GetSQLValueString($total_netProfit, "double"),
					GetSQLValueString($cluster_id, "int"),
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
	$updateRS = mysql_query($updateSQL, $fer) or die(mysql_error());
	
	//return
	return;
}

//function to recalculate overall cluster totals for a particular month
function ClusterOverallTotal ($month, $year) {
	global $database_fer;
	global $fer;
	mysql_select_db($database_fer, $fer);
	
	//create totals entry for the month if it's not available
	$insertSQL = sprintf("INSERT IGNORE INTO cluster_overall_totals (cot_month, cot_year) VALUES (%s, %s)",
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
	$insertRS = mysql_query($insertSQL, $fer) or die(mysql_error());
	
	//retrieve all cluster totals for this month
	$reportsSQL = sprintf("SELECT * FROM site_cluster_totals WHERE sct_month=%s AND sct_year=%s",
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
	$reports = mysql_query($reportsSQL, $fer) or die(mysql_error());
	$row_reports = mysql_fetch_assoc($reports);
	//initialize totals
	$total_revenue = 0;
	$total_directCost = 0;	 
	//loop through the reports and add up their $revenue and $directCost
	do {
		$total_revenue += $row_reports['scr_revenue'];
		$total_directCost += $row_reports['scr_directCost'];
	} while ($row_reports = mysql_fetch_assoc($reports));
	//update totals entry for the month with the total $revenue and $directCost
	$updateSQL = sprintf("UPDATE cluster_overall_totals SET cot_revenue=%s, cot_directCost=%s WHERE cot_month=%s AND cot_year=%s",
					GetSQLValueString($total_revenue, "double"),
					GetSQLValueString($total_directCost, "double"),
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
	$updateRS = mysql_query($updateSQL, $fer) or die(mysql_error());
	
	//return
	return;
}

//function to get a site's cluster id
function GetClusterID ($site_id) {
	global $database_fer;
	global $fer;
	mysql_select_db($database_fer, $fer);

	$siteSQL = sprintf("SELECT * FROM sites WHERE site_id = %s", GetSQLValueString($site_id,"int"));
	$siteRS = mysql_query($siteSQL, $fer) or die(mysql_error());
	$site = mysql_fetch_assoc($siteRS);
	
	if (mysql_num_rows($siteRS) > 0) {
		return $site['cluster_id'];
	} else {
		return false;
	}
}

//function to recalculate derived fields for sites and clusters in a particular month
function DerivedFields ($month, $year) {
	global $database_fer;
	global $fer;
	mysql_select_db($database_fer, $fer);
	
	//site reports
	//get total direct cost for the month
	$dcSQL = sprintf("SELECT * FROM site_overall_totals WHERE sot_month=%s AND sot_year=%s",
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
	$dcRS = mysql_query($dcSQL, $fer) or die(mysql_error());
	$row_dc = mysql_fetch_assoc($dcRS);
	$total_directCost = $row_dc['sot_directCost'];
	//get apportioned cost for the month
	$acSQL = sprintf("SELECT * FROM apportioned_costs WHERE ac_month=%s AND ac_year=%s",
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
	$acRS = mysql_query($acSQL, $fer) or die(mysql_error());	
	$row_ac = mysql_fetch_assoc($acRS);
	$total_apportionedCost = $row_ac['ac_apportionedCost'];
	//select site reports for the month
	$reportsSQL = sprintf("SELECT * FROM site_reports WHERE month=%s AND year=%s",
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
	$reports = mysql_query($reportsSQL, $fer) or die(mysql_error());
	$row_reports = mysql_fetch_assoc($reports);
	
	//loop through site reports
	do {
		//calculate and update derived fields in db
		$updateSQL = sprintf("UPDATE site_reports SET apportionedCost=%s, totalCost=%s, projectProfit=%s, projectPer=%s, netProfit=%s, netProfitPer=%s WHERE report_id=%s",
						GetSQLValueString($apportionedCost = $row_reports['directCost']/$total_directCost*$total_apportionedCost, "double"),
						GetSQLValueString($totalCost = round($row_reports['directCost'] + $apportionedCost, 2), "double"),
						GetSQLValueString($projectProfit = round($row_reports['revenue'] - $row_reports['directCost'],2), "double"),
						GetSQLValueString($projectPer = round($projectProfit/$row_reports['revenue']*100,2), "double"),
						GetSQLValueString($netProfit = round($row_reports['revenue'] - $totalCost,2), "double"),
						GetSQLValueString($netProfitPer = round($netProfit/$row_reports['revenue']*100,2), "double"),
						GetSQLValueString($row_reports['report_id'], "int"));
		$updateRS = mysql_query($updateSQL, $fer) or die(mysql_error());		
		
	} while ($row_reports = mysql_fetch_assoc($reports));
	//recalculate all site totals for the month
	//SiteOverallTotal($month, $year);
	//get existing cluster totals
	$sctsSQL = sprintf("SELECT * FROM site_cluster_totals WHERE sct_month=%s AND sct_year=%s",
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
	$scts = mysql_query($sctsSQL, $fer) or die(mysql_error());	
	$row_scts = mysql_fetch_assoc($scts);
	//loop through cluster totals	
	do {		
		//recalculate site totals for the cluster
		SiteClusterTotal($row_scts['sct_cluster_id'],$month,$year);
		//calculate percentages for the cluster and update in db
		//retrieve new values
		$ct_query = sprintf("SELECT * FROM site_cluster_totals WHERE sct_cluster_id=%s AND sct_month=%s AND sct_year=%s", 
					GetSQLValueString($row_scts['sct_cluster_id'], "int"),
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));

		$ct = mysql_query($ct_query, $fer) or die(mysql_error());
		$row_ct = mysql_fetch_assoc($ct);
		//update db with percentages
		$updateSQL = sprintf("UPDATE site_cluster_totals SET sct_projectPer=%s, sct_netProfitPer=%s WHERE sct_cluster_id=%s AND sct_month=%s AND sct_year=%s",
					GetSQLValueString(round($row_ct['sct_projectProfit']/$row_ct['sct_revenue']*100,2), "double"),
					GetSQLValueString(round($row_ct['sct_netProfit']/$row_ct['sct_revenue']*100,2), "double"),
					GetSQLValueString($row_scts['sct_cluster_id'], "int"),
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
		$updateRS = mysql_query($updateSQL, $fer) or die(mysql_error());		
		
	} while ($row_scts = mysql_fetch_assoc($scts));

}

//function to check if Apportioned Cost has been provided for a particular month
function AppCostAvailable($month, $year) {
	global $database_fer;
	global $fer;
	mysql_select_db($database_fer, $fer);
	
	$acSQL = sprintf("SELECT * FROM apportioned_costs WHERE ac_month=%s AND ac_year=%s",
					GetSQLValueString($month, "int"),
					GetSQLValueString($year, "int"));
	$acRS = mysql_query($acSQL, $fer) or die(mysql_error());
	if(mysql_num_rows($acRS) > 0) {
		return true;
	}  else {
		return false;
	}
}

?>