<?php error_reporting(0); ?>
<?php require_once("_inc_checkSession.php"); ?>
<?php $thisPage = basename( $_SERVER['PHP_SELF'] ); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_employers = 20;
$pageNum_employers = 0;
if (isset($_GET['pageNum_employers'])) {
  $pageNum_employers = $_GET['pageNum_employers'];
}
$startRow_employers = $pageNum_employers * $maxRows_employers;

mysql_select_db($database_fer, $fer);
$query_employers = "SELECT *, (SELECT COUNT(*) FROM vacancies v WHERE v.employer_id = e.id) AS vCount FROM employers e ORDER BY e.dateReg DESC";
$query_limit_employers = sprintf("%s LIMIT %d, %d", $query_employers, $startRow_employers, $maxRows_employers);
$employers = mysql_query($query_limit_employers, $fer) or die(mysql_error());
$row_employers = mysql_fetch_assoc($employers);

if (isset($_GET['totalRows_employers'])) {
  $totalRows_employers = $_GET['totalRows_employers'];
} else {
  $all_employers = mysql_query($query_employers);
  $totalRows_employers = mysql_num_rows($all_employers);
}
$totalPages_employers = ceil($totalRows_employers/$maxRows_employers)-1;

$queryString_employers = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_employers") == false && 
        stristr($param, "totalRows_employers") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_employers = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_employers = sprintf("&totalRows_employers=%d%s", $totalRows_employers, $queryString_employers);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Companies | <?php echo $config['shortname'] ?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
	<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
	<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
	<link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
	<link href="../SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<script type="text/javascript">
<!-- Validation for delete -->
function ValidateDel (id,dest,msg) {

	var where_to = confirm(msg);
	
	if (where_to== true)
	 {
	   window.location.href = dest + id;
	   //alert('where to is true');
	 }
	else
	 {
	  return false;
	  }
}	  
</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('-inc-top.php'); ?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<?php include('-inc-navbar-side.php'); ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					Widget settings form goes here
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Company <small> </small> </h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
		                  <li><a href="#">Companies</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) { ?>
			    <div class="alert  alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg'] ?></strong> </div>
			    <?php } ?>
			    <?php if (isset($_GET['error'])) { ?>
			    <div class="alert alert-error">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['error'] ?></strong> </div>
			    <?php } ?>
			    <div id="dashboard">
			        <!-- BEGIN DASHBOARD STATS -->
			        <div class="row-fluid">
			            <div class="span12">
			                <!-- BEGIN SAMPLE FORM PORTLET-->
			                <div class="portlet box green tabbable">
			                    <div class="portlet-title">
			                        <div class="caption"> <i class="icon-reorder"></i> <span class="hidden-480">Company List</span> </div>
		                        </div>
			                    <div class="portlet-body">
			                        <div class="tabbable portlet-tabs">
			                            <!-- BEGIN FORM-->
										<div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid">
										    <table class="table table-striped table-bordered table-hover" id="sample_1" style="margin:10px;">
										        <thead>
										            <tr>
										                <th width="50%"><strong>Company Name</strong></th>
										                <th width="20%">Vacancy Count</th>
										                <th width="15%">&nbsp;</th>
										                <th width="15%">&nbsp;</th>
									                </tr>
									            </thead>
										        <tbody>
										            <?php do { ?>
									                <tr>
									                    <td valign="top"><a href="employer-details.php?id=<?php echo $row_employers['id']; ?>"><?php echo $row_employers['companyName']; ?></a></td>
									                    <td valign="top"><a href="vacancies.php?company=<?php echo $row_employers['id']; ?>" title="View Vacancies" class="tooltips"><?php echo $row_employers['vCount'] ?></a></td>
									                    <td align="center" valign="top"><strong>
									                        <?php if (isset($row_employers['enabled'])) { ?>
									                            <a href="#" onclick=" return ValidateDel('<?php echo $row_employers['id']; ?>','employerStatus.php?action=disable&amp;id=','Are you sure you want to DISABLE THIS USER? (The user will not be able to access the portal at all)!!!');">                        
								                                Disable</a>
									                            <?php } else { ?>
									                            <a href="employerStatus.php?id=<?php echo $row_employers['id']; ?>&amp;action=enable">Enable</a>
									                            <?php } ?>
								                        </strong></td>
									                    <td valign="top">
									                        <a href="employer-edit.php?id=<?php echo $row_employers['id']; ?>" class="tooltips" title="Edit"><i class="icon-edit"></i></a>
									                        &nbsp;
									                        <a href="#" class="tooltips" title="Delete" onclick=" return ValidateDel('<?php echo $row_employers['id']; ?>','employer-delete.php?id=','Are you sure you want to DELETE THIS EMPLOYER, ALL THEIR VACANCIES, APPLICATIONS etc? (This Action is NOT reversible)!!!');"><i class="icon-remove"></i></a>
								                        </td>
								                    </tr>
									                <?php } while ($row_employers = mysql_fetch_assoc($employers)); ?>   
									            </tbody>
									        </table>
									    </div>	 
		                              <!-- END FORM-->
                                     <div class="row-fluid">
			                            <div class="span6">
			                                <div class="dataTables_info" id="sample_1_info">Showing <?php echo ($startRow_employers + 1) ?> to <?php echo min($startRow_employers + $maxRows_employers, $totalRows_employers) ?> of <?php echo $totalRows_employers ?> entries</div>
		                                </div>
			                            <div class="span6">
			                                <div class="dataTables_paginate paging_bootstrap pagination">
			                                    <ul>
			                                        <?php if ($pageNum_employers > 0) { // Show if not first page ?>
			                                        <li class="prev"><a href="<?php printf("%s?pageNum_employers=%d%s", $thispage, 0, $queryString_employers); ?>" title="First Page">&laquo; </a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_employers > 0) { // Show if not first page ?>
			                                        <li class="prev"><a href="<?php printf("%s?pageNum_employers=%d%s", $thispage, max(0, $pageNum_employers - 1), $queryString_employers); ?>" title="Previous Page">&lsaquo;</a></li>
			                                        <?php } ?>
			                                        <?php $thisPageNumber = $pageNum_employers+1;
                                                //determine startPage and endPage
                                                $totalPages = $totalPages_employers +1;
                                                if($totalPages < 4) {
                                                    $startPage = 1;
                                                    $endPage = $totalPages;
                                                } elseif($thisPageNumber <= 4) {
                                                    $startPage = 1;
                                                    $endPage = 4;
                                                } else {
                                                    $startPage = $thisPageNumber - 3;
                                                    $endPage = $thisPageNumber;
                                                }
                                              ?>
			                                        <?php for ($i = $startPage; $i<=$endPage; $i++) { ?>
			                                        <li class="<?php if ($i == $thisPageNumber) echo "active"; ?>"><a href="<?php echo $thispage ?>?pageNum_employers=<?php echo $i-1 ?>&amp;totalRows_employers=<?php echo $totalRows_employers ?>" title="Page <?php echo $i ?>"><?php echo $i ?></a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_employers < $totalPages_employers) { // Show if not last page ?>
			                                        <li class="next"><a href="<?php printf("%s?pageNum_employers=%d%s", $thispage, min($totalPages_employers, $pageNum_employers + 1), $queryString_employers); ?>" title="Next Page">&rsaquo; </a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_employers < $totalPages_employers) { // Show if not last page ?>
			                                        <li class="next"><a href="<?php printf("%s?pageNum_employers=%d%s", $thispage, $totalPages_employers, $queryString_employers); ?>" title="Last Page">&raquo; </a></li>
			                                        <?php } ?>
		                                        </ul>
		                                    </div>
		                                </div>
		                            </div>
		                            </div>
</div>
</div>
                            <!-- END SAMPLE FORM PORTLET-->
                        </div>
</div>
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>
                
		    </div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include('-inc-footer.php'); ?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script> 
	<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>   
	<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>   
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script> 
	<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>     
	<!-- END PAGE LEVEL SCRIPTS -->  
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
<script>
		$(document).ready(function(e) {
		   $("#month").val('<?php echo $row_report['month'] ?>');
		   $("#year").val('<?php echo $row_report['year'] ?>');
        });
	</script>
	<script>
jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   FormComponents.init();
		});
    </script>
<script type="text/javascript">
$(document).ready(function() {
	if($("#country2").val() != '') {
		$("#country").val($("#country2").val());
	}

	$('.stateArea').hide();
	if($('select#country').val() == 'Nigeria') {
		$('.stateArea').show();
	}
		
	if($("#state2").val() != '') {
		$("#state").val($("#state2").val());
	}

  	//actions for dropdowns - nationality (to display state) and state( to display LGAs)
	$("select#country").change(function()
	{
		//alert("change nationality");
		if($(this).val() != "Nigeria")
		{
			$('.stateArea').hide();//alert("not Nigeria");
		}
		else
		{
			$('.stateArea').show();
		}
	});//end of nationality
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>