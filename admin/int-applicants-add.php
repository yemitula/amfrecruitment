<?php require_once "_inc_checkSession.php";?>
<?php $thisPage = basename($_SERVER['PHP_SELF']);?>
<?php require_once '../_inc_config.php';?>
<?php require_once '../Connections/fer.php';?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

mysql_select_db($database_fer, $fer);

//interview
$query_interview = sprintf("SELECT * FROM interviews LEFT JOIN vacancies ON int_vacancy_id = id WHERE int_id=%s", GetSQLValueString($_GET['id'],"int"));
$interview = mysql_query($query_interview, $fer) or die(mysql_error());
$row_interview = mysql_fetch_assoc($interview);

//applicants
$applicants_SQL = "SELECT id, surname, firstname FROM applicants ORDER BY surname";
$applicants = mysql_query($applicants_SQL, $fer) or die(mysql_error());
$row_applicants = mysql_fetch_assoc($applicants);

$currentPage = $_SERVER["PHP_SELF"];

//delete applicant from interview list
if(isset($_GET['d']) && !empty($_GET['d'])) {
	//get d interview id
	$interview_SQL = sprintf("SELECT ir_interview_id FROM interview_records WHERE ir_id=%s", GetSQLValueString($_GET['d'], "int"));
	$interview = mysql_query($interview_SQL, $fer) or die(mysql_error());
	$row_interview = mysql_fetch_assoc($interview);
	$int_id = $row_interview['ir_interview_id'];

	//delete the record
	$del_SQL = sprintf("DELETE FROM interview_records WHERE ir_id=%s", GetSQLValueString($_GET['d'], "int"));
	$del_RS = mysql_query($del_SQL, $fer) or die(mysql_error());

	header("location: int-applicants-list.php?id=$int_id&msg=Applicant Removed successfully!"); exit;
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "formIntervieW")) {
	
	//var_dump($_POST); die;
	//die(date("Y-m-d h:i:s", strtotime($_POST['int_date'])));

	foreach ($_POST['applicants'] as $app_id) {
		$insertSQL = sprintf("INSERT INTO interview_records (ir_interview_id, ir_applicant_id) VALUES (%s, %s)",
		GetSQLValueString($_POST['int_id'], "int"),
		GetSQLValueString($app_id, "int"));
		$Result1 = mysql_query($insertSQL, $fer) or die(mysql_error());
	}
	
	$insertGoTo = "int-applicants-list.php?id=$_POST[int_id]&msg=".urlencode("Applicants Added for Interview Successfully!");
	if (isset($_SERVER['QUERY_STRING'])) {
		$insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
		$insertGoTo .= $_SERVER['QUERY_STRING'];
	}
	header(sprintf("Location: %s", $insertGoTo));
}

$currentPage = $_SERVER["PHP_SELF"];
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Add Applicants for Interview | <?php echo $config['shortname']?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
	<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
	<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
	<link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include '-inc-top.php';?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<?php include '-inc-navbar-side.php';?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Interview Applicants <small>FOR <strong><?php echo $row_interview['int_vacancy_id']? $row_interview['title'] : $row_interview['int_purpose'] ?></strong> SCHEDULED FOR <strong><?php echo date("d-M-Y g:i A", strtotime($row_interview['int_date'])) ?></strong> </small> </h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="int-list.php">Interviews</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#"> Add Applicants for Interview</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) {?>
			    <div class="alert  alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg']?></strong> </div>
			    <?php }
?>
			    <?php if (isset($_GET['error'])) {?>
			    <div class="alert alert-error">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['error']?></strong> </div>
			    <?php }
?>
			    <div id="dashboard">
			        <!-- BEGIN DASHBOARD STATS -->
			        <div class="row-fluid">
			            <div class="span12">
			                <!-- BEGIN SAMPLE FORM PORTLET-->
			                <div class="portlet box green tabbable">
			                    <div class="portlet-title">
			                        <div class="caption"> <i class="icon-reorder"></i> <span class="hidden-480">Select Applicants to Add</span> </div>
		                        </div>
			                    <div class="portlet-body form">
			                        <div class="tabbable portlet-tabs">
			                            <!-- BEGIN FORM-->
			                            <form method="POST" name="formCreate" action="<?php echo $editFormAction;?>" class="form-horizontal" id="formCreate">
			                                <p>&nbsp;</p>
			                                <span id="dates">


                                            
                                            </span>

			                                <div class="control-group">
			                                    <label class="control-label">Applicants to add</label>
			                                    <div class="controls">
													<select data-placeholder="Type applicant's name to filter" class="chosen span6" multiple="multiple" tabindex="6" name="applicants[]" id="applicants_selector">
														<option value="">&nbsp;</option>
														<?php do { ?>
														<option value="<?php echo $row_applicants['id'] ?>">(<?php echo $row_applicants['id'] ?>) <?php echo $row_applicants['surname'] ?> <?php echo $row_applicants['firstname'] ?></option>
														<?php } while($row_applicants = mysql_fetch_assoc($applicants)); ?>
													</select>
												</div>
		                                    </div>

			                                <br><br><br><br><br><br><br><br><br><br><br><br>
			                                

		                                    <div class="form-actions">
			                                    <button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
			                                    <a href="int-applicants-list.php?id=<?php echo $_GET['id'] ?>">
			                                        <button type="button" class="btn">&laquo; Back to Interview Applicant List</button>
		                                        </a>

			                                    <input name="int_id" type="hidden" id="int_id" value="<?php echo $row_interview['int_id'];?>">
			                                </div>
			                                <input type="hidden" name="MM_update" value="formCreate">
		                                    <input type="hidden" name="MM_insert" value="formIntervieW">
			                            </form>
			                            <!-- END FORM-->
		                            </div>
</div>
</div>
                            <!-- END SAMPLE FORM PORTLET-->
                        </div>
</div>
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>

		    </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include '-inc-footer.php';?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>
	<![endif]-->
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
<script>
		$(document).ready(function(e) {
		   //$("#employer_id").val('<?php echo $row_vacancy['employer_id']?>');
		   if($("#int_vacancy_id").val() == '0') {
	   			$(".interview-purpose").show();
	   		} else {
				$(".interview-purpose").hide();
	   		}

		   $("#int_vacancy_id").change(function(){
		   		if($(this).val() == '0') {
		   			$(".interview-purpose").show();
		   		} else {
					$(".interview-purpose").hide();
		   			$("#int_purpose").val('');
		   		}
		   });

		   $("#formCreate").submit(function() {
		   		//alert('save now!');
		   		if($("#int_vacancy_id").val() == 0 && $("#int_purpose").val() == '') {
		   			alert("Since this interview is not attached to an existing vacancy, please specify the purpose of this interview!");
		   			return false;
		   		}
		   })
        });
	</script>
	<script>
jQuery(document).ready(function() {
		   App.init(); // initlayout and core plugins
		   FormComponents.init();

		   $(".form_datetime").datetimepicker({
	            isRTL: App.isRTL(),
	            format: "dd-mm-yyyy hh:ii",
	            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
        	});

		});
    </script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>