<?php require_once("_inc_checkSession.php"); ?>
<?php $thisPage = basename( $_SERVER['PHP_SELF'] ); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$applicant_id = $_GET['id'];
mysql_select_db($database_fer, $fer);
$query_applications = "SELECT a.id, v.title, a.vacancy_id, a.dateApplied, a.status, ap.firstname, ap.surname FROM applications a LEFT JOIN vacancies v ON (a.vacancy_id = v.id) LEFT JOIN applicants ap ON (a.applicant_id = ap.id) WHERE a.applicant_id = '$applicant_id'";
$applications = mysql_query($query_applications, $fer) or die(mysql_error());
$row_applications = mysql_fetch_assoc($applications);
$totalRows_applications = mysql_num_rows($applications);

$colname_applicant = "-1";
if (isset($_GET['id'])) {
  $colname_applicant = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_applicant = sprintf("SELECT * FROM applicants WHERE id = %s", GetSQLValueString($colname_applicant, "int"));
$applicant = mysql_query($query_applicant, $fer) or die(mysql_error());
$row_applicant = mysql_fetch_assoc($applicant);
$totalRows_applicant = mysql_num_rows($applicant);

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Applicant's Applications | <?php echo $config['shortname'] ?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link href="assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('-inc-top.php'); ?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<?php include('-inc-navbar-side.php'); ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> <?php echo $row_applicant['firstname']; ?> <?php echo $row_applicant['surname']; ?>'s Applications <small>applications <?php echo $row_applicant['firstname']; ?> applied for</small></h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="applicants.php">Applicants</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#">Applicant's Applications</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) { ?>
			    <div class="alert alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg'] ?></strong> </div>
			    <?php } ?>
			    <?php if ($totalRows_applications == 0) { // Show if recordset empty ?>
			    <div class="row-fluid">
			        <div class="alert">
			            <button class="close" data-dismiss="alert"></button>
			            <strong>Empty List!</strong> No applications yet! </div>
		        </div>
			    <?php } // Show if recordset empty ?>			        
		        </p>
			    <div class="row-fluid">
			        <div class="span12">
                                    <?php if ($totalRows_applications > 0) { // Show if recordset not empty ?>
			            <!-- BEGIN EXAMPLE TABLE PORTLET-->
			            <div class="portlet box light-grey">
			                <div class="portlet-title">
			                    <div class="caption"><i class="icon-barcode"></i>Vacancies <?php echo $row_applicant['firstname']; ?> Applied for</div>
		                    </div>
			                <div class="portlet-body">
			                    <div class="table-toolbar"></div>
			                    <div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid"> <br>
			                        
			                        <div class="row-fluid">
			                            <div class="span6"></div>
			                            <div class="span6">
			                                <!--<div class="dataTables_filter" id="sample_1_filter">
                  <label>Search: <input type="text" aria-controls="sample_1" class="m-wrap medium"></label>
                  </div>-->
		                                </div>
		                            </div>
			                        <table class="table table-striped table-bordered table-hover" id="sample_1">
			                            <thead>
			                                <tr>
			                                    <th width="40%">Vacancy Title</th>
			                                    <th width="10%" >Date Applied</th>
			                                    <th width="10%" >Shortlisted?</th>
			                                    <th width="20%" >Current Status</th>
			                                    <th width="20%" >&nbsp;</th>
		                                    </tr>
		                                </thead>
			                            <tbody>
			                                <?php do { ?>
			                                <tr class="odd gradeX">
			                                    <td><a href="vacancy-details.php?id=<?php echo $row_applications['vacancy_id']; ?>" target="_blank"><?php echo $row_applications['title']; ?></a></td>
			                                    <td ><?php echo date("d-M-Y",strtotime($row_applications['dateApplied'])); ?></td>
			                                    <td ><?php echo $row_applications['shortlisted']? 'Yes':'No' ?></td>
			                                    <td class="center"><?php echo $row_applications['status'] ?></td>
			                                    <td >
			                                        
		                                        <a title="Delete Application" class="ValidateAction tooltips" data-placement="top" data-confirm-msg="Are you sure you want to Delete? All information associated with this application will also be cleared!!!" href="application-delete.php?id=<?php echo $row_applications['id']; ?>&"><i class="icon-remove"></i></a></td>
		                                    </tr>
			                                <?php } while ($row_applications = mysql_fetch_assoc($applications)); ?>
		                                </tbody>
		                            </table>
			                        
		                        </div>
		                    </div>
		                </div>
			            <!-- END EXAMPLE TABLE PORTLET-->
                                    <?php } // Show if recordset not empty ?>
		            </div>
		        </div>
			    
			    <div class="clearfix"></div>
		    </div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include('-inc-footer.php'); ?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>   
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  
	<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
	<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>     
	<!-- END PAGE LEVEL SCRIPTS -->  
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
    <script>
		$(document).ready(function(e) {
		   $("#company").val('<?php echo $_GET['company'] ?>');
		   $("#status").val('<?php echo $_GET['status'] ?>');
		   $("#fromDate").val('<?php echo $_GET['fromDate'] ?>');
		   $("#toDate").val('<?php echo $_GET['toDate'] ?>');
        });
	</script>
	<script>
		jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   FormComponents.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<?php
mysql_free_result($applications);

mysql_free_result($applicant);

mysql_free_result($companies);

mysql_free_result($sites);
?>
