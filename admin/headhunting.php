<?php require_once("_inc_checkSession.php"); ?>
<?php error_reporting(0); ?>
<?php //var_dump($_SESSION['formHeadhunt']); die; ?>
<?php require_once('../Connections/fer.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_fer, $fer);
$query_country = "SELECT * FROM countries";
$country = mysql_query($query_country, $fer) or die(mysql_error());
$row_country = mysql_fetch_assoc($country);
$totalRows_country = mysql_num_rows($country);

mysql_select_db($database_fer, $fer);
$query_industry = "SELECT * FROM job_categories";
$industry = mysql_query($query_industry, $fer) or die(mysql_error());
$row_industry = mysql_fetch_assoc($industry);
$totalRows_industry = mysql_num_rows($industry);

mysql_select_db($database_fer, $fer);
$query_discipline = "SELECT * FROM job_categories";
$discipline = mysql_query($query_discipline, $fer) or die(mysql_error());
$row_discipline = mysql_fetch_assoc($discipline);
$totalRows_discipline = mysql_num_rows($discipline);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Headhunting | <?php echo $config['shortname'] ?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
		<link rel="stylesheet" type="text/css" href="plugin/tag/jquery.tag-editor.css" />

	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
	<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
	
	<link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>

<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('-inc-top.php'); ?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<?php include('-inc-navbar-side.php'); ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Headhunting <small>hunt for applicants matching your criteria </small> </h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="applicants.php">Applicants</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#"> Headhunting</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) { ?>
			    <div class="alert  alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg'] ?></strong> </div>
			    <?php } ?>
			    <?php if (isset($_GET['error'])) { ?>
			    <div class="alert alert-error">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['error'] ?></strong> </div>
			    <?php } ?>
			    <div id="dashboard">
			        <!-- BEGIN DASHBOARD STATS -->
			        <div class="row-fluid">
			            <div class="span12">
			                <!-- BEGIN SAMPLE FORM PORTLET-->
			                <div class="portlet box green tabbable">
			                    <div class="portlet-title">
			                        <div class="caption"> <i class="icon-reorder"></i> Select Criteria for Headhunting  <strong><?php echo $row_vacancy['title']; ?></strong></div>
		                        </div>
			                    <div class="portlet-body form">
			                        <div class="tabbable portlet-tabs">
			                            <!-- BEGIN FORM-->
			                            <br>
                                        <form action="headhunting-results.php" method="get" enctype="multipart/form-data" name="formHeadhunting" id="formHeadhunting">
<div class="header"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
                                            <tr>
                                                <td width="30%" align="right" valign="top"><strong>Gender</strong></td>
                                                <td width="74%" valign="top"><select name="gender" id="gender" class="filterField">
	                                                    <option selected="selected" value="" <?php if (!(strcmp("", $_SESSION['formHeadhunt']['gender']))) {echo "selected=\"selected\"";} ?>>Any</option>
	                                                    <option value="male" <?php if (!(strcmp("male", $_SESSION['formHeadhunt']['gender']))) {echo "selected=\"selected\"";} ?>>Male</option>
	                                                    <option value="female" <?php if (!(strcmp("female", $_SESSION['formHeadhunt']['gender']))) {echo "selected=\"selected\"";} ?>>Female</option>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top"><strong>Age</strong></td>
                                                <td valign="top"><select name="ageOp" id="ageOp" class="filterField">
	                                                    <option selected="selected" value="" <?php if (!(strcmp("", $_SESSION['formHeadhunt']['ageOp']))) {echo "selected=\"selected\"";} ?>>Any</option>
	                                                    <option value="=" <?php if (!(strcmp("=", $_SESSION['formHeadhunt']['ageOp']))) {echo "selected=\"selected\"";} ?>>Equal To</option>
	                                                    <option <?php if (!(strcmp(">", $_SESSION['formHeadhunt']['ageOp']))) {echo "selected=\"selected\"";} ?> value="&gt;">Greater Than</option>
	                                                    <option <?php if (!(strcmp(">=", $_SESSION['formHeadhunt']['ageOp']))) {echo "selected=\"selected\"";} ?> value="&gt;=">Greater Than or Equal To</option>
	                                                    <option value="BETWEEN" <?php if (!(strcmp("BETWEEN", $_SESSION['formHeadhunt']['ageOp']))) {echo "selected=\"selected\"";} ?>>BETWEEN</option>
	                                                    </select>
                                                    <input name="age" type="text" id="age" size="10" value="<?php echo $_SESSION['formHeadhunt']['age'] ?>" />
                                                    <input name="age2" type="text" id="age2" size="10" value="<?php echo $_SESSION['formHeadhunt']['age2'] ?>" /></td>
                                            </tr>
                                            <tr class="stateArea">
                                                <td align="right" valign="top"><strong>NYSC Status</strong></td>
                                                <td valign="top"><select name="nyscStatus" id="nyscStatus" class="filterField">
	                                                    <option value="" selected="selected" <?php if (!(strcmp("", $_SESSION['formHeadhunt']['nyscStatus']))) {echo "selected=\"selected\"";} ?>>Doesn't Matter</option>
	                                                    <option value="Yet to start" <?php if (!(strcmp("Yet to start", $_SESSION['formHeadhunt']['nyscStatus']))) {echo "selected=\"selected\"";} ?>>Yet to start</option>
	                                                    <option value="Completed" <?php if (!(strcmp("Completed", $_SESSION['formHeadhunt']['nyscStatus']))) {echo "selected=\"selected\"";} ?>>Completed</option>
	                                                    <option value="Currently Serving" <?php if (!(strcmp("Currently Serving", $_SESSION['formHeadhunt']['nyscStatus']))) {echo "selected=\"selected\"";} ?>>Currently Serving</option>
	                                                    <option value="Exempted" <?php if (!(strcmp("Exempted", $_SESSION['formHeadhunt']['nyscStatus']))) {echo "selected=\"selected\"";} ?>>Exempted</option>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top"><strong> Professional Certification?</strong></td>
                                                <td valign="top"><select name="profcert" id="profcert" class="filterField">
	                                                    <option value="" <?php if (!(strcmp("", $_SESSION['formHeadhunt']['profcert']))) {echo "selected=\"selected\"";} ?>>Doesn't Matter</option>
	                                                    <option value="Yes" <?php if (!(strcmp("Yes", $_SESSION['formHeadhunt']['profcert']))) {echo "selected=\"selected\"";} ?>>Yes</option>
	                                                    <option value="No" <?php if (!(strcmp("No", $_SESSION['formHeadhunt']['profcert']))) {echo "selected=\"selected\"";} ?>>No</option>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top"><strong>Tertiary Course</strong></td>
                                                <td valign="top"><input name="course" type="text" class="fullBox filterField" id="course" placeholder="Leave as blank to exclude this criterion" value="<?php echo $_SESSION['formHeadhunt']['course'] ?>" /></td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top"><strong>Tertiary Institution</strong></td>
                                                <td valign="top"><input name="institution" type="text" class="fullBox filterField" id="institution" placeholder="Leave as blank to exclude this criterion" value="<?php echo $_SESSION['formHeadhunt']['institution'] ?>" /></td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top"><strong>Tertiary Class</strong></td>
                                                <td valign="top" class="fieldDescription"><select name="class" id="class" class="filterField">
	                                                    <option value="" selected="selected" <?php if (!(strcmp("", $_SESSION['formHeadhunt']['class']))) {echo "selected=\"selected\"";} ?>>Any</option>
	                                                    <option value="First Class" <?php if (!(strcmp("First Class", $_SESSION['formHeadhunt']['class']))) {echo "selected=\"selected\"";} ?>>First Class</option>
	                                                    <option value="Second Class Upper" <?php if (!(strcmp("Second Class Upper", $_SESSION['formHeadhunt']['class']))) {echo "selected=\"selected\"";} ?>>2:1</option>
	                                                    <option value="Second Class Lower" <?php if (!(strcmp("Second Class Lower", $_SESSION['formHeadhunt']['class']))) {echo "selected=\"selected\"";} ?>>2:2</option>
	                                                    <option value="Third Class" <?php if (!(strcmp("Third Class", $_SESSION['formHeadhunt']['class']))) {echo "selected=\"selected\"";} ?>>3rd Class</option>
	                                                    <option value="Pass" <?php if (!(strcmp("Pass", $_SESSION['formHeadhunt']['class']))) {echo "selected=\"selected\"";} ?>>Pass</option>
	                                                    <option value="Distinction" <?php if (!(strcmp("Distinction", $_SESSION['formHeadhunt']['class']))) {echo "selected=\"selected\"";} ?>>Distinction</option>
	                                                    <option value="Upper Credit" <?php if (!(strcmp("Upper Credit", $_SESSION['formHeadhunt']['class']))) {echo "selected=\"selected\"";} ?>>Upper Credit</option>
	                                                    <option value="Lower Credit" <?php if (!(strcmp("Lower Credit", $_SESSION['formHeadhunt']['class']))) {echo "selected=\"selected\"";} ?>>Lower Credit</option>
	                                                    <option value="In View/Awaiting Result" <?php if (!(strcmp("In View/Awaiting Result", $_SESSION['formHeadhunt']['class']))) {echo "selected=\"selected\"";} ?>>In View/Awaiting Result</option>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top"><strong>Education country</strong></td>
                                                <td valign="top">
                                                	<select name="country" id="country" class="filterField">
                                                	  <option value="">--Any Country--</option>
                                                	  <?php
do {  
?>
                                                	  <option value="<?php echo $row_country['Country']?>"><?php echo $row_country['Country']?></option>
                                                	  <?php
} while ($row_country = mysql_fetch_assoc($country));
  $rows = mysql_num_rows($country);
  if($rows > 0) {
      mysql_data_seek($country, 0);
	  $row_country = mysql_fetch_assoc($country);
  }
?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top"><strong>Work Experience Industry</strong></td>
                                                <td valign="top" class="fieldDescription">
                                                    <p>
                                                    	<SELECT name="industry" id="industry" class="filterField">
                                                    	  <option value="">-- Any Industry --</option>
                                                    	  <?php
do {  
?>
                                                    	  <option value="<?php echo $row_industry['id_cat']?>"><?php echo $row_industry['cat_name']?></option>
                                                    	  <?php
} while ($row_industry = mysql_fetch_assoc($industry));
  $rows = mysql_num_rows($industry);
  if($rows > 0) {
      mysql_data_seek($industry, 0);
	  $row_industry = mysql_fetch_assoc($industry);
  }
?>
                                                        </SELECT>
                                                    </p></td>
                                            </tr>

                                            <tr>
                                                <td align="right" valign="top"><strong>Work Experience Position</strong></td>
                                                <td valign="top" class="fieldDescription"><p>looks like</p>
                                                    <p>
                                                        <input type="text" class="filterField" name="position" id="position" placeholder="Leave as blank to exclude this criterion" value="<?php echo $_SESSION['formHeadhunt']['position'] ?>" />
                                                    </p></td>
                                            </tr>
                                            <!-- <tr>
                                                <td align="right" valign="top"><strong>Work Experience Level</strong></td>
                                                <td valign="top" class="fieldDescription"><p></p>
                                                    <p>
                                                        <select name="job_level" class="width244" id="job_level">
										       <option value="">Any Level</option>
										       <option value="Managerial">Managerial</option>
										    	<option value="Non-Managerial">Non-Managerial</option>
										       </select>
                                                    </p></td>
                                            </tr> -->
                                            <tr>
                                                <td align="right" valign="top"><strong>Work Experience Description</strong></td>
                                                <td valign="top" class="fieldDescription"><p>contains</p>
                                                    <p>
                                                        <input name="description" type="text" class="fullBox filterField" id="description" placeholder="Leave as blank to exclude this criterion" value="<?php echo $_SESSION['formHeadhunt']['description'] ?>" />
                                                    </p></td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top"><strong>Work Experience Employer</strong></td>
                                                <td valign="top" class="fieldDescription"><p>looks like</p>
                                                    <p>
                                                        <input name="employer" type="text" class="fullBox filterField" id="employer" placeholder="Leave as blank to exclude this criterion" value="<?php echo $_SESSION['formHeadhunt']['employer'] ?>" />
                                                    </p></td>
                                            </tr>
											<tr>
                                                <td align="right" valign="top"><strong>Preferred Work Location</strong></td>
                                                <td valign="top"><input name="location" type="text" class="fullBox filterField" id="location" placeholder="Leave as blank to exclude this criterion" value="<?php echo $_SESSION['formHeadhunt']['location'] ?>" /></td>
                                            </tr>
                                            <tr>
                                            	<td align="right"><strong>Applicant's Primary Discipline</strong></td>
                                            	<td>
                                            		<select name="discipline" class="discipline" id="discipline">
			                                          <option value="">--Any Discipline--</option>
			                                          <?php
do {  
?>
			                                          <option value="<?php echo $row_discipline['id_cat']?>"<?php if (!(strcmp($row_discipline['id_cat'], $_SESSION['formHeadhunt']['discipline']))) {echo "selected=\"selected\"";} ?>><?php echo $row_discipline['cat_name']?></option>
			                                          <?php
} while ($row_discipline = mysql_fetch_assoc($discipline));
  $rows = mysql_num_rows($discipline);
  if($rows > 0) {
      mysql_data_seek($discipline, 0);
	  $row_discipline = mysql_fetch_assoc($discipline);
  }
?>
                                                    </select>
                                            	</td>
                                            </tr>
                                            <tr>
                                            	<td align="right"><strong>Complete Profiles Only?</strong></td>
                                            	<td>
                                            		<input type="checkbox" name="completed" value="1" <?php if($_SESSION['formHeadhunt']['completed']) echo "checked" ?>>
                                            	</td>
                                            </tr>
                                            <tr>
                                            	<td>&nbsp;</td>
                                            	<td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top"><input name="vID" type="hidden" id="vID" value="<?php echo $row_vacancy['id']; ?>" /></td>
                                                <td valign="top"><input name="" type="reset" class="btn red" id="clearForm" value="Reset" />
                                                    <input name="headhunt" type="submit" class="btn green" id="headhunt" value="Search" /></td>
                                            </tr>
                                            </table>

			                                <div class="formBox"></div>
		                                </form>
                                        <br>
			                            <!-- END FORM-->
		                            </div>
</div>
</div>
                            <!-- END SAMPLE FORM PORTLET-->
                        </div>
</div>
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>
                
		    </div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include('-inc-footer.php'); ?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script> 
	<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>   
	<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>   
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script> 
	<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
		<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="plugin/tag/jquery.tag-editor.js"></script>
	<script type="text/javascript" src="plugin/tag/jquery.caret.min.js" ></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>     
	<!-- END PAGE LEVEL SCRIPTS -->  
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
<script type="text/javascript">
$(document).ready(function() {

<?php if($_SESSION['formHeadhunt']['location']) { 
	$location = explode(',', $_SESSION['formHeadhunt']['location']);
	foreach($location as $tag) {
		$initialTags .= "'" . $tag . "',";
	}
	$initialTags = substr($initialTags, 0, -1);
} ?>
$('#location').tagEditor({ initialTags: [<?php echo $initialTags ?>] });

$("#clearForm").click(function() {
	$(".filterField").val('');
});

function doAge() {
        switch($("#ageOp").val()) {
			case 'BETWEEN':
				$("#age").show();
				$("#age2").show();
			break;
			
			case '=': 
			case '>': 
			case '<': 
			case '>=': 
			case '<=':
				$("#age").show();
				$("#age2").hide(); $("#age2").val('');
			break;
			
			default:
				$("#age").hide();
				$("#age2").hide();
		}	
}
//initialize search fields
doAge();
$("#nationality").val('');
if($("#nationality2").val() != '') {
	$("#nationality").val($("#nationality2").val());
}
	$("#stateOfResidence").val('<?php echo $_SESSION['formHeadhunt']['stateOfResidence'] ?>');
	$("#stateOfOrigin").val('<?php echo $_SESSION['formHeadhunt']['stateOfOrigin'] ?>');
	$("#job_level").val('<?php echo $_SESSION['formHeadhunt']['job_level'] ?>');
	$("#industry").val('<?php echo $_SESSION['formHeadhunt']['industry'] ?>');

	
	$("#ageOp").change(function(e) {
		doAge();
    });
});
</script>
	<script>
jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   FormComponents.init();
		});

    </script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
