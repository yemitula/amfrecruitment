<?php require_once "_inc_checkSession.php";?>
<?php $thisPage = basename($_SERVER['PHP_SELF']);?>
<?php require_once '../_inc_config.php';?>
<?php require_once '../Connections/fer.php';?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
// Update vancany to publish...
if ((isset($_GET["vid"])) && ($_GET["query"] == "true") && !empty($_GET["vid"])) {
	$updateSQL = sprintf("UPDATE vacancies SET published=%s  WHERE id=%s",
		GetSQLValueString(1, "int"),
		GetSQLValueString($_GET["vid"], "int"));

	mysql_select_db($database_fer, $fer);
	$Result1 = mysql_query($updateSQL, $fer) or die(mysql_error());

	$updateGoTo = "vacancy-edit.php?id=".$_GET["vid"]."&msg=".urlencode("Vacancy Successfully Published!");
	if (isset($_SERVER['QUERY_STRING'])) {
		$updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
		$updateGoTo .= $_SERVER['QUERY_STRING'];
	}
	header(sprintf("Location: %s", $updateGoTo));
}

//Upadate vancany to unpublish...
if ((isset($_GET["vid"])) && ($_GET["query"] == "unpublish") && !empty($_GET["vid"])) {
	$updateSQL = sprintf("UPDATE vacancies SET published=%s  WHERE id=%s",
		GetSQLValueString("", "int"),
		GetSQLValueString($_GET["vid"], "int"));

	mysql_select_db($database_fer, $fer);
	$Result1 = mysql_query($updateSQL, $fer) or die(mysql_error());

	$updateGoTo = "vacancy-edit.php?id=".$_GET["vid"]."&msg=".urlencode("Vacancy Successfully UnPublished!");
	if (isset($_SERVER['QUERY_STRING'])) {
		$updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
		$updateGoTo .= $_SERVER['QUERY_STRING'];
	}
	header(sprintf("Location: %s", $updateGoTo));
}

?>