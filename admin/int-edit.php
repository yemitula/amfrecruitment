<?php require_once "_inc_checkSession.php";?>
<?php $thisPage = basename($_SERVER['PHP_SELF']);?>
<?php require_once '../_inc_config.php';?>
<?php require_once '../Connections/fer.php';?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

mysql_select_db($database_fer, $fer);
$query_vacancies = "SELECT * FROM vacancies ORDER BY title";
$vacancies = mysql_query($query_vacancies, $fer) or die(mysql_error());
$row_vacancies = mysql_fetch_assoc($vacancies);

$query_interview = sprintf("SELECT * FROM interviews WHERE int_id=%s", GetSQLValueString($_GET['id'],"int"));
$interview = mysql_query($query_interview, $fer) or die(mysql_error());
$row_interview = mysql_fetch_assoc($interview);

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "formCreate") && !empty($_POST["int_id"])) {
	
	//var_dump($_POST); die;
	//die(date("Y-m-d h:i:s", strtotime($_POST['int_date'])));

	$updateSQL = sprintf("UPDATE interviews SET int_vacancy_id=%s, int_date=%s, int_purpose=%s, int_status=%s WHERE int_id=%s",
		GetSQLValueString($_POST['int_vacancy_id'], "int"),
		GetSQLValueString(date("Y-m-d h:i:s", strtotime($_POST['int_date'])), "date"),
		GetSQLValueString($_POST['int_purpose'], "text"),
		GetSQLValueString($_POST['int_status'], "text"),
		GetSQLValueString($_POST['int_id'], "int"));

	mysql_select_db($database_fer, $fer);
	$Result1 = mysql_query($updateSQL, $fer) or die(mysql_error());
	$int_id = $_POST['int_id'];
	$updateGoTo = "int-edit.php?id=$int_id&msg=".urlencode("Interview Details Updated Successfully!");
	if (isset($_SERVER['QUERY_STRING'])) {
		$updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
		$updateGoTo .= $_SERVER['QUERY_STRING'];
	}
	header(sprintf("Location: %s", $updateGoTo));
}

$currentPage = $_SERVER["PHP_SELF"];

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "formIntervieW") && empty($_POST["int_id"])) {
	
	//var_dump($_POST); die;
	//die(date("Y-m-d h:i:s", strtotime($_POST['int_date'])));

	$insertSQL = sprintf("INSERT INTO interviews (int_vacancy_id, int_date, int_purpose, int_create_date, int_creator) VALUES (%s, %s, %s, %s, %s)",
		GetSQLValueString($_POST['int_vacancy_id'], "int"),
		GetSQLValueString(date("Y-m-d h:i:s", strtotime($_POST['int_date'])), "date"),
		GetSQLValueString($_POST['int_purpose'], "text"),
		GetSQLValueString(date('Y-m-d'), "date"),
		GetSQLValueString($FERadmin['firstname'].' '.$FERadmin['surname'], "text"));

	mysql_select_db($database_fer, $fer);
	$Result1 = mysql_query($insertSQL, $fer) or die(mysql_error());
	$int_id = mysql_insert_id();
	$insertGoTo = "int-applicants-list.php?id=$int_id&msg=".urlencode("Interview Created Successfully! Now add applicants.");
	if (isset($_SERVER['QUERY_STRING'])) {
		$insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
		$insertGoTo .= $_SERVER['QUERY_STRING'];
	}
	header(sprintf("Location: %s", $insertGoTo));
}

$currentPage = $_SERVER["PHP_SELF"];
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title><?php echo ($_GET['id'])? 'Edit':'Add'; ?> Interview | <?php echo $config['shortname']?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
	<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
	<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
	<link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include '-inc-top.php';?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<?php include '-inc-navbar-side.php';?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> <?php echo ($_GET['id'])? 'Edit':'Add'; ?> Interview  </h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="vacancies.php">Interviews</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#"> <?php echo ($_GET['id'])? 'Edit':'Add'; ?> Interview</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) {?>
			    <div class="alert  alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg']?></strong> </div>
			    <?php }
?>
			    <?php if (isset($_GET['error'])) {?>
			    <div class="alert alert-error">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['error']?></strong> </div>
			    <?php }
?>
			    <div id="dashboard">
			        <!-- BEGIN DASHBOARD STATS -->
			        <div class="row-fluid">
			            <div class="span12">
			                <!-- BEGIN SAMPLE FORM PORTLET-->
			                <div class="portlet box green tabbable">
			                    <div class="portlet-title">
			                        <div class="caption"> <i class="icon-reorder"></i> <span class="hidden-480">Interview Details</span> </div>
		                        </div>
			                    <div class="portlet-body form">
			                        <div class="tabbable portlet-tabs">
			                            <!-- BEGIN FORM-->
			                            <form method="POST" name="formCreate" action="<?php echo $editFormAction;?>" class="form-horizontal" id="formCreate">
			                                <p>&nbsp;</p>
			                                <span id="dates">

			                                <div class="control-group">
												<label class="control-label">Interview Date & Time</label>
												<div class="controls">
													<div class="input-append date form_datetime">
														<input size="16" type="text" value="<?php echo $row_interview['int_date'] ?>" data-date-format="yyyy-mm-dd hh:ii" data-date-type="php" readonly class="m-wrap" required name="int_date" id="int_date">
														<span class="add-on"><i class="icon-calendar"></i></span>
													</div>
												</div>
											</div>

                                            
                                            </span>

			                                <div class="control-group">
			                                    <label class="control-label">Select Vacancy</label>
			                                    <div class="controls">
			                                        
			                                        <span id="spryselect1">
			                                        <select name="int_vacancy_id" class="m-wrap span6" id = "int_vacancy_id" required>
		                                              <option value=""> -- Select -- </option>
		                                              <?php do { ?>
		                                              	<option value="<?php echo $row_vacancies['id'] ?>" <?php if($row_vacancies['id'] == $row_interview['int_vacancy_id']) echo "selected" ?>><?php echo $row_vacancies['title'] ?></option>
		                                              <?php } while($row_vacancies = mysql_fetch_assoc($vacancies)); ?>
		                                              <option value="0" <?php if($row_interview['int_vacancy_id'] == '0') echo "selected" ?>>No Vacancy (Off-Portal)</option>

		                                            </select>
	                                            </div>
		                                    </div>

		                                    <div class="control-group interview-purpose">
			                                    <label class="control-label">Specify Purpose</label>
			                                    <div class="controls"><span id="sprytextfield1">
			                                        <input name="int_purpose" type="text" class="m-wrap span6" id="int_purpose" placeholder="" value="<?php echo $row_interview['int_purpose'] ?>" />
		                                        </div>
    </div>

			                                
			                                <?php if(!empty($_GET['id'])) { ?>
			                                <div class="control-group">
			                                    <label class="control-label">Status</label>
			                                    <div class="controls">
			                                        <span id="spryselect1">
			                                        <select name="int_status" class="m-wrap span2" id = "int_status" required>
		                                              <option value="pending" <?php if($row_interview['int_status'] == 'pending') echo "selected" ?>>pending</option>
		                                              <option value="ongoing" <?php if($row_interview['int_status'] == 'ongoing') echo "selected" ?>>ongoing</option>
		                                              <option value="completed" <?php if($row_interview['int_status'] == 'completed') echo "selected" ?>>completed</option>
		                                            </select>
	                                            </div>
		                                    </div>
		                                    <?php } ?>

		                                    <div class="form-actions">
			                                    <button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
			                                    <a href="int-list.php">
			                                        <button type="button" class="btn">&laquo; Back to Interview List</button>
		                                        </a>

		                                        <?php if(isset($_GET['id']) && !empty($_GET['id'])) { ?>
		                                        <a href="int-applicants-list.php?id=<?php echo $_GET['id'] ?>">
			                                        <button type="button" class="btn green">Applicants Listed for this Interview &raquo</button>
		                                        </a>
		                                        <?php } ?>


			                                    <input name="int_id" type="hidden" id="int_id" value="<?php echo $row_interview['int_id'];?>">
			                                </div>
			                                <input type="hidden" name="MM_update" value="formCreate">
		                                    <input type="hidden" name="MM_insert" value="formIntervieW">
			                            </form>
			                            <!-- END FORM-->
		                            </div>
</div>
</div>
                            <!-- END SAMPLE FORM PORTLET-->
                        </div>
</div>
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>

		    </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include '-inc-footer.php';?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>
	<![endif]-->
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
<script>
		$(document).ready(function(e) {
		   //$("#employer_id").val('<?php echo $row_vacancy['employer_id']?>');
		   if($("#int_vacancy_id").val() == '0') {
	   			$(".interview-purpose").show();
	   		} else {
				$(".interview-purpose").hide();
	   		}

		   $("#int_vacancy_id").change(function(){
		   		if($(this).val() == '0') {
		   			$(".interview-purpose").show();
		   		} else {
					$(".interview-purpose").hide();
		   			$("#int_purpose").val('');
		   		}
		   });

		   $("#formCreate").submit(function() {
		   		//alert('save now!');
		   		if($("#int_vacancy_id").val() == 0 && $("#int_purpose").val() == '') {
		   			alert("Since this interview is not attached to an existing vacancy, please specify the purpose of this interview!");
		   			return false;
		   		}
		   })
        });
	</script>
	<script>
jQuery(document).ready(function() {
		   App.init(); // initlayout and core plugins
		   FormComponents.init();

		   $(".form_datetime").datetimepicker({
	            isRTL: App.isRTL(),
	            format: "dd-mm-yyyy hh:ii",
	            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
        	});

		});
    </script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>