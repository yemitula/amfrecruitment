<?php require_once("_inc_checkSession.php"); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php require_once('../_inc_Functions.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$applicant_id = $_GET['id'];

mysql_select_db($database_fer, $fer);
$query_personal = sprintf("SELECT * FROM applicants WHERE id = %s", GetSQLValueString($applicant_id, "int"));
$personal = mysql_query($query_personal, $fer) or die(mysql_error());
$row_personal = mysql_fetch_assoc($personal);
//var_dump($row_personal); die;
$totalRows_personal = mysql_num_rows($personal);

$colname_secondary = "-1";
if (isset($_GET['id'])) {
  $colname_secondary = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_secondary = "SELECT * FROM secschool WHERE applicant_id = '$colname_secondary' ORDER BY `date` DESC";
$secondary = mysql_query($query_secondary, $fer) or die(mysql_error());
$row_secondary = mysql_fetch_assoc($secondary);
$totalRows_secondary = mysql_num_rows($secondary);

$colname_tertiary = "-1";
if (isset($_GET['id'])) {
  $colname_tertiary = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_tertiary = "SELECT * FROM tertiary WHERE applicant_id = '$colname_tertiary' ORDER BY `date` DESC";
$tertiary = mysql_query($query_tertiary, $fer) or die(mysql_error());
$row_tertiary = mysql_fetch_assoc($tertiary);
$totalRows_tertiary = mysql_num_rows($tertiary);

$colname_nysc = "-1";
if (isset($_GET['id'])) {
  $colname_nysc = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_nysc = sprintf("SELECT * FROM nysc WHERE applicant_id = %s", GetSQLValueString($colname_nysc, "int"));
$nysc = mysql_query($query_nysc, $fer) or die(mysql_error());
$row_nysc = mysql_fetch_assoc($nysc);
$totalRows_nysc = mysql_num_rows($nysc);

mysql_select_db($database_fer, $fer);
$query_currentJob = sprintf("SELECT * FROM workexp LEFT JOIN job_categories ON industry = id_cat WHERE applicant_id = %s AND endDate IS NULL", GetSQLValueString($colname_nysc, "int"));
$currentJob = mysql_query($query_currentJob, $fer) or die(mysql_error());
$row_currentJob = mysql_fetch_assoc($currentJob);
$totalRows_currentJob = mysql_num_rows($currentJob);

mysql_select_db($database_fer, $fer);
$query_categories = "SELECT * FROM job_categories ORDER BY cat_name ASC";
$categories = mysql_query($query_categories, $fer) or die(mysql_error());
$row_categories = mysql_fetch_assoc($categories);
$totalRows_categories = mysql_num_rows($categories);

$colname_areas = "-1";
if (isset($_GET['id'])) {
  $colname_areas = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_current_areas = "SELECT * FROM applicants_categories WHERE app_id = '$colname_areas'";
$current_areas = mysql_query($query_current_areas, $fer) or die(mysql_error());
$row_current_areas = mysql_fetch_assoc($current_areas);
$totalRows_current_areas = mysql_num_rows($current_areas);
do {
	$myAreas[] = $row_current_areas['cat_id'];
} while ($row_current_areas = mysql_fetch_assoc($current_areas));

$colname_skill = "-1";
if (isset($_GET['id'])) {
  $colname_skill = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_skill = sprintf("SELECT * FROM skills WHERE applicant_id = %s ORDER BY skill ASC", GetSQLValueString($colname_skill, "int"));
$skill = mysql_query($query_skill, $fer) or die(mysql_error());
$row_skill = mysql_fetch_assoc($skill);
$totalRows_skill = mysql_num_rows($skill);

$colname_workExp = "-1";
if (isset($_GET['id'])) {
  $colname_workExp = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_workExp ="SELECT * FROM workexp LEFT JOIN job_categories ON industry = id_cat WHERE applicant_id = '$colname_workExp' AND endDate IS NOT NULL ORDER BY endDate DESC, startDate DESC";
$workExp = mysql_query($query_workExp, $fer) or die(mysql_error());
$row_workExp = mysql_fetch_assoc($workExp);
$totalRows_workExp = mysql_num_rows($workExp);

$colname_certs = "-1";
if (isset($_GET['id'])) {
  $colname_certs = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_certs = sprintf("SELECT id, certification, `year` FROM profcerts WHERE applicant_id = %s ORDER BY `year` DESC", GetSQLValueString($colname_certs, "int"));
$certs = mysql_query($query_certs, $fer) or die(mysql_error());
$row_certs = mysql_fetch_assoc($certs);
$totalRows_certs = mysql_num_rows($certs); 
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title><?php echo $row_personal['surname']; ?> <?php echo $row_personal['firstname']; ?> <?php echo $row_personal['middlename']; ?> | ABC Bid Management Sytem</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
	
	
	



</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-closed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('-inc-top.php'); ?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR --><!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
                <?php if (isset($_GET['browse'])) { ?>
				<?php if  ($_GET['id'] != $_SESSION['shortlisted'][$_SESSION['current_applicant']] ) {?>
                	<?php if (isset($_GET['forward'])) $_SESSION['current_applicant']++; ?>
                    <?php if (isset($_GET['rewind'])) $_SESSION['current_applicant']--; ?>
                 <?php } ?>
                <div class="row-fluid">
                <br>
                <?php $n = $_SESSION['current_applicant'] ?>
                <?php if ($n > 0) { ?>
                <a href="applicant-details.php?id=<?php echo $_SESSION['shortlisted'][$n-1] ?>&browse&rewind"><button type="button" id="" class="btn btn-black pull-left"><i class="m-icon-swapleft"></i> Previous Applicant in Shortlist</button></a>
                <?php } ?>
                                
                <?php if ($n < count($_SESSION['shortlisted'])-1) { ?>
<a href="applicant-details.php?id=<?php echo $_SESSION['shortlisted'][$n+1] ?>&browse&forward"><button type="button" id="" class="btn btn-black pull-right">Next Applicant in Shortlist <i class="m-icon-swapright"></i> </button></a>
<?php } ?>
                </div>
                <?php } ?>
                
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> <?php echo $row_personal['surname']; ?> <?php echo $row_personal['firstname']; ?> <?php echo $row_personal['middlename']; ?> <small>applicant details</small></h3>
<button type="button" id="closeButton" class="btn btn-black pull-right"><i class=" icon-power-off"></i> Close</button>
<button type="button" id="printButton" class="btn btn-black pull-right"><i class="icon-print"></i> Print</button>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="applicants.php">Applicants</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#">Applicant Details</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['err'])) { ?>
			    <div class="alert alert-error">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['err'] ?></strong> </div>
			    <?php } ?>
			    <div id="dashboard">
			        <!-- BEGIN DASHBOARD STATS -->
			        <div class="row-fluid">
			            
                        <div class="span12">
			                <!-- BEGIN SAMPLE FORM PORTLET-->
			                <div class="portlet-body">
		                        <h3>Personal Details <small><?php if (SectionComplete('personal', $row_personal['id'])) { ?><span class="label label-success">complete</span> <?php } else { ?><span class="label label-important">incomplete</span><?php } ?></small></h3>
		                        <table class="table table-striped table-bordered table-hover" id="sample_1">
	                                <tr class="">
	                                    <td width="40%">Applicant ID</td>
	                                    <td width="60%"><?php echo str_pad($row_personal['id'],6,'0',STR_PAD_LEFT); ?></td>
                                    </tr>
	                                 <?php if ($row_personal['title']) { ?>
                                    <tr class="">
	                                    <td>Title</td>
	                                    <td><?php echo $row_personal['title']; ?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr class="">
	                                    <td>Name</td>
	                                    <td><?php echo strtoupper($row_personal['surname']) ?> <?php echo $row_personal['firstname'] ?> <?php echo $row_personal['middlename'] ?></td>
                                    </tr>
                                <?php if ($row_personal['dob']) { ?>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td><?php echo $row_personal['dob']; ?></td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td><?php echo $row_personal['gender']; ?></td>
                                </tr>
                                <?php } ?>
                                <?php if ($row_personal['email']) { ?>
                                <tr>
                                    <td>E-mail(s)</td>
                                    <td><?php echo $row_personal['email']; ?>
                                        <?php if ($row_personal['email2']) echo ', '.$row_personal['email2']; ?></td>
                                </tr>
                                <?php } ?>
                                <?php if ($row_personal['gsm']) { ?>
                                <tr>
                                    <td>Mobile Number(s)</td>
                                    <td><?php echo $row_personal['gsm']; ?>
                                        <?php if ($row_personal['gsm2']) echo ', '.$row_personal['gsm2']; ?></td>
                                </tr>
                                <?php } ?>
                                <?php if ($row_personal['street']) { ?>
                                <tr>
                                    <td>Address</td>
                                    <td><?php if ($row_personal['houseNumber']) echo $row_personal['houseNumber'].','; ?>
                                        <?php echo $row_personal['street']; ?>
                                        <?php if ($row_personal['busStop']) echo ', ('.$row_personal['busStop'].')'; ?></td>
                                </tr>
                                <tr>
                                    <td>City/Town</td>
                                    <td><?php echo $row_personal['city']; ?></td>
                                </tr>
                                <?php if ($row_personal['countryOfResidence'] == 'Nigeria') { ?>
                                <tr>
                                    <td>State of Residence</td>
                                    <td><?php echo $row_personal['stateOfResidence']; ?></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td>Country of Residence</td>
                                    <td><?php echo $row_personal['countryOfResidence']; ?></td>
                                </tr>
                                <tr>
                                    <td>Languages Spoken</td>
                                    <td><?php echo $row_personal['languages']; ?></td>
                                </tr>
                                <?php } ?>	                           
                                <tr>
                                    <td>Primary Discipline</td>
                                    <td>
                                        <?php 
                                            if(isset($row_personal["discipline"])) {

                                                $des = $row_personal["discipline"];

                                                mysql_select_db($database_fer, $fer);
                                                $query_discipline_name = "SELECT * FROM job_categories WHERE id_cat = $des ";
                                                $discipline_name = mysql_query($query_discipline_name, $fer) or die(mysql_error());
                                                $row_discipline_name = mysql_fetch_assoc($discipline_name);
                                                $totalRows_current_areas = mysql_num_rows($discipline_name);

                                                echo $row_discipline_name["cat_name"];
                                            }else {
                                                echo "N/A";
                                            } 
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Preferred Job Location</td>
                                    <td>
                                        <?php echo $row_personal['prefTestLoc'] ?>
                                    </td>
                                </tr>                                
                             </table>
                                
                                <div class="clearfix"></div>
		                        <h3>Educational/Academic Qualifications <small><?php if (SectionComplete('educational', $row_personal['id'])) { ?><span class="label label-success">complete</span> <?php } else { ?><span class="label label-important">incomplete</span><?php } ?></small></h3>
						<?php if ($totalRows_secondary > 0 || $totalRows_tertiary > 0) { ?>
                              <?php if ($totalRows_tertiary > 0) { // Show if recordset not empty ?>
                              <h6>Tertiary</h6>
		                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th width="40%">Institution (Course)</th>
                                        <th width="30%">Qualification (Class)</th>
                                        <th width="15%">Course Category and Institution Country</th>
                                        <th width="15%">Date</th>
                                    </tr>
                                </thead>
                        
                                <tbody>
                                    <?php do { ?>
                                    <tr>
                                        <td><?php echo $row_tertiary['institution']; ?> (<?php echo $row_tertiary['course']; ?>)</td>
                                        <td><?php echo $row_tertiary['qualification']; ?> (<?php echo $row_tertiary['class']; ?>)</td>
                                        <td><?php echo $row_tertiary['Course_Category']; ?>/<?php echo $row_tertiary['uni_country']; ?></td>
                                        <td><?php echo date("F, Y",strtotime($row_tertiary['date'])); ?></td>
                                    </tr>
                                    <?php } while ($row_tertiary = mysql_fetch_assoc($tertiary)); ?>
                                </tbody>
	                            </table>
				<?php } // Show if recordset not empty ?>
                <?php if ($totalRows_secondary > 0) { // Show if recordset not empty ?>
                              <h6>Secondary</h6>
		                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
					<tr>
						<th width="50%">School</th>
						<th width="30%">Certificate</th>
						<th width="20%">Date</th>
					</tr>
				</thead>

				<tbody>
					<?php do { ?>
					<tr>
						<td><?php echo $row_secondary['school']; ?></td>
						<td><?php echo $row_secondary['certificate']; ?></td>
						<td><?php echo date("F, Y",strtotime($row_secondary['date'])); ?></td>
					</tr>
					<?php } while ($row_secondary = mysql_fetch_assoc($secondary)); ?>
				</tbody>
	                            </table>
				<?php } // Show if recordset not empty ?>
                            <?php } // end tertiary and secondary section ?>  
                            
                <div class="clearfix"></div>
		                        <h3>Professional Certifications <small><?php if (SectionComplete('profcerts', $row_personal['id'])) { ?><span class="label label-success">complete</span> <?php } else { ?><span class="label label-important">incomplete</span><?php } ?></small></h3>
                              <?php if ($totalRows_certs > 0) { // Show if recordset not empty ?>
		                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
					<tr>
						<th width="80%">Certification</th>
						<th width="20%">Year Obtained</th>
					</tr>
				</thead>

				<tbody>
					<?php do { ?>
					<tr>
						<td><?php echo $row_certs['certification']; ?></td>
						<td><?php if($row_certs['year'] != 0) echo $row_certs['year']; ?></td>
					</tr>
					<?php } while ($row_certs = mysql_fetch_assoc($certs)); ?>
				</tbody>
	                            </table>
				<?php } // Show if recordset not empty ?>
                
                
                
                <div class="clearfix"></div>
		                        <h3>NYSC Details <small><?php if (SectionComplete('nysc', $row_personal['id'])) { ?><span class="label label-success">complete</span> <?php } else { ?><span class="label label-important">incomplete</span><?php } ?></small></h3>
                              <?php if ($totalRows_nysc > 0) { // Show if recordset not empty ?>
		                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
					<tr>
						<th width="40%">NYSC Status</th>
						<th width="40%">NYSC Number</th>
						<th width="20%">NYSC Year</th>
					</tr>
				</thead>

				<tbody>
					<?php do { ?>
					<tr>
						<td><?php echo $row_nysc['nyscStatus']; ?></td>
						<td><?php echo $row_nysc['nyscNumber']; ?></td>
						<td><?php echo $row_nysc['nyscYear']; ?></td>
					</tr>
					<?php } while ($row_nysc = mysql_fetch_assoc($nysc)); ?>
				</tbody>
	                            </table>
				<?php } // Show if recordset not empty ?>
                
                
                
                <div class="clearfix"></div>
		                        <h3>Work Experience <small><?php if (SectionComplete('workExp', $row_personal['id'])) { ?><span class="label label-success">complete</span> <?php } else { ?><span class="label label-important">incomplete</span><?php } ?></small></h3>
                              <?php if ($totalRows_workExp > 0 || $totalRows_currentJob > 0) { // Show if recordset not empty ?>
		                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
					<tr>
						<th width="30%">Company</th>
						<th width="40%">Job Description</th>
						<th width="40%">salary / Location</th>
						<th width="30%">Period</th>
					</tr>
				</thead>

				<tbody>
					<?php if ($totalRows_currentJob > 0) { // Show if recordset not empty ?>
					<tr>
      <td valign="top" style="word-wrap: break-word;"><p><strong><?php echo $row_currentJob['company']; ?></strong></p>
        <?php echo $row_currentJob['cat_name']; ?><br>
        <em><?php echo $row_currentJob['position']; ?></em>
</td>
      <td valign="top" align="justify" style="word-wrap: break-word;"><?php echo $row_currentJob['description']; ?></td>
      <td valign="top" align="justify" style="word-wrap: break-word;"><?php echo $row_currentJob['job_salary']; ?><br /><?php echo $row_currentJob['State']; ?>/<?php echo $row_currentJob['country']; ?></td>
      <td valign="top"><?php echo date("M Y",strtotime($row_currentJob['startDate'])); ?> - Date</td>
					</tr>
				<?php } // Show if recordset not empty ?>
				 <?php if ($totalRows_workExp > 0) do { ?>
					<tr>
      <td valign="top" style="word-wrap: break-word;"><p><strong><?php echo $row_workExp['company']; ?></strong></p>
        <?php echo $row_workExp['cat_name']; ?><br>
        <em><?php echo $row_workExp['position']; ?></em>
</td>
      <td valign="top" align="justify" style="word-wrap: break-word;"><?php echo $row_workExp['description']; ?></td>
      <td valign="top" align="justify" style="word-wrap: break-word;"><?php echo $row_workExp['job_salary']; ?><br /><?php echo $row_workExp['State']; ?>/<?php echo $row_workExp['country']; ?></td>
      <td valign="top"><?php echo date("M Y",strtotime($row_workExp['startDate'])); ?> - <?php echo date("M Y",strtotime($row_workExp['endDate'])); ?></td>
					</tr>
				<?php } while ($row_workExp = mysql_fetch_assoc($workExp)); ?>
				</tbody>
	                            </table>
				<?php } // Show if recordset not empty ?>
                
                
                
                <div class="clearfix"></div>
		                        <h3>Areas of Interest <small><?php if (SectionComplete('areas', $row_personal['id'])) { ?><span class="label label-success">complete</span> <?php } else { ?><span class="label label-important">incomplete</span><?php } ?></small></h3>
                              <?php if (SectionComplete('areas', $row_personal['id'])) { // Show if recordset not empty ?>
		                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                               

				<tbody>
					<tr>
                <?php $count = 0; ?>
                <?php do { ?>
                <td width="50%" valign="top"><label>
                    <input name="areas[]" type="checkbox" id="area_<?php echo $row_categories['id_cat']; ?>" value="<?php echo $row_categories['id_cat']; ?>" <?php if (in_array($row_categories['id_cat'],$myAreas)) echo "checked='checked'" ?> disabled="disabled" readonly/>
                    <?php echo $row_categories['cat_name']; ?></label></td>
                <?php $count++; 
					   		if($count % 2 == 0) {
                            	echo "</tr> <tr>";
                        	} ?>
                <?php } while ($row_categories = mysql_fetch_assoc($categories)); ?>
                <?php if ($count % 2 == 0) echo "</tr>"; else echo "<td>&nbsp;</td>";?>
            </tr>
				
            <tr>
                <td width="23%">Preferred Job Level</td>
                <td width="77%"><strong><?php echo !empty($row_personal['prefJobLevel'])?$row_personal['prefJobLevel']:'Any' ?></strong></td>
            </tr>
            <tr>
                <td>Preferred Job Type</td>
                <td><strong><?php echo isset($row_personal['prefJobType']) && !empty($row_personal['prefJobType'])? $row_personal['prefJobType'] : 'Any' ?></strong></td>
            </tr>
 				</tbody>
       </table>
				<?php } // Show if recordset not empty ?>
                
                
                
                <div class="clearfix"></div>
		                        <h3>Skills <small><?php if (SectionComplete('skills', $row_personal['id'])) { ?><span class="label label-success">complete</span> <?php } else { ?><span class="label label-important">incomplete</span><?php } ?></small></h3>
                              <?php if ($totalRows_skill > 0) { // Show if recordset not empty ?>
		                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
					<tr>
						<th width="30%"><strong>Skill</strong></td>
                                <th width="70%"><strong>Competency</strong></td>
					</tr>
				</thead>

				<tbody>
					<?php do { ?>
                            <tr>
                                <td><?php echo $row_skill['skill']; ?></td>
                                <td><?php echo $row_skill['competency']; ?></td>
                            </tr>
                            <?php } while ($row_skill = mysql_fetch_assoc($skill)); ?>
				</tbody>
	                            </table>
				<?php } // Show if recordset not empty ?>
                
                <div class="clearfix"></div>
		                        <h3>Uploaded CV <small><?php if (SectionComplete('uploadCV', $row_personal['id'])) { ?><span class="label label-success">complete</span> <?php } else { ?><span class="label label-important">incomplete</span><?php } ?></small></h3>
                              <?php if (isset($row_personal['cv'])) { ?>
          <h5><strong><a href="../uploadedCVs/<?php echo $row_personal['cv']; ?>" target="_blank"><?php echo $row_personal['cv']; ?></a></strong></h5>
      <?php } ?>
                
               
                                <div class="clearfix"></div>
                                <p>&nbsp;</p>
			                </div>
			                <!-- END SAMPLE FORM PORTLET-->
                        </div>
</div>
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>
			</div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER --><!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>   
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/index.js" type="text/javascript"></script>
	<script src="assets/scripts/tasks.js" type="text/javascript"></script>        
	<script src="assets/scripts/ui-jqueryui.js"></script>     
	<!-- END PAGE LEVEL SCRIPTS -->  
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
	<script type="text/javascript">
	$(document).ready(function(e) {
        //window.print();
		$("#printButton").click(function(e) {
            window.print();
        });
		$("#closeButton").click(function(e) {
            window.close();
        });
    });
	</script>
	<script>
jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		});
		</script>
    
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<?php
mysql_free_result($estimate);

mysql_free_result($labours);

mysql_free_result($pms);
?>
