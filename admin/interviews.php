<?php require_once "_inc_checkSession.php";?>
<?php include '../_inc_Functions.php';?>
<?php require_once '../_inc_config.php';?>
<?php require_once '../Connections/fer.php';?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}

$maxRows_pages = 20;
$pageNum_pages = 0;
if (isset($_GET['pageNum_pages'])) {
	$pageNum_pages = $_GET['pageNum_pages'];
}
$startRow_pages = $pageNum_pages * $maxRows_pages;

mysql_select_db($database_fer, $fer);
$query_pages = "SELECT v.title, a.firstname, a.id, i.date, a.surname, i.remarks, i.recommended, i.int_id FROM interview_records i LEFT JOIN vacancies v ON vacancy_id = v.id LEFT JOIN applicants a ON a.id = i.applicant_id WHERE 1=1 ";
if(!empty($_GET['Name'])) {
	$query_pages .= sprintf(" AND a.firstname OR a.surname = %s", GetSQLValueString($_GET['Name'],"text"));
}
//Vacancy check...
if(!empty($_GET['Vacancy'])) {
	$query_pages .= sprintf(" AND v.title = %s", GetSQLValueString($_GET['Vacancy'],"text"));
}
$query_limit_pages = sprintf("%s LIMIT %d, %d", $query_pages, $startRow_pages, $maxRows_pages);
$pages = mysql_query($query_limit_pages, $fer) or die(mysql_error());
$row_pages = mysql_fetch_assoc($pages);

if (isset($_GET['totalRows_pages'])) {
	$totalRows_pages = $_GET['totalRows_pages'];
} else {
	$all_pages = mysql_query($query_pages);
	$totalRows_pages = mysql_num_rows($all_pages);
}
$totalPages_pages = ceil($totalRows_pages / $maxRows_pages) - 1;
$maxRows_pages = 20;
$pageNum_pages = 0;
if (isset($_GET['pageNum_pages'])) {
	$pageNum_pages = $_GET['pageNum_pages'];
}
$startRow_pages = $pageNum_pages * $maxRows_pages;

if (isset($_GET['totalRows_pages'])) {
	$totalRows_pages = $_GET['totalRows_pages'];
} else {
	$all_pages = mysql_query($query_pages);
	$totalRows_pages = mysql_num_rows($all_pages);
}
$totalPages_pages = ceil($totalRows_pages / $maxRows_pages) - 1;

if (isset($_GET['totalRows_pages'])) {
	$totalRows_pages = $_GET['totalRows_pages'];
} else {
	$all_pages = mysql_query($query_pages);
	$totalRows_pages = mysql_num_rows($all_pages);
}
$totalPages_pages = ceil($totalRows_pages / $maxRows_pages) - 1;

$queryString_pages = "";
if (!empty($_SERVER['QUERY_STRING'])) {
	$params = explode("&", $_SERVER['QUERY_STRING']);
	$newParams = array();
	foreach ($params as $param) {
		if (stristr($param, "pageNum_pages") == false &&
			stristr($param, "totalRows_pages") == false) {
			array_push($newParams, $param);
		}
	}
	if (count($newParams) != 0) {
		$queryString_pages = "&" . htmlentities(implode("&", $newParams));
	}
}
$queryString_pages = sprintf("&totalRows_pages=%d%s", $totalRows_pages, $queryString_pages);

mysql_select_db($database_fer, $fer);
$query_vacancy = "SELECT * FROM vacancies";
$vacancy = mysql_query($query_vacancy, $fer) or die(mysql_error());
$row_vacancy = mysql_fetch_assoc($vacancy);
$totalRows_vacancy = mysql_num_rows($vacancy);

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Interview Records | <?php echo $config['shortname']?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<link href="assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include '-inc-top.php';?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<?php include '-inc-navbar-side.php';?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					Widget settings form goes here
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> interview Applicants <small>browse interviewed applicants </small></h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="vacancies.php">Vacancies</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#"> interview Applicants</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) {?>
			    <div class="alert alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg']?></strong> </div>
			    <?php }
?>
			    <?php if ($totalRows_pages == 0) { // Show if recordset empty ?>
		        <div class="row-fluid">
			        <div class="alert">
			          
			          <strong>Empty List!</strong> No applicants found. </div>
		          </div>
		        <?php } // Show if recordset empty ?>
		      <p>&nbsp;</p>
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN EXAMPLE TABLE PORTLET-->
			            <div class="portlet box light-grey">
			                <div class="portlet-title">
			                    <div class="caption">Shortlisted Applicants Interviewed</div>
		                    </div>
			                <div class="portlet-body">
			                    <div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid"> <br>
			                        <form action="" method="get" name="formFilter" id="formFilter" >
			                        	&nbsp;
										<input type="text" name="Name" id="Name" class="m-wrap span6" placeholder="Applicant Name" >		                        	
			                        	&nbsp;
									  <select class="span4" name="Vacancy" id="Vacancy">
									    <option value="">All Vacancy</option>
									    <?php
do {  
?>
									    <option value="<?php echo $row_vacancy['title']?>"><?php echo $row_vacancy['title']?></option>
									    <?php
} while ($row_vacancy = mysql_fetch_assoc($vacancy));
  $rows = mysql_num_rows($vacancy);
  if($rows > 0) {
      mysql_data_seek($vacancy, 0);
	  $row_vacancy = mysql_fetch_assoc($vacancy);
  }
?>
                                      </select>
			                        	&nbsp;
			                        	<button type="submit" class="btn green">Filter <i class="m-icon-swapright m-icon-white"></i></button>
			                        	&nbsp;
		                          </form>
			                        <div class="row-fluid">
			                            <div class="span6"></div>
			                            <div class="span6">
			                                <!--<div class="dataTables_filter" id="sample_1_filter">
                  <label>Search: <input type="text" aria-controls="sample_1" class="m-wrap medium"></label>
                  </div>-->
		                                </div>
		                            </div>
            <?php if ($totalRows_pages > 0) {
	?>
            <table class="table table-striped table-bordered table-hover" id="sample_1">
              <thead>
                <tr>
                  <th width="" >Name</th>
                  <th width="" >Vacancy</th>
                  <th width="" >Date</th>
                  <th>Remarks</th>
                  <th width="">Status</th>
                  <th width="" >&nbsp;</th>
                  </tr>
              </thead>
              <tbody>
                <?php do {?>
                  <tr class="odd gradeX">
                    <td ><a href="applicant-details.php?id=<?php echo $row_pages["id"]; ?>" target="_new"><?php echo $row_pages['firstname'];?> <?php echo $row_pages["surname"]?> </a></td>
                    <td ><?php echo $row_pages['title'];?></td>
                    <td ><?php echo $row_pages['date']?></td>
                    <td><?php echo $row_pages["remarks"];?></td>
                    <td><?php echo $row_pages["recommended"] ?></td>
                    <td >
                    <?php 
                    	/*<a href="applicant-interview.php?query=<?php echo $row_pages["int_id"];?>&n=<?php echo $row_pages['firstname'];?> <?php echo $row_pages["surname"]?>&v"><i class="icon-edit"></i></a>&nbsp;&nbsp; */
                    ?>
                    <a href="#"><i class="icon-eye-open" ></i></a>&nbsp;
                    <a href="applicant-interview.php?del_query=<?php echo $row_pages["int_id"];?>" title="Delete Interview" class="ValidateAction tooltips" data-placement="top" data-confirm-msg="Are you sure you want to Delete? interview Details..."><i class="icon-remove"></i></a>
                    &nbsp;
                    </td>
                  </tr>
                  <?php } while ($row_pages = mysql_fetch_assoc($pages));?>
              </tbody>
            </table>
            <div class="row-fluid">
              <div class="span6">
                <div class="dataTables_info" id="sample_1_info">Showing <?php echo ($startRow_pages + 1)?> to <?php echo min($startRow_pages + $maxRows_pages, $totalRows_pages)?> of <?php echo $totalRows_pages?> entries</div>
                </div>
              <div class="span6">
                <div class="dataTables_paginate paging_bootstrap pagination">
                  <ul>
                    <?php if ($pageNum_pages > 0) { // Show if not first page ?>
                    <li class="prev"><a href="<?php printf("%s?pageNum_pages=%d%s", $thispage, 0, $queryString_pages);?>" title="First Page">&laquo; </a></li><?php }
	?>
                    <?php if ($pageNum_pages > 0) { // Show if not first page ?>
                    <li class="prev"><a href="<?php printf("%s?pageNum_pages=%d%s", $thispage, max(0, $pageNum_pages - 1), $queryString_pages);?>" title="Previous Page">&lsaquo;</a></li>
                    <?php }
	?>

                    <?php $thisPageNumber = $pageNum_pages + 1;
	//determine startPage and endPage
	$totalPages = $totalPages_pages + 1;
	if ($totalPages < 4) {
		$startPage = 1;
		$endPage = $totalPages;
	} elseif ($thisPageNumber <= 4) {
		$startPage = 1;
		$endPage = 4;
	} else {
		$startPage = $thisPageNumber - 3;
		$endPage = $thisPageNumber;
	}
	?>
                    <?php for ($i = $startPage; $i <= $endPage; $i++) {
		?>
                    <li class="<?php if ($i == $thisPageNumber) {
			echo "active";
		}
		?>"><a href="<?php echo $thispage?>?pageNum_pages=<?php echo $i - 1?>&amp;totalRows_pages=<?php echo $totalRows_pages?>" title="Page <?php echo $i?>"><?php echo $i?></a></li>
                    <?php }
	?>
                    <?php if ($pageNum_pages < $totalPages_pages) { // Show if not last page ?>
                    <li class="next"><a href="<?php printf("%s?pageNum_pages=%d%s", $thispage, min($totalPages_pages, $pageNum_pages + 1), $queryString_pages);?>" title="Next Page">&rsaquo; </a></li>
                    <?php }
	?>
                    <?php if ($pageNum_pages < $totalPages_pages) { // Show if not last page ?>
                    <li class="next"><a href="<?php printf("%s?pageNum_pages=%d%s", $thispage, $totalPages_pages, $queryString_pages);?>" title="Last Page">&raquo; </a></li>
                    <?php }
	?>
                    </ul>
                  </div>
                </div>
            </div>

                    <?php }
?>
		                        </div>
		                    </div>
		                </div>
			            <!-- END EXAMPLE TABLE PORTLET-->
		            </div>
		        </div>

			    <div class="clearfix"></div>
		    </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include '-inc-footer.php';?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>
	<![endif]-->
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
	<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
	<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
	<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
    <script>
		$(document).ready(function(e) {
		   $("#company").val('<?php echo $_GET['company']?>');
		   $("#status").val('<?php echo $_GET['status']?>');
		   $("#fromDate").val('<?php echo $_GET['fromDate']?>');
		   $("#toDate").val('<?php echo $_GET['toDate']?>');
        });
	</script>
	<script>
		jQuery(document).ready(function() {
		   App.init(); // initlayout and core plugins
		   FormComponents.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<?php
mysql_free_result($vacancy);
?>
