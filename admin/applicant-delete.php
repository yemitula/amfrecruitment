<?php require_once("_inc_checkSession.php"); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_applicant = "-1";
if (isset($_GET['id'])) {
  $colname_applicant = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_applicant = sprintf("SELECT * FROM applicants WHERE id = %s", GetSQLValueString($colname_applicant, "int"));
$applicant = mysql_query($query_applicant, $fer) or die(mysql_error());
$row_applicant = mysql_fetch_assoc($applicant);
$totalRows_applicant = mysql_num_rows($applicant);

//delete dependencies
if ((isset($_GET['id'])) && ($_GET['id'] != "")) {
//applications
  $deleteSQL = sprintf("DELETE FROM applications WHERE applicant_id=%s",
                       GetSQLValueString($_GET['id'], "int"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($deleteSQL, $fer) or die(mysql_error());

//areas of interest
  $deleteSQL = sprintf("DELETE FROM areasofinterest WHERE applicant_id=%s",
                       GetSQLValueString($_GET['id'], "int"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($deleteSQL, $fer) or die(mysql_error());


//nysc
  $deleteSQL = sprintf("DELETE FROM nysc WHERE applicant_id=%s",
                       GetSQLValueString($_GET['id'], "int"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($deleteSQL, $fer) or die(mysql_error());

//profcerts
  $deleteSQL = sprintf("DELETE FROM profcerts WHERE applicant_id=%s",
                       GetSQLValueString($_GET['id'], "int"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($deleteSQL, $fer) or die(mysql_error());

//secschool
  $deleteSQL = sprintf("DELETE FROM secschool WHERE applicant_id=%s",
                       GetSQLValueString($_GET['id'], "int"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($deleteSQL, $fer) or die(mysql_error());

//sectionstatus
  $deleteSQL = sprintf("DELETE FROM sectionstatus WHERE applicant_id=%s",
                       GetSQLValueString($_GET['id'], "int"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($deleteSQL, $fer) or die(mysql_error());

//skills
  $deleteSQL = sprintf("DELETE FROM skills WHERE applicant_id=%s",
                       GetSQLValueString($_GET['id'], "int"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($deleteSQL, $fer) or die(mysql_error());

//tertiary
  $deleteSQL = sprintf("DELETE FROM tertiary WHERE applicant_id=%s",
                       GetSQLValueString($_GET['id'], "int"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($deleteSQL, $fer) or die(mysql_error());

//workexp
  $deleteSQL = sprintf("DELETE FROM workexp WHERE applicant_id=%s",
                       GetSQLValueString($_GET['id'], "int"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($deleteSQL, $fer) or die(mysql_error());
}

if ((isset($_GET['id'])) && ($_GET['id'] != "")) {

	//delete if file exists
	if($row_applicant['profilePix'] != '' && file_exists('../images/profilePix/'.$row_applicant['profilePix'])) {
		//echo "<br>Profile Pix exists, delete it";
		unlink('../images/profilePix/'.$row_applicant['profilePix']);
	}

	//delete if file exists
	if($row_applicant['cv'] != '' && file_exists('../eSelection/uploadedCVs/'.$row_applicant['cv'])) {
		//echo "<br>Profile Pix exists, delete it";
		unlink('../eSelection/uploadedCVs/'.$row_applicant['cv']);
	}

  $deleteSQL = sprintf("DELETE FROM applicants WHERE id=%s",
                       GetSQLValueString($_GET['id'], "int"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($deleteSQL, $fer) or die(mysql_error());

  $deleteGoTo = "applicants.php?msg=".urlencode("Applicant Deleted!");
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}


mysql_free_result($applicant);
?>