<?php require_once("_inc_checkSession.php"); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php include ('../_inc_Functions.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$vacancy_id = $_GET['id'];
$currentPage = $_SERVER["PHP_SELF"];

mysql_select_db($database_fer, $fer);
//$vacancy_query = sprintf("SELECT * FROM vacancies WHERE id=%s",
			// GetSQLValueString($vacancy_id, "int"));
$query_vacancy = sprintf("SELECT vac.id, vac.title, vac.longDescription, vac.techReqs, vac.eduQual,vac.datePosted, vac.behavReqs, vac.staffLevel,  vac.dateOpening, vac.dateClosing,emp.companyName, vac.summary, vac.jobType, vac.status , job.cat_name FROM vacancies vac LEFT JOIN  employers emp ON (vac.employer_id=emp.id) LEFT JOIN job_categories job ON
 (vac.jobCategory_id=job.id_cat) WHERE vac.id=%s", GetSQLValueString($vacancy_id,"int"));

$vacancy = mysql_query($query_vacancy, $fer) or die(mysql_error());
$row_vacancy = mysql_fetch_assoc($vacancy);

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Vacancy | <?php echo $config['shortname'] ?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link href="assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('-inc-top.php'); ?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<?php include('-inc-navbar-side.php'); ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					Widget settings form goes here
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Vacancies <small>view vacancy details</small>
			            </h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i> </li>
			                <li><a href="#">Vacancies</a> <i class="icon-angle-right"></i> </li>
			                <li><a href="#"> Vacancy Details</a> </li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) { ?>
			    <div class="alert  alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg'] ?></strong> </div>
			    <?php } ?>
			    <?php if (isset($_GET['error'])) { ?>
			    <div class="alert alert-error">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['error'] ?></strong> </div>
			    <?php } ?>
			    <div id="dashboard">
			        <!-- BEGIN DASHBOARD STATS -->
			        <div class="row-fluid">
			            <div class="span12">
			                <!-- BEGIN EXAMPLE TABLE PORTLET-->
			                <div class="portlet box light-grey">
			                    <div class="portlet-title">
			                        <div class="caption"><i class="icon-barcode"></i>Vacancy Details </div>
		                        </div>
			                    <div class="portlet-body">
			                        <table class="table table-bordered">
			                            <tr>
			                                <td class="bottomBorder" width="25%">Company Name</td>
			                                <td class="bottomBorderWhite" width="75%"><strong><?php echo $row_vacancy['companyName']; ?></strong></td>
		                                </tr>
			                            <tr>
			                                <td class="bottomBorder">Summary</td>
			                                <td class="bottomBorderWhite"><strong><?php echo $row_vacancy['summary']; ?></strong></td>
		                                </tr>
			                            <tr>
			                                <td width="22%" class="bottomBorder">Full Description</td>
			                                <td width="78%" class="bottomBorderWhite"><strong><?php echo $row_vacancy['longDescription']; ?></strong></td>
		                                </tr>
			                            <tr>
			                                <td class="bottomBorder">Technical Requirements</td>
			                                <td class="bottomBorderWhite"><strong><?php echo $row_vacancy['techReqs']; ?></strong></td>
		                                </tr>
			                            <tr>
			                                <td class="bottomBorder">Educational Qualifications</td>
			                                <td class="bottomBorderWhite"><strong><?php echo $row_vacancy['eduQual']; ?></strong></td>
		                                </tr>
			                            <tr>
			                                <td class="bottomBorder">Behavioural Requirements</td>
			                                <td class="bottomBorderWhite"><strong><?php echo $row_vacancy['behavReqs']; ?></strong></td>
		                                </tr>
			                            <tr>
			                                <td class="bottomBorder">Job Category</td>
			                                <td class="bottomBorderWhite"><strong><?php echo ($row_vacancy['cat_name'])? $row_vacancy['cat_name'] : 'General'; ?></strong></td>
		                                </tr>
			                            <tr>
			                                <td class="bottomBorder">Job Type</td>
			                                <td class="bottomBorderWhite"><strong><?php echo $row_vacancy['jobType']; ?></strong></td>
		                                </tr>
			                            <tr>
			                                <td class="bottomBorder">Staff Level</td>
			                                <td class="bottomBorderWhite"><strong><?php echo $row_vacancy['staffLevel']; ?></strong></td>
		                                </tr>
			                            <tr>
			                                <td class="bottomBorder">Date Posted</td>
			                                <td class="bottomBorderWhite"><strong><?php echo date("d-M-Y", strtotime($row_vacancy['datePosted'])); ?></strong></td>
		                                </tr>
			                            <tr>
			                                <td class="bottomBorder">Opening Date</td>
			                                <td class="bottomBorderWhite"><strong><?php echo date("d-M-Y", strtotime($row_vacancy['dateOpening'])); ?></strong></td>
		                                </tr>
			                            <tr>
			                                <td class="bottomBorder">Closing Date</td>
			                                <td class="bottomBorderWhite"><strong><?php echo date("d-M-Y", strtotime($row_vacancy['dateClosing'])); ?></strong></td>
		                                </tr>
                                        <tr>
			                                <td class="bottomBorder">Number of Applications</td>
			                                <td class="bottomBorderWhite"><strong><?php echo CountApplications($row_vacancy['id']); ?></strong></td>
		                                </tr>
                                        <tr>
			                                <td class="bottomBorder">Status</td>
			                                <td class="bottomBorderWhite"><strong><?php echo $row_vacancy['status']; ?></strong></td>
		                                </tr>
		                            </table>
			                        <p><a class="btn black" href="vacancies.php"><i class="m-icon-swapleft m-icon-white"></i> Vacancies</a>
			                            <a class="btn green" href="vacancy-applicants.php?id=<?php echo $row_vacancy['id']; ?>">View Applicants</a>
                                        <a class="btn yellow" href="vacancy-edit.php?id=<?php echo $row_vacancy['id']; ?>">Edit Vacancy</a>
                                    <a title="Delete Vacancy" class="ValidateAction tooltips btn red" data-placement="top" data-confirm-msg="Are you sure you want to Delete? All information associated with this vacancy will also be cleared!!!" href="vacancy-delete.php?id=<?php echo $row_vacancy['id']; ?>">Delete Vacancy</a></p>
		                        </div>
		                    </div>
			                <!-- END EXAMPLE TABLE PORTLET-->
		                </div>
		            </div>
			        <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>
                <div></div>
		    </div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include('-inc-footer.php'); ?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>   
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  
	<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
	<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/index.js" type="text/javascript"></script>
	<script src="assets/scripts/tasks.js" type="text/javascript"></script>        
	<!-- END PAGE LEVEL SCRIPTS -->  
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
	<script>
jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   Index.init();
		   Index.initJQVMAP(); // init index page's custom scripts
		   Index.initCalendar(); // init index page's custom scripts
		   Index.initCharts(); // init index page's custom scripts
		   Index.initChat();
		   Index.initMiniCharts();
		   Index.initDashboardDaterange();
		   Index.initIntro();
		   Tasks.initDashboardWidget();
		});
    </script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<?php
mysql_free_result($manager);
?>
