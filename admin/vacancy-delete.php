<?php require_once("_inc_checkSession.php"); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_GET['id'])) && ($_GET['id'] != "")) {
	
	$deleteApplications = sprintf("DELETE  FROM applications WHERE vacancy_id=%s",
	  GetSQLValueString($_GET['id'], "int"));
	  
  $deleteSQL = sprintf("DELETE FROM vacancies WHERE id=%s",
                       GetSQLValueString($_GET['id'], "int"));
					   
	
	  
  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($deleteSQL, $fer) or die(mysql_error());
  
   mysql_select_db($database_fer, $fer);
  $Result2 = mysql_query($deleteApplications, $fer) or die(mysql_error());

  $deleteGoTo = "vacancies.php?msg=".urlencode("Vacancy Successfully Deleted");
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}
?>
