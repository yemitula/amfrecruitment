<?php error_reporting(0); ?>
<?php require_once("_inc_checkSession.php"); ?>
<?php $thisPage = basename( $_SERVER['PHP_SELF'] ); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

mysql_select_db($database_fer, $fer);

//applicants interviewed
$applicants_sql = "SELECT *, vacancies.title AS vacancy_title FROM interview_records LEFT JOIN applicants ON ir_applicant_id = id LEFT JOIN interviews ON ir_interview_id = int_id LEFT JOIN vacancies ON int_vacancy_id = vacancies.id WHERE ir_date_conducted IS NOT NULL AND ir_status <> 'pending'";
//role
if($_GET['role'] && !empty($_GET["role"])) {
	$applicants_sql .= sprintf(" AND int_id = %s", GetSQLValueString($_GET['role'],"int"));
}

//status
if($_GET['status'] && !empty($_GET["status"])) {
	$applicants_sql .= sprintf(" AND ir_status = %s", GetSQLValueString($_GET['status'],"text"));
}

//date
if(!empty($_GET['fromDate']) && !empty($_GET['toDate']) && strtotime($_GET['toDate']) >= strtotime($_GET['fromDate'])) {
	$applicants_sql .= sprintf(" AND ir_date_conducted BETWEEN %s AND %s", GetSQLValueString(date('Y-m-d h:i:s', strtotime($_GET['fromDate'])),"date"), GetSQLValueString(date('Y-m-d h:i:s', strtotime($_GET['toDate'])),"date"));
}

$applicants_sql .= " ORDER BY ir_date_conducted DESC";

$applicants = mysql_query($applicants_sql, $fer) or die(mysql_error());
$row_applicants = mysql_fetch_assoc($applicants);
$count_applicants = mysql_num_rows($applicants);

//roles from interview list for roles dropdown
$roles_sql = "SELECT int_id, IF(int_vacancy_id <> 0, title, int_purpose) AS role FROM interviews LEFT JOIN vacancies ON int_vacancy_id = id";
$roles = mysql_query($roles_sql, $fer) or die(mysql_error());
$row_roles = mysql_fetch_assoc($roles);
$count_roles = mysql_num_rows($roles);

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>All Interviewed Applicants | <?php echo $config['shortname'] ?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link href="assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<!--  -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<link rel="stylesheet" href="assets/plugins/data-tables/DT_bootstrap.css" />
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
	<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
	<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
	<link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
	<link href="../SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<script type="text/javascript">
<!-- Validation for delete -->
function ValidateDel (id,dest,msg) {

	var where_to = confirm(msg);
	
	if (where_to== true)
	 {
	   window.location.href = dest + id;
	   //alert('where to is true');
	 }
	else
	 {
	  return false;
	  }
}	  
</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('-inc-top.php'); ?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<?php include('-inc-navbar-side.php'); ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Interviewed Applicants <small>full list of all interviewed applicants </small> </h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
		                  <li><a href="int-list.php">Interviews</a> <i class="icon-angle-right"></i></li>
		                  <li><a href="#">Interviewed Applicants</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->

			    
			    <?php if (isset($_GET['msg'])) { ?>
			    <div class="alert  alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg'] ?></strong> </div>
			    <?php } ?>
			    <?php if (isset($_GET['error'])) { ?>
			    <div class="alert alert-error">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['error'] ?></strong> </div>
			    <?php } ?>

			    <div id="dashboard">
			        <!-- BEGIN DASHBOARD STATS -->
			        <div class="row-fluid">
			            <div class="span12">
			                <!-- BEGIN SAMPLE FORM PORTLET-->
			                <div class="portlet box green tabbable">
			                    <div class="portlet-title">
			                        <div class="caption"> <i class="icon-reorder"></i> <span class="hidden-480">List of Interviewed Applicants</span> </div>
		                        </div>
			                    <div class="portlet-body">
			                        <div class="tabbable portlet-tabs">
			                            <br><br>

							        	
							        	<form action="" method="GET" name="formFilter" id="formFilter" >
				                            <div class="row-fluid">
				                                <div class="span12">
				                                    <div id="sample_1_length" class="dataTables_length">
				                                        
	                                                    <span class="controls">

	                                                    <select name="role" class="m-wrap m-ctrl-medium span3" id="role">
				                                          <option value="" <?php if (empty($_GET['role'])) {echo "selected=\"selected\"";} ?>> All Roles </option>
				                                          <?php
	do {  
	?>
				                                          <option value="<?php echo $row_roles['int_id']?>"<?php if (!(strcmp($row_roles['int_id'], $_GET['role']))) {echo "selected=\"selected\"";} ?>><?php echo $row_roles['role']?></option>
				                                          <?php
	} while ($row_roles = mysql_fetch_assoc($roles));
	  $rows = mysql_num_rows($roles);
	  if($rows > 0) {
	      mysql_data_seek($roles, 0);
		  $row_roles = mysql_fetch_assoc($roles);
	  }
	?>
	                                                    </select>
				                                        
	                                                    <select name="status" class="m-wrap m-ctrl-medium span3" id="status" >
				                                            <option value="">Any Status</option>
				                                            <option value="RECOMMENDED" <?php if(isset($_GET["status"]) && $_GET["status"] == "RECOMMENDED") { echo "selected"; } ?> >RECOMMENDED</option>
				                                            <option value="KIV" <?php if(isset($_GET["status"]) && $_GET["status"] == "KIV") { echo "selected"; } ?>>KIV</option>
				                                            <option value="NOT-RECOMMENDED" <?php if(isset($_GET["status"]) && $_GET["status"] == "NOT-RECOMMENDED") { echo "selected"; } ?>>NOT-RECOMMENDED</option>
	                                          			</select>
	                                                    &nbsp;
	                                                    
				                                        
	                                                    

				                                        
				                                        <input id="fromDate" name="fromDate" class="m-wrap m-ctrl-medium date-picker" style="width: 120px;" readonly size="16" type="text" value="" placeholder="From Date" />
	                                                    &nbsp;
	                                                    <input id="toDate" name="toDate" class="m-wrap m-ctrl-medium date-picker" style="width: 120px;" readonly size="16" type="text" value="" placeholder="To Date" />

				                                        <button type="submit" class="m-wrap m-ctrl-medium btn black">Filter <i class="m-icon-swapright m-icon-white"></i></button>
				                                        </span>

			                                        </div>
			                                    </div>
			                                </div>
			                            </form>
							        	

			                            <?php if($count_applicants < 1) { ?>
			                            <br><br>
			                            <div class="row-fluid">
									        <div class="alert">
									            
									            <strong>Empty List!</strong> No interviews conducted (matching your parameters). </div>
								        </div>
								        <?php } else { ?>

								        
			                            <!-- BEGIN FORM-->
										<div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid"  style="padding: 10px !important;">
										    
										    

										    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1" style="margin:10px;">
										        <thead>
										            <tr>
										                <th width="15%">Date Interviewed</th>
										                <th width="25%">Vacancy/Purpose</th>
										                <th width="25%">Applicant</th>
										                <th width="15%">Status</th>
										                <th width="20%">&nbsp;</th>
									                </tr>
									            </thead>
										        <tbody>
										            <?php do { ?>
									                <tr>
									                    <td valign="top"><?php echo date("d-M-Y g:i A", strtotime($row_applicants['ir_date_conducted'])) ?></td>
									                    <td valign="top"><?php echo ($row_applicants['int_vacancy_id']=='0')? $row_applicants['int_purpose'] : $row_applicants['vacancy_title'] ?></td>
									                    <td valign="top"><a href="applicant-details.php?id=<?php echo $row_applicants['id'] ?>" target="_blank" title="Applicant Details" class="tooltips"><?php echo $row_applicants['surname'] ?> <?php echo $row_applicants['firstname'] ?></a></td>

									                    <td valign="top"><?php echo $row_applicants['ir_status'] ?></td>
									                    
									                    <td valign="top">
									                        
									                        <a href="int-record-details.php?id=<?php echo $row_applicants['ir_id']; ?>" class="tooltips" title="Interview Sheet"><i class="icon-edit"></i></a>
								                        </td>
								                    </tr>
									                <?php } while ($row_applicants = mysql_fetch_assoc($applicants)); ?>   
									            </tbody>
									        </table>
									    </div>	 
		                              <!-- END FORM-->
		                              <?php } ?>
		                            </div>
</div>
</div>
                            <!-- END SAMPLE FORM PORTLET-->
                        </div>
</div>
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>
                
		    </div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include('-inc-footer.php'); ?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>   
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  
	<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
	<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<!--  -->
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js"></script>
	<script src="assets/scripts/table-advanced.js"></script>
		<script src="assets/scripts/form-components.js"></script>     
	<script src="assets/scripts/table-managed.js"></script>     
     
	<script>
		jQuery(document).ready(function() {       
		   App.init();
		   TableAdvanced.init();
		   FormComponents.init();
		   TableManaged.init();
		});
	</script> 
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
<script>
		$(document).ready(function(e) {
		   $("#role").val('<?php echo $_GET['role'] ?>');
		   $("#status").val('<?php echo $_GET['status'] ?>');
		   $("#fromDate").val('<?php echo $_GET['fromDate'] ?>');
		   $("#toDate").val('<?php echo $_GET['toDate'] ?>');
        });
	</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>