<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_application = "-1";
if (isset($_GET['id'])) {
  $colname_application = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_application = sprintf("SELECT * FROM applications WHERE id = %s", GetSQLValueString($colname_application, "int"));
$application = mysql_query($query_application, $fer) or die(mysql_error());
$row_application = mysql_fetch_assoc($application);
$totalRows_application = mysql_num_rows($application);

if ((isset($_GET['id'])) && ($_GET['id'] != "")) {
  $deleteSQL = sprintf("DELETE FROM applications WHERE id=%s",
                       GetSQLValueString($_GET['id'], "int"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($deleteSQL, $fer) or die(mysql_error());

  $deleteGoTo = "applicant-applications.php?id=".$row_application['applicant_id']."&msg=".urlencode("Application Deleted!");
  header(sprintf("Location: %s", $deleteGoTo));
}

mysql_free_result($application);
?>
