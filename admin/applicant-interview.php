<?php require_once "_inc_checkSession.php";?>
<?php require_once '../Connections/fer.php';?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}

if ((isset($_GET['del_query'])) && ($_GET['del_query'] != "")) {
	$deleteSQL = sprintf("DELETE FROM interview_records WHERE int_id=%s",
		GetSQLValueString($_GET['del_query'], "int"));

	mysql_select_db($database_fer, $fer);
	$Result1 = mysql_query($deleteSQL, $fer) or die(mysql_error());

	$deleteGoTo = "interview.php?msg=Interview Details Deleted Successfully...";
	if (isset($_SERVER['QUERY_STRING'])) {
		$deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
		$deleteGoTo .= $_SERVER['QUERY_STRING'];
	}
	header(sprintf("Location: %s", $deleteGoTo));
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "interview") && empty($_POST["int_id"]) && empty($_GET["query"])) {
	$insertSQL = sprintf("INSERT INTO interview_records (applicant_id, vacancy_id, `date`, recommended, remarks) VALUES (%s, %s, %s, %s, %s)",
		GetSQLValueString($_POST['applicant_id'], "int"),
		GetSQLValueString($_POST['vacancy_id'], "int"),
		GetSQLValueString($_POST['date'], "date"),
		GetSQLValueString($_POST['recommended'], "int"),
		GetSQLValueString($_POST["remarks"], "text"));

	mysql_select_db($database_fer, $fer);
	$Result1 = mysql_query($insertSQL, $fer) or die(mysql_error());

	$insertGoTo = "interviews.php?msg=Interview added sucessfully...";
	if (isset($_SERVER['QUERY_STRING'])) {
		$insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
		$insertGoTo .= $_SERVER['QUERY_STRING'];
	}
	header(sprintf("Location: %s", $insertGoTo));
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "interview") && !empty($_POST["int_id"]) && !empty($_GET["query"])) {
	$updateSQL = sprintf("UPDATE interview_records SET `date`=%s, remarks=%s, recommended=%s WHERE int_id=%s",
		GetSQLValueString($_POST['date'], "date"),
		GetSQLValueString($_POST['remarks'], "text"),
		GetSQLValueString($_POST["recommended"], "text"),
		GetSQLValueString($_POST['int_id'], "int"));

	mysql_select_db($database_fer, $fer);
	$Result1 = mysql_query($updateSQL, $fer) or die(mysql_error());

	$updateGoTo = "interviews.php?msg=Interview details edited successfully...";
	if (isset($_SERVER['QUERY_STRING'])) {
		$updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
		$updateGoTo .= $_SERVER['QUERY_STRING'];
	}
	header(sprintf("Location: %s", $updateGoTo));
}

$colname_interview = "-1";
if (isset($_GET['query'])) {
	$colname_interview = $_GET['query'];
}
mysql_select_db($database_fer, $fer);
$query_interview = sprintf("SELECT * FROM interview_records WHERE int_id = %s", GetSQLValueString($colname_interview, "int"));
$interview = mysql_query($query_interview, $fer) or die(mysql_error());
$row_interview = mysql_fetch_assoc($interview);
$totalRows_interview = mysql_num_rows($interview);

$colname_applicants = "-1";
if (isset($_GET['a'])) {
	$colname_applicants = $_GET['a'];
}
mysql_select_db($database_fer, $fer);
$query_applicants = sprintf("SELECT * FROM applicants WHERE id = %s", GetSQLValueString($colname_applicants, "int"));
$applicants = mysql_query($query_applicants, $fer) or die(mysql_error());
$row_applicants = mysql_fetch_assoc($applicants);
$totalRows_applicants = mysql_num_rows($applicants);

$colname_application = "-1";
if (isset($_GET['v'])) {
	$colname_application = $_GET['v'];
}
mysql_select_db($database_fer, $fer);
$query_application = sprintf("SELECT * FROM applications WHERE applicant_id = ".$_GET['a']." vacancy_id = %s", GetSQLValueString($colname_application, "int"));
$application = mysql_query($query_applicants, $fer) or die(mysql_error());
$row_application = mysql_fetch_assoc($application);
$totalRows_applicants = mysql_num_rows($application);

mysql_select_db($database_fer, $fer);
$query_vacancy = "SELECT * FROM vacancies WHERE id =".$_GET['v']."";
$vacancy = mysql_query($query_vacancy, $fer) or die(mysql_error());
$row_vacancy = mysql_fetch_assoc($vacancy);

mysql_select_db($database_fer, $fer);
$query_workexp = "SELECT * FROM workexp WHERE id = ".$_GET['a']."";
$workexp = mysql_query($query_workexp, $fer) or die(mysql_error());
$row_workexp = mysql_fetch_assoc($workexp);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Applicant Interview | <?php echo $config['shortname']?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
	<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
	<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
	<link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">

<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include '-inc-top.php';?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<?php include '-inc-navbar-side.php';?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					Widget settings form goes here
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Interview <small>Enter interview details</small> </h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="vacancies.php">Vacancies</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#"> Interviews</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) {?>
			    <div class="alert  alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg']?></strong> </div>
			    <?php }
?>
			    <?php if (isset($_GET['error'])) {?>
			    <div class="alert alert-error">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['error']?></strong> </div>
			    <?php }
?>
			    <div id="dashboard">
			        <!-- BEGIN DASHBOARD STATS -->
			        <div class="row-fluid">
			            <div class="span12">
			                <!-- BEGIN SAMPLE FORM PORTLET-->
			                <div class="portlet box green tabbable">
			                    <div class="portlet-title">
			                        <div class="caption"> <i class="icon-reorder"></i> <span class="hidden-480">Enter ( <?php echo $row_applicants['surname'];?> <?php echo $row_applicants['firstname'];?> <?php echo $_GET["n"];?>) interview details</span> </div>
		                        </div>
			                    <div class="portlet-body form">
			                        <div class="tabbable portlet-tabs">
			                            <!-- BEGIN FORM-->
			                            <form method="POST" name="interview" action="<?php echo $editFormAction;?>" class="form-horizontal" id="formCreate">
			                                <p>&nbsp;</p>
			                                <div class="control-group">
			                                    <label class="control-label"> Position</label>
			                                    <div class="controls">
			                                      <input name="" type="text" class="m-wrap m-ctrl-medium" id="" disabled placeholder="" value="<?php echo $row_vacancy['staffLevel'];?>" />
		                                       </div>
											</div>
			                                <div class="control-group">
			                                    <label class="control-label"> Current Salary</label>
			                                    <div class="controls"><span id="sprytextfield1">
			                                      <input name="" type="text" class="m-wrap m-ctrl-medium" id="" placeholder="" disabled value="N<?php echo $row_workexp['job_salary'];?>" />
		                                        <span class="textfieldRequiredMsg">A value is required.</span></span></div>
											</div>
			                                <div class="control-group">
			                                    <label class="control-label"> Expected Salary</label>
			                                    <div class="controls">
			                                      <input name="" type="text" class="m-wrap m-ctrl-medium" id="" placeholder="" value="N<?php echo $row_application['min_salary'];?> - N<?php echo $row_application["max_salary"]; ?>" />
		                                        </div>
											</div>											
			                                <div class="control-group">
			                                    <label class="control-label"> Date Of Interview</label>
			                                    <div class="controls"><span id="sprytextfield1">
			                                      <input name="date" type="text" class="m-wrap m-ctrl-medium date-picker" id="dateClosing" placeholder="" value="<?php echo $row_interview['date'];?>" />
		                                        <span class="textfieldRequiredMsg">A value is required.</span></span></div>
											</div>
			                                <div class="control-group">
			                                    <label class="control-label"> Remarks</label>
			                                    <div class="controls"><span id="sprytextarea1">
			                                      <textarea name="remarks" id="remarks" class="span9 ckeditor m-wrap" rows="6"><?php echo $row_interview['remarks'];?></textarea>
		                                        <span class="textareaRequiredMsg">A value is required.</span></span></div>
											</div>
			                                <div class="control-group">
			                                    <label class="control-label"> Recommended ?</label>
			                                    <div class="controls"><span id="sprytextfield1">
			                                      <input name="recommended " type="checkbox"  <?php if (isset($row_interview["recommended"])) { echo "checked" ; } ?>class="m-wrap m-ctrl-medium" id="" placeholder="" />
		                                        <span class="textfieldRequiredMsg">A value is required.</span></span></div>
											</div>											
			                                <div class="form-actions">
			                                    <button type="submit" class="btn green"><i class="icon-ok"></i> Save</button>
			                                    <a href="interviews.php">
			                                        <button type="button" class="btn">Cancel</button>
		                                        </a>
			                                    <input name="vacancy_id" type="hidden" id="vacancy_id" value="<?php echo $_GET['v'];?>">
			                                </div>
			                                <input type="hidden" name="MM_insert" value="interview">
			                                <input name="applicant_id" type="hidden" id="applicant_id" value="<?php echo $_GET["a"];?>">
			                                <input name="int_id" type="hidden" id="int_id" value="<?php echo $row_interview['int_id'];?>">
			                                <input type="hidden" name="MM_update" value="interview">
		                                </form>
			                            <!-- END FORM-->
		                            </div>
</div>
</div>
                            <!-- END SAMPLE FORM PORTLET-->
                        </div>
</div>
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>
                <div>
                </div>
		    </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include '-inc-footer.php';?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>
	<![endif]-->
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
	<script>
Query(document).ready(function() {
		   App.init(); // initlayout and core plugins
		   FormComponents.init();
		});
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {validateOn:["blur", "change"]});
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1", {validateOn:["blur", "change"]});

$('.datepicker').datepicker({
    format: 'mm/dd/yyyy',
    startDate: '-3d'
})

    </script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<?php
mysql_free_result($interview);

mysql_free_result($applicants);
?>
