<?php require_once("_inc_checkSession.php"); ?>
<?php $thisPage = basename( $_SERVER['PHP_SELF'] ); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php require_once("../classes/class.upload.php"); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_employer = "-1";
if (isset($_GET['id'])) {
  $colname_employer = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_employer = sprintf("SELECT * FROM employers WHERE id = %s", GetSQLValueString($colname_employer, "int"));
$employer = mysql_query($query_employer, $fer) or die(mysql_error());
$row_employer = mysql_fetch_assoc($employer);
$totalRows_employer = mysql_num_rows($employer);

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "formEmployer")) {

	
	mysql_select_db($database_fer, $fer);
	//check username
	$username = $_POST['username'];
	if ($username != $row_employer['username']) {
		$query_employers = "SELECT * FROM employers WHERE username = '$username'";
		$employers = mysql_query($query_employers, $fer) or die(mysql_error());
		$row_employers = mysql_fetch_assoc($employers);
		$totalRows_employers = mysql_num_rows($employers);
		
		if($totalRows_employers) {
			//employer already exists
			header("Location: employer-edit.php?error=".urlencode("This username has already been used for registration by another employer!"));
			exit;
		}
	}
	//check email
	$email = $_POST['email'];
	if ($email != $row_employer['email']) {
		$query_employers = "SELECT * FROM employers WHERE email = '$email'";
		$employers = mysql_query($query_employers, $fer) or die(mysql_error());
		$row_employers = mysql_fetch_assoc($employers);
		$totalRows_employers = mysql_num_rows($employers);
		
		if($totalRows_employers) {
			//employer already exists
			header("Location: employer-edit.php?error=".urlencode("This email has already been used for registration by another employer!"));
			exit;
		}
	}
	
	//logo
	//upload the file
	$logo = new upload($_FILES['logo']);
	//file uploaded?	
	if($logo->uploaded) {
		//is file extension compliant?
		$ext = strtolower($logo->file_src_name_ext);
		if($ext != 'jpg' && $ext != 'jpeg' && $ext != 'png') {
			//extension non-compliant, display error
			header("Location: employer-add.php?error=".urlencode("Your Logo is NOT in a valid Format!"));
			exit;
		}
		//is file size compliant?
		if($logo->file_src_size > 51200) {
			//file size non-compliant, display error
			header("Location: employer-add.php?error=".urlencode("Your Logo's file size is greater than 50KB!"));
			exit;
		}
		//echo("<br>No errors! Ready to store file");		
		
		//delete if it already exists
		if($row_employer['logo'] != '') {
			//echo "<br>Profile Pix exists, delete it";
			unlink('../images/employerLogos/'.$row_employer['logo']);
		}
		
		$logo->file_new_name_body = $_POST['username'];
		$uploadComplete = $logo->Process("../images/employerLogos/");
		if($uploadComplete) {
			//echo "<br>File successfully uploaded"; die;
		} else {
			//echo "<br>File didn't upload! Error From Class:".$logo->file_src_error; die;
		}
	}
	
	//thumbnail
	//upload the file
	$thumbnail = new upload($_FILES['thumbnail']);
	//file uploaded?	
	if($thumbnail->uploaded) {
		//is file extension compliant?
		$ext = strtolower($thumbnail->file_src_name_ext);
		if($ext != 'jpg' && $ext != 'jpeg' && $ext != 'png') {
			//extension non-compliant, display error
			header("Location: employer-add.php?error=".urlencode("Your Thumbnail is NOT in a valid Format!"));
			exit;
		}
		//is file size compliant?
		if($thumbnail->file_src_size > 20480) {
			//file size non-compliant, display error
			header("Location: employer-add.php?error=".urlencode("Your Thumbnail's file size is greater than 20KB!"));
			exit;
		}
		//echo("<br>No errors! Ready to store file");		

		//delete if it already exists
		if($row_employer['thumbnail'] != '') {
			//echo "<br>Profile Pix exists, delete it";
			unlink('../images/employerThumbnails/'.$row_employer['thumbnail']);
		}

		$thumbnail->file_new_name_body = $_POST['username'];
		$uploadComplete = $thumbnail->Process("../images/employerThumbnails/");
		if($uploadComplete) {
			//echo "<br>File successfully uploaded"; die;
		} else {
			//echo "<br>File didn't upload! Error From Class:".$thumbnail->file_src_error; die;
		}
	}
	$logoFile = ($logo->file_dst_name)? $logo->file_dst_name : $row_employer['logo'];
	$thumbnailFile = ($thumbnail->file_dst_name)? $thumbnail->file_dst_name : $row_employer['thumbnail'];	

  $updateSQL = sprintf("UPDATE employers SET companyName=%s, username=%s, password=%s, address=%s, city=%s, country=%s, `state`=%s, phone=%s, email=%s, repName=%s, repPhone=%s, logo=%s, thumbnail=%s WHERE id=%s",
                       GetSQLValueString($_POST['companyName'], "text"),
                       GetSQLValueString($_POST['username'], "text"),
                       GetSQLValueString($_POST['password'], "text"),
                       GetSQLValueString($_POST['address'], "text"),
                       GetSQLValueString($_POST['city'], "text"),
                       GetSQLValueString($_POST['country'], "text"),
                       GetSQLValueString($_POST['state'], "text"),
                       GetSQLValueString($_POST['phone'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['repName'], "text"),
                       GetSQLValueString($_POST['repPhone'], "text"),
                       GetSQLValueString($logoFile, "text"),
                       GetSQLValueString($thumbnailFile, "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($updateSQL, $fer) or die(mysql_error());

  $insertGoTo = "employers.php?msg=".urlencode("Company Updated!");
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Employers | <?php echo $config['shortname'] ?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
	<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
	<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
	<link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
	<link href="../SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('-inc-top.php'); ?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<?php include('-inc-navbar-side.php'); ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					Widget settings form goes here
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Company <small> </small> </h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
		                  <li><a href="employers.php">Companies</a> <i class="icon-angle-right"></i></li>
		                  <li><a href="#">Company</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) { ?>
			    <div class="alert  alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg'] ?></strong> </div>
			    <?php } ?>
			    <?php if (isset($_GET['error'])) { ?>
			    <div class="alert alert-error">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['error'] ?></strong> </div>
			    <?php } ?>
			    <div id="dashboard">
			        <!-- BEGIN DASHBOARD STATS -->
			        <div class="row-fluid">
			            <div class="span12">
			                <!-- BEGIN SAMPLE FORM PORTLET-->
			                <div class="portlet box green tabbable">
			                    <div class="portlet-title">
			                        <div class="caption"> <i class="icon-reorder"></i> <span class="hidden-480">Edit Company</span> </div>
		                        </div>
			                    <div class="portlet-body form">
			                        <div class="tabbable portlet-tabs">
			                            <!-- BEGIN FORM-->
											  <form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="formEmployer" id="formEmployer">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                              <tr>
                                  <td width="32%" align="right" valign="middle"><strong>Company Name</strong></td>
                                  <td width="68%"><span id="sprytextfield1">
                                  <label for="companyName"></label>
                                      <input name="companyName" type="text" class="m-wrap span7" id="companyName" value="<?php echo $row_employer['companyName'] ?>" />
                                <span class="textfieldRequiredMsg">A value is required.</span></span></td>
</tr>
                              <tr>
                                  <td align="right" valign="middle"><strong>Username</strong></td>
                                  <td><span id="sprytextfield2">
                                  <label for="username"></label>
                                      <input name="username" type="text" class="m-wrap span7" id="username"  value="<?php echo $row_employer['username'] ?>"/>
                                <span class="textfieldRequiredMsg">A value is required.</span></span></td>
                              </tr>
                              <tr>
                                  <td align="right" valign="middle"><strong>Password</strong></td>
                                  <td><span id="sprytextfield3">
                                  <label for="password"></label>
                                  <input name="password" type="password" class="m-wrap span7" id="password" value="<?php echo $row_employer['password'] ?>" />
                                <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldMinCharsMsg">Minimum number of characters not met.</span></span></td>
                              </tr>
                              <tr>
                                  <td align="right" valign="middle"><strong>Retype Password</strong></td>
                                  <td><span id="spryconfirm1">
                                      <input name="password2" type="password" class="m-wrap span7" id="password2" value="<?php echo $row_employer['password'] ?>" />
                                <span class="confirmRequiredMsg">A value is required.</span><span class="confirmInvalidMsg">The values don't match.</span></span></td>
                              </tr>
                              <tr>
                                  <td align="right" valign="middle">&nbsp;</td>
                                  <td>&nbsp;</td>
                              </tr>
                              <tr>
                                  <td align="right" valign="middle"><strong>Email</strong></td>
                                  <td><span id="sprytextfield5">
                                  <label for="email"></label>
                                  <input name="email" type="text" class="m-wrap span7" id="email"  value="<?php echo $row_employer['email'] ?>"/>
                                <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldInvalidFormatMsg">Invalid format.</span></span></td>
                              </tr>
                              <tr>
                                  <td align="right" valign="middle"><strong>Phone</strong></td>
                                  <td><span id="sprytextfield6">
                                  <label for="phone"></label>
                                      <input name="phone" type="text" class="m-wrap span7" id="phone"  value="<?php echo $row_employer['phone'] ?>"/>
                                <span class="textfieldRequiredMsg">A value is required.</span></span></td>
                              </tr>
                              <tr>
                                  <td align="right" valign="middle"><strong>Address</strong></td>
                                  <td><span id="sprytextarea1">
                                  <label for="address"></label>
                                      <textarea name="address" cols="45" rows="5" class="m-wrap span7" id="address"><?php echo $row_employer['address'] ?></textarea>
                                <span class="textareaRequiredMsg">A value is required.</span></span></td>
                              </tr>
                              <tr>
                                  <td align="right" valign="middle"><strong>City</strong></td>
                                  <td><span id="sprytextfield7">
                                  <label for="city"></label>
                                      <input name="city" type="text" class="m-wrap span7" id="city"  value="<?php echo $row_employer['city'] ?>"/>
                                <span class="textfieldRequiredMsg">A value is required.</span></span></td>
                              </tr>
                              <tr>
                                  <td align="right" valign="middle"><strong>Country</strong></td>
                                  <td><span id="spryselect1">
                                      <select name="country" class="m-wrap span7" id="country">
                                          <?php include('../_inc_countryList.php'); ?>
                                          <option value="other">- Country Not Listed -</option>
                                  </select> <input name="country2" id="country2" type="hidden"  value="<?php echo $row_employer['country'] ?>" />
                                <span class="selectRequiredMsg">Please select an item.</span></span></td>
                              </tr>
                              <tr class="stateArea">
                                  <td align="right" valign="middle"><strong>State</strong></td>
                                  <td><select name="state" id="state" class="m-wrap span7">
                                      <?php include('../_inc_stateList.php'); ?>
                                </select><input id="state2" name="state2" type="hidden"  value="<?php echo $row_employer['state'] ?>" /></td>
                              </tr>
                              <tr>
                                  <td align="right" valign="middle">&nbsp;</td>
                                  <td>&nbsp;</td>
                              </tr>
                              <tr>
                                  <td align="right" valign="middle"><strong>Name of Representative/Contact</strong></td>
                                  <td><span id="sprytextfield8">
                                  <label for="repName"></label>
                                      <input name="repName" type="text" class="m-wrap span7" id="repName"  value="<?php echo $row_employer['repName'] ?>"/>
                                <span class="textfieldRequiredMsg">A value is required.</span></span></td>
                              </tr>
                              <tr>
                                  <td align="right" valign="middle"><strong>Phone Number</strong></td>
                                  <td><span id="sprytextfield9">
                                  <label for="repPhone"></label>
                                      <input name="repPhone" type="text" class="m-wrap span7" id="repPhone"  value="<?php echo $row_employer['repPhone'] ?>"/>
                                <span class="textfieldRequiredMsg">A value is required.</span></span></td>
                              </tr>
                              <tr>
                                  <td align="right" valign="middle">&nbsp;</td>
                                  <td>&nbsp;</td>
                              </tr>
                              <?php if ($row_employer['logo']) { ?>
                              <tr>
                                  <td align="right" valign="middle"><strong>Current Logo</strong></td>
                                  <td valign="top"><img src="../images/employerLogos/<?php echo $row_employer['logo']; ?>" width="150" height="150" alt="" /></td>
                              </tr>
                              <?php } ?>
                              
                              <tr>
                                  <td align="right" valign="middle"><strong>Logo</strong></td>
                                  <td><label for="logo"></label>
                                <input type="file" name="logo" id="logo" /></td>
                              </tr>
                              <?php if ($row_employer['thumbnail']) { ?>
                              <tr>
                                  <td align="right" valign="middle"><strong>Current Thumbnail</strong></td>
                                  <td valign="top"><img src="../images/employerThumbnails/<?php echo $row_employer['thumbnail'] ?>" width="48" alt="" /></td>
                              </tr>
                              <?php } ?>
                              <tr>
                                  <td align="right" valign="middle"><strong>Thumbnail</strong></td>
                                  <td><label for="thumbnail"></label>
                                <input type="file" name="thumbnail" id="thumbnail" /></td>
                              </tr>
                              <tr>
                                  <td align="right" valign="middle">&nbsp;</td>
                                  <td>&nbsp;</td>
                              </tr>
                              <tr>
                                  <td align="right" valign="middle">
                                  <input type="hidden" name="MM_insert" value="formEmployer" />
                                <input type="hidden" name="MM_update" value="formEmployer" />                                    <input name="id" type="hidden" id="id" value="<?php echo $row_employer['id']; ?>" /></td>
                                  <td align="left" id="captchaArea"><a href="employers.php">
                                  <button type="button" class="btn red">&laquo; Cancel</button>
                                </a>&nbsp;&nbsp;&nbsp;                                       <input name="submit" type="submit" class="btn green" id="submit" value="Update" /></td>
                              </tr>
                          </table>  
                                      </form>
		                              <!-- END FORM-->
		                            </div>
</div>
</div>
                            <!-- END SAMPLE FORM PORTLET-->
                        </div>
</div>
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>
                
		    </div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include('-inc-footer.php'); ?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script> 
	<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>   
	<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>   
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script> 
	<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>     
	<!-- END PAGE LEVEL SCRIPTS -->  
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
<script>
		$(document).ready(function(e) {
		   $("#month").val('<?php echo $row_report['month'] ?>');
		   $("#year").val('<?php echo $row_report['year'] ?>');
        });
	</script>
	<script>
jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   FormComponents.init();
		});
    </script>
    <script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3", "none", {minChars:8});
var sprytextfield5 = new Spry.Widget.ValidationTextField("sprytextfield5", "email");
var sprytextfield6 = new Spry.Widget.ValidationTextField("sprytextfield6");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1");
var sprytextfield7 = new Spry.Widget.ValidationTextField("sprytextfield7");
var sprytextfield8 = new Spry.Widget.ValidationTextField("sprytextfield8");
var sprytextfield9 = new Spry.Widget.ValidationTextField("sprytextfield9");
var spryconfirm1 = new Spry.Widget.ValidationConfirm("spryconfirm1", "password");
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
</script>
<script type="text/javascript">
$(document).ready(function() {
	if($("#country2").val() != '') {
		$("#country").val($("#country2").val());
	}

	$('.stateArea').hide();
	if($('select#country').val() == 'Nigeria') {
		$('.stateArea').show();
	}
		
	if($("#state2").val() != '') {
		$("#state").val($("#state2").val());
	}

  	//actions for dropdowns - nationality (to display state) and state( to display LGAs)
	$("select#country").change(function()
	{
		//alert("change nationality");
		if($(this).val() != "Nigeria")
		{
			$('.stateArea').hide();//alert("not Nigeria");
		}
		else
		{
			$('.stateArea').show();
		}
	});//end of nationality
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>