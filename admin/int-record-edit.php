<?php require_once "_inc_checkSession.php";?>
<?php $thisPage = basename($_SERVER['PHP_SELF']);?>
<?php require_once '../_inc_config.php';?>
<?php require_once '../Connections/fer.php';?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

mysql_select_db($database_fer, $fer);

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "formCreate") && !empty($_POST["ir_id"])) {
	
	//var_dump($_POST); die;
	//die(date("Y-m-d h:i:s", strtotime($_POST['int_date'])));

	//save ratings and calculate total
	$ratings_total = 0;
	//criteria ratings
	//delete existing ratings
	$delete_SQL = sprintf("DELETE FROM interview_criteria_ratings WHERE icr_record_id=%s",
		GetSQLValueString($_POST['ir_id'],"int"));
	$delete_RS = mysql_query($delete_SQL, $fer) or die(mysql_error());
	//insert new ones
	foreach ($_POST['icr_rating'] as $i => $rating) {
		//add to total
		$ratings_total += $rating;
		
		//insert to db
		$insert_SQL = sprintf("INSERT INTO interview_criteria_ratings (icr_record_id, icr_criteria_id, icr_rating, icr_comments) VALUES (%s, %s, %s, %s)", 
			GetSQLValueString($_POST['ir_id'],"int"),
			GetSQLValueString($_POST['icr_criteria_id'][$i],"int"),
			GetSQLValueString($rating,"int"),
			GetSQLValueString($_POST['icr_comments'][$i],"text")
			);
		$insert_RS = mysql_query($insert_SQL, $fer) or die(mysql_error());
	}
	//knowledge ratings
	//delete existing ratings
	$delete_SQL = sprintf("DELETE FROM interview_knowledge_ratings WHERE ikr_record_id=%s",
		GetSQLValueString($_POST['ir_id'],"int"));
	$delete_RS = mysql_query($delete_SQL, $fer) or die(mysql_error());
	//insert new ones
	foreach ($_POST['ikr_rating'] as $i => $rating) {
		//add to total
		$ratings_total += $rating;
		
		//insert to db
		$insert_SQL = sprintf("INSERT INTO interview_knowledge_ratings (ikr_record_id, ikr_knowledge, ikr_rating, ikr_comments) VALUES (%s, %s, %s, %s)", 
			GetSQLValueString($_POST['ir_id'],"int"),
			GetSQLValueString($_POST['ikr_knowledge'][$i],"text"),
			GetSQLValueString($rating,"int"),
			GetSQLValueString($_POST['ikr_comments'][$i],"text")
			);
		$insert_RS = mysql_query($insert_SQL, $fer) or die(mysql_error());
	}

	//update interview record
	$updateSQL = sprintf("UPDATE interview_records SET ir_date_conducted=%s, ir_totalScore=%s, ir_generalComments=%s, ir_strengths=%s, ir_weaknesses=%s, ir_otherArea1=%s, ir_otherArea2=%s, ir_currentSalary=%s, ir_expectedSalary=%s, ir_approvedSalary=%s, ir_interviewerName=%s, ir_interviewerDept=%s, ir_status=%s WHERE ir_id=%s",
		GetSQLValueString(date("Y-m-d h:i:s", strtotime($_POST['ir_date_conducted'])), "date"),
		GetSQLValueString($ratings_total, "double"),
		GetSQLValueString($_POST['ir_generalComments'], "text"),
		GetSQLValueString($_POST['ir_strengths'], "text"),
		GetSQLValueString($_POST['ir_weaknesses'], "text"),
		GetSQLValueString($_POST['ir_otherArea1'], "text"),
		GetSQLValueString($_POST['ir_otherArea2'], "text"),
		GetSQLValueString($_POST['ir_currentSalary'], "text"),
		GetSQLValueString($_POST['ir_expectedSalary'], "text"),
		GetSQLValueString($_POST['ir_approvedSalary'], "text"),
		GetSQLValueString($_POST['ir_interviewerName'], "text"),
		GetSQLValueString($_POST['ir_interviewerDept'], "text"),
		GetSQLValueString($_POST['ir_status'], "text"),
		GetSQLValueString($_POST['ir_id'], "int"));

	mysql_select_db($database_fer, $fer);
	$Result1 = mysql_query($updateSQL, $fer) or die(mysql_error());
	$ir_id = $_POST['ir_id'];
	$updateGoTo = "int-record-edit.php?id=$ir_id&msg=".urlencode("Interview Sheet Updated Successfully!");
	header(sprintf("Location: %s", $updateGoTo));
}

//interview record using ir_id
$record_sql = sprintf("SELECT * FROM interview_records WHERE ir_id=%s",GetSQLValueString($_GET['id'], "int"));
$record = mysql_query($record_sql, $fer) or die(mysql_error());
$row_record = mysql_fetch_assoc($record);
//var_dump($row_record); die;

//interview details using ir_interview_id
$query_interview = sprintf("SELECT * FROM interviews LEFT JOIN vacancies ON int_vacancy_id=id WHERE int_id=%s", GetSQLValueString($row_record['ir_interview_id'], "int"));
$interview = mysql_query($query_interview, $fer) or die(mysql_error());
$row_interview = mysql_fetch_assoc($interview);

//applicant
$query_applicant = sprintf("SELECT * FROM applicants WHERE id = %s", GetSQLValueString($row_record['ir_applicant_id'], "int"));
$applicant = mysql_query($query_applicant, $fer) or die(mysql_error());
$row_applicant = mysql_fetch_assoc($applicant);

//interview criteria
$query_criteria = "SELECT * FROM interview_criteria";
$criteria = mysql_query($query_criteria, $fer) or die(mysql_error());
$row_criteria = mysql_fetch_assoc($criteria);

$ir_id = $row_record['ir_id'];
//criteria ratings
$query_cratings = "SELECT * FROM interview_criteria_ratings WHERE icr_record_id = '$ir_id'";
$cratings = mysql_query($query_cratings, $fer) or die(mysql_error());
$row_cratings = mysql_fetch_assoc($cratings);

//knowledge ratings
$query_kratings = "SELECT * FROM interview_knowledge_ratings WHERE ikr_record_id = '$ir_id'";
$kratings = mysql_query($query_kratings, $fer) or die(mysql_error());

$currentPage = $_SERVER["PHP_SELF"];
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Edit Interview Sheet | <?php echo $config['shortname']?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
	<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
	<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
	<link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include '-inc-top.php';?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<?php include '-inc-navbar-side.php';?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Edit Interview Sheet  </h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="int-list.php">Interviews</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#"> Interview Sheet</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) {?>
			    <div class="alert  alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg']?></strong> </div>
			    <?php }
?>
			    <?php if (isset($_GET['error'])) {?>
			    <div class="alert alert-error">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['error']?></strong> </div>
			    <?php }
?>
			    <div id="dashboard">
			        <!-- BEGIN DASHBOARD STATS -->
			        <div class="row-fluid">
			            <div class="span12">
			                <!-- BEGIN SAMPLE FORM PORTLET-->
			                <div class="portlet box green tabbable">
			                    <div class="portlet-title">
			                        <div class="caption"> <i class="icon-reorder"></i> <span class="hidden-480">Interview Records</span> </div>
		                        </div>
			                    <div class="portlet-body form">
			                        <div class="tabbable portlet-tabs">
			                            <!-- BEGIN FORM-->
			                            <form method="POST" name="formCreate" action="<?php echo $editFormAction;?>" class="form-horizontal" id="formCreate">
			                                <p>&nbsp;</p>

			                                <div class="control-group">
												<label class="control-label">Name of Applicant</label>
												<div class="controls">
													<strong><?php echo $row_applicant['firstname'] ?> <?php echo $row_applicant['surname'] ?></strong>
												</div>
											</div>

											<div class="control-group">
												<label class="control-label">Interview Position</label>
												<div class="controls">
													<strong><?php echo $row_interview['int_vacancy_id']? $row_interview['title'] : $row_interview['int_purpose'] ?></strong>
												</div>
											</div>

			                                <span id="dates">
			                                <div class="control-group">
												<label class="control-label">Date & Time Interview was Conducted</label>
												<div class="controls">
													<div class="input-append date form_datetime">
														<input size="16" type="text" value="<?php echo date("Y-m-d h:i", strtotime($row_interview['ir_date_conducted'])) ?>" data-date-format="yyyy-mm-dd hh:ii" data-date-type="php" readonly class="m-wrap" required name="ir_date_conducted" id="ir_date_conducted">
														<span class="add-on"><i class="icon-calendar"></i></span>
													</div>
												</div>
											</div>
                                            </span>

                                            
											<div class="control-group">
											<table class="table table-striped table-bordered span10" style="margin:20px">
												<tr>
													<th width="50%">Recruitment Criteria</td>
													<th width="10%">Rating on Criteria</td>
													<th width="40%">Comments about Rating</td>
												</tr>
												<?php do { ?>
												<tr>
													<td>
														
														<?php echo $row_criteria['ic_name'] ?>
														<input type="hidden" name="icr_criteria_id[]" value="<?php echo $row_criteria['ic_id'] ?>">
														
													</td>
													<td>
														
														<select name="icr_rating[]" class="m-wrap rating_item">
															<?php for($i=1;$i<=5;$i++) { ?>
															<option <?php if($i == $row_cratings['icr_rating']) echo "selected" ?>><?php echo $i ?></option>
															<?php } ?>
														</select>
														
													</td>
													<td>
														
														<textarea name="ict_comments[]" placeholder="Enter Comments Here"><?php echo $row_cratings['icr_comments'] ?></textarea>
														
													</td>
												</tr>
												<?php 
												$row_cratings = mysql_fetch_assoc($cratings);
												} while($row_criteria = mysql_fetch_assoc($criteria)); ?>
												<tr>
													<td><strong>GENERAL KNOWLEDGE</strong></td>
													<td colspan="2"><em>(Pls score the candidate on any 4 job specific  questions asked)</em></td>
												</tr>

												<?php for($kcount=1; $kcount<=4; $kcount++) { ?>
												<?php $row_kratings = mysql_fetch_assoc($kratings); ?>
												<tr>
													<td>
														
														<input type="text" class="m-wrap span12" name="ikr_knowledge[]" value="<?php echo $row_kratings['ikr_knowledge'] ?>" required>
														
													</td>
													<td>
														
														<select name="ikr_rating[]" class="m-wrap rating_item">
															<?php for($i=1;$i<=5;$i++) { ?>
															<option <?php if($i == $row_kratings['ikr_rating']) echo "selected" ?>><?php echo $i ?></option>
															<?php } ?>
														</select>
														
													</td>
													<td>
														
														<textarea name="ikr_comments[]" placeholder="Enter Comments Here"><?php echo $row_kratings['ikr_comments'] ?></textarea>
														
													</td>
												</tr>
												<?php } ?>
												<tr>
													<th><strong>TOTAL</strong></th>
													<th><span id="ratings_total">0</span></th>
													<th></th>
												</tr>
											</table>
											</div>

											<div class="control-group">
												<label class="control-label">General Comments</label>
												<div class="controls span6">
													<textarea name="ir_generalComments" class="m-wrap span12" required><?php echo $row_record['ir_generalComments'] ?></textarea>
												</div>
											</div>

											<div class="control-group">
												<label class="control-label">Observed Strengths</label>
												<div class="controls span6">
													<textarea name="ir_strengths" class="m-wrap span12" required><?php echo $row_record['ir_strengths'] ?></textarea>
												</div>
											</div>

											<div class="control-group">
												<label class="control-label">Observed Weaknesses</label>
												<div class="controls span6">
													<textarea name="ir_weaknesses" class="m-wrap span12" required><?php echo $row_record['ir_weaknesses'] ?></textarea>
												</div>
											</div>

											<div class="control-group">
												<label class="control-label">IN YOUR OPINION, STATE 2 OTHER AREAS IN THIS ORGANIZATION THAT THE CANDIDATE CAN FIT INTO</label>
												<div class="controls span6">
													<textarea name="ir_otherArea1" class="m-wrap" required placeholder="1"><?php echo $row_record['ir_otherArea1'] ?></textarea>
												</div>
												<br>
												<div class="controls span6">
													<textarea name="ir_otherArea2" class="m-wrap" required placeholder="2"><?php echo $row_record['ir_otherArea2'] ?></textarea>
												</div>
											</div>

											<div class="control-group">
												<label class="control-label">Current Salary</label>
												<div class="controls">
													<input type="text" class="m-wrap" name="ir_currentSalary" value="<?php echo $row_record['ir_currentSalary'] ?>" required>
												</div>
											</div>

											<div class="control-group">
												<label class="control-label">Expected Salary</label>
												<div class="controls">
													<input type="text" class="m-wrap" name="ir_expectedSalary" value="<?php echo $row_record['ir_expectedSalary'] ?>" required>
												</div>
											</div>

											<div class="control-group">
												<label class="control-label">Approved Salary</label>
												<div class="controls">
													<input type="text" class="m-wrap" name="ir_approvedSalary" value="<?php echo $row_record['ir_approvedSalary'] ?>" required>
												</div>
											</div>

											<div class="control-group">
												<label class="control-label">Interviewer's Name</label>
												<div class="controls">
													<input type="text" class="m-wrap span6" name="ir_interviewerName" value="<?php echo $row_record['ir_interviewerName'] ?>" required>
												</div>
											</div>

											<div class="control-group">
												<label class="control-label">Interviewer's Department</label>
												<div class="controls">
													<input type="text" class="m-wrap span6" name="ir_interviewerDept" value="<?php echo $row_record['ir_interviewerDept'] ?>" required>
												</div>
											</div>
											

			                                <div class="control-group">
			                                    <label class="control-label">Status</label>
			                                    <div class="controls">
			                                    	<select name="ir_status" class="m-wrap span6" id = "ir_status" required>
			                                    		<option value="NOT-RECOMMENDED">NOT-RECOMMENDED</option>
			                                    		<option value="RECOMMENDED">RECOMMENDED</option>
			                                    		<option value="KIV">KIV</option>
		                                            </select>
	                                            </div>
		                                    </div>

		                                    <div class="form-actions">
			                                    <button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>

			                                    <a href="int-applicants-list.php?id=<?php echo $row_record['ir_interview_id'] ?>">
			                                        <button type="button" class="btn">&laquo; Back to Interview's Applicant List</button>
		                                        </a>

			                                    <a href="int-list.php">
			                                        <button type="button" class="btn">&laquo; Back to Interview List</button>
		                                        </a>

		                                        <?php if($row_record['ir_date_conducted']) { ?>
		                                        <a href="int-record-details.php?id=<?php echo $_GET['id'] ?>">
			                                        <button type="button" class="btn green">&laquo; Back to Details</button>
		                                        </a>
		                                        <?php } ?>


			                                    <input name="ir_id" type="hidden" id="ir_id" value="<?php echo $row_record['ir_id'];?>">
			                                </div>
			                                <input type="hidden" name="MM_update" value="formCreate">
		                                    <input type="hidden" name="MM_insert" value="formIntervieW">
			                            </form>
			                            <!-- END FORM-->
		                            </div>
</div>
</div>
                            <!-- END SAMPLE FORM PORTLET-->
                        </div>
</div>
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>

		    </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include '-inc-footer.php';?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>
	<![endif]-->
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
<script>
		$(document).ready(function(e) {

		   var getRatingsTotal = function() {
		   		var ratings_total = 0;
		   		$(".rating_item").each(function() {
		   			ratings_total += parseInt($(this).val());
		   		});
		   		return ratings_total;
		   }

		   $("#ir_status").val('<?php echo $row_record['ir_status']?>');

		   //initialize rating total
		   $("#ratings_total").html(getRatingsTotal());


		   //calculate rating total
		   $(".rating_item").change(function() {
		   		$("#ratings_total").html(getRatingsTotal());
		   });
		   
        });
	</script>
	<script>
jQuery(document).ready(function() {
		   App.init(); // initlayout and core plugins
		   FormComponents.init();

		   $(".form_datetime").datetimepicker({
	            isRTL: App.isRTL(),
	            format: "dd-mm-yyyy hh:ii",
	            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
        	});

		});
    </script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>