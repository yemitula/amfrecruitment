<?php 

//function AddFile
// This function uploads a file and prepares the filename for database input on a Add/Insert page
// Written by Yemi Tula using class.upload.php by Colin Verot http://verot.net

function AddFile($field_name, $file_type, $description, $maxFileSize, $location, $required = false, $resizeImage = false, $imageWidth = '1024', $imageHeight = '768') {
	global $editFormAction;
	
	$currentFilename = basename($_SERVER['PHP_SELF']);
	//upload the file
	$file_name = new upload($_FILES[$field_name]);
	//file uploaded?	
	if($file_name->uploaded) {
		//is file extension check necessary?
		if($file_type != '*') {
		//is file extension compliant?
		switch($file_type) {
			case 'image': 
				$exts = array("jpg","jpeg","png");
				$narrative = "JPEG and PNG formats";
			break;
			case 'doc': 
				$exts = array("doc","docx","pdf"); 
				$narrative = "WORD and PDF formats";
			break;
		}
		$ext = strtolower($file_name->file_src_name_ext);
		if(!in_array($ext, $exts)) {
			//extension non-compliant, display error
			header("Location: $editFormAction&error=".urlencode("Your $description is NOT in a valid Format! Only $narrative allowed"));
			exit;
		}
		}
		//is file size compliant?
		if($file_name->file_src_size > ($maxFileSize*1024)) {
			//file size non-compliant, display error
			header("Location: $editFormAction&error=".urlencode("Maximum file size is $maxFileSize KB!"));
			exit;
		}
		//echo("<br>No errors! Ready to store file"); die;
		
		//resize image
		if($resizeImage) {
			$file_name->image_resize = $resizeImage;
			$file_name->image_x = $imageWidth;
			$file_name->image_y = $imageHeight;
			$file_name->image_ratio = true;
			$file_name->image_ratio_fill = true;
		}
				
		$file_name->file_new_name_body = time().'_'.mt_rand(11111,99999);
		$uploadComplete = $file_name->Process($location);
		if($file_name->processed) {
			//echo "<br>File successfully uploaded"; die;
		} else {
			echo "<br>File didn't upload! Error From Class: ".$file_name->file_src_error.' : '.$file_name->error; die;
		}
		return $file_name->file_dst_name;
		
	} else {
		
		if ($required) {
			//echo "<br>No file_name attached<br>"; die;
			header("Location: $editFormAction&error=".urlencode("Please attach your $description!"));
			exit;
		}
		
		return false;
	}
	
}


//function EditFile
// This function uploads a file and prepares the filename for database input on an Edit Page
// Written by Yemi Tula

function EditFile($field_name, $file_type, $description, $maxFileSize, $location, $oldfile, $resizeImage = false, $imageWidth = '1024', $imageHeight = '768') {
	global $editFormAction;

	$currentFilename = basename($_SERVER['PHP_SELF']);
	//upload the file
	$file_name = new upload($_FILES[$field_name]);
	//file uploaded?	
	if($file_name->uploaded) {
		//is file extension check necessary?
		if($file_type != '*') {
		//is file extension compliant?
		switch($file_type) {
			case 'image': 
				$exts = array("jpg","jpeg","png");
				$narrative = "JPEG and PNG formats";
			break;
			case 'doc': 
				$exts = array("doc","docx","pdf"); 
				$narrative = "WORD and PDF formats";
			break;
		}
		$ext = strtolower($file_name->file_src_name_ext);
		if(!in_array($ext, $exts)) {
			//extension non-compliant, display error
			header("Location: $editFormAction&error=".urlencode("Your $description is NOT in a valid Format! Only $narrative allowed"));
			exit;
		}
		}
		//is file size compliant?
		if($file_name->file_src_size > ($maxFileSize*1024)) {
			//file size non-compliant, display error
			header("Location: $editFormAction&error=".urlencode("Maximum file size is $maxFileSize KB!"));
			exit;
		}
		//echo("<br>No errors! Ready to store file"); die;
		
		//resize image
		if($resizeImage) {
			$file_name->image_resize = $resizeImage;
			$file_name->image_x = $imageWidth;
			$file_name->image_y = $imageHeight;
			$file_name->image_ratio = true;
			$file_name->image_ratio_fill = true;
		}

		//upload new file		
		$file_name->file_new_name_body = time().'_'.mt_rand(11111,99999);
		$uploadComplete = $file_name->Process($location);
		if($file_name->processed) {
			//echo "<br>File successfully uploaded"; die;
		} else {
			echo "<br>File didn't upload! Error From Class:".$file_name->file_src_error; die;
		}
		
		//delete old file
		if(isset($oldfile) && file_exists($location.$oldfile)) {
			unlink($location.$oldfile);
		}
		
		return $file_name->file_dst_name;
		
	} else {		
		return $oldfile;
	}
	
}

//function EditFile
// This function uploads a file and prepares the filename for database input on an Edit Page
// Written by Yemi Tula

function DeleteFile($filetodelete, $location) {
	if(isset($filetodelete) && file_exists($location.$filetodelete)) {
		unlink($location.$filetodelete);
	}	
}
?>