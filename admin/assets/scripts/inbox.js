var Inbox = function () {

    var initTags = function (input) {
        input.tag({
            autosizedInput: true,
            containerClasses: 'span12',
            inputClasses: 'm-wrap',
            source: function (query, process) {
                return [
                    'Bob Nilson <bob.nilson@metronic.com>',
                    'Lisa Miller <lisa.miller@metronic.com>',
                    'Test <test@domain.com>',
                    'Dino <dino@demo.com>',
					'Yemi Tula <yemitula@gmail.com>',
                    'Support <support@demo.com>']
            }
        });
    }

    var initWysihtml5 = function () {
        $('.inbox-wysihtml5').wysihtml5({
            "stylesheets": ["../assets/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
        });
    }

}();