<?php require_once("_inc_checkSession.php"); ?>
<?php include('../_inc_Functions.php'); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if(isset($_POST['shortlistedApps'])) {
	//print_r($_POST['shortlistedApps']);
	//do shortlisting
	mysql_select_db($database_fer, $fer);
	$appIDs = implode(',', $_POST['shortlistedApps']);
	//die($appIDs);
	$vID = $_GET['id'];
	$updateSQL = "UPDATE applications SET shortlisted= '1', status = 'Shortlisted' WHERE vacancy_id='$vID' AND applicant_id IN($appIDs)";
	//die($updateSQL);
	$updateRS = mysql_query($updateSQL, $fer) or die(mysql_error());
	
	header("Location: vacancy-applicants.php?id=$vID&msg=Shortlist updated!");
	exit;
}

$colname_vacancy = "-1";
if (isset($_GET['id'])) {
  $colname_vacancy = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_vacancy = sprintf("SELECT * FROM vacancies WHERE id = %s", GetSQLValueString($colname_vacancy, "int"));
$vacancy = mysql_query($query_vacancy, $fer) or die(mysql_error());
$row_vacancy = mysql_fetch_assoc($vacancy);
$totalRows_vacancy = mysql_num_rows($vacancy);
$_SESSION['Vacancy-Title'] = $row_vacancy['title'];

$maxRows_applications = 30;
$pageNum_applications = 0;
if (isset($_GET['pageNum_applications'])) {
  $pageNum_applications = $_GET['pageNum_applications'];
}
$startRow_applications = $pageNum_applications * $maxRows_applications;

$colname_applications = "-1";
if (isset($_GET['id'])) {
  $colname_applications = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_applications = sprintf("SELECT *, a.id AS application_id FROM applications a LEFT JOIN applicants b ON (a.applicant_id = b.id) WHERE `vacancy_id` = %s", GetSQLValueString($colname_applications, "int"));
if (isset($_GET['internal']) and $_GET['internal'] == 'yes') {
	$query_applications .= " AND internal_candidate IS NOT NULL";
}
if (isset($_GET['external']) and $_GET['external'] == 'yes') {
	$query_applications .= " AND internal_candidate IS NULL";
}
$query_applications .= " ORDER BY b.surname";
//put query in a session variable for later reference
$_SESSION['VacancyApplicationsQuery'] = $query_applications;

$query_limit_applications = sprintf("%s LIMIT %d, %d", $query_applications, $startRow_applications, $maxRows_applications);
$applications = mysql_query($query_limit_applications, $fer) or die(mysql_error());
$row_applications = mysql_fetch_assoc($applications);

if (isset($_GET['totalRows_applications'])) {
  $totalRows_applications = $_GET['totalRows_applications'];
} else {
  $all_applications = mysql_query($query_applications);
  $totalRows_applications = mysql_num_rows($all_applications);
}
$totalPages_applications = ceil($totalRows_applications/$maxRows_applications)-1;

$queryString_applications = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_applications") == false && 
        stristr($param, "totalRows_applications") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_applications = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_applications = sprintf("&totalRows_applications=%d%s", $totalRows_applications, $queryString_applications);

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Vacancy Applicants | <?php echo $config['shortname'] ?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link href="assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('-inc-top.php'); ?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<?php include('-inc-navbar-side.php'); ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					Widget settings form goes here
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Vacancy Applicants <small>browse  applicants that applied for a vacancy</small></h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="vacancies.php">Vacancies</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#"> Vacancy Applicants</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) { ?>
			    <div class="alert alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg'] ?></strong> </div>
			    <?php } ?>
			    <?php if ($totalRows_applications == 0) { // Show if recordset empty ?>
			    <div class="row-fluid">
			        <div class="alert">
			            
			            <strong>Empty List!</strong> No applicants found. </div>
		        </div>
			    <?php } // Show if recordset empty ?>
                <?php if ($totalRows_applications > 0) { // Show if recordset not empty ?>    
                <!--<a href="e_vacancyShortlist.php?id=<?php echo $row_vacancy['id']; ?>">
                    </a>-->
                <a href="vacancy-applicants-filter.php?id=<?php echo $row_vacancy['id']; ?>" target="_blank">
                <button name="viewShortlist" type="button" class="btn green" id="viewShortlist" value="">
                <i class="icon-filter"></i> Filter Applicants</button> </a>
                     
                <?php if ((isset($_GET['internal']) && $_GET['internal']=='yes') || (isset($_GET['external']) && $_GET['external']=='yes')) { ?>
                <a href="vacancy-applicants.php?id=<?php echo $row_vacancy['id']; ?>">
                   	<button type="button" class="btn green">All Applicants</button>
                </a>
                <?php } else { ?>
                <a href="vacancy-applicants.php?id=<?php echo $row_vacancy['id']; ?>&internal=yes">
                   	<button type="button" class="btn green">Internal Applicants Only</button>
                </a>
                <a href="vacancy-applicants.php?id=<?php echo $row_vacancy['id']; ?>&external=yes">
                   	<button type="button" class="btn green">External Applicants Only</button>
                </a>
                <?php } ?>
                <a href="vacancy-applicants-export.php?id=<?php echo $row_vacancy['id']; ?>">
                <button type="button" class="btn green">Export Applicants</button>
                </a>
                <p>&nbsp;</p>
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN EXAMPLE TABLE PORTLET-->
			            <div class="portlet box light-grey">
			                <div class="portlet-title">
			                    <div class="caption"><?php if (isset($_GET['internal']) and $_GET['internal']=='yes') echo "Internal " ?><?php if (isset($_GET['external']) and $_GET['external']=='yes') echo "External " ?>Applicants for Vacancy:</strong> <?php echo $row_vacancy['title']; ?></div>
		                    </div>
			                <div class="portlet-body">
			                    <form id="formShortlist" name="formShortlist" method="post" action="">
			                    <div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid"> <br>
			                        
                                    
			                        <div class="row-fluid">
			                            <div class="span6"></div>
			                            <div class="span6">
			                                <!--<div class="dataTables_filter" id="sample_1_filter">
                  <label>Search: <input type="text" aria-controls="sample_1" class="m-wrap medium"></label>
                  </div>-->
		                                </div>
		                            </div>
                                    
			                        <table class="table table-striped table-bordered table-hover" id="sample_1">
			                            <thead>
			                                <tr>
			                                    <th width="5%"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
                                                <th width="5%">ID</th>
                                                <th width="30%">Name</th>
                                                <th width="10%">Internal?</th>
                                                <th width="15%">Salary Range (N)</th>
			                                    <th width="10%" >Date Applied</th>
			                                    <th width="15%" >Status</th>
			                                    <th width="10%" >&nbsp;</th>
		                                    </tr>
		                                </thead>
			                            <tbody>
			                                <?php do { ?>
			                                <tr class="odd gradeX">
			                                    <td><input type="checkbox" name="shortlistedApps[]" id="<?php echo $row_applications['applicant_id']; ?>" value="<?php echo $row_applications['applicant_id']; ?>" <?php if ($row_applications['shortlisted']) echo "checked='checked'" ?> class="checkboxes" /></td>
                                                <td><?php echo $row_applications['applicant_id']; ?></td>
			                                    <td ><a href="applicant-details.php?id=<?php echo $row_applications['applicant_id']; ?>" target="_blank"><?php echo $row_applications['surname']; ?> <?php echo $row_applications['firstname']; ?></a></td>
			                                    <td><?php echo ($row_applications['internal_candidate'])? 'Yes':'No' ?></td>
			                                    <td><?php echo number_format($row_applications['min_salary'],0,'.',',') ?> - <?php echo number_format($row_applications['max_salary'],0,'.',',') ?></td>
			                                    <td ><?php echo date("d-M-Y", strtotime($row_applications['dateApplied'])); ?></td>
			                                    <td ><?php echo $row_applications['status']; ?></td>
			                                    <td >
			                                        <a href="applicant-details.php?id=<?php echo $row_applications['applicant_id']; ?>" target="_blank" title="View Applicant Details" class="tooltips"><i class="icon-eye-open"></i></a>&nbsp;&nbsp;
		                                        <a title="Delete Application" class="ValidateAction tooltips" data-placement="top" data-confirm-msg="Are you sure you want to Delete!" href="application-delete.php?v=<?php echo $row_vacancy['id']; ?>&a=<?php echo $row_applications['applicant_id']; ?>"><i class="icon-remove"></i></a></td>
		                                    </tr>
			                                <?php } while ($row_applications = mysql_fetch_assoc($applications)); ?>
		                                </tbody>
		                            </table>
			                        <div class="row-fluid">
			                            <div class="span6">
			                                <div class="dataTables_info" id="sample_1_info">Showing <?php echo ($startRow_applications + 1) ?> to <?php echo min($startRow_applications + $maxRows_applications, $totalRows_applications) ?> of <?php echo $totalRows_applications ?> entries</div>
		                                </div>
			                            <div class="span6">
			                                <div class="dataTables_paginate paging_bootstrap pagination">
			                                    <ul>
			                                        <?php if ($pageNum_applications > 0) { // Show if not first page ?>
			                                        <li class="prev"><a href="<?php printf("%s?pageNum_applications=%d%s", $thispage, 0, $queryString_applications); ?>" title="First Page">&laquo; </a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_applications > 0) { // Show if not first page ?>
			                                        <li class="prev"><a href="<?php printf("%s?pageNum_applications=%d%s", $thispage, max(0, $pageNum_applications - 1), $queryString_applications); ?>" title="Previous Page">&lsaquo;</a></li>
			                                        <?php } ?>
			                                        <?php $thisPageNumber = $pageNum_applications+1;
                                                //determine startPage and endPage
                                                $totalPages = $totalPages_applications +1;
                                                if($totalPages < 4) {
                                                    $startPage = 1;
                                                    $endPage = $totalPages;
                                                } elseif($thisPageNumber <= 4) {
                                                    $startPage = 1;
                                                    $endPage = 4;
                                                } else {
                                                    $startPage = $thisPageNumber - 3;
                                                    $endPage = $thisPageNumber;
                                                }
                                              ?>
			                                        <?php for ($i = $startPage; $i<=$endPage; $i++) { ?>
			                                        <li class="<?php if ($i == $thisPageNumber) echo "active"; ?>"><a href="<?php echo $thispage ?>?pageNum_applications=<?php echo $i-1 ?>&amp;totalRows_applications=<?php echo $totalRows_applications ?>" title="Page <?php echo $i ?>"><?php echo $i ?></a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_applications < $totalPages_applications) { // Show if not last page ?>
			                                        <li class="next"><a href="<?php printf("%s?pageNum_applications=%d%s", $thispage, min($totalPages_applications, $pageNum_applications + 1), $queryString_applications); ?>" title="Next Page">&rsaquo; </a></li>
			                                        <?php } ?>
			                                        <?php if ($pageNum_applications < $totalPages_applications) { // Show if not last page ?>
			                                        <li class="next"><a href="<?php printf("%s?pageNum_applications=%d%s", $thispage, $totalPages_applications, $queryString_applications); ?>" title="Last Page">&raquo; </a></li>
			                                        <?php } ?>
		                                        </ul>
		                                    </div>
		                                </div>
		                            </div>
                                    
		                        </div>
                                
                                <p>
								 <input name="shortlist" type="submit" class="btn green" id="shortlist" value="Add Selected to Shortlist" />
                                 <a class="btn black" href="vacancy-shortlist.php?id=<?php echo $row_vacancy['id']; ?>"><i class="icon-eye-open m-icon-white"></i> View Shortlist</a>
                                </p>
                                </form>
		                    </div>
		                </div>
			            <!-- END EXAMPLE TABLE PORTLET-->
		            </div>
		        </div>
			    <?php } // Show if applicant list not empty ?>
			    <div class="clearfix"></div>
		    </div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include('-inc-footer.php'); ?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>   
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  
	<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
	<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>     
	<!-- END PAGE LEVEL SCRIPTS -->  
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
    <script>
		$(document).ready(function(e) {
		   $("#company").val('<?php echo $_GET['company'] ?>');
		   $("#status").val('<?php echo $_GET['status'] ?>');
		   $("#fromDate").val('<?php echo $_GET['fromDate'] ?>');
		   $("#toDate").val('<?php echo $_GET['toDate'] ?>');
        });
	</script>
	<script>
		jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   FormComponents.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<?php
mysql_free_result($companies);

mysql_free_result($sites);
?>
