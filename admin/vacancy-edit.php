<?php require_once "_inc_checkSession.php";?>
<?php $thisPage = basename($_SERVER['PHP_SELF']);?>
<?php require_once '../_inc_config.php';?>
<?php require_once '../Connections/fer.php';?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$vacancy_id = $_GET['id'];
$query_vacancy = sprintf("SELECT * FROM vacancies WHERE id = %s", GetSQLValueString($vacancy_id, "int"));
$vacancy = mysql_query($query_vacancy) or die(mysql_error());
$row_vacancy = mysql_fetch_assoc($vacancy);

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "formPersonalDetails") && !empty($_GET["id"])) {
	$updateSQL = sprintf("UPDATE vacancies SET title=%s, employer_id=%s, summary=%s, longDescription=%s, techReqs=%s, eduQual=%s, behavReqs=%s, jobCategory_id=%s, jobType=%s, staffLevel=%s, job_loc=%s, dateOpening=%s, dateClosing=%s, status=%s WHERE id=%s",
		GetSQLValueString($_POST['title'], "text"),
		GetSQLValueString($_POST['employer_id'], "int"),
		GetSQLValueString($_POST['summary'], "text"),
		GetSQLValueString($_POST['longDescription'], "text"),
		GetSQLValueString($_POST['techReqs'], "text"),
		GetSQLValueString($_POST['eduQual'], "text"),
		GetSQLValueString($_POST['behavReqs'], "text"),
		GetSQLValueString($_POST['jobCategory_id'], "int"),
		GetSQLValueString($_POST['jobType'], "text"),
		GetSQLValueString($_POST["staffLevel"], "text"),
		GetSQLValueString($_POST['job_loc'], "text"),
		GetSQLValueString($_POST['dateOpening'], "date"),
		GetSQLValueString($_POST['dateClosing'], "date"),
		GetSQLValueString($_POST['status'], "text"),
		GetSQLValueString($vacancy_id, "int"));

	//die($_POST['employer_id']);
	//die($vacancy_id);
	mysql_select_db($database_fer, $fer);
	$Result1 = mysql_query($updateSQL, $fer) or die(mysql_error());
	//$updateGoTo = "vacancies-edit.php?id=" . mysql_insert_id() . "&msg=";

	$updateGoTo = "vacancy-edit.php?id=".$_GET["id"]."&msg=".urlencode("Vacancy Successfully Updated!");
	if (isset($_SERVER['QUERY_STRING'])) {
		$updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
		$updateGoTo .= $_SERVER['QUERY_STRING'];
	}
	header(sprintf("Location: %s", $updateGoTo));
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_vacancies = 20;
$pageNum_vacancies = 0;
if (isset($_GET['pageNum_vacancies'])) {
	$pageNum_vacancies = $_GET['pageNum_vacancies'];
}
$startRow_vacancies = $pageNum_vacancies * $maxRows_vacancies;

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "formPersonalDetails") && empty($_GET["id"])) {
	$insertSQL = sprintf("INSERT INTO vacancies (title, employer_id, summary, longDescription, techReqs, eduQual, behavReqs, jobCategory_id, jobType, staffLevel, job_loc, datePosted, dateOpening, dateClosing, status) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
		GetSQLValueString($_POST['title'], "text"),
		GetSQLValueString($_POST['employer_id'], "int"),
		GetSQLValueString($_POST['summary'], "text"),
		GetSQLValueString($_POST['longDescription'], "text"),
		GetSQLValueString($_POST['techReqs'], "text"),
		GetSQLValueString($_POST['eduQual'], "text"),
		GetSQLValueString($_POST['behavReqs'], "text"),
		GetSQLValueString($_POST['jobCategory_id'], "int"),
		GetSQLValueString($_POST['jobType'], "text"),
		GetSQLValueString($_POST["staffLevel"], "text"),
		GetSQLValueString($_POST['job_loc'], "text"),
		GetSQLValueString($_POST['datePosted'], "date"),
		GetSQLValueString($_POST['dateOpening'], "date"),
		GetSQLValueString($_POST['dateClosing'], "date"),
		GetSQLValueString(strtotime('today') >= strtotime($_POST['dateOpening']) ? 'open' : 'closed', "text"));

	mysql_select_db($database_fer, $fer);
	$Result1 = mysql_query($insertSQL, $fer) or die(mysql_error());
	//$updateGoTo = "vacancies-edit.php?id=".mysql_insert_id()."&msg=".urlencode("Vacancy Successfully Updated!");
	$insertGoTo = "vacancy-edit.php?id=".mysql_insert_id()."&msg=".urlencode("Vacancy Successfully Added!");
	if (isset($_SERVER['QUERY_STRING'])) {
		$insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
		$insertGoTo .= $_SERVER['QUERY_STRING'];
	}
	header(sprintf("Location: %s", $insertGoTo));
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_vacancies = 20;
$pageNum_vacancies = 0;
if (isset($_GET['pageNum_vacancies'])) {
	$pageNum_vacancies = $_GET['pageNum_vacancies'];
}
$startRow_vacancies = $pageNum_vacancies * $maxRows_vacancies;

mysql_select_db($database_fer, $fer);
$query_employers = "SELECT * FROM employers ORDER BY companyName ASC";
$employers = mysql_query($query_employers, $fer) or die(mysql_error());
$row_employers = mysql_fetch_assoc($employers);
$totalRows_employers = mysql_num_rows($employers);

mysql_select_db($database_fer, $fer);
$query_categories = "SELECT * FROM job_categories ORDER BY cat_name ASC";
$categories = mysql_query($query_categories, $fer) or die(mysql_error());
$row_categories = mysql_fetch_assoc($categories);
$totalRows_categories = mysql_num_rows($categories);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Vacancies | <?php echo $config['shortname']?> Recruitment Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.png" />
	<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
	<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
	<link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include '-inc-top.php';?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<?php include '-inc-navbar-side.php';?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					Widget settings form goes here
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
			    <!-- BEGIN PAGE HEADER-->
			    <div class="row-fluid">
			        <div class="span12">
			            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
			            <h3 class="page-title"> Vacancies <small>edit vacancy </small> </h3>
			            <ul class="breadcrumb">
			                <li> <i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
			                <li><a href="vacancies.php">Vacancies</a> <i class="icon-angle-right"></i></li>
			                <li><a href="#"> Edit Vacancy</a></li>
		                </ul>
			            <!-- END PAGE TITLE & BREADCRUMB-->
		            </div>
		        </div>
			    <!-- END PAGE HEADER-->
			    <?php if (isset($_GET['msg'])) {?>
			    <div class="alert  alert-success">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['msg']?></strong> </div>
			    <?php }
?>
			    <?php if (isset($_GET['error'])) {?>
			    <div class="alert alert-error">
			        <button class="close" data-dismiss="alert"></button>
			        <strong><?php echo $_GET['error']?></strong> </div>
			    <?php }
?>
			    <div id="dashboard">
			        <!-- BEGIN DASHBOARD STATS -->
			        <div class="row-fluid">
			            <div class="span12">
			                <!-- BEGIN SAMPLE FORM PORTLET-->
			                <div class="portlet box green tabbable">
			                    <div class="portlet-title">
			                        <div class="caption"> <i class="icon-reorder"></i> <span class="hidden-480">Vacancy Details</span> </div>
		                        </div>
			                    <div class="portlet-body form">
			                        <div class="tabbable portlet-tabs">
			                            <!-- BEGIN FORM-->
			                            <form method="POST" name="formCreate" action="<?php echo $editFormAction;?>" class="form-horizontal" id="formCreate">
			                                <p>&nbsp;</p>
			                                <div class="control-group">
			                                    <label class="control-label">Company</label>
			                                    <div class="controls">
			                                        <label for="site_id"></label>
			                                        <span id="spryselect1">
			                                        <select name="employer_id" class="m-wrap span6" id = "employer_id">
                                              <option value=""> -- Select a Company -- </option>
                                              <?php
do {
	?>
                                              <option value="<?php echo $row_employers['id']?>"><?php echo $row_employers['companyName']?></option>
                                              <?php
} while ($row_employers = mysql_fetch_assoc($employers));
$rows = mysql_num_rows($employers);
if ($rows > 0) {
	mysql_data_seek($employers, 0);
	$row_employers = mysql_fetch_assoc($employers);
}
?>
                                            </select>
			                                        <span class="selectRequiredMsg">Please select an item.</span></span><span class="help-inline"></span></div>
		                                    </div>
			                                <div class="control-group">
			                                    <label class="control-label">Title</label>
			                                    <div class="controls"><span id="sprytextfield1">
			                                        <input name="title" type="text" class="m-wrap span6" id="title" placeholder="" value="<?php echo $row_vacancy['title']?>" />
		                                        <span class="textfieldRequiredMsg">A value is required.</span></span></div>
    </div>
			                                <div class="control-group">
			                                    <label class="control-label">Summary</label>
			                                    <div class="controls"><span id="sprytextarea1">
			                                        <textarea name="summary" id="summary" class="span6 m-wrap" rows="4"><?php echo $row_vacancy['summary']?></textarea>
		                                        <span class="textareaRequiredMsg">A value is required.</span></span></div>
</div>

			                                <div class="control-group">
			                                    <label class="control-label">Full Description</label>
			                                    <div class="controls"><span id="sprytextarea2">
			                                        <textarea name="longDescription" id="longDescription" class="span9 ckeditor m-wrap" rows="6"><?php echo $row_vacancy['longDescription']?></textarea>
		                                        <span class="textareaRequiredMsg">A value is required.</span></span></div>
</div>

			                                <div class="control-group">
			                                    <label class="control-label">Competencies</label>
			                                    <div class="controls"><span id="sprytextarea3">
			                                        <textarea name="techReqs" id="techReqs" class="span9 ckeditor m-wrap" rows="6"><?php echo $row_vacancy['techReqs']?></textarea>
		                                        <span class="textareaRequiredMsg">A value is required.</span></span></div>
</div>

			                                <div class="control-group">
			                                    <label class="control-label">Educational Requirements</label>
			                                    <div class="controls"><span id="sprytextarea4">
			                                        <textarea name="eduQual" id="eduQual" class="span9 ckeditor m-wrap" rows="6"><?php echo $row_vacancy['eduQual']?></textarea>
		                                        <span class="textareaRequiredMsg">A value is required.</span></span></div>
											</div>

                                            <div class="control-group">
			                                    <label class="control-label">Personal Attributes</label>
			                                    <div class="controls"><span id="sprytextarea4">
			                                        <textarea name="behavReqs" id="behavReqs" class="span9 ckeditor m-wrap" rows="6"><?php echo $row_vacancy['behavReqs']?></textarea>
		                                        <span class="textareaRequiredMsg">A value is required.</span></span></div>
											</div>


			                                <div class="control-group">
			                                    <label class="control-label">Job Category</label>
			                                    <div class="controls"><span id="spryselect2">
			                                        <select name="jobCategory_id" id="jobCategory_id" class="m-wrap span6">
			                                            <option value="">-- Select a Category --</option>
			                                            <option value="0">General</option>
			                                            <?php
do {
	?>
			                                            <option value="<?php echo $row_categories['id_cat']?>"><?php echo $row_categories['cat_name']?></option>
			                                            <?php
} while ($row_categories = mysql_fetch_assoc($categories));
$rows = mysql_num_rows($categories);
if ($rows > 0) {
	mysql_data_seek($categories, 0);
	$row_categories = mysql_fetch_assoc($categories);
}
?>
	                                            </select>
		                                        <span class="selectRequiredMsg">Please select an item.</span></span></div>
</div>


			                                <div class="control-group">
			                                    <label class="control-label">Job Type</label>
			                                    <div class="controls"><span id="spryselect3">
			                                        <select name="jobType" class="width244" id="jobType">
			                                            <option value=""> -- Select a Type -- </option>
			                                            <option value="Full Time">Full Time</option>
			                                            <option value="Part Time">Part Time</option>
	                                            </select>
		                                        <span class="selectRequiredMsg">Please select an item.</span></span></div>
											</div>

			                                <div class="control-group">
			                                    <label class="control-label">Job Level</label>
			                                    <div class="controls"><span id="spryselect4">
			                                        <select name="staffLevel" class="width244" id="staffLevel">
			                                            <option value=""> -- Select a Level -- </option>
			                                            <option value="Managerial">Managerial</option>
			                                            <option value="Non-Managerial">Non-Managerial</option>
	                                            </select>
		                                        <span class="selectRequiredMsg">Please select an item.</span></span></div>
											</div>

                                            <div class="control-group">
			                                    <label class="control-label">Job Location</label>
			                                    <div class="controls">
			                                        <input name="job_loc" type="text" class="m-wrap span6" id="job_loc" placeholder="" value="<?php echo $row_vacancy['job_loc']?>" />
			                                        </div>
											</div>


                                            <div class="control-group">
			                                    <label class="control-label">Ongoing ?</label>
			                                    <div class="controls">
			                                        <input name="ongoing" type="checkbox" class="m-wrap span6" id="ongoing" <?php if ($row_vacancy["dateOpening"] == "") {
	echo "checked";
}
?> />
			                                        </div>
											</div>
                                            <span id="dates">
                                            <div class="control-group">
			                                    <label class="control-label">Opening Date</label>
			                                    <div class="controls"><span id="sprytextfield2">
			                                        <input id="dateOpening" name="dateOpening" class="m-wrap m-ctrl-medium date-picker" readonly size="16" type="text" value="<?php echo $row_vacancy['dateOpening']?>" />
		                                        <span class="textfieldRequiredMsg">A value is required.</span></span></div>
											</div>

                                            <div class="control-group">
			                                    <label class="control-label">Closing Date</label>
			                                    <div class="controls"><span id="sprytextfield3">
			                                        <input id="dateClosing" name="dateClosing" class="m-wrap m-ctrl-medium date-picker" readonly size="16" type="text" value="<?php echo $row_vacancy['dateClosing']?>" />
		                                        <span class="textfieldRequiredMsg">A value is required.</span></span></div>
											</div>
                                            </span>


			                                <div class="form-actions">
			                                    <button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
			                                    <a href="vacancies.php">
			                                        <button type="button" class="btn">Cancel</button>
		                                        </a>
		                                        <?php if ($row_vacancy["published"] == NULL) {?>
		                                        	<a href="vacancy-publish.php?vid=<?php echo $row_vacancy["id"];?>&query=true" ><button type="button" class="btn green">Publish</button></a>
		                                        <?php } else {?>
		                                        	<a href="vacancy-publish.php?vid=<?php echo $row_vacancy["id"];?>&query=unpublish"><button type="button" class="btn red">Unpublish</button></a>
		                                        <?php }
?>
		                                        <?php if (isset($_GET["id"])) {?>
		                                        <a href="../a_vacancyPublishDetails.php?id=<?php echo $row_vacancy["id"]?>" target="_new" ><button type="button" class="btn yellow">Preview</button></a>
			                                    <?php }
?>
			                                    <input name="report_id" type="hidden" id="report_id" value="<?php echo $row_report['report_id'];?>">
			                                </div>
                                            <input name="datePosted" type="hidden" id="datePosted" value="<?php echo date("Y-m-d h:i:s");?>" />
			                                <input type="hidden" name="MM_update" value="formCreate">
		                                    <input type="hidden" name="MM_insert" value="formPersonalDetails">
			                            </form>
			                            <!-- END FORM-->
		                            </div>
</div>
</div>
                            <!-- END SAMPLE FORM PORTLET-->
                        </div>
</div>
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>

		    </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php include '-inc-footer.php';?>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>
	<![endif]-->
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/form-components.js"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
    <!-- Validate Delete Plugin -->
    <script type="text/javascript" src="assets/scripts/validateAction.js"></script>
<script>
		$(document).ready(function(e) {
		   $("#employer_id").val('<?php echo $row_vacancy['employer_id']?>');
		   $("#jobCategory_id").val('<?php echo $row_vacancy['jobCategory_id']?>');
		   $("#staffLevel").val('<?php echo $row_vacancy['staffLevel']?>');
		   $("#jobType").val('<?php echo $row_vacancy['jobType']?>');
		   $("#status").val('<?php echo $row_vacancy['status']?>');
        });
	</script>
	<script>
jQuery(document).ready(function() {
		   App.init(); // initlayout and core plugins
		   FormComponents.init();
		});
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1");
var sprytextarea2 = new Spry.Widget.ValidationTextarea("sprytextarea2");
var sprytextarea3 = new Spry.Widget.ValidationTextarea("sprytextarea3");
var sprytextarea4 = new Spry.Widget.ValidationTextarea("sprytextarea4");
var spryselect2 = new Spry.Widget.ValidationSelect("spryselect2");
var spryselect3 = new Spry.Widget.ValidationSelect("spryselect3");
var spryselect4 = new Spry.Widget.ValidationSelect("spryselect4");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");

$(document).ready(function() {

//on edit page, set values based on check!
	$('#ongoing').change(function() {
	if ($('#ongoing').is(':checked')) {
		$("#dates").hide();
		$("#dateOpening").val('');
		$("#dateClosing").val('');
	} else {
		$("#dates").show();
	}
});

	//on edit page, set values based on check!
	if ($('#ongoing').is(':checked')) {
		$("#dates").hide();
		$("#dateOpening").val('');
		$("#dateClosing").val('');
	} else {
		$("#dates").show();
	}

});
    </script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<?php
mysql_free_result($sites);

mysql_free_result($report);
?>
