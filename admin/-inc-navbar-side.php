<?php $thispage = basename($_SERVER['PHP_SELF']) ?>
<ul class="page-sidebar-menu">
				<li>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone"></div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<li class="start <?php if (in_array($thispage, array('index.php', 'change-password.php', 'profile-edit.php'))) echo "active" ?>">
					<a href="index.php">
					<i class="icon-bar-chart"></i> 
					<span class="title">Dashboard</span>
					<span class="selected"></span>
					</a>
				</li>
				
				<li  class="<?php if (in_array($thispage, array('vacancies.php', 'vacancy-add.php', "applicant-interview.php", 'vacancy-edit.php', 'vacancy-details.php', 'vacancy-import.php', 'job-categories.php', 'vacancy-applicants-filter-rs.php', 'vacancy-applicants-filter.php', 'vacancy-applicants.php', 'vacancy-notify-shortlisted.php', 'vacancy-notify-non-shorlisted.php', 'vacancy-shortlist.php'))) echo "active" ?>">
					<a href="javascript:;">
					<i class="icon-home"></i> 
					<span class="title">Vacancies</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li class="<?php if (in_array($thispage, array('vacancy-import.php'))) echo "active" ?>">
							<a href="vacancy-import.php">
							Import Vacancies</a>
						</li>
						<li class="<?php if (in_array($thispage, array('vacancies.php', 'vacancy-details.php', 'vacancy-edit.php'))) echo "active" ?>">
							<a href="vacancies.php">
							Vacancies</a>
                        </li>
                        
                        <li class="<?php if (in_array($thispage, array('job-categories.php', 'job-category-add.php', 'job-category-edit.php'))) echo "active" ?>">
							<a href="job-categories.php">
							Manage Job Categories</a>
                        </li>
					</ul>
				</li>
                
                <li  class="<?php if (in_array($thispage, array('applicants.php', 'applicant-add.php', 'applicant-edit.php', 'applicant-applications.php', 'headhunting.php', 'headhunting-results.php'))) echo "active" ?>">
					<a href="javascript:;">
					<i class="icon-qrcode"></i> 
					<span class="title">Applicants</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						
                        <li class="<?php if (in_array($thispage, array('applicants.php', 'applicant-add.php', 'applicant-edit.php'))) echo "active" ?>">
							<a href="applicants.php">
							Manage Applicants</a>
						</li>
                        <li class="<?php if (in_array($thispage, array('headhunting.php', 'headhunting-results.php'))) echo "active" ?>">
							<a href="headhunting.php">
							Headhunt</a>
						</li>
					</ul>
				</li>
                
				<li class="<?php if (in_array($thispage, array('employers.php', 'employer-add.php', 'employer-edit.php', 'employer-details.php'))) echo "active" ?>">
					<a href="javascript:;">
					<i class="icon-reorder"></i>
                    <span class="title">Companies</span>
                    <span class="arrow "></span>
					</a>
					<ul class="sub-menu">
                        <li class="<?php if (in_array($thispage, array('employer-add.php'))) echo "active" ?>">
							<a href="employer-add.php">
							Create Company</a>
						</li>
                        <li class="<?php if (in_array($thispage, array('employers.php', 'employer-edit.php', 'employer-details.php'))) echo "active" ?>">
							<a href="employers.php">
							Companies</a>
						</li>
					</ul>
				</li>

				<li class="<?php if (in_array($thispage, array('int-list.php', 'int-edit.php', 'int-applicants-list.php', 'int-applicants-add.php','int-record-details.php','int-record-edit.php', 'int-applicants-all.php'))) echo "active" ?>">
					<a href="javascript:;">
					<i class="icon-comments"></i>
                    <span class="title">Interviews</span>
                    <span class="arrow "></span>
					</a>
					<ul class="sub-menu">
                        <li class="<?php if (in_array($thispage, array('int-edit.php'))) echo "active" ?>">
							<a href="int-edit.php">
							Schedule New Interview</a>
						</li>
                        <li class="<?php if (in_array($thispage, array('int-list.php'))) echo "active" ?>">
							<a href="int-list.php">
							List Interviews</a>
						</li>
						<li class="<?php if (in_array($thispage, array('int-applicants-all.php'))) echo "active" ?>">
							<a href="int-applicants-all.php">
							Interviewed Applicants</a>
						</li>
					</ul>
				</li>
                
                <?php if ($FERadmin['type'] == 'superadmin') { ?>
                <li class="last <?php if (in_array($thispage, array('admins.php', 'admin-edit.php', 'admin-add.php'))) echo "active" ?>">
					<a href="javascript:;">
					<i class="icon-user"></i>
                    <span class="title">Admins</span>
                    <span class="arrow "></span>
					</a>
                    <ul class="sub-menu">
						<li class="<?php if (in_array($thispage, array('admin-add.php'))) echo "active" ?>">
							<a href="admin-add.php">
							Create Admin</a>
						</li>
						<li class="<?php if (in_array($thispage, array('admins.php', 'admin-edit.php'))) echo "active" ?>">
							<a href="admins.php">
							Admins</a>
						</li>
					</ul>
				</li>
                <?php } ?>
                
			</ul>