<?php ini_set('max_execution_time', 3600); //execution time in seconds ?>
<?php require_once("_inc_checkSession.php"); ?>
<?php include('../_inc_Functions.php'); ?>
<?php require_once('../_inc_config.php'); ?>
<?php require_once('../Connections/fer.php'); ?>
<?php 
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_applications = "-1";
if (isset($_GET['id'])) {
  $colname_applications = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_applicants = $_SESSION['VacancyApplicationsQuery'];	
$applicants = mysql_query($query_applicants, $fer) or die(mysql_error());
$row_applicants = mysql_fetch_assoc($applicants);



			$line = array();
			$i= 1;

			do {
			// loop for each line
				$line[$i] = $row_applicants;
				$profileLink = "http://amfacilities.com/recruitment1/admin/applicant-details.php?id=".$line[$i]['id'];
				$line[$i]['profile'] = $profileLink;


				$applicant_id = $row_applicants['applicant_id'];
				
				//add other things to the array
				
				//educational
				
				//tertiary
				$query_tertiary = "SELECT institution, course, qualification, class, cgpaScore, cgpaTotal, date FROM tertiary WHERE applicant_id = '$applicant_id' ORDER BY `date` DESC";
				$tertiary = mysql_query($query_tertiary, $fer) or die(mysql_error());
				$row_tertiary = mysql_fetch_assoc($tertiary);
				$totalRows_tertiary = mysql_num_rows($tertiary);
				$length = 5; // nuumber of columns to keep
				$k = 0; //counter
				if ($totalRows_tertiary > 0) {
					do {
						$k++;
						$line[$i]['tertiary-institution-'.$k] = $row_tertiary['institution'];
						$line[$i]['tertiary-course-'.$k] = $row_tertiary['course'];
						$line[$i]['tertiary-qualification-'.$k] = $row_tertiary['qualification'];
						$line[$i]['tertiary-class-'.$k] = $row_tertiary['class'];
						$line[$i]['tertiary-cgpaScore-'.$k] = $row_tertiary['cgpaScore'];
						$line[$i]['tertiary-cgpaTotal-'.$k] = $row_tertiary['cgpaTotal'];
						$line[$i]['tertiary-date-'.$k] = $row_tertiary['date'];
					} while ($row_tertiary = mysql_fetch_assoc($tertiary));
				}
				
				if($k < $length) { //columns not filled, fill up
					for($c=$k+1;$c<=$length;$c++) {
						$line[$i]['tertiary-institution-'.$c] = '';
						$line[$i]['tertiary-course-'.$c] = '';
						$line[$i]['tertiary-qualification-'.$c] = '';
						$line[$i]['tertiary-class-'.$c] = '';
						$line[$i]['tertiary-cgpaScore-'.$c] = '';
						$line[$i]['tertiary-cgpaTotal-'.$c] = '';
						$line[$i]['tertiary-date-'.$c] = '';
					}
				}

				//secondary
				$query_secondary = "SELECT school, certificate, date FROM secschool WHERE applicant_id = '$applicant_id' ORDER BY `date` DESC";
				$secondary = mysql_query($query_secondary, $fer) or die(mysql_error());
				$row_secondary = mysql_fetch_assoc($secondary);
				$totalRows_secondary = mysql_num_rows($secondary);
				$length = 3; // nuumber of columns to keep
				$k = 0; //counter
				if ($totalRows_secondary > 0) {
					do {
						$k++;
						$line[$i]['secondary-school-'.$k] = $row_secondary['school'];
						$line[$i]['secondary-certificate-'.$k] = $row_secondary['certificate'];
						$line[$i]['secondary-date-'.$k] = $row_secondary['date'];
					} while ($row_secondary = mysql_fetch_assoc($secondary));
				}
				
				if($k < $length) { //columns not filled, fill up
					for($c=$k+1;$c<=$length;$c++) {
						$line[$i]['secondary-school-'.$c] = '';
						$line[$i]['secondary-certificate-'.$c] = '';
						$line[$i]['secondary-date-'.$c] = '';
					}
				}


				//profcerts
				$query_profcerts = "SELECT certification, year FROM profcerts WHERE applicant_id = '$applicant_id' ORDER BY `year` DESC";
				$profcerts = mysql_query($query_profcerts, $fer) or die(mysql_error());
				$row_profcerts = mysql_fetch_assoc($profcerts);
				$totalRows_profcerts = mysql_num_rows($profcerts);
				$length = 5; // nuumber of columns to keep
				$k = 0; //counter
				if ($totalRows_profcerts > 0) {
					do {
						$k++;
						$line[$i]['profcerts-certification-'.$k] = $row_profcerts['certification'];
						$line[$i]['profcerts-year-'.$k] = $row_profcerts['year'];
					} while ($row_profcerts = mysql_fetch_assoc($profcerts));
				}
				
				if($k < $length) { //columns not filled, fill up
					for($c=$k+1;$c<=$length;$c++) {
						$line[$i]['profcerts-certification-'.$c] = $row_profcerts['certification'];
						$line[$i]['profcerts-year-'.$c] = $row_profcerts['year'];
					}
				}

				//nysc
				$query_nysc = sprintf("SELECT * FROM nysc WHERE applicant_id = %s", GetSQLValueString($applicant_id, "int"));
				$nysc = mysql_query($query_nysc, $fer) or die(mysql_error());
				$row_nysc = mysql_fetch_assoc($nysc);
				$totalRows_nysc = mysql_num_rows($nysc);
				
				$line[$i]['nysc-nyscStatus'] = $row_nysc['nyscStatus'];
				$line[$i]['nysc-nyscNumber'] = $row_nysc['nyscNumber'];
				$line[$i]['nysc-nyscYear'] = $row_nysc['nyscYear'];
				
				//work experience
				//current job first
				$query_currentJob = sprintf("SELECT * FROM workexp WHERE applicant_id = %s AND endDate IS NULL", GetSQLValueString($applicant_id, "int"));
				$currentJob = mysql_query($query_currentJob, $fer) or die(mysql_error());
				$row_currentJob = mysql_fetch_assoc($currentJob);
				$totalRows_currentJob = mysql_num_rows($currentJob);
				$length = 10; // nuumber of columns to keep
				$k = 0; //counter
				if($totalRows_currentJob > 0) {
					$k = 1;
					$line[$i]['workexp-company-'.$k] = $row_currentJob['company'];
					$line[$i]['workexp-position-'.$k] = $row_currentJob['position'];
					$line[$i]['workexp-description-'.$k] = $row_currentJob['description'];
					$line[$i]['workexp-startDate-'.$k] = $row_currentJob['startDate'];
					$line[$i]['workexp-endDate-'.$k] = $row_currentJob['endDate'];
				}
				//other jobs
				$query_workExp ="SELECT * FROM workexp WHERE applicant_id = '$applicant_id' AND endDate IS NOT NULL ORDER BY endDate DESC, startDate DESC";
				$workExp = mysql_query($query_workExp, $fer) or die(mysql_error());
				$row_workExp = mysql_fetch_assoc($workExp);
				$totalRows_workExp = mysql_num_rows($workExp);
				if ($totalRows_profcerts > 0) {
					do {
						$k++;
						$line[$i]['workexp-company-'.$k] = $row_workExp['company'];
						$line[$i]['workexp-position-'.$k] = $row_workExp['position'];
						$line[$i]['workexp-description-'.$k] = $row_workExp['description'];
						$line[$i]['workexp-startDate-'.$k] = $row_workExp['startDate'];
						$line[$i]['workexp-endDate-'.$k] = $row_workExp['endDate'];						
					} while ($row_workExp = mysql_fetch_assoc($workExp));
				}
				if($k < $length) { //columns not filled, fill up
					for($c=$k+1;$c<=$length;$c++) {
						$line[$i]['workexp-company-'.$c] = '';
						$line[$i]['workexp-position-'.$c] = '';
						$line[$i]['workexp-description-'.$c] = '';
						$line[$i]['workexp-startDate-'.$c] = '';
						$line[$i]['workexp-endDate-'.$c] = '';						
					}
				}
				
				//areas of interest
				$query_categories = "SELECT * FROM job_categories ORDER BY cat_name ASC";
				$categories = mysql_query($query_categories, $fer) or die(mysql_error());
				$row_categories = mysql_fetch_assoc($categories);
				$totalRows_categories = mysql_num_rows($categories);

				$query_current_areas = "SELECT * FROM applicants_categories WHERE app_id = '$applicant_id'";
				$current_areas = mysql_query($query_current_areas, $fer) or die(mysql_error());
				$row_current_areas = mysql_fetch_assoc($current_areas);
				$totalRows_current_areas = mysql_num_rows($current_areas);
				
				do {
					$myAreas[] = $row_current_areas['cat_id'];
				} while ($row_current_areas = mysql_fetch_assoc($current_areas));
				
				do {
					$line[$i]['areas-'.$row_categories['cat_name']] = (in_array($row_categories['id_cat'],$myAreas))? '1':'0';
				} while ($row_categories = mysql_fetch_assoc($categories));
				
				//skills
				$query_skill = sprintf("SELECT * FROM skills WHERE applicant_id = %s ORDER BY skill ASC", GetSQLValueString($applicant_id, "int"));
				$skill = mysql_query($query_skill, $fer) or die(mysql_error());
				$row_skill = mysql_fetch_assoc($skill);
				$totalRows_skill = mysql_num_rows($skill);
				
				$length = 10; // nuumber of columns to keep
				$k = 0; //counter
				if ($totalRows_skill > 0) {
					do {
						$k++;
						$line[$i]['skill-'.$k] = $row_skill['skill'].' ('.$row_skill['competency'].')';
					} while ($row_skill = mysql_fetch_assoc($skill));
				}
				
				if($k < $length) { //columns not filled, fill up
					for($c=$k+1;$c<=$length;$c++) {
						$line[$i]['skill-'.$c] = '';
					}
				}
				
				

				$i++; //increment counter
			} while ($row_applicants = mysql_fetch_assoc($applicants));
			
			// do output to CSV
			$fp = fopen('php://output', 'w');
			if ($fp) {
				header('Content-Type: text/csv');
				header('Content-Disposition: attachment; filename="Applicants List for '.$_SESSION['Vacancy-Title'].' - (Generated on '.date("d-m-Y").').csv"');
				header('Pragma: no-cache');
				header('Expires: 0');
				fputcsv($fp, array_keys($line[1]));
				for($i=1;$i<= count($line);$i++)
					fputcsv($fp, $line[$i]);
				die;
			}
					
