<?php error_reporting(E_ALL); ini_set("display_errors", 1); ?>
<?php require_once('_inc_config.php'); ?>
<?php require_once('Connections/fer.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
session_start();

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
$thisPage = basename( $_SERVER['PHP_SELF'] ); 
?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['email'])) {
  $loginUsername=$_POST['email'];
  $password=$_POST['password'];
  $MM_fldUserAuthorization = "";
  $MM_redirectLoginSuccess = "a_profile.php";
  $MM_redirectLoginFailed = "a_login.php?error=Login Failed! Please check username or password and try again.";
  $MM_redirecttoReferrer = true;
  mysql_select_db($database_fer, $fer);
  
  $LoginRS__query=sprintf("SELECT * FROM applicants WHERE email=%s AND password=%s",
    GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  $LoginRS = mysql_query($LoginRS__query, $fer) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);
  if ($loginFoundUser) {
     $rowLogin = mysql_fetch_assoc($LoginRS);
	 $loginStrGroup = "";
    
	if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
    //declare two session variables and assign them
    $_SESSION['FER_User'] = $rowLogin;
    $_SESSION['FER_Usertype'] = 'applicant';
	
	//check activations status
	if($rowLogin['activated'] == NULL) {
		//activate account
		$aID = $rowLogin['id'];
		$updateSQL = "UPDATE applicants SET activated = '1' WHERE id = 'aID'";
		$updateRS = mysql_query($updateSQL, $fer) or die(mysql_error());
	}

    if (isset($_SESSION['PrevUrl']) && true) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];
	  unset($_SESSION['PrevUrl']);	
    }
    header("Location: " . $MM_redirectLoginSuccess );
	exit;
  }
  else {
    header("Location: ". $MM_redirectLoginFailed );
	exit;
  }
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<link rel="shortcut icon" href="favicon.png" />
    
	<title>Login | <?php echo $config['shortname'] ?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">

    <!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include('-inc-header-top.php'); ?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include('-inc-header-nav.php'); ?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
			<div class="container">
				<h1>Login</h1>

				    <ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					<li><a href="#">Login</a></li>
				</ul>
			</div>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
			<div class="row page-content">
			    <div class="col-sm-6">
					<form id="formLogin" name="formLogin" method="POST" action="<?php echo $loginFormAction; ?>">
					<div class="white-container sign-up-form">
						<div>
							<h2>Login Details</h2>
							<section>

                                <?php if (isset($_GET['error'])) { ?>
                                <div class="alert alert-error">
                                    <h6>Oops!</h6>
                                    <p><?php echo $_GET['error'] ?></p>
                                    <a href="#" class="close fa fa-times"></a>
                                </div>	
                                <?php } ?>							

                                <?php if (isset($_GET['msg'])) { ?>
                                <div class="alert alert-success">
                                    <h6>Wow!</h6>
                                    <p><?php echo $_GET['msg'] ?></p>
                                    <a href="#" class="close fa fa-times"></a>
                                </div>	
                                <?php } ?>							
                                
                                <div class="row">
									<div class="col-sm-9">
                                    <label for="email"></label>
                                    <input name="email" type="text" class="form-control" id="email" value="" placeholder="Email Address"/>
                                    </div>
								</div>
                                
                                <div class="row">
									<div class="col-sm-9">
                                    <label for="password"></label>
                                    <input name="password" type="password" class="form-control" id="password" value="" placeholder="Password"/>
                                    </div>
								</div>


							</section>
						</div>

						<hr class="mt60">

						<div class="clearfix">
							<a href="a_passwordReset.php">Forgot Password?</a>
							<input name="Login" type="submit" class="btn btn-default btn-large pull-right" id="Login" value="Enter" />
						</div>
                        <input type="hidden" name="MM_insert" value="formAppReg" />
					</div>
                    </form>
				</div>

				<div class="col-sm-6">
					<h6>NOT YET REGISTERED?</h6>
					<p>Registering on AMF Recruitment enables you to create a profile to showcase your capabilities to us. You can update this profile anytime using your unique email and password.</p>
					<p><a href="register.php" class="btn btn-large btn-default">Create Account</a></p>
				</div>
			    <!-- end .page-content -->
			</div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include('-inc-footer-top.php'); ?>

		<div class="copyright">
			<?php include('-inc-footer-bottom.php'); ?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>