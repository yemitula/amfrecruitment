<?php require_once("_inc_checkSession.php"); ?>
<?php require_once("_inc_applicantsOnly.php"); ?>
<?php $thispage = basename( $_SERVER['PHP_SELF'] ); ?>
<?php include('_inc_Functions.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

//check if section status exists for this applicant and create it if otherwise
if(SectionStatusExists($FER_User['id']) == false) {
	CreateSectionStatus($FER_User['id']);
}

$applicant_id = $FER_User['id'];

mysql_select_db($database_fer, $fer);
$query_personal = "SELECT * FROM applicants WHERE id = $applicant_id";
$personal = mysql_query($query_personal, $fer) or die(mysql_error());
$row_personal = mysql_fetch_assoc($personal);
$totalRows_personal = mysql_num_rows($personal);

$colname_secondary = "-1";
if (isset($_SESSION['FER_User']['id'])) {
  $colname_secondary = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_secondary = "SELECT * FROM secschool WHERE applicant_id = '$colname_secondary' ORDER BY `date` DESC";
$secondary = mysql_query($query_secondary, $fer) or die(mysql_error());
$row_secondary = mysql_fetch_assoc($secondary);
$totalRows_secondary = mysql_num_rows($secondary);

$colname_tertiary = "-1";
if (isset($_SESSION['FER_User']['id'])) {
  $colname_tertiary = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_tertiary = "SELECT * FROM tertiary WHERE applicant_id = '$colname_tertiary' ORDER BY `date` DESC";
$tertiary = mysql_query($query_tertiary, $fer) or die(mysql_error());
$row_tertiary = mysql_fetch_assoc($tertiary);
$totalRows_tertiary = mysql_num_rows($tertiary);

$colname_nysc = "-1";
if (isset($_SESSION['FER_User']['id'])) {
  $colname_nysc = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_nysc = sprintf("SELECT * FROM nysc WHERE applicant_id = %s", GetSQLValueString($colname_nysc, "int"));
$nysc = mysql_query($query_nysc, $fer) or die(mysql_error());
$row_nysc = mysql_fetch_assoc($nysc);
$totalRows_nysc = mysql_num_rows($nysc);

mysql_select_db($database_fer, $fer);
$query_currentJob = sprintf("SELECT * FROM workexp WHERE applicant_id = %s AND endDate IS NULL", GetSQLValueString($_SESSION['FER_User']['id'], "int"));
$currentJob = mysql_query($query_currentJob, $fer) or die(mysql_error());
$row_currentJob = mysql_fetch_assoc($currentJob);
$totalRows_currentJob = mysql_num_rows($currentJob);

mysql_select_db($database_fer, $fer);
$query_categories = "SELECT * FROM job_categories ORDER BY cat_name ASC";
$categories = mysql_query($query_categories, $fer) or die(mysql_error());
$row_categories = mysql_fetch_assoc($categories);
$totalRows_categories = mysql_num_rows($categories);

$colname_areas = "-1";
if (isset($_SESSION['FER_User']['id'])) {
  $colname_areas = $_SESSION['FER_User']['id'];
}

mysql_select_db($database_fer, $fer);
$query_current_areas = "SELECT * FROM applicants_categories WHERE app_id = '$colname_areas'";
$current_areas = mysql_query($query_current_areas, $fer) or die(mysql_error());
$row_current_areas = mysql_fetch_assoc($current_areas);
$totalRows_current_areas = mysql_num_rows($current_areas);
do {
	$myAreas[] = $row_current_areas['cat_id'];
} while ($row_current_areas = mysql_fetch_assoc($current_areas));

mysql_select_db($database_fer, $fer);
$query_areas = sprintf("SELECT * FROM areasofinterest WHERE applicant_id = %s", GetSQLValueString($colname_areas, "int"));
$areas = mysql_query($query_areas, $fer) or die(mysql_error());
$row_areas = mysql_fetch_assoc($areas);
$totalRows_areas = mysql_num_rows($areas);

$colname_skill = "-1";
if (isset($_SESSION['FER_User']['id'])) {
  $colname_skill = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_skill = sprintf("SELECT * FROM skills WHERE applicant_id = %s ORDER BY skill ASC", GetSQLValueString($colname_skill, "int"));
$skill = mysql_query($query_skill, $fer) or die(mysql_error());
$row_skill = mysql_fetch_assoc($skill);
$totalRows_skill = mysql_num_rows($skill);

$colname_workExp = "-1";
if (isset($_SESSION['FER_User']['id'])) {
  $colname_workExp = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_workExp ="SELECT * FROM workexp WHERE applicant_id = '$colname_workExp' AND endDate IS NOT NULL ORDER BY endDate DESC, startDate DESC";
$workExp = mysql_query($query_workExp, $fer) or die(mysql_error());
$row_workExp = mysql_fetch_assoc($workExp);
$totalRows_workExp = mysql_num_rows($workExp);

$colname_certs = "-1";
if (isset($_SESSION['FER_User']['id'])) {
  $colname_certs = $_SESSION['FER_User']['id'];
}
mysql_select_db($database_fer, $fer);
$query_certs = sprintf("SELECT id, certification, `year` FROM profcerts WHERE applicant_id = %s ORDER BY `year` DESC", GetSQLValueString($colname_certs, "int"));
$certs = mysql_query($query_certs, $fer) or die(mysql_error());
$row_certs = mysql_fetch_assoc($certs);
$totalRows_certs = mysql_num_rows($certs); 

//do quick check and update for all sections
//personal

//educational
if($totalRows_tertiary && $totalRows_secondary)
{
	  //update section status
	 UpdateSectionStatus($FER_User['id'],'educational','1');
}
else
{
	  //update section status
	 UpdateSectionStatus($FER_User['id'],'educational','0');
}

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<link rel="shortcut icon" href="favicon.png" />
    
	<title>Applicant Profile - <?php echo $FER_User['firstname'] ?> <?php echo $FER_User['surname'] ?> | <?php echo $config['shortname'] ?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">

	<!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include('-inc-header-top.php'); ?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include('-inc-header-nav.php'); ?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
		    <?php include('-inc-applicant-top.php'); ?>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
		    <div class="row">
		        <div class="col-sm-4 page-sidebar">
		            <?php include('-inc-applicant-side.php'); ?>
	            </div>
		        <!-- end .page-sidebar -->
		        <div class="col-sm-8 page-content">
		            <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>
		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>
	                </div>-->
		            
                    <!--Educational-->
                    <?php if (isset($_GET['msg'])) { ?>
                    <div class="alert alert-success">
                        <h6>Wow!</h6>
                        <p><?php echo $_GET['msg'] ?></p>
                        <a href="#" class="close fa fa-times"></a></div>
                    <?php } ?>
                    <div class="candidates-item candidates-single-item">
                        <h5 class="title">PERSONAL DETAILS <span class="small"><a href="a_personalDetails.php">(Edit)</a></span></h5>
                        <ul class="top-btns">
                            <li><a href="a_personalDetails.php" title="Edit Section" class="btn btn-gray fa fa-edit"></a></li>
                        </ul>
                        <?php if (SectionComplete('personal')) { ?>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Applicant ID</td>
                                    <td><?php echo str_pad($FER_User['id'],6,'0',STR_PAD_LEFT); ?></td>
                                </tr>
                                <?php if ($FER_User['title']) { ?>
                                <tr>
                                    <td>Title</td>
                                    <td><?php echo $FER_User['title']; ?></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td>Name</td>
                                    <td><?php echo strtoupper($FER_User['surname']) ?> <?php echo $FER_User['firstname'] ?> <?php echo $FER_User['middlename'] ?></td>
                                </tr>
                                <?php if ($FER_User['dob']) { ?>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td><?php echo $FER_User['dob']; ?></td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td><?php echo $FER_User['gender']; ?></td>
                                </tr>
                                <tr>
                                    <td>Marital Status</td>
                                    <td><?php echo $FER_User['marital']; ?></td>
                                </tr>
                                <?php } ?>
                                <?php if ($FER_User['email']) { ?>
                                <tr>
                                    <td>E-mail(s)</td>
                                    <td><?php echo $FER_User['email']; ?>
                                        <?php if ($FER_User['email2']) echo ', '.$FER_User['email2']; ?></td>
                                </tr>
                                <?php } ?>
                                <?php if ($FER_User['gsm']) { ?>
                                <tr>
                                    <td>Mobile Number(s)</td>
                                    <td><?php echo $FER_User['gsm']; ?>
                                        <?php if ($FER_User['gsm2']) echo ', '.$FER_User['gsm2']; ?></td>
                                </tr>
                                <?php } ?>
                                <?php if ($FER_User['street']) { ?>
                                <tr>
                                    <td>Address</td>
                                    <td><?php if ($FER_User['houseNumber']) echo $FER_User['houseNumber'].','; ?>
                                        <?php echo $FER_User['street']; ?>
                                        <?php if ($FER_User['busStop']) echo ', ('.$FER_User['busStop'].')'; ?></td>
                                </tr>
                                <tr>
                                    <td>City/Town</td>
                                    <td><?php echo $FER_User['city']; ?></td>
                                </tr>
                                <tr>
                                    <td>State of Residence</td>
                                    <td><?php echo $FER_User['stateOfResidence']; ?></td>
                                </tr>
                                <tr>
                                    <td>Languages Spoken</td>
                                    <td><?php echo $FER_User['languages']; ?></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td>Primary Discipline</td>
                                    <td>
                                        <?php 
                                            if(isset($FER_User["discipline"])) {

                                                $des = $FER_User["discipline"];

                                                mysql_select_db($database_fer, $fer);
                                                $query_discipline_name = "SELECT * FROM job_categories WHERE id_cat = $des ";
                                                $discipline_name = mysql_query($query_discipline_name, $fer) or die(mysql_error());
                                                $row_discipline_name = mysql_fetch_assoc($discipline_name);
                                                $totalRows_current_areas = mysql_num_rows($discipline_name);

                                                echo $row_discipline_name["cat_name"];
                                            }else {
                                                echo "N/A";
                                            } 
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                        <?php } else { ?>
                        <div class="alert alert-error">
                            <p>You have not completed your Personal Details section. You are required to complete this section before you can apply for vacancies. <a href="a_personalDetails.php"><strong>Complete Now</strong></a></p>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="candidates-item candidates-single-item">
		                <h5 class="title">EDUCATIONAL/ACADEMIC QUALIFICATIONS 
                        <span class="small"><a href="a_educational.php">(Edit)</a></span></h5>
                        <ul class="top-btns">
							<li><a href="a_educational.php" title="Edit Section" class="btn btn-gray fa fa-edit"></a></li>
						</ul>
			  <?php if ($totalRows_secondary > 0 || $totalRows_tertiary > 0) { ?>
				  <?php if ($totalRows_tertiary > 0) { // Show if recordset not empty ?>
                        <h6>Tertiary</h6>
            <table class="table-striped">
				<thead>
					<tr>
						<th width="40%">Institution (Course)</th>
						<th width="30%">Qualification (Class)</th>
						<th width="15%">Date</th>
					</tr>
				</thead>

				<tbody>
					<?php do { ?>
					<tr>
						<td><?php echo $row_tertiary['institution']; ?> (<?php echo $row_tertiary['course']; ?>)</td>
						<td><?php echo $row_tertiary['qualification']; ?> (<?php echo $row_tertiary['class']; ?>)</td>
						<td><?php echo date("F, Y",strtotime($row_tertiary['date'])); ?></td>
					</tr>
					<?php } while ($row_tertiary = mysql_fetch_assoc($tertiary)); ?>
				</tbody>
			</table>
				<?php } // Show if recordset not empty ?>
                <?php if ($totalRows_secondary > 0) { // Show if recordset not empty ?>
            <h6>Secondary</h6>
            <table class="table-striped">
				<thead>
					<tr>
						<th width="50%">School</th>
						<th width="30%">Certificate</th>
						<th width="20%">Date</th>
					</tr>
				</thead>

				<tbody>
					<?php do { ?>
					<tr>
						<td><?php echo $row_secondary['school']; ?></td>
						<td><?php echo $row_secondary['certificate']; ?></td>
						<td><?php echo date("F, Y",strtotime($row_secondary['date'])); ?></td>
					</tr>
					<?php } while ($row_secondary = mysql_fetch_assoc($secondary)); ?>
				</tbody>
			</table>
                <?php } // Show if recordset not empty ?>
			<?php } // end tertiary and secondary section ?>
            <?php if (!SectionComplete('educational')) { ?>
            <div class="alert alert-error">
                <p>You have not completed your Educational/Academic section. <strong>You need to fill in at least 1 Secondary and 1 Tertiary Education</strong>. You are required to complete this section before you can apply for vacancies. <a href="a_educational.php"><strong>Complete Now</strong></a></p>
            </div>
            <?php } ?>
		            </div>
                    <!--End Educational -->
                    
                    <!--Start Professional Certs-->
                    <div class="candidates-item candidates-single-item">
		                <h5 class="title">PROFESSIONAL CERTIFICATIONS 
                        <span class="small"><a href="a_profCerts.php">(Edit)</a></span></h5>
                        <ul class="top-btns">
							<li><a href="a_profCerts.php" title="Edit Section" class="btn btn-gray fa fa-edit"></a></li>
						</ul>
			  <?php if ($totalRows_certs > 0) { // Show if recordset not empty ?>
			          <table class="table-striped">
			              <thead>
					<tr>
						<th width="80%">Certification</th>
						<th width="20%">Year Obtained</th>
					</tr>
				</thead>

				<tbody>
					<?php do { ?>
					<tr>
						<td><?php echo $row_certs['certification']; ?></td>
						<td><?php if($row_certs['year'] != 0) echo $row_certs['year']; ?></td>
					</tr>
					<?php } while ($row_certs = mysql_fetch_assoc($certs)); ?>
				</tbody>
			</table>
			<?php } // Show if recordset not empty ?>
            <?php if (!SectionComplete('profcerts')) { ?>
            <div class="alert alert-error">
                <p>You have not completed your Professional Ceritifications section. <strong>If you have NO professional certification, please indicate NO CERTIFICATION in that section</strong>. You are required to complete this section before you can apply for vacancies. <a href="a_profCerts.php"><strong>Complete Now</strong></a></p>
            </div>
            <?php } ?>
		            </div>
                    <!--End Professional Certs-->
                    
                    <!--Start NYSC-->
                    <div class="candidates-item candidates-single-item">
		                <h5 class="title">NYSC DETAILS 
                        <span class="small"><a href="a_nysc.php">(Edit)</a></span></h5>
                        <ul class="top-btns">
							<li><a href="a_nysc.php" title="Edit Section" class="btn btn-gray fa fa-edit"></a></li>
						</ul>
			  <?php if ($totalRows_nysc > 0) { // Show if recordset not empty ?>
			          <table class="table-striped">
			              <thead>
					<tr>
						<th width="40%">NYSC Status</th>
						<th width="40%">NYSC Number</th>
						<th width="20%">NYSC Year</th>
					</tr>
				</thead>

				<tbody>
					<?php do { ?>
					<tr>
						<td><?php echo $row_nysc['nyscStatus']; ?></td>
						<td><?php echo $row_nysc['nyscNumber']; ?></td>
						<td><?php echo $row_nysc['nyscYear']; ?></td>
					</tr>
					<?php } while ($row_nysc = mysql_fetch_assoc($nysc)); ?>
				</tbody>
			</table>
			<?php } // Show if recordset not empty ?>
            <?php if (!SectionComplete('nysc')) { ?>
            <div class="alert alert-error">
                <p>You have not completed your NYSC section. <strong>If you are yet to start your NYSC, please select YET TO START in that section</strong>. You are required to complete this section before you can apply for vacancies. <a href="a_nysc.php"><strong>Complete Now</strong></a></p>
            </div>
            <?php } ?>
		            </div>
                    <!--End NYSC-->
                    
                    <!--Start Work Experience-->
                    <div class="candidates-item candidates-single-item">
		                <h5 class="title">WORK EXPERIENCE 
                        <span class="small"><a href="a_workExp.php">(Edit)</a></span></h5>
                        <ul class="top-btns">
							<li><a href="a_workExp.php" title="Edit Section" class="btn btn-gray fa fa-edit"></a></li>
						</ul>
			  <?php if ($totalRows_workExp > 0 || $totalRows_currentJob > 0) { // Show if recordset not empty ?>
				          <table class="table-striped">
				              <thead>
					<tr>
						<th width="30%">Company</th>
						<th width="50%">Job Description</th>
						<th width="20%">Period</th>
					</tr>
				</thead>

				<tbody>
				 <?php if ($totalRows_currentJob > 0) { // Show if recordset not empty ?>
					<tr>
      <td valign="top" style="word-wrap: break-word;"><strong><?php echo $row_currentJob['company']; ?></strong><br>
        <?php echo $row_currentJob['position']; ?><br>
        <em><?php echo $row_currentJob['job_level']; ?></em>
</td>
      <td valign="top" align="justify" style="word-wrap: break-word;"><?php echo $row_currentJob['description']; ?></td>
      <td valign="top"><?php echo date("M Y",strtotime($row_currentJob['startDate'])); ?> - Date</td>
					</tr>
				<?php } // Show if recordset not empty ?>
				 <?php if ($totalRows_workExp > 0) do { ?>
					<tr>
      <td valign="top" style="word-wrap: break-word;"><strong><?php echo $row_workExp['company']; ?></strong>
        <?php echo $row_workExp['position']; ?><br>
        <em><?php echo $row_workExp['job_level']; ?></em>
</td>
      <td valign="top" align="justify" style="word-wrap: break-word;"><?php echo $row_workExp['description']; ?></td>
      <td valign="top"><?php echo date("M Y",strtotime($row_workExp['startDate'])); ?> - <?php echo date("M Y",strtotime($row_workExp['endDate'])); ?></td>
					</tr>
				<?php } while ($row_workExp = mysql_fetch_assoc($workExp)); ?>
				</tbody>
			</table>
			<?php } // Show if recordset not empty ?>
            <?php if (!SectionComplete('workExp')) { ?>
            <div class="alert alert-error">
                <p>You have not completed your Work Experience section. You are required to complete this section before you can apply for vacancies. <a href="a_workExp.php"><strong>Complete Now</strong></a></p>
            </div>
            <?php } ?>
		            </div>
                    <!--End Work Experience-->
                    
                    <!--Start Areas of Interest-->
                    <div class="candidates-item candidates-single-item">
		                <h5 class="title">AREAS OF INTEREST 
                        <span class="small"><a href="a_areas.php">(Edit)</a></span></h5>
                        <ul class="top-btns">
							<li><a href="a_areas.php" title="Edit Section" class="btn btn-gray fa fa-edit"></a></li>
						</ul>
			          <table class=" table-condensed">
			             <tr>
                <td valign="top">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <?php $count = 1; ?>
                <?php do { ?>
                <td width="50%" valign="top"><label>
                    <input name="areas[]" type="checkbox" id="area_<?php echo $row_categories['id_cat']; ?>" value="<?php echo $row_categories['id_cat']; ?>" <?php if (in_array($row_categories['id_cat'],$myAreas)) echo "checked='checked'" ?> disabled="disabled" readonly/>
                    <?php echo $row_categories['cat_name']; ?></label></td>
                <?php $count++; 
					   		if($count % 2 == 0) {
                            	echo "</tr> <tr>";
                        	} ?>
                <?php } while ($row_categories = mysql_fetch_assoc($categories)); ?>
                <?php if ($count % 2 == 0) echo "</tr>"?>
            </tr>
			</table>
            
            <table>
            <tr>
                <td width="23%">Preferred Job Level</td>
                <td width="77%"><strong><?php echo !empty($row_personal['prefJobLevel'])?$row_personal['prefJobLevel']:'Any' ?></strong></td>
            </tr>
            <tr>
                <td>Preferred Job Type</td>
                <td><strong><?php echo isset($row_personal['prefJobType']) && !empty($row_personal['prefJobType'])? $row_personal['prefJobType'] : 'Any' ?></strong></td>
            </tr>
        </table>
        
            <?php if (!SectionComplete('areas')) { ?>
            <div class="alert alert-error">
                <p>You have not selected your Areas of Interest. You are required to complete this section before you can apply for vacancies. <a href="a_areas.php"><strong>Complete Now</strong></a></p>
            </div>
            <?php } ?>
		            </div>
                    <!--End Areas of Interest-->
                    
                    <!--Start Skills-->
                    <div class="candidates-item candidates-single-item">
		                <h5 class="title">SKILLS 
                        <span class="small"><a href="a_skills.php">(Edit)</a></span></h5>
                        <ul class="top-btns">
							<li><a href="a_skills.php" title="Edit Section" class="btn btn-gray fa fa-edit"></a></li>
						</ul>
			  <?php if ($totalRows_skill > 0) { // Show if recordset not empty ?>
					<?php do { ?>
                    <div class="progress-bar toggle" data-progress="<?php echo GetSkillPercent($row_skill['competency']); ?>">
                        <a href="#" class="progress-bar-toggle"></a>
                        <h6 class="progress-bar-title"><?php echo $row_skill['skill']; ?></h6>
                        <div class="progress-bar-inner"><span></span></div>
                        <div class="progress-bar-content">
                            <?php echo $row_skill['competency']; ?>
                        </div>
                    </div>
					<?php } while ($row_skill = mysql_fetch_assoc($skill)); ?>
			<?php } // Show if recordset not empty ?>
            <?php if (!SectionComplete('skills')) { ?>
            <div class="alert alert-error">
                <p>You have not completed your Skills section.You are required to complete this section before you can apply for vacancies. <a href="a_skills.php"><strong>Complete Now</strong></a></p>
            </div>
            <?php } ?>
		            </div>
                    <!--End Skills-->
                    
                    <!--Start Uploaded CV-->
                    <div class="candidates-item candidates-single-item">
		                <h5 class="title">UPLOADED CV 
                        <span class="small"><a href="a_uploadCV.php">(Edit)</a></span></h5>
                        <ul class="top-btns">
							<li><a href="a_uploadCV.php" title="Edit Section" class="btn btn-gray fa fa-edit"></a></li>
						</ul>
	                    <br>
	                    <?php if (isset($row_personal['cv'])) { ?>
          <p><a href="uploadedCVs/<?php echo $row_personal['cv']; ?>" target="_blank"><?php echo $row_personal['cv']; ?></a></p>
      <?php } ?>
            <?php if (!SectionComplete('uploadCV')) { ?>
            <div class="alert alert-error">
                <p>You have not uploaded your CV.  You are required to complete this section before you can apply for vacancies. <a href="a_uploadCV.php"><strong>Complete Now</strong></a></p>
            </div>
            <?php } ?>
		            </div>
                    <!--End Uploaded CV-->
                    
		        </div>
		        <!-- end .page-content -->
	        </div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include('-inc-footer-top.php'); ?>

		<div class="copyright">
			<?php include('-inc-footer-bottom.php'); ?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>

</body>
</html>