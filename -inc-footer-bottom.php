<div class="container">
				<p>&copy; Copyright 2014 <a href="http://alphamead.com" target="_blank"><?php echo $config['shortname'] ?></a> | All Rights Reserved | App Developed by <a href="http://tulabyte.net" target="_blank">Tulabyte</a></p>

				<ul class="footer-social">
					<li><a href="<?php echo $config['facebook'] ?>" target="_blank" class="fa fa-facebook"></a></li>
					<li><a href="<?php echo $config['twitter'] ?>" target="_blank" class="fa fa-twitter"></a></li>
					<li><a href="<?php echo $config['linkedin'] ?>" target="_blank" class="fa fa-linkedin"></a></li>
				</ul>
			</div>