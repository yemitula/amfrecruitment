<?php require_once("_inc_checkSession.php"); ?>
<?php require_once("_inc_applicantsOnly.php"); ?>
<?php include('_inc_Functions.php'); ?>
<?php require_once('_inc_config.php'); ?>
<?php require_once('Connections/fer.php'); ?>
<?php $thisPage = basename( $_SERVER['PHP_SELF'] ); ?>
<?php 
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_vacancy = "-1";
if (isset($_GET['id'])) {
  $colname_vacancy = $_GET['id'];
}
mysql_select_db($database_fer, $fer);
$query_vacancy = sprintf("SELECT * FROM vacancies WHERE id = %s", GetSQLValueString($colname_vacancy, "int"));
$vacancy = mysql_query($query_vacancy, $fer) or die(mysql_error());
$row_vacancy = mysql_fetch_assoc($vacancy);
$totalRows_vacancy = mysql_num_rows($vacancy);

if (isset($_POST['internal_candidate'])) {
	//check if applicant has a complete CV
	if(CVComplete()) { //cv is complete
		//check if applicant has already applied for this vacancy
		$vID = $_GET['id'];
		$aID = $FER_User['id'];		
		if(AlreadyApplied($vID)) {	//already applied
			//display error and stop
			header("Location: a_vacancyDetails.php?id=$vID&error=You have already applied for this vacancy");
			exit;
		} elseif (NotEligible($vID, $row_vacancy['employer_id'])) { //applied for vac from same employer
			//display error and stop
			header("Location: a_vacancyDetails.php?id=$vID&error=You are NOT eligible for this vacancy, you have already applied for a vacancy from this same employer");
			exit;
		} else { //not applied yet
			//submit application
			$now = date("Y-m-d h:i:s");
			$salaries = explode(",", $_POST['salary']);
			$insertSQL = sprintf("INSERT INTO applications (applicant_id, vacancy_id, min_salary, max_salary, internal_candidate, dateApplied) values (%s,%s,%s,%s,%s,%s)",
			GetSQLValueString($aID, "int"),
			GetSQLValueString($vID, "int"),
			GetSQLValueString($salaries[0], "double"),
			GetSQLValueString($salaries[1], "double"),
			GetSQLValueString($_POST['internal_candidate'], "int"),
			GetSQLValueString($now, "date")
			);
			$insertRS = mysql_query($insertSQL, $fer) or die(mysql_error());
			header("Location: a_vacancyDetails.php?id=$vID&msg=You have successfully applied for this vacancy");
			exit;
		}
	} else { //cv incomplete
		//tell applicant to complete cv and stop
		header("Location: a_cvSummary.php?error=Sorry! You have to complete your CV before you can apply for ANY vacancy.");
		exit;
	}
}
	

mysql_select_db($database_fer, $fer);
$employerID = $row_vacancy['employer_id'];
$query_employer = "SELECT * FROM employers WHERE id = '$employerID'";
$employer = mysql_query($query_employer, $fer) or die(mysql_error());
$row_employer = mysql_fetch_assoc($employer);
$totalRows_employer = mysql_num_rows($employer);
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<link rel="shortcut icon" href="favicon.png" />
    
	<title>Professional Certifications - <?php echo $FER_User['firstname'] ?> <?php echo $FER_User['surname'] ?>| <?php echo $config['shortname'] ?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
    <link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/jquery.range.css">
    <!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include('-inc-header-top.php'); ?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include('-inc-header-nav.php'); ?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
		    <?php include('-inc-applicant-top.php'); ?>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
		    <div class="row">
		        <div class="col-sm-4 page-sidebar">
		            <?php include('-inc-applicant-side.php'); ?>
	            </div>
		        <!-- end .page-sidebar -->
	          <div class="col-sm-8 page-content">
		            <h3>
		                <!--<div class="clearfix mb30 hidden-xs"> <a href="#" class="btn btn-gray pull-left">Back to Listings</a>
		                <div class="pull-right"> <a href="#" class="btn btn-gray">Previous</a> <a href="#" class="btn btn-gray">Next</a> </div>
	                </div>-->
		                 Apply for Vacancy
                </h3>
              <form id="formApply" name="formApply" action="a_applyForVacancy.php?id=<?php echo $_GET['id'] ?>" method="post">
	                    <div class="white-container sign-up-form">
	                        <div>
	                           <?php //<h5>Upload</h5> ?>
                              <section>
	                                <?php if (isset($_GET['error'])) { ?>
		                                <div class="alert alert-error">
		                                    <h6>Oops!</h6>
		                                    <p><?php echo $_GET['error'] ?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php } ?>
	                                <?php if (isset($_GET['msg'])) { ?>
		                                <div class="alert alert-success">
		                                    <h6>Wow!</h6>
		                                    <p><?php echo $_GET['msg'] ?></p>
	                                    <a href="#" class="close fa fa-times"></a></div>
		                                <?php } ?>
                                <div class="row">
                  			<div class="row">
                              <p>You&nbsp;are applying for the Vacancy <a href="a_vacancyDetails.php?id=<?php echo $row_vacancy['id'] ?>" target="_blank"><strong><?php echo strtoupper($row_vacancy['title']); ?></strong></a> by <a href="a_employerProfile.php?id=<?php echo $row_vacancy['employer_id']; ?>&amp;v=<?php echo $row_vacancy['id']; ?>" target="_blank"><strong><?php echo strtoupper($row_employer['companyName']) ?></strong></a></p>
							<div class="col-sm-12" style="background-color:#666; padding:5px; margin:8px 0px; color:#fff;">
						  		Additional Information Required
							  </div>

									<div class="col-sm-8">
										Are you an internal candidiate (Do you currently work for this employer)?
									</div>

							  <div class="col-sm-4">
										<label>
                                <input name="internal_candidate" type="radio" id="internal_candidate_0" value="" checked="checked" />
                                No</label>
                            
                            <label style="margin:0px 5px;">
                                <input type="radio" name="internal_candidate" value="1" id="internal_candidate_1" />
                                Yes</label>
									</div>
                  			</div>
                  			 <div class="row">
                                <div class="col-sm-6">
                                    <label for="Salary">Expected salary (Monthly)</label><br><br>
                                    <input type="hidden" name="salary" id="salary" class="slider-input" value="1000000" style="margin:10px 0px;" />
                                    </div>
							</div>

                            <hr class="mt60">
                            <div class="clearfix" id="captchaArea">
                                <a href="a_vacancyDetails.php?id=<?php echo $row_vacancy['id']; ?>"><input name="cancel" type="button" class="btn btn-red" id="cancel" value="&laquo; Cancel" /></a>
                            <input name="submit" type="submit" class="btn btn-default pull-right" id="submit" value="Submit Application" />
                            </div>
                                </div>
                              </section>
							</div>
                            </div>
                  </form>
	          </div>
		        <!-- end .page-content -->
	        </div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include('-inc-footer-top.php'); ?>

		<div class="copyright">
			<?php include('-inc-footer-bottom.php'); ?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
<script src="js/ValidateDel.js"></script>
<script src="js/jquery.range.js"></script>
<script type="text/javascript">
$(".slider-input").jRange({
    from: 18000,
    to: 2000000,
    step: 100000,
    //scale: [0,25,50,75,100],
    format: '%s',
    width: 600,
    showLabels: true,
    isRange : true
});

$("#formApply").submit(function() { 
//check if end salary is filled
if($("#salary").val() == '0,0' ) {
	alert("Please supply your Salary Estimation for this Job!!!");
	return false;
}
});
</script>
</body>
</html>