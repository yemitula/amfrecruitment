<?php require_once('_inc_config.php'); ?>
<?php require_once('Connections/fer.php'); ?>
<?php require_once('swiftMailer/lib/swift_required.php'); ?>
<?php include('_inc_Functions.php'); ?>
<?php include("_inc_funcHTMLEmail.php"); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

session_start();

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "formAppReg")) {
	//save form in session
	$_SESSION['formAppReg'] = $_POST;
	
	mysql_select_db($database_fer, $fer);
	$email = $_POST['email'];
	$query_applicants = "SELECT * FROM applicants WHERE email = '$email'";
	$applicants = mysql_query($query_applicants, $fer) or die(mysql_error());
	$row_applicants = mysql_fetch_assoc($applicants);
	$totalRows_applicants = mysql_num_rows($applicants);
	
	if($totalRows_applicants) {
		//applicant already exists
		header("Location: a_register.php?error=This email has already been used for registration by someone else! Please use another one.<br>If you have forgotten your password, you can <a href='a_passwordReset.php'>Reset your password.</a>");
		exit;
	}
	
	//generate password
	$password = randomPassword();

	$insertSQL = sprintf("INSERT INTO applicants (email, password, surname, firstname, gsm, dateReg) VALUES (%s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($password, "text"),
                       GetSQLValueString($_POST['surname'], "text"),
                       GetSQLValueString($_POST['firstname'], "text"),
                       GetSQLValueString($_POST['gsm'], "text"),
                       GetSQLValueString($_POST['dateReg'], "date"));

  mysql_select_db($database_fer, $fer);
  $Result1 = mysql_query($insertSQL, $fer) or die(mysql_error());
  
  //create section status entry
  $appID = mysql_insert_id();
  CreateSectionStatus($appID);
  
	//send emails
	//store the form contents in variables for easy use in email
	$firstname = $_POST['firstname'];
	$surname = $_POST['surname'];
	//set the sender email
	$emailFrom = $config['sender'];
	//send recepient email for alerts
	$to = $config['notify'];
	
	//Create the Transport
	$transport = Swift_MailTransport::newInstance();
	
	//Create the Mailer using your created Transport
	$mailer = Swift_Mailer::newInstance($transport);
	
	/////////////////	Alert Message	////////////////////
	
	//set the subject of the alert
	$subject = "$firstname $surname just registered as an Applicant on ".$config['shortname']." Recruitment Portal";
	
	
	//body of the alert email - html
	$body = "
<p>Hello,</p>
<p><strong>$firstname $surname </strong> has registered on the ".$config['shortname']." Recruitment.</p>
<p>Thanks.</p>
<p><strong>Webmaster</strong></p>
	";
	$body = CreateHTMLEmail($body);
	//die($body);
	
	//Create a message
	$message = Swift_Message::newInstance($subject)
	->setFrom(array($emailFrom => $config['shortname']." Recruitment"))
	->setTo(array($to => $config['shortname']." Recruitment"))
	//->setBcc(array("alerts@tulabyte.net" => "Yemi Adetula"))
	->setBody($body, 'text/html')
	;
	//send the message
	$result = $mailer->send($message);
	
	
	/////////////////	Autoresponse Message	////////////////////
	
	$subject2 = "Candidate Registration on ".$config['shortname']." Recruitment";
	
	$body2 = "
<p>Welcome to ".$config['shortname']." Recruitment <strong>$firstname</strong>.<br />
  Thank you for registering with us. We are here to make your job search easier and more effective.<br />
  <strong>Your account details are:</strong><br />
  Username: $email  <br />
Password: $password   </p>
<p>Click on the link below to login using the above details:<br />
  <a href='".$config['url']."a_login.php'>".$config['url']."a_login.php</a></p>
<p>We recommend that you save this email for future reference. To change your password go to <a href='".$config['url']."a_changePass.php'>".$config['url']."a_changePass.php</a></p>
<p> Whenever you require any support, please send an email to <a href='mailto:".$config['notify']."'><strong>".$config['notify']."</strong></a></p>
<p> We look forward to being of service to you.</p>
<p> Regards</p>
<p> <strong>".$config['shortname']." Recruitment Team</strong><br />
</p>
	";
	$body2 = CreateHTMLEmail($body2);
	//die($body);
	
	//Create a message
	$message2 = Swift_Message::newInstance($subject2)
	->setFrom(array($emailFrom => $config['shortname']." Recruitment"))
	->setTo(array($email => $firstname.' '.$surname))
	//->setBcc(array("alerts@tulabyte.net" => "Yemi Adetula"))
	->setBody($body2, 'text/html')
	;
	//send the message
	$result2 = $mailer->send($message2);

  
  //clear form session
  unset($_SESSION['formAppReg']);

  $insertGoTo = "a_regSuccess.php?e=$email";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
if(isset($_SESSION['formAppReg'])) $formAppReg = $_SESSION['formAppReg'];

?>
<!doctype html><?php error_reporting(0); ?>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<link rel="shortcut icon" href="favicon.png" />
    
	<title>Register | <?php echo $config['shortname'] ?> Recruitment Portal</title>

	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">

	<!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
</head>

<body>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<?php include('-inc-header-top.php'); ?>
             <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<?php include('-inc-header-nav.php'); ?>
             <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->

		<div class="header-page-title">
			<div class="container">
				<h1>Register</h1>

				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					<li><a href="#">Register</a></li>
				</ul>
			</div>
		</div>

	</header> <!-- end #header -->

	<div id="page-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 page-content">
					
					<form id="formAppReg" name="formAppReg" method="POST" action="<?php echo $editFormAction; ?>">
					<div class="white-container sign-up-form">
						<div>
							<h2>Applicant Registration</h2>
							<section>

								<h6 class="label">Name</h6>

								<div class="row">
									<div class="col-sm-4"><span id="sprytextfield2">
                                    <label for="firstname"></label>
                                    <input name="firstname" type="text" class="form-control" id="firstname" value="<?php if ($formAppReg['firstname']) echo $formAppReg['firstname'] ?>" placeholder="First Name"/>
                                    <span class="textfieldRequiredMsg">A value is required.</span></span></div>

									<div class="col-sm-4"><span id="sprytextfield1">
                                    <label for="surname"></label>
                                    <input name="surname" type="text" class="form-control" id="surname" value="<?php if ($formAppReg['surname']) echo $formAppReg['surname'] ?>" placeholder="Surname" />
                                    <span class="textfieldRequiredMsg">A value is required.</span></span></div>
								</div>


								<h6 class="label">Account Information</h6>

								<div class="row">
									<div class="col-sm-6"><span id="sprytextfield5">
                                    <label for="email"></label>
                                    <input name="email" type="text" class="form-control" id="email" value="<?php if ($formAppReg['email']) echo $formAppReg['email'] ?>" placeholder="Email" />
                                    <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldInvalidFormatMsg">Invalid format.</span></span></div>

									<div class="col-sm-4"><span id="sprytextfield4">
                                    <label for="gsm"></label>
                                    <input name="gsm" type="text" class="form-control" id="gsm" value="<?php if ($formAppReg['gsm']) echo $formAppReg['gsm'] ?>" placeholder="GSM Number"/>
                                    <span class="textfieldRequiredMsg">A value is required.</span></span></div>

								</div>

							</section>
						</div>

						<hr class="mt60">

						<div class="clearfix">
							
                            <input name="register" type="submit" class="btn btn-default btn-large pull-right" id="register" value="Create Account" />
						</div>
                        <input type="hidden" name="MM_insert" value="formAppReg" />
					    <input name="dateReg" type="hidden" id="dateReg" value="<?php echo date("Y-m-d h:i:s"); ?>" />
					</div>
                    </form>
				</div> <!-- end .page-content -->
			</div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->

	<footer id="footer">
		<?php include('-inc-footer-top.php'); ?>

		<div class="copyright">
			<?php include('-inc-footer-bottom.php'); ?>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="js/maplace.min.js"></script>
<script src="js/jquery.ba-outside-events.min.js"></script>
<script src="js/jquery.responsive-tabs.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript">
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield5 = new Spry.Widget.ValidationTextField("sprytextfield5", "email");
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4");
</script>
</body>
</html>