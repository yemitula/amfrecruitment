<div class="container">

				<?php $thispage = basename( $_SERVER['PHP_SELF'] ); ?>
				<!-- Bookmarks -->
				<?php if(!isset($_SESSION['FER_User'])) { ?>
                <a href="admin/" class="btn btn-link bookmarks">Admin</a>
				<?php } ?>
				<?php if ($thispage != 'register.php' && $thispage != 'a_regSuccess.php' && $thispage != 'a_passwordReset.php' && !isset($_SESSION['FER_User'])) { ?>
                <!-- Header Register -->
				<div class="header-register">
					<a href="#" class="btn btn-link">Register</a>
					<div>
						<form id="formAppReg" name="formAppReg" method="POST" action="register.php">
							<input name="firstname" type="text" class="form-control" id="firstname" placeholder="First name">
							<input name="surname" type="text" class="form-control" id="surname" placeholder="Surname">
							<input name="gsm" type="text" class="form-control" id="gsm" placeholder="GSM Number">
							<input name="email" type="email" class="form-control" id="email" placeholder="Email">
							<input type="submit" class="btn btn-default" value="Register">
                            <input type="hidden" name="MM_insert" value="formAppReg" />
					    	<input name="dateReg" type="hidden" id="dateReg" value="<?php echo date("Y-m-d h:i:s"); ?>" />
						</form>
					</div>
				</div> <!-- end .header-register -->
                <?php } ?>

				<?php if ($thispage != 'a_login.php' && $thispage != 'a_passwordReset.php' && !isset($_SESSION['FER_User'])) { ?>
                <!-- Header Login -->
				<div class="header-login">
					<a href="#" class="btn btn-link">Login</a>
					<div>
						<form id="formLogin" name="formLogin" method="POST" action="a_login.php">
							<input id="email" name="email" type="text" class="form-control" placeholder="Email">
							<input id="password" name="password" type="password" class="form-control" placeholder="Password">
							<input type="submit" class="btn btn-default" value="Login">
							<a href="a_passwordReset.php" class="btn btn-link">Forgot Password?</a>
						</form>
					</div>
				</div> <!-- end .header-login -->
                <?php } ?>
                <?php if (isset($_SESSION['FER_User'])) { ?>
                <a href="logout.php" class="btn btn-link bookmarks">Logout</a>
                <a href="a_profile.php" class="btn btn-link bookmarks"><?php echo $FER_User['firstname'] ?> <?php echo $FER_User['surname'] ?></a>
				<?php }?>
			</div>